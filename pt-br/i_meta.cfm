<cfprocessingdirective pageencoding="utf-8">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<cfoutput>
<title>#translations[APPLICATION.langID]['platform_title'][rsPlatform.platform_key]# | #translations[APPLICATION.langID]['platform_subtitle'][rsPlatform.platform_key]#</title>
<META HTTP-EQUIV="Content-Language" CONTENT="#rsPlatform.platform_lang#">
<meta name="author" content="m4yweb">
<meta name="msvalidate.01" content="F653C66795BAED0625F78C9664BADED2" />
<meta name="description" content="#translations[APPLICATION.langID]['platform_title'][rsPlatform.platform_key]# | #translations[APPLICATION.langID]['platform_subtitle'][rsPlatform.platform_key]# | #translations[langID]['platform_teaser'][rsPlatform.platform_key]#">
<meta property="og:title" content="#translations[APPLICATION.langID]['platform_title'][rsPlatform.platform_key]# | #translations[APPLICATION.langID]['platform_subtitle'][rsPlatform.platform_key]#"/>
<meta property="og:image" content="#rsPlatform.platform_url#/media/images/fb/fb_img1.png"/>
<meta property="og:image" content="#rsPlatform.platform_url#/media/images/fb/fb_img2.jpg"/>
<meta property="og:image" content="#rsPlatform.platform_url#/media/images/fb/fb_img3.jpg"/>
<meta property="og:url" content="#rsPlatform.platform_url#/#rsPlatform.platform_lang#"/>
<meta property="og:description" content="#translations[langID]['platform_teaser'][rsPlatform.platform_key]#"/>
<link rel="alternate" href="http://www.videntecibele.com/pt-pt" hreflang="pt-pt" />
</cfoutput>