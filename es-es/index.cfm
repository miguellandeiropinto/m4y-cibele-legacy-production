<cfprocessingdirective pageencoding="utf-8">
<cfsetting requesttimeout="9000">
<cfcontent type="text/html; charset=utf-8">
<cfset setEncoding("URL", "utf-8")>
<cfset setEncoding("Form", "utf-8")>


<cfinclude template="config.cfc">
<cfinclude template="../cfc/contents.cfc">
<cfinclude template="../cfc/tracking.cfc">


<!DOCTYPE HTML>
<html lang="<cfoutput>#left(rsPlatform.platform_lang,2)#</cfoutput>">
<head>
<!-- META -->
<cfinclude template="i_meta.cfm">
<cfinclude template="../i_css.cfm">
<cfinclude template="i_analytics.cfm">
</head>
<body class="bg">
	<div class="container-fluid">
	
		<div class="container">

			<section id="zone1">

				<div class="row">
					
					<div class="col-sm-12 text-center">
						<cfoutput>
						<h1 class="site_title">#translations[langID]['platform_title'][rsPlatform.platform_key]#</h1>
						</cfoutput>
					</div>

				</div>

				<div class="row">
					
					<div class="col-sm-10 col-sm-offset-2 rose_bg text-center">
					
						<cfoutput>
						<h2 class="site_subtitle">#translations[langID]['platform_subtitle'][rsPlatform.platform_key]#</h2>
						</cfoutput>

					</div>

				</div>

				<div class="row">
					
					<div class="col-sm-4">
					
						<div class="row">
							<div class="col-sm-12">

								<div class="photo_box">
									<cfoutput>
									<img src="../media/images/#rsPlatform.seer_photo#" class="img-responsive" alt="#rsPlatform.seer_name#">
									</cfoutput>
								</div>

							</div>
							<div class="col-sm-12">

								<div class="tip_box">
									<cfoutput>
									<span>"#rsOpener.sentence#"</span>
									</cfoutput>
								</div>
							
							</div>
							<div class="col-sm-12">
								<div class="fb_box">
									<div class="fb_box_img">
									<img src="../media/images/fb_btn_bg.jpg" class="img-responsive">
									</div>
									<div class="fb_box_txt white">
									<cfoutput>#translations[langID]['ext_link']['facebook']#</cfoutput>
									</div>
								</div>

							</div>
						</div>

					</div>

					<div class="col-sm-8" style="padding-right:0px;">
					
						<div class="chat_box">

							<cfoutput>
							<span class="h3 chat_teaser">#translations[langID]['platform_teaser'][rsPlatform.platform_key]#</span>
							</cfoutput>

							<div id="chatcanvas" class="chatcanvas">
								
								<div id="c_birthdate" class="cibele_s"></div>
								<div id="u_birthdate" class="user_s"></div>
								<div id="c_gender" class="cibele_s"></div>
								<div id="u_gender" class="user_s"></div>
								<div id="c_zip" class="cibele_s"></div>
								<div id="u_zip" class="user_s"></div>
								<div id="c_name" class="cibele_s"></div>
								<div id="u_name" class="user_s"></div>
								<div id="c_interest" class="cibele_s"></div>
								<div id="u_interest" class="user_s"></div>
								<div id="c_emaili" class="cibele_s"></div>
								<div id="u_emaili" class="user_s"></div>
								<div id="c_email" class="cibele_s"></div>
								<div id="u_email" class="user_s"></div>
								<div id="c_phonei" class="cibele_s"></div>
								<div id="u_phonei" class="user_s"></div>
								<div id="c_phone" class="cibele_s"></div>
								<div id="u_phone" class="user_s"></div>
								<div id="c_goodbye" class="cibele_s"></div>

							</div>

							<div id="istypingcanvas" class="istypingcanvas">
								<div id="istyping" class="chat_istyping">
								<cfoutput>
								<img src="../media/images/chat_pen.png">#translations[langID]['cibele']['writing']#<span id="dots"></span>
								</cfoutput>
								</div>
							</div>

							<div id="dialogcanvas" class="dialogcanvas">
								
								<cfform name="mform" id="mform">
									<cfinput type="hidden" name="leadID" id="leadID" value="">
									<cfinput type="hidden" name="lead_gender" id="lead_gender" value="">
									<cfinput type="hidden" name="lead_sign" id="lead_sign" value="">

									<div id="birthdatecanvas" style="display:none;">
										
										<div class="row">
											<div class="col-xs-3">
												<cfoutput>
												<cfset year_ini=year(now())-18>
												<cfset year_end=year_ini-80>	
												<cfselect name="lead_birthyear" class="form-control selectpicker" data-live-search="true" title="#translations[langID]['chat']['year']#">
													<cfloop index="y" from="#year_ini#" to="#year_end#" step="-1">
													<option value="#y#">#y#</option>		
													</cfloop>
												</cfselect>
												</cfoutput>
												<!---
												<cfinput type="text" name="lead_birthyear" class="form-control" maxlength="4" placeholder="#translations[langID]['chat']['year']#" mask="9999">--->
											</div>
											<div class="col-xs-3">
												<cfoutput>	
												<cfselect name="lead_birthmonth" class="form-control selectpicker" data-live-search="true" title="#translations[langID]['chat']['month']#">
													<cfloop index="m" from="1" to="12">
													<option value="#m#">#translations[langID]['months_short'][m]#</option>		
													</cfloop>
												</cfselect>
												</cfoutput>
												<!---
												<cfinput type="text" name="lead_birthmonth" class="form-control" maxlength="2" placeholder="#translations[langID]['chat']['month']#" mask="99">--->
											</div>
											<div class="col-xs-3">	
												<cfoutput>	
												<cfselect name="lead_birthday" class="form-control selectpicker" data-live-search="true" title="#translations[langID]['chat']['day']#">
													<cfloop index="d" from="1" to="31">
													<option value="#d#">#d#</option>		
													</cfloop>
												</cfselect>
												</cfoutput>
												<!---
												<cfinput type="text" name="lead_birthday" class="form-control" maxlength="2" placeholder="#translations[langID]['chat']['day']#" mask="9999">--->
											</div>
											<div class="col-xs-3">
												<cfoutput>
												<cfinput type="button" name="submit_birthdate" id="submit_birthdate" class="btn btn-default" value="#translations[langID]['chat']['btn_send']#">
												</cfoutput>
											</div>

								        </div>

								    </div>


							        <div id="gendercanvas" style="display:none;">

										<div class="row">
											<div class="col-xs-6 text-center">
												<cfoutput>
												<cfinput type="button" name="submit_gender" id="gender_male" class="submit_gender btn btn-default" value="#translations[langID]['chat']['btn_male']#">
												</cfoutput>
											</div>
											<div class="col-xs-6 text-center">
												<cfoutput>
												<cfinput type="button" name="submit_gender" id="gender_female" class="submit_gender btn btn-default" value="#translations[langID]['chat']['btn_female']#">
												</cfoutput>
											</div>
											
								        </div>
								    
									</div>


									<div id="zipcanvas" style="display:none;">
										
										<div class="row">
											<div class="col-md-4">	
												<cfinput type="text" name="lead_zip" id="lead_zip" class="form-control" maxlength="5" mask="99999" placeholder="99999">
											</div>
											<div class="col-md-8">
												<cfoutput>
												<cfinput type="button" name="submit_zip" id="submit_zip" class="submit_zip btn btn-default" value="#translations[langID]['chat']['btn_send']#">
												</cfoutput>
											</div>

								        </div>
								    
									</div>

									
									<div id="namecanvas" style="display:none;">
										
										<div class="row">
											<div class="col-xs-5">	
												<cfinput type="text" name="lead_name" class="form-control" maxlength="50" placeholder="#translations[langID]['chat']['first_name']#" mask="">
											</div>
											<div class="col-xs-5">
												<cfinput type="text" name="lead_surname" class="form-control" maxlength="50" placeholder="#translations[langID]['chat']['surname']#" mask="">
											</div>
											<div class="col-xs-2">
												<cfoutput>
												<cfinput type="button" name="submit_name" id="submit_name" class="btn btn-default" value="#translations[langID]['chat']['btn_send']#">
												</cfoutput>
											</div>

								        </div>
								    
									</div>


									<div id="interestcanvas" style="display:none;">
										
										<div class="row">
											<div class="col-xs-2 text-center">
												<cfoutput>
												<cfinput type="button" name="submit_interest" id="love" class="submit_interest btn btn-default" value="#translations[langID]['interests']['love']#">
												</cfoutput>
											</div>
											<div class="col-xs-2 text-center">
												<cfoutput>
												<cfinput type="button" name="submit_interest" id="money" class="submit_interest btn btn-default" value="#translations[langID]['interests']['money']#">
												</cfoutput>
											</div>
											<div class="col-xs-2 text-center">
												<cfoutput>
												<cfinput type="button" name="submit_interest" id="health" class="submit_interest btn btn-default" value="#translations[langID]['interests']['health']#">
												</cfoutput>
											</div>
											<div class="col-xs-2 text-center">
												<cfoutput>
												<cfinput type="button" name="submit_interest" id="family" class="submit_interest btn btn-default" value="#translations[langID]['interests']['family']#">
												</cfoutput>
											</div>
											<div class="col-xs-2 text-center">
												<cfoutput>
												<cfinput type="button" name="submit_interest" id="career" class="submit_interest btn btn-default" value="#translations[langID]['interests']['career']#">
												</cfoutput>
											</div>
											<div class="col-xs-2 text-center">
												<cfoutput>
												<cfinput type="button" name="submit_interest" id="luck" class="submit_interest btn btn-default" value="#translations[langID]['interests']['luck']#">
												</cfoutput>
											</div>
											
								        </div>
								    
									</div>

									
									<div id="emailintrocanvas" style="display:none;">
										
										<div class="row">
											<div class="col-xs-6 text-center">
												<cfoutput>
												<cfinput type="button" name="submit_emailintro" id="email_yes" class="submit_emailintro btn btn-default" value="#translations[langID]['chat']['yes']#">
												</cfoutput>
											</div>
											<div class="col-xs-6 text-center">
												<cfoutput>
												<cfinput type="button" name="submit_emailintro" id="email_no" class="submit_emailintro btn btn-default" value="#translations[langID]['chat']['no']#">
												</cfoutput>
											</div>
											
								        </div>
								    
									</div>


									<div id="emailcanvas" style="display:none;">
										
										<div class="row">
											<div class="col-xs-10">
												<!--- <cfoutput>#translations[langID]['chat']['first_name']#</cfoutput>: --->
												<cfinput type="text" name="lead_email" id="lead_email" class="form-control" maxlength="100" placeholder="email" mask="">
											</div>
											<div class="col-xs-2">
												<cfoutput>
												<cfinput type="button" name="submit_email" id="submit_email" class="btn btn-default" value="#translations[langID]['chat']['btn_send']#">
												</cfoutput>
											</div>

								        </div>
								    
									</div>


									<div id="phoneintrocanvas" style="display:none;">
										
										<div class="row">
											<div class="col-xs-6 text-center">
												<cfoutput>
												<cfinput type="button" name="submit_phoneintro" id="phone_yes" class="submit_phoneintro btn btn-default" value="#translations[langID]['chat']['yes']#">
												</cfoutput>
											</div>
											<div class="col-xs-6 text-center">
												<cfoutput>
												<cfinput type="button" name="submit_phoneintro" id="phone_no" class="submit_phoneintro btn btn-default" value="#translations[langID]['chat']['no']#">
												</cfoutput>
											</div>
											
								        </div>
								    
									</div>


									<div id="phonecanvas" style="display:none;">
										
										<div class="row">
											<div class="col-md-4">
												<cfinput type="hidden" name="lead_phone_dd" class="form-control" maxlength="2" placeholder="DD" mask="99">
												<cfinput type="text" name="lead_phone" class="form-control" maxlength="9" placeholder="#translations[langID]['chat']['phone']#" mask="999999999">
											</div>
											<div class="col-md-6">
												<cfoutput>
												<cfinput type="button" name="submit_phone" id="submit_phone" class="btn btn-default" value="#translations[langID]['chat']['btn_send']#">
												</cfoutput>
											</div>

								        </div>
								    
									</div>

								</cfform>	
							    
							</div>

						</div>

					</div>

				</div>

			</section>

			<section id="zone2">

				<div class="row">
					
					<cfoutput query="rsZodiac">
						
						<div class="col-sm-1 col-xs-4 zodiac_images_cont text-center">
							<a href="###rsZodiac.sign_key#">
							<div class="zodiac_images rose_bg3">
							<img src="../media/dynimages/zodiac/#rsZodiac.sign_logo_inv#" class="img-responsive center-block" alt="#translations[langID]['zodiac'][rsZodiac.sign_key]#">
							<div class="zodiac_images_text">#translations[langID]['zodiac'][rsZodiac.sign_key]#</div>
							</div>
							</a>
						</div>
			
					</cfoutput>

				</div>

			</section>

			<section id="zone3">

				<div class="row">
					<cfloop query="rsZodiac">
						
						<div class="col-sm-4">
							
							<cfoutput>
							<div class="zodiac_area">
								
							
								<div class="zodiac_box" id="#rsZodiac.sign_key#">

									<div class="zodiac_box_content">
										<div style="margin:0 auto;" class="text-center">
											<div class="zodiac_title">#translations[langID]['zodiac'][rsZodiac.sign_key]#</div>
											<div class="zodiac_date">#translations[langID]['zodiac_long_date'][rsZodiac.sign_key]#</div>
										</div>
										<div class="zodiac_text">
										#translations[langID]['zodiac_long'][rsZodiac.sign_key]#
										</div>
									</div>

								</div>

								<div style="position:absolute; top:0px; left:0px;">
									<img src="../media/dynimages/zodiac/#rsZodiac.sign_logo#" class="img-responsive" alt="#translations[langID]['zodiac'][rsZodiac.sign_key]#">
								</div>

								

							</div>
							</cfoutput>
						</div>
			
					</cfloop>

				</div>

			</section>

		</div>		

	</div>

	<section id="zone4">

	<div class="container-fluid rose_bg2">
	
		<div class="container references_box">

				<div class="row">
				
					<cfloop query="rsReferences">
					<cfoutput>	
					<div class="col-sm-4 text-center">
						<div class="references_image">
						<img src="../media/dynimages/references/#reference_image#">
						</div>
						<div class="references_name">
						#reference_name#, #reference_country#
						</div>
						<div class="references_date">
						#reference_date#
						</div>
						<div class="references_text">
						"#reference_text#"
						</div>
					</div>
					</cfoutput>
					</cfloop>	
				

				</div>

			

		</div>		

	</div>

	</section>

	<section id="zone5">

	<div class="container-fluid">

		<div class="row">

			<div class="col-sm-6 text-center footer_text"><cfoutput>&copy #year(now())# #translations[langID]['footer']['copyright']#</cfoutput></div>

			<div class="col-sm-6 text-center footer_text">
				<cfoutput>
				<a href="##popup_privacy" class="open_popup">#translations[langID]['footer']['privacy']#</a> | 
				<a href="##popup_terms" class="open_popup">#translations[langID]['footer']['terms']#</a> | 
				<a href="##popup_partners" class="open_popup">#translations[langID]['footer']['partners']#</a> | 
				<a href="##popup_cookies" class="open_popup">#translations[langID]['footer']['cookies']#</a>
				</cfoutput>
			</div>

		</div>

	</div>

	</section>

	<cfinclude template="../i_divs.cfm">

	<cfinclude template="../i_scripts.cfm">
	<script type="text/javascript" src="validation.js"></script>
	<!---<script>jQuery.magnificPopup.open({items: {src: '#popup_cookies'},type: 'inline'}, 0);</script>--->
	<script>

		$(document).ready(function(){

			<cfinclude template="../scripts/scripts.cfm">

		});

	</script>


</body>
</html>
