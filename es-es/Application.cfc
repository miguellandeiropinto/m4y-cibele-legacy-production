<cfcomponent displayname="Application" output="true" hint="Handle the application.">

	<!--- Set up the application. --->
	<cfset THIS.Name = "CIBELLE.ES.WAPP" />
	<cfset THIS.ApplicationTimeout = CreateTimeSpan( 0, 0, 45, 0 ) />
	<cfset THIS.SessionManagement = true />
	<cfset THIS.SetClientCookies = true />
	<cfset THIS.SessionTimeout= CreateTimeSpan( 0, 0, 30, 0 ) /> 

<!---
	<cfset THIS.clientManagement="False"> 
	<cfset THIS.loginStorage="Cookie"> 
	
	<cfset THIS.setDomainCookies="False"> 
--->


	
	<!--- Define the page request properties. --->
	<cfsetting requestTimeout="20" showDebugOutput="false" enableCFOutputOnly="false" />

	
	<cfset THIS.datasource="leaddam_wapp">


	<cffunction name="OnApplicationStart" access="public" returntype="boolean" output="false" hint="Fires when the application is first created.">
		
		<cflog file="#This.Name#" type="Information" text="Application Started">

		<cfset APPLICATION.langID='es'>
		<cfset APPLICATION.platformID='cibeleESc'>
		<cfset SetLocale("Spanish (Standard)")>
		<cfset APPLICATION.locale=SetLocale("Spanish (Standard)")>		
		
		<cfreturn true />
	</cffunction>


	<cffunction name="OnSessionStart" access="public" returntype="void" output="false" hint="Fires when the session is first created.">

		<CFIF #CGI.HTTP_X_Forwarded_For# EQ "">
			<CFSET theip="#CGI.Remote_Addr#">
		<CFELSE>
			<CFSET theip="#CGI.HTTP_X_Forwarded_For#">
		</CFIF>

		<cftry>

			<CFSTOREDPROC procedure="dbo.getCountry">
			  <CFPROCPARAM type="IN" value="#theip#" cfsqltype="CF_SQL_VARCHAR">
			  <CFPROCPARAM type="OUT" variable="country" cfsqltype="CF_SQL_VARCHAR">
			</CFSTOREDPROC>
		<cfcatch>
		 	<cfset country="">
		</cfcatch>
		</cftry>


		<cflock timeout=20 scope="Session" type="Exclusive"> 
			<cfset session.platformID=APPLICATION.platformID>
			<cfset session.sourceID="">
			<cfset session.mediumID="">
			<cfset session.campaignID="">
			<cfset session.lead_birthdate="">
			<cfset session.lead_sign="">
			<cfset session.lead_gender="">
			<cfset session.lead_name="">
			<cfset session.lead_surname="">
			<cfset session.lead_interest="">
			<cfset session.lead_optin_email=1>
			<cfset session.lead_email="">
			<cfset session.lead_phone="">
			<cfset session.lead_zip="">
			<cfset session.lead_optin_phone=0>
			<cfset session.lead_ip=theip>
			<cfset session.lead_country=country>
			<CFSTOREDPROC procedure="dbo.getPartners">
			  <CFPROCPARAM type="IN" value="#APPLICATION.langID#" cfsqltype="CF_SQL_VARCHAR">
			  <CFPROCRESULT name="rsPartners" resultset="1">
			  <CFPROCRESULT name="rsCoregs" resultset="2">
			</CFSTOREDPROC>
			<cfset session.partners=StructNew()>
			<cfloop query="rsPartners">
				<cfset session.partners[partnerID]=1>
			</cfloop>
			<cfloop query="rsCoregs">
				<cfset session.partners[partnerID]=0>
			</cfloop>
		</cflock>

		<cfreturn />
	</cffunction>


	<cffunction name="OnRequestStart" access="public" returntype="boolean" output="false" hint="Fires at first part of page processing.">

		<cfargument name="TargetPage" type="string" required="true" />

		<cfif isdefined("url.reset")>
		    <cfset OnApplicationStart()>
		    <cfset StructClear(session)>
		    <cfset OnSessionStart()>
		</cfif>

		<cfreturn true />
	</cffunction>


	<cffunction name="OnRequest" access="public" returntype="void" output="true" hint="Fires after pre page processing is complete.">

		<cfargument name="TargetPage" type="string" required="true" />

		<!--- Include the requested page. --->
		<cfinclude template="#ARGUMENTS.TargetPage#" />

		
		<cfreturn />
	</cffunction>


	<cffunction name="OnRequestEnd" access="public" returntype="void" output="true" hint="Fires after the page processing is complete.">


		<cfreturn />
	</cffunction>


	<cffunction name="OnSessionEnd" access="public" returntype="void" output="false" hint="Fires when the session is terminated.">

		<!--- Define arguments. --->
		<cfargument name="SessionScope" type="struct" required="true"/>
		<cfargument name="ApplicationScope" type="struct" required="false" default="#StructNew()#" />

		<cfreturn />
	</cffunction>


	<cffunction name="OnApplicationEnd" access="public" returntype="void" output="false" hint="Fires when the application is terminated.">

		<!--- Define arguments. --->
		<cfargument name="ApplicationScope" type="struct" required="false" default="#StructNew()#" />

		<cfreturn />
	</cffunction>


	<cffunction name="OnError" access="public" returntype="void" output="true" hint="Fires when an exception occures that is not caught by a try/catch.">

		<!--- Define arguments. --->
		<cfargument name="Exception" type="any" required="true" />
		<cfargument name="EventName" type="string" required="false" default=""/>

		<cflog file="#This.Name#" type="error" text="Event Name: #Arguments.Eventname#" > 
		<cflog file="#This.Name#" type="error" text="Message: #Arguments.Exception.message#"> 
		<cfif isdefined("Arguments.Exception.rootcause")> 
        <cflog file="#This.Name#" type="error"  text="Root Cause Message: #Arguments.Exception.rootcause.message#"> 
    	</cfif>
		<!--- Display an error message if there is a page context. ---> 
		<cfif NOT (Arguments.EventName IS "onSessionEnd") OR  (Arguments.EventName IS "onApplicationEnd")> 
	        <cfoutput>
	        
	        <!---<cflocation url="../error.cfm?token=#key#"> <cfinclude template="cfc/queries.cfc">--->
	        </cfoutput>
	        <cfoutput> 
	            <h2>An unexpected error occurred.</h2> 
	            <p>Please provide the following information to technical support:"</p> 
	            <p>Error Event: #Arguments.EventName#</p> 
	            <p>Error details:<br> 
	            <cfdump var=#Arguments.Exception#></p> 
	        </cfoutput> 
    	</cfif>

		<cfreturn />
	</cffunction>

</cfcomponent>