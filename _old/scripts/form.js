$(document).ready(function(){

	$("#name").focus();

	// roll over generico
	$(".rollover").hover( function() { this.src = this.src.replace("_off","_on"); }, function() { this.src = this.src.replace("_on","_off"); } );

	var $root = $('html, body');
	$('a').click(function() {
	    var href = $.attr(this, 'href');
	    $root.animate({
	        scrollTop: $(href).offset().top
	    }, 500, function () {
	        window.location.hash = href;
	    });
	    return false;
	});


	 $("#mform").validate({
		rules: {
			lead_name: {required: true, minlength: 2 },
			lead_surname: {required: true, minlength: 2 },
			lead_email: {required: true, email: true},
			lead_phone: {required: true, digits:true, minlength:9, maxlength:9},
			lead_zip: {required: true, minlength:4},
			lead_birthdate: {required: true, dateISO: true},
			lead_gender: {required:true}
		},
		messages: {
			lead_name: "Insert your name",
			lead_surname: "Insert your surname",
			lead_email: "Insert a valid email",
			lead_phone: "Insert your phone no",
			lead_zip: "Insert your zip code",
			lead_birthdate: "Insert a valid date",
			lead_gender: "Indicate your gender"
		},
		wrapper: 'div',
		ignore: ':hidden:not(:radio)',
		errorPlacement: function (error, element) {
        	if (element.attr("name") == "lead_name") {
            	$("#errplace-lead_name").append(error);
            }
            if (element.attr("name") == "lead_surname") {
            	$("#errplace-lead_surname").append(error);
            }
            if (element.attr("name") == "lead_email") {
            	$("#errplace-lead_email").append(error);
            }
            if (element.attr("name") == "lead_phone") {
            	$("#errplace-lead_phone").append(error);
            }
            if (element.attr("name") == "lead_zip") {
            	$("#errplace-lead_zip").append(error);
            }
            if (element.attr("name") == "lead_birthdate") {
            	$("#errplace-lead_birthdate").append(error);
            }
            if (element.attr("name") == "lead_gender") {
            	$("#errplace-lead_gender").append(error);
            }
		 },
		highlight: function(element) {
            $(element).addClass('error');
             $(element).closest('.form-control').addClass('input-error');
        }, unhighlight: function(element) {
            $(element).removeClass('error');
        }
	});

});
