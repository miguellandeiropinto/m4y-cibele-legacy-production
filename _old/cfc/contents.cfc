<cfprocessingdirective pageencoding="utf-8">

<cfinclude template="config.cfc">

<CFSTOREDPROC procedure="dbo.getPlatform">
  <CFPROCPARAM type="IN" value="#platformID#" cfsqltype="CF_SQL_VARCHAR">
  <CFPROCRESULT name="rsPlatform" resultset="1">
</CFSTOREDPROC>


<CFSTOREDPROC procedure="dbo.getTranslations">
  <!--- <CFPROCPARAM type="IN" value="#Application.bIDv#" cfsqltype="CF_SQL_VARCHAR">--->
  <CFPROCRESULT name="rsTranslations" resultset="1">
</CFSTOREDPROC>

<cfset translations = StructNew() >	

<cfloop query="rsTranslations">
	<cfset translations.pt["#rsTranslations.tgroup#"]["#rsTranslations.tkey#"] = rsTranslations.pt >
	<cfset translations.br["#rsTranslations.tgroup#"]["#rsTranslations.tkey#"] = rsTranslations.br >
</cfloop>


<CFSTOREDPROC procedure="dbo.getZodiac">
  <!--- <CFPROCPARAM type="IN" value="#Application.bIDv#" cfsqltype="CF_SQL_VARCHAR">--->
  <CFPROCRESULT name="rsZodiac" resultset="1">
</CFSTOREDPROC>
	
<cfset zodiac = StructNew() >	

<cfloop query="rsZodiac">
	<cfset zodiac["#rsZodiac.signID#"]["key"] = rsZodiac.sign_key >
	<cfset zodiac["#rsZodiac.signID#"]["day_start"] = rsZodiac.sign_day_start >
	<cfset zodiac["#rsZodiac.signID#"]["month_start"] = rsZodiac.sign_month_start >
	<cfset zodiac["#rsZodiac.signID#"]["day_finish"] = rsZodiac.sign_day_finish >
	<cfset zodiac["#rsZodiac.signID#"]["month_finish"] = rsZodiac.sign_month_finish >
	<cfset zodiac["#rsZodiac.signID#"]["element"] = rsZodiac.sign_element >
	<cfset zodiac["#rsZodiac.signID#"]["characteristics1"] = rsZodiac.sign_characteristics1 >
	<cfset zodiac["#rsZodiac.signID#"]["characteristics2"] = rsZodiac.sign_characteristics2 >
	<cfset zodiac["#rsZodiac.signID#"]["planet1"] = rsZodiac.sign_planet1 >
	<cfset zodiac["#rsZodiac.signID#"]["planet2"] = rsZodiac.sign_planet2 >
	<cfset zodiac["#rsZodiac.signID#"]["incense"] = rsZodiac.sign_incense >
	<cfset zodiac["#rsZodiac.signID#"]["stone"] = rsZodiac.sign_stone >
	<cfset zodiac["#rsZodiac.signID#"]["metal"] = rsZodiac.sign_metal >
	<cfset zodiac["#rsZodiac.signID#"]["color"] = rsZodiac.sign_color >
	<cfset zodiac["#rsZodiac.signID#"]["number"] = rsZodiac.sign_number >
	<cfset zodiac["#rsZodiac.signID#"]["short_desc"] = rsZodiac.sign_short_desc >
	<cfset zodiac["#rsZodiac.signID#"]["long_desc"] = rsZodiac.sign_long_desc >
</cfloop>

<!--- <cfset x=ToScript(zodiac, "jsVar")> --->


<CFSTOREDPROC procedure="dbo.getReferences">
  <CFPROCPARAM type="IN" value="#langID#" cfsqltype="CF_SQL_VARCHAR">
  <CFPROCRESULT name="rsReferences" resultset="1">
</CFSTOREDPROC>


<CFSTOREDPROC procedure="dbo.getForecasts">
  <CFPROCPARAM type="IN" value="#langID#" cfsqltype="CF_SQL_VARCHAR">
  <CFPROCPARAM type="IN" value="" cfsqltype="CF_SQL_VARCHAR">
  <CFPROCRESULT name="rsOpener" resultset="2">
</CFSTOREDPROC>