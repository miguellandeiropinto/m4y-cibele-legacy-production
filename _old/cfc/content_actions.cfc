<cfcomponent>
<cfprocessingdirective pageencoding="utf-8">

<cfinclude template="config.cfc">
<cfinclude template="contents.cfc">


	<cffunction name="getSign" access="remote" returnformat="json">
      	
		<cfloop query="rsZodiac">
			
			<cfif rsZodiac.sign_month_start eq 1>
				<cfset the_ini_date=CreateDate(URL.year-1, rsZodiac.sign_month_start, rsZodiac.sign_day_start)>
				<cfset the_end_date=CreateDate(URL.year, rsZodiac.sign_month_finish, rsZodiac.sign_day_finish)>
			<cfelseif rsZodiac.sign_month_start eq 12>
				<cfset the_ini_date=CreateDate(URL.year, rsZodiac.sign_month_start, rsZodiac.sign_day_start)>
				<cfset the_end_date=CreateDate(URL.year+1, rsZodiac.sign_month_finish, rsZodiac.sign_day_finish)>
			<cfelse>
				<cfset the_ini_date=CreateDate(URL.year, rsZodiac.sign_month_start, rsZodiac.sign_day_start)>
				<cfset the_end_date=CreateDate(URL.year, rsZodiac.sign_month_finish, rsZodiac.sign_day_finish)>
			</cfif>
			
			<cfset the_date=CreateDate(URL.year, URL.month, URL.day)>
			<!---<cfoutput>#the_date# | #the_ini_date# | #the_end_date#</cfoutput><br>--->

			<cfif the_date gte the_ini_date and the_date lte the_end_date>
			
				<cfset signID=rsZodiac.signID>
				<cfset sign_key=rsZodiac.sign_key>

			</cfif>

		</cfloop>

    	<cfset result.sign_key=sign_key>
    	<cfset result.sign=translations[langID]['zodiac'][sign_key]>
    	<cfset result.short_desc=translations[langID]['zodiac_short'][sign_key]>
    	<!---
    	<cfset result.characteristics1=translations[langID]['characteristics'][zodiac[signID]['characteristics1']]>
    	<cfset result.characteristics2=translations[langID]['characteristics'][zodiac[signID]['characteristics2']]>
    	--->

    <cfreturn result>
    
    </cffunction>


	<cffunction name="getForecast" access="remote" returnformat="json">
      	
		<CFSTOREDPROC procedure="dbo.getForecasts">
		  <CFPROCPARAM type="IN" value="#langID#" cfsqltype="CF_SQL_VARCHAR">
		  <CFPROCPARAM type="IN" value="#URL.int#" cfsqltype="CF_SQL_VARCHAR">
		  <CFPROCRESULT name="rsForecasts" resultset="1">
		</CFSTOREDPROC>
		

    	<cfset result.forecast=rsForecasts.sentence>
    
    <cfreturn result>
    
    </cffunction>

</cfcomponent>