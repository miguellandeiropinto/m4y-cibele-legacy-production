<cfcomponent>
<cfprocessingdirective pageencoding="utf-8">

<cfinclude template="config.cfc">

<cfparam name="URL.leadID" default="">
<cfparam name="URL.ccID" default="">


	<cffunction name="do_integration" access="remote">


		<CFSTOREDPROC procedure="dbo.getLeadByID">
		  <CFPROCPARAM type="IN" value="#URL.leadID#" cfsqltype="CF_SQL_VARCHAR">
		  <CFPROCRESULT name="rsLead" resultset="1">
		</CFSTOREDPROC>

		<CFSTOREDPROC procedure="dbo.getClientCampaign">
		  <CFPROCPARAM type="IN" value="#URL.ccID#" cfsqltype="CF_SQL_VARCHAR">
		  <CFPROCRESULT name="rsCampaign" resultset="1">
		  <CFPROCRESULT name="rsRules" resultset="2">
		  <CFPROCRESULT name="rsFields" resultset="3">
		</CFSTOREDPROC>

		<CFSTOREDPROC procedure="dbo.checkCampaignRules">
		  <CFPROCPARAM type="IN" value="#URL.ccID#" cfsqltype="CF_SQL_VARCHAR">
		  <CFPROCPARAM type="IN" value="#URL.leadID#" cfsqltype="CF_SQL_VARCHAR">
		  <CFPROCRESULT name="rsLeadRules" resultset="1">
		</CFSTOREDPROC>

		<cfset ftranslate = StructNew() >

		<cfloop query="rsFields">
			<cfset ftranslate["#rsFields.field_name#"]["#rsFields.field_value#"] = rsFields.field_value_client >
		</cfloop>


		<cfswitch expression="#URL.ccID#">
			
			<cfcase value="tara-coreg-pt-0001">
			
				<cfif rsLeadRules.recordcount eq 1>
				
					<cfhttp url="http://www.tara-vidente.com/remote-registration.php" method="get" result="result" charset="utf-8"> 
					    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
					    <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#"> 
					    <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#"> 
					    <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#"> 
					    <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
					    <cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> 
					    <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#"> 
					    <cfhttpparam type="url" name="campaignarea" value="PT"> 
					    <cfhttpparam type="url" name="media" value="coreg">
					    <cfhttpparam type="url" name="partner" value="Yours">  
					    <cfhttpparam type="url" name="campaign" value="coreg2016">  
					</cfhttp>


					<CFSTOREDPROC procedure="dbo.integrate_lead">
						<CFPROCPARAM type="IN" value="#rsCampaign.campaignID#" cfsqltype="CF_SQL_VARCHAR">
						<CFPROCPARAM type="IN" value="#rsLead.leadID#" cfsqltype="CF_SQL_VARCHAR">
						<CFPROCPARAM type="IN" value="#result.filecontent#" cfsqltype="CF_SQL_VARCHAR">
						<!---<CFPROCPARAM type="OUT" variable="leadID" cfsqltype="CF_SQL_VARCHAR">--->
					</CFSTOREDPROC>


					<cfif result.filecontent eq 'ok'>
						
						<cfoutput>lead #rsLead.leadID# integrada com sucesso na campanha #rsCampaign.campaignID#</cfoutput>

					<cfelse>

						<cfoutput>lead #rsLead.leadID# não integrada na campanha #rsCampaign.campaignID#. Motivo:
						#result.filecontent#
						</cfoutput>
					
					</cfif>

				<cfelse>

					<cfoutput>lead #rsLead.leadID# não integrada na campanha #rsCampaign.campaignID#. Motivo
					não satisfaz os critérios (país ou idade)
					</cfoutput>

				</cfif>



			</cfcase>
			<cfcase value="tara-coreg-br-0002">
				
				<cfif rsLeadRules.recordcount eq 1>
				
					<cfhttp url="http://www.tara-clarividencia.com/remote-registration.php" method="get" result="result" charset="utf-8"> 
					    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
					    <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#"> 
					    <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#"> 
					    <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#"> 
					    <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
					    <cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> 
					    <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#"> 
					    <cfhttpparam type="url" name="campaignarea" value="PT"> 
					    <cfhttpparam type="url" name="media" value="coreg">
					    <cfhttpparam type="url" name="partner" value="Yours">  
					    <cfhttpparam type="url" name="campaign" value="coreg2016">  
					</cfhttp>


					<CFSTOREDPROC procedure="dbo.integrate_lead">
						<CFPROCPARAM type="IN" value="#rsCampaign.campaignID#" cfsqltype="CF_SQL_VARCHAR">
						<CFPROCPARAM type="IN" value="#rsLead.leadID#" cfsqltype="CF_SQL_VARCHAR">
						<CFPROCPARAM type="IN" value="#result.filecontent#" cfsqltype="CF_SQL_VARCHAR">
						<!---<CFPROCPARAM type="OUT" variable="leadID" cfsqltype="CF_SQL_VARCHAR">--->
					</CFSTOREDPROC>


					<cfif result.filecontent eq 'ok'>
						
						<cfoutput>lead #rsLead.leadID# integrada com sucesso na campanha #rsCampaign.campaignID#</cfoutput>

					<cfelse>

						<cfoutput>lead #rsLead.leadID# não integrada na campanha #rsCampaign.campaignID#. Motivo:
						#result.filecontent#
						</cfoutput>
					
					</cfif>

				<cfelse>

					<cfoutput>lead #rsLead.leadID# não integrada na campanha #rsCampaign.campaignID#. Motivo
					não satisfaz os critérios (país ou idade)
					</cfoutput>

				</cfif>


			</cfcase>
			<cfcase value="maria-coreg-pt-0003">
				
				<cfif rsLeadRules.recordcount eq 1>
				
					<cfhttp url="http://www.vidente-maria.com/remote-registration.php" method="get" result="result" charset="utf-8"> 
					    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
					    <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#"> 
					    <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#"> 
					    <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#"> 
					    <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
					    <!---<cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> --->
					    <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#">
					    <cfhttpparam type="url" name="optin" value="#rsLead.lead_optin#">
					    <cfhttpparam type="url" name="campaignarea" value="PT"> 
					    <cfhttpparam type="url" name="media" value="coreg">
					    <cfhttpparam type="url" name="partner" value="Yours">  
					    <cfhttpparam type="url" name="campaign" value="coreg2016">  
					</cfhttp>


					<CFSTOREDPROC procedure="dbo.integrate_lead">
						<CFPROCPARAM type="IN" value="#rsCampaign.campaignID#" cfsqltype="CF_SQL_VARCHAR">
						<CFPROCPARAM type="IN" value="#rsLead.leadID#" cfsqltype="CF_SQL_VARCHAR">
						<CFPROCPARAM type="IN" value="#result.filecontent#" cfsqltype="CF_SQL_VARCHAR">
						<!---<CFPROCPARAM type="OUT" variable="leadID" cfsqltype="CF_SQL_VARCHAR">--->
					</CFSTOREDPROC>


					<cfif result.filecontent eq 'ok'>
						
						<cfoutput>lead #rsLead.leadID# integrada com sucesso na campanha #rsCampaign.campaignID#</cfoutput>

					<cfelse>

						<cfoutput>lead #rsLead.leadID# não integrada na campanha #rsCampaign.campaignID#. Motivo:
						#result.filecontent#
						</cfoutput>
					
					</cfif>

				<cfelse>

					<cfoutput>lead #rsLead.leadID# não integrada na campanha #rsCampaign.campaignID#. Motivo
					não satisfaz os critérios (país ou idade)
					</cfoutput>

				</cfif>

			</cfcase>
			<cfcase value="maria-coreg-br-0004">

				<cfif rsLeadRules.recordcount eq 1>
				
					<cfhttp url="http://www.maria-medium-numerologa.com/remote-registration.php" method="get" result="result" charset="utf-8"> 
					    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
					    <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#"> 
					    <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#"> 
					    <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#"> 
					    <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
					    <!---<cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> --->
					    <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#">
					    <cfhttpparam type="url" name="optin" value="#rsLead.lead_optin#">
					    <cfhttpparam type="url" name="campaignarea" value="PT"> 
					    <cfhttpparam type="url" name="media" value="coreg">
					    <cfhttpparam type="url" name="partner" value="Yours">  
					    <cfhttpparam type="url" name="campaign" value="coreg2016">  
					</cfhttp>


					<CFSTOREDPROC procedure="dbo.integrate_lead">
						<CFPROCPARAM type="IN" value="#rsCampaign.campaignID#" cfsqltype="CF_SQL_VARCHAR">
						<CFPROCPARAM type="IN" value="#rsLead.leadID#" cfsqltype="CF_SQL_VARCHAR">
						<CFPROCPARAM type="IN" value="#result.filecontent#" cfsqltype="CF_SQL_VARCHAR">
						<!---<CFPROCPARAM type="OUT" variable="leadID" cfsqltype="CF_SQL_VARCHAR">--->
					</CFSTOREDPROC>


					<cfif result.filecontent eq 'ok'>
						
						<cfoutput>lead #rsLead.leadID# integrada com sucesso na campanha #rsCampaign.campaignID#</cfoutput>

					<cfelse>

						<cfoutput>lead #rsLead.leadID# não integrada na campanha #rsCampaign.campaignID#. Motivo:
						#result.filecontent#
						</cfoutput>
					
					</cfif>

				<cfelse>

					<cfoutput>lead #rsLead.leadID# não integrada na campanha #rsCampaign.campaignID#. Motivo
					não satisfaz os critérios (país ou idade)
					</cfoutput>

				</cfif>				


			</cfcase>
		</cfswitch>



	</cffunction>


</cfcomponent>