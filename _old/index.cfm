<cfprocessingdirective pageencoding="utf-8">
<cfcontent type="text/html; charset=utf-8">
<cfset setEncoding("URL", "utf-8")>
<cfset setEncoding("Form", "utf-8")>

<cfinclude template="cfc/config.cfc">
<cfinclude template="cfc/contents.cfc">
<cfinclude template="cfc/tracking.cfc">



<!DOCTYPE HTML>
<html>
<head>
<!-- META -->
<cfinclude template="i_meta.cfm">
<cfinclude template="i_css.cfm">
<cfinclude template="i_analytics.cfm">
</head>

<body class="bg">

	<div class="container-fluid">
	
		<div class="container">

			<section id="zone1">

				<div class="row">
					
					<div class="col-sm-12 text-center">
						<cfoutput>
						<h1 class="site_title">#translations[langID]['platform_title'][rsPlatform.platform_key]#</h1>
						</cfoutput>
					</div>

				</div>

				<div class="row">
					
					<div class="col-sm-10 col-sm-offset-2 rose_bg text-center">
					
						<cfoutput>
						<h2 class="site_subtitle">#translations[langID]['platform_subtitle'][rsPlatform.platform_key]#</h2>
						</cfoutput>

					</div>

				</div>

				<div class="row">
					
					<div class="col-sm-4">
					
						<div class="photo_box">
							<img src="media/images/cibele.png" class="img-responsive">
						</div>

						<div class="tip_box">
							<cfoutput>
							<span>"#rsOpener.sentence#"</span>
							</cfoutput>
						</div>

						<div class="fb_box">
							<a href="https://www.facebook.com/vidente.cibele/" target="_blank"><img src="media/images/fb_btn.jpg" class="img-responsive"></a>
						</div>

					</div>

					<div class="col-sm-8" style="padding-right:0px;">
					
						<div class="chat_box">

							<cfoutput>
							<span class="h3 chat_teaser">#translations[langID]['platform_teaser'][rsPlatform.platform_key]#</span>
							</cfoutput>

							<div id="chatcanvas" class="chatcanvas">
								
								<div id="c_birthdate" class="cibele_s"></div>
								<div id="u_birthdate" class="user_s"></div>
								<div id="c_gender" class="cibele_s"></div>
								<div id="u_gender" class="user_s"></div>
								<div id="c_name" class="cibele_s"></div>
								<div id="u_name" class="user_s"></div>
								<div id="c_interest" class="cibele_s"></div>
								<div id="u_interest" class="user_s"></div>
								<div id="c_emaili" class="cibele_s"></div>
								<div id="u_emaili" class="user_s"></div>
								<div id="c_email" class="cibele_s"></div>
								<div id="u_email" class="user_s"></div>
								<div id="c_phonei" class="cibele_s"></div>
								<div id="u_phonei" class="user_s"></div>
								<div id="c_phone" class="cibele_s"></div>
								<div id="u_phone" class="user_s"></div>
								<div id="c_goodbye" class="cibele_s"></div>

							</div>

							<div id="istypingcanvas" class="istypingcanvas">
								<div id="istyping" class="chat_istyping">
								<cfoutput>
								<img src="media/images/chat_pen.png">#translations[langID]['cibele']['writing']#<span id="dots"></span>
								</cfoutput>
								</div>
							</div>

							<div id="dialogcanvas" class="dialogcanvas">
								
							<cfform name="mform" id="mform">
								<cfinput type="hidden" name="leadID" id="leadID" value="">
								<cfinput type="hidden" name="lead_gender" id="lead_gender" value="">

								<div id="birthdatecanvas" style="display:none;">
									
									<div class="row">
										<div class="col-xs-3">
											<cfoutput>
											<cfset year_ini=year(now())-18>
											<cfset year_end=year_ini-80>	
											<cfselect name="lead_birthyear" class="form-control selectpicker" data-live-search="true" title="#translations[langID]['chat']['year']#">
												<cfloop index="y" from="#year_ini#" to="#year_end#" step="-1">
												<option value="#y#">#y#</option>		
												</cfloop>
											</cfselect>
											</cfoutput>
											<!---
											<cfinput type="text" name="lead_birthyear" class="form-control" maxlength="4" placeholder="#translations[langID]['chat']['year']#" mask="9999">--->
										</div>
										<div class="col-xs-3">
											<cfoutput>	
											<cfselect name="lead_birthmonth" class="form-control selectpicker" data-live-search="true" title="#translations[langID]['chat']['month']#">
												<cfloop index="m" from="1" to="12">
												<option value="#m#">#translations[langID]['months_short'][m]#</option>		
												</cfloop>
											</cfselect>
											</cfoutput>
											<!---
											<cfinput type="text" name="lead_birthmonth" class="form-control" maxlength="2" placeholder="#translations[langID]['chat']['month']#" mask="99">--->
										</div>
										<div class="col-xs-3">	
											<cfoutput>	
											<cfselect name="lead_birthday" class="form-control selectpicker" data-live-search="true" title="#translations[langID]['chat']['day']#">
												<cfloop index="d" from="1" to="31">
												<option value="#d#">#d#</option>		
												</cfloop>
											</cfselect>
											</cfoutput>
											<!---
											<cfinput type="text" name="lead_birthday" class="form-control" maxlength="2" placeholder="#translations[langID]['chat']['day']#" mask="9999">--->
										</div>
										<div class="col-xs-3">
											<cfoutput>
											<cfinput type="button" name="submit_birthdate" id="submit_birthdate" class="btn btn-default" value="#translations[langID]['chat']['btn_send']#">
											</cfoutput>
										</div>

							        </div>

							    </div>


						        <div id="gendercanvas" style="display:none;">

									<div class="row">
										<div class="col-xs-6 text-center">
											<cfoutput>
											<cfinput type="button" name="submit_gender" id="gender_male" class="submit_gender btn btn-default" value="#translations[langID]['chat']['btn_male']#">
											</cfoutput>
										</div>
										<div class="col-xs-6 text-center">
											<cfoutput>
											<cfinput type="button" name="submit_gender" id="gender_female" class="submit_gender btn btn-default" value="#translations[langID]['chat']['btn_female']#">
											</cfoutput>
										</div>
										
							        </div>
							    
								</div>

								
								<div id="namecanvas" style="display:none;">
									
									<div class="row">
										<div class="col-xs-5">	
											<cfinput type="text" name="lead_name" class="form-control" maxlength="50" placeholder="#translations[langID]['chat']['first_name']#" mask="">
										</div>
										<div class="col-xs-5">
											<cfinput type="text" name="lead_surname" class="form-control" maxlength="50" placeholder="#translations[langID]['chat']['surname']#" mask="">
										</div>
										<div class="col-xs-2">
											<cfoutput>
											<cfinput type="button" name="submit_name" id="submit_name" class="btn btn-default" value="#translations[langID]['chat']['btn_send']#">
											</cfoutput>
										</div>

							        </div>
							    
								</div>


								<div id="interestcanvas" style="display:none;">
									
									<div class="row">
										<div class="col-xs-2 text-center">
											<cfoutput>
											<cfinput type="button" name="submit_interest" id="love" class="submit_interest btn btn-default" value="#translations[langID]['interests']['love']#">
											</cfoutput>
										</div>
										<div class="col-xs-2 text-center">
											<cfoutput>
											<cfinput type="button" name="submit_interest" id="money" class="submit_interest btn btn-default" value="#translations[langID]['interests']['money']#">
											</cfoutput>
										</div>
										<div class="col-xs-2 text-center">
											<cfoutput>
											<cfinput type="button" name="submit_interest" id="health" class="submit_interest btn btn-default" value="#translations[langID]['interests']['health']#">
											</cfoutput>
										</div>
										<div class="col-xs-2 text-center">
											<cfoutput>
											<cfinput type="button" name="submit_interest" id="family" class="submit_interest btn btn-default" value="#translations[langID]['interests']['family']#">
											</cfoutput>
										</div>
										<div class="col-xs-2 text-center">
											<cfoutput>
											<cfinput type="button" name="submit_interest" id="career" class="submit_interest btn btn-default" value="#translations[langID]['interests']['career']#">
											</cfoutput>
										</div>
										<div class="col-xs-2 text-center">
											<cfoutput>
											<cfinput type="button" name="submit_interest" id="luck" class="submit_interest btn btn-default" value="#translations[langID]['interests']['luck']#">
											</cfoutput>
										</div>
										
							        </div>
							    
								</div>

								
								<div id="emailintrocanvas" style="display:none;">
									
									<div class="row">
										<div class="col-xs-6 text-center">
											<cfoutput>
											<cfinput type="button" name="submit_emailintro" id="email_yes" class="submit_emailintro btn btn-default" value="#translations[langID]['chat']['yes']#">
											</cfoutput>
										</div>
										<div class="col-xs-6 text-center">
											<cfoutput>
											<cfinput type="button" name="submit_emailintro" id="email_no" class="submit_emailintro btn btn-default" value="#translations[langID]['chat']['no']#">
											</cfoutput>
										</div>
										
							        </div>
							    
								</div>



								<div id="emailcanvas" style="display:none;">
									
									<div class="row">
										<div class="col-xs-10">
											<!--- <cfoutput>#translations[langID]['chat']['first_name']#</cfoutput>: --->
											<cfinput type="text" name="lead_email" id="lead_email" class="form-control" maxlength="100" placeholder="email" mask="">
										</div>
										<div class="col-xs-2">
											<cfoutput>
											<cfinput type="button" name="submit_email" id="submit_email" class="btn btn-default" value="#translations[langID]['chat']['btn_send']#">
											</cfoutput>
										</div>

							        </div>
							    
								</div>


								<div id="phoneintrocanvas" style="display:none;">
									
									<div class="row">
										<div class="col-xs-6 text-center">
											<cfoutput>
											<cfinput type="button" name="submit_phoneintro" id="phone_yes" class="submit_phoneintro btn btn-default" value="#translations[langID]['chat']['yes']#">
											</cfoutput>
										</div>
										<div class="col-xs-6 text-center">
											<cfoutput>
											<cfinput type="button" name="submit_phoneintro" id="phone_no" class="submit_phoneintro btn btn-default" value="#translations[langID]['chat']['no']#">
											</cfoutput>
										</div>
										
							        </div>
							    
								</div>


								<div id="phonecanvas" style="display:none;">
									
									<div class="row">
										<div class="col-md-4">	
											<cfinput type="text" name="lead_phone" class="form-control" maxlength="9" placeholder="#translations[langID]['chat']['phone']#" mask="999999999">
										</div>
										<div class="col-md-8">
											<cfoutput>
											<cfinput type="button" name="submit_phone" id="submit_phone" class="btn btn-default" value="#translations[langID]['chat']['btn_send']#">
											</cfoutput>
										</div>

							        </div>
							    
								</div>

							</cfform>	
							    

							</div>

						</div>

					</div>

				</div>

			</section>

			<section id="zone2">

				<div class="row">
					
					<cfoutput query="rsZodiac">
						
						<div class="col-sm-1 col-xs-4 zodiac_images_cont text-center">
							<a href="###rsZodiac.sign_key#">
							<div class="zodiac_images rose_bg3">
							<img src="media/dynimages/zodiac/#rsZodiac.sign_logo_inv#" class="img-responsive center-block">
							<div class="zodiac_images_text">#translations[langID]['zodiac'][rsZodiac.sign_key]#</div>
							</div>
							</a>
						</div>
			
					</cfoutput>

				</div>

			</section>

			<section id="zone3">

				<div class="row">
					<cfloop query="rsZodiac">
						
						<div class="col-sm-4">
							
							<cfoutput>
							<div class="zodiac_area">
								
							
								<div class="zodiac_box" id="#rsZodiac.sign_key#">

									<div class="zodiac_box_content">
										<div style="margin:0 auto;" class="text-center">
											<div class="zodiac_title">#translations[langID]['zodiac'][rsZodiac.sign_key]#</div>
											<div class="zodiac_date">#translations[langID]['zodiac_long_date'][rsZodiac.sign_key]#</div>
										</div>
										<div class="zodiac_text">
										#translations[langID]['zodiac_long'][rsZodiac.sign_key]#
										</div>
									</div>

								</div>

								<div style="position:absolute; top:0px; left:0px;">
									<img src="media/dynimages/zodiac/#rsZodiac.sign_logo#" class="img-responsive">
								</div>

								

							</div>
							</cfoutput>
						</div>
			
					</cfloop>

				</div>

			</section>

		</div>		

	</div>

	<section id="zone4">

	<div class="container-fluid rose_bg2">
	
		<div class="container references_box">

				<div class="row">
				
					<cfloop query="rsReferences">
					<cfoutput>	
					<div class="col-sm-4 text-center">
						<div class="references_image">
						<img src="media/dynimages/references/#reference_image#">
						</div>
						<div class="references_name">
						#reference_name#, #reference_country#
						</div>
						<div class="references_date">
						#reference_date#
						</div>
						<div class="references_text">
						"#reference_text#"
						</div>
					</div>
					</cfoutput>
					</cfloop>	
				

				</div>

			

		</div>		

	</div>

	</section>

	<section id="zone5">

	<div class="container-fluid">

		<div class="row">

			<div class="col-sm-6 text-center footer_text"><cfoutput>&copy #year(now())# Todos os direitos reservados</cfoutput></div>

			<div class="col-sm-6 text-center footer_text">
				<a href="#popup_privacy" class="open_popup">Política de Privacidade</a> | 
				<a href="#popup_terms" class="open_popup">Termos e Condições</a> | 
				<a href="#popup_cookies" class="open_popup">Política de Cookies</a>
			</div>

		</div>

	</div>

	</section>

	<cfinclude template="i_divs.cfm">

	<cfinclude template="i_scripts.cfm">
	<script>

		$(document).ready(function(){
		<cfoutput>

			/* do the typing magic */
			setInterval(dothetype, 600);

			/* do the select box magic */
			$('.selectpicker').selectpicker({});

			/* do the pop up magin */
			$('.open_popup').magnificPopup({
			  type:'inline',
			  midClick: true
			});
			
			/* reset input boxes */
			$('##lead_birthyear').val("");
			$('##lead_birthmonth').val("");
			$('##lead_birthday').val("");
			$('##lead_gender').val("");
			$('##lead_name').val("");
			$('##lead_surname').val("");
			$('##lead_interest').val("");
			$('##lead_email').val("");
			$('##lead_phone').val("");


			/* check time of the day and welcome */
			var now = new Date();

			setTimeout(function(){ 
			

				$('##c_birthdate').show();	
				istyping(true);

				if (now.getHours() >= 0 && now.getHours() < 6) {

					$('##c_birthdate').append("<p>#translations[langID]['chat']['good_night']#</p>" );
				
				} else {

					if (now.getHours() >= 6 && now.getHours() < 12) {

						$('##c_birthdate').append("<p>#translations[langID]['chat']['good_day']#</p>");

					} else {

						$('##c_birthdate').append("<p>#translations[langID]['chat']['good_afternoon']#</p>");

					}
				}
				scrolldown();

				setTimeout(function(){ 

					$('##c_birthdate').append("<p>#translations[langID]['chat']['welcome1']#</p>");

					setTimeout(function(){ 

						$('##c_birthdate').append("<p>#translations[langID]['chat']['welcome2']#</p>");

						setTimeout(function(){ 

							$('##c_birthdate').append("<p>#translations[langID]['chat']['welcome3']#</p>");

							setTimeout(function(){ 

								$('##c_birthdate').append("<p>#translations[langID]['chat']['welcome4']#</p>");
								
								setTimeout(function(){ 

									$('##c_birthdate').append("<p>#translations[langID]['chat']['question_birthdate']#</p>");
									$('##birthdatecanvas').show();
									
									istyping(false);

								}, 3000);

							}, 2000);

						}, 2000);

					}, 3000);

				}, 3000);

			}, 2000);



			/* define birthdate and ask gender */
			$('##submit_birthdate').click(function() {


				if ( $('##lead_birthyear').val() =='' || $('##lead_birthmonth').val() =='' || $('##lead_birthday').val() =='' ) {

				istyping(true);
				setTimeout(function(){
					$('##c_birthdate').append("<p>#translations[langID]['chat']['val_birthdate_blanks']#</p>");
					scrolldown();
					istyping(false);
				}, 2000);

				} else {

					var d = $('##lead_birthday').val() ;
					var m = $('##lead_birthmonth').val() ;
					var y = $('##lead_birthyear').val() ;
					var date = new Date(y,m-1,d);

					if (date.getFullYear() == y && date.getMonth() + 1 == m && date.getDate() == d) {

						$('##u_birthdate').show();	
						$('##u_birthdate').append("<p>" + d + "-" + m + "-" + y + "</p>");
						scrolldown();
					 	$.ajax({
							method:'get',
							url:'cfc/content_actions.cfc?method=getSign&day=' + d +'&month=' + m + '&year=' + y,
							success: function(data){
					        	JSON.stringify(data);
					        	//console.log(JSON.parse(data).SIGN_KEY);
					        	//console.log(JSON.parse(data).SIGN);
					        	$('##birthdatecanvas').hide();
					        	istyping(true);
					        	setTimeout(function(){
					        		$('##c_gender').show();
					        		$('##c_gender').append("<p>#translations[langID]['chat']['your_sign']# " + JSON.parse(data).SIGN +"</p>");
					        		scrolldown();
					        			setTimeout(function(){
					        				$('##c_gender').append("<p>#translations[langID]['chat']['question_gender']#</p>");
					        				scrolldown();
					        				$('##gendercanvas').show();
					        				istyping(false);
					        			}, 3000);
					        	}, 2000);
					        	
							}
						});

					 	$.ajax({
							method:'get',
							url:'cfc/tracking_actions.cfc?method=store_birthdate&day=' + d +'&month=' + m + '&year=' + y,
							success: function(data){}
						});



					} else {
					  
					  
						istyping(true);
						setTimeout(function(){
							$('##c_birthdate').append("<p>#translations[langID]['chat']['val_birthdate_invalid']#</p>");
							scrolldown();
							istyping(false);
						}, 3000);
					  	//istyping(true);

					}

				}

			});


			/* define gender and ask name */
			var gender='';
			$('.submit_gender').click(function() {

				$('##u_gender').show();
				if ($(this).attr('id') == 'gender_male') {
					var gender='M';
					$('##u_gender').append("<p>#translations[langID]['chat']['btn_male']#</p>");
					$('##lead_gender').val(gender);
				} else {
					var gender='F';
					$('##u_gender').append("<p>#translations[langID]['chat']['btn_female']#</p>");
					$('##lead_gender').val(gender);
				};
				scrolldown();

				var d = $('##lead_birthday').val() ;
				var m = $('##lead_birthmonth').val() ;
				var y = $('##lead_birthyear').val() ;
				var date = new Date(y,m-1,d);

				if (date.getFullYear() == y && date.getMonth() + 1 == m && date.getDate() == d) {

				 	$.ajax({
						method:'get',
						url:'cfc/content_actions.cfc?method=getSign&day=' + d +'&month=' + m + '&year=' + y,
						success: function(data){
				        	JSON.stringify(data);
				        	$('##gendercanvas').hide();
				        	istyping(true);
				        	setTimeout(function(){
				        		$('##c_name').show(); 
				        		$('##c_name').append("<p>" + JSON.parse(data).SHORT_DESC + "</p>");
					        	scrolldown();
				        		setTimeout(function(){
				        			$('##c_name').append("<p>#translations[langID]['chat']['question_name']#</p>");
				        			scrolldown();
				        			$('##namecanvas').show();
				        			istyping(false);
				        		}, 2000);   		
				        	}, 4000);
				        	
						}
					});

				 	$.ajax({
						method:'get',
						url:'cfc/tracking_actions.cfc?method=store_gender&gender=' + gender,
						success: function(data){}
					});

				}

			});


			/* define name and ask interest */
			$('##submit_name').click(function() {


				if ( $('##lead_name').val() == '' ) {

					
					istyping(true);
					setTimeout(function(){
						$('##c_name').append("<p>#translations[langID]['chat']['val_name_blanks']#</p>");
						scrolldown();
						istyping(false);
					}, 2000);
				  	istyping(true);

				} else {

					if ( $('##lead_surname').val() == '' ) {
					
						istyping(true);
						setTimeout(function(){
							$('##c_name').append("<p>#translations[langID]['chat']['val_surname_blanks']#</p>");
							scrolldown();
							istyping(false);
						}, 2000);
					  	istyping(true);


					} else {

						$('##namecanvas').hide();
						$('##u_name').show();
						$('##u_name').append("<p>" + $('##lead_name').val() + " " + $('##lead_surname').val() );
						scrolldown();
						istyping(true);
						setTimeout(function(){

							$('##c_interest').show();
							var gender= $('##lead_gender').val();
							if (gender == 'M') {
								$('##c_interest').append("<p>" +  $('##lead_name').val() + ", #translations[langID]['chat']['meet_male']#</p>");
							} else {
								$('##c_interest').append("<p>" +  $('##lead_name').val() + ", #translations[langID]['chat']['meet_female']#</p>");
							}
							scrolldown();
							setTimeout(function(){
								$('##c_interest').append("<p>#translations[langID]['chat']['question_interest']#</p>");
								scrolldown();
								$('##interestcanvas').show();
								istyping(false);
							}, 3000); 
						}, 2000);

						$.ajax({
							method:'get',
							url:'cfc/tracking_actions.cfc?method=store_name&name=' + $('##lead_name').val() + '&surname=' + $('##lead_surname').val(),
							success: function(data){}
						});  

					}				

				}

			});



			/* define interest and ask email */
			var interest='';
			$('.submit_interest').click(function() {

				$('##u_interest').show();
				if ($(this).attr('id') == 'love') {
					var interest='love';
					$('##u_interest').append("<p>#translations[langID]['interests']['love']#</p>");
				};
				if ($(this).attr('id') == 'money') {
					var interest='money';
					$('##u_interest').append("<p>#translations[langID]['interests']['money']#</p>");
				};
				if ($(this).attr('id') == 'health') {
					var interest='health';
					$('##u_interest').append("<p>#translations[langID]['interests']['health']#</p>");
				};
				if ($(this).attr('id') == 'family') {
					var interest='family';
					$('##u_interest').append("<p>#translations[langID]['interests']['family']#</p>");
				};
				if ($(this).attr('id') == 'career') {
					var interest='career';
					$('##u_interest').append("<p>#translations[langID]['interests']['career']#</p>");
				};
				if ($(this).attr('id') == 'luck') {
					var interest='luck';
					$('##u_interest').append("<p>#translations[langID]['interests']['luck']#</p>");
				};
				scrolldown();

			 	$.ajax({
					method:'get',
					url:'cfc/content_actions.cfc?method=getForecast&int=' + interest,
					success: function(data){
			        	JSON.stringify(data);
			        	$('##interestcanvas').hide();
		        		scrolldown();
		        		istyping(true);
		        		setTimeout(function(){
		        			$('##c_emaili').show();
		        			$('##c_emaili').append("<p>" + JSON.parse(data).FORECAST + "</p>");
		        			scrolldown();
			        		setTimeout(function(){ 
		        				$('##c_emaili').append("<p>#translations[langID]['chat']['forecast_intro']#</p>");
		        				scrolldown();
				        		setTimeout(function(){	
				        			$('##c_emaili').append("<p>#translations[langID]['chat']['email_optin_intro']#</p>");
				        			scrolldown();
				        			setTimeout(function(){
				        				$('##c_emaili').append("<p>#translations[langID]['chat']['question_email_optinA']#</p>");
				        				scrolldown();
				        				$('##emailintrocanvas').show();
				        				istyping(false);
				        			}, 2000); 
			        			}, 2000); 
			        		}, 4000);
		        		}, 3000);   				
					}
				});

				$.ajax({
					method:'get',
					url:'cfc/tracking_actions.cfc?method=store_interest&interest=' + interest,
					success: function(data){}
				});  

			});


			/* ask email */
			$('.submit_emailintro').click(function() {


				if ($(this).attr('id') == 'email_no') {
					
					istyping(true);
					setTimeout(function(){ 
						var gender= $('##lead_gender').val();
						if (gender == 'M') {

							$('##c_emaili').append("<p>#translations[langID]['chat']['email_optin_fail_male']#</p>");

						
						} else {

							$('##c_emaili').append("<p>#translations[langID]['chat']['email_optin_fail_female']#</p>");

						}
						scrolldown();
	        			setTimeout(function(){
	        				$('##c_emaili').append("<p>#translations[langID]['chat']['question_email_optinB']#</p>");
	        				scrolldown();
	        				istyping(false);
	        			}, 2000); 	
					}, 4000);

				} else {

					$('##u_emaili').show();
					$('##u_emaili').append("<p>#translations[langID]['chat']['yes']#</p>");
					scrolldown();
					istyping(true);
					setTimeout(function(){ 
							$('##c_email').show();
			        		$('##c_email').append("<p>#translations[langID]['chat']['question_email']#</p>");
			        		scrolldown();
			        		$('##emailintrocanvas').hide();
			        		$('##emailcanvas').show();
			        		istyping(false);
			        }, 2000);

			        $.ajax({
						method:'get',
						url:'cfc/tracking_actions.cfc?method=store_optin_email&optin_email=1',
						success: function(data){}
					});  

				}

			});


			/* validate email and ask phone*/
			$('##submit_email').click(function() {

				if ( $('##lead_email').valid() && $('##lead_email').val() !== '' ) {

					istyping(true);
					$('##emailcanvas').hide();
					$('##u_email').show();
					$('##u_email').append( $('##lead_email').val() );
					scrolldown();
					$.ajax({
						method:'get',
						url:'cfc/tracking_actions.cfc?method=store_email&email=' + $('##lead_email').val(),
						success: function(data){
							/* insert lead */
							$.ajax({
								method:'get',
								url:'cfc/tracking_actions.cfc?method=insert_lead',
								success: function(data){
									JSON.stringify(data);
									console.log(JSON.parse(data).LEADID);
									$('##leadID').val(JSON.parse(data).LEADID);
									ga('send', 'pageview', '/successemail');
								}
							});
						}
					});
					  
					setTimeout(function(){ 
						$('##c_phonei').show();
						$('##c_phonei').append("<p>#translations[langID]['chat']['email_conclusion']#</p>");
						scrolldown();
						setTimeout(function(){
							$('##c_phonei').append("<p>#translations[langID]['chat']['email_conclusion2']#</p>");
							scrolldown();
							setTimeout(function(){
								var gender = $('##lead_gender').val();
								if (gender == 'M') {
									$('##c_phonei').append("<p>#translations[langID]['chat']['question_phone_male']#</p>");
								} else {
									$('##c_phonei').append("<p>#translations[langID]['chat']['question_phone_female']#</p>");
								}
								scrolldown();
								istyping(false);
								$('##phoneintrocanvas').show();
							}, 5000);
						}, 4000);
					}, 5000);

					 

				} else {

					istyping(true);
					setTimeout(function(){ 
						$('##c_email').append("<p>#translations[langID]['chat']['val_email']#</p>");
						scrolldown();
				        istyping(false);
				    }, 3000);

				}

			});


			/* ask phone */
			$('.submit_phoneintro').click(function() {

				$('##phoneintrocanvas').hide();

				if ($(this).attr('id') == 'phone_no') {
					$('##u_phonei').show();
					$('##u_phonei').append("<p>#translations[langID]['chat']['no']#</p>");
					scrolldown();
					istyping(true);
					setTimeout(function(){ 
						$('##c_goodbye').show();
						$('##c_goodbye').append("<p>#translations[langID]['chat']['phone_conclusionB']#</p>");
						scrolldown();
						setTimeout(function(){ 
							
							var gender= $('##lead_gender').val();
							if (gender == 'M') {

								$('##c_goodbye').append("<p>#translations[langID]['chat']['goodbye_compliment_male']#</p>");

							} else {

								$('##c_goodbye').append("<p>#translations[langID]['chat']['goodbye_compliment_female']#</p>");

							}
							scrolldown();
		        			setTimeout(function(){
		        				var gender= $('##lead_gender').val();
								if (gender == 'M') {

									$('##c_goodbye').append("<p>#translations[langID]['chat']['goodbye_male']#</p>");

								} else {

									$('##c_goodbye').append("<p>#translations[langID]['chat']['goodbye_female']#</p>");

								}
		        				scrolldown();
		        				istyping(false);
		        			}, 2000); 	
						}, 2000);
					}, 2000);
				} else {

					$('##phonecanvas').show();
					$('##u_phonei').show();
					$('##u_phonei').append("<p>#translations[langID]['chat']['yes']#</p>");
					scrolldown();

					$.ajax({
						method:'get',
						url:'cfc/tracking_actions.cfc?method=store_optin_phone&optin_phone=1',
						success: function(data){}
					});  

				}

			});

			/* validate phone and say goodbye*/
			$('##submit_phone').click(function() {

				if ( $('##lead_phone').valid() && $('##lead_phone').val() !== '' ) {

					istyping(true);
					$('##phonecanvas').hide();
					$('##u_phone').show();
					$('##u_phone').append( $('##lead_phone').val() );
					scrolldown();
					$.ajax({
						method:'get',
						url:'cfc/tracking_actions.cfc?method=store_phone&phone=' + $('##lead_phone').val(),
						success: function(data){
							/* update lead */
							$.ajax({
								method:'get',
								url:'cfc/tracking_actions.cfc?method=update_lead&leadID=' + $('##leadID').val(),
								success: function(data){
									ga('send', 'pageview', '/successphone');
								}
							});
						}
					});
					
					setTimeout(function(){ 
						$('##c_goodbye').show();
						$('##c_goodbye').append("<p>#translations[langID]['chat']['phone_conclusionA']#</p>");
						scrolldown();
						setTimeout(function(){ 
							var gender= $('##lead_gender').val();
							if (gender == 'M') {

								$('##c_goodbye').append("<p>#translations[langID]['chat']['goodbye_compliment_male']#</p>");

							} else {

								$('##c_goodbye').append("<p>#translations[langID]['chat']['goodbye_compliment_female']#</p>");

							}
							scrolldown();
		        			setTimeout(function(){
		        				var gender= $('##lead_gender').val();
								if (gender == 'M') {

									$('##c_goodbye').append("<p>#translations[langID]['chat']['goodbye_male']#</p>");

								} else {

									$('##c_goodbye').append("<p>#translations[langID]['chat']['goodbye_female']#</p>");

								}
		        				scrolldown();
		        				istyping(false);
		        			}, 2000); 	
						}, 2000);
					}, 2000);
				} else {

					istyping(true);
					setTimeout(function(){ 
						$('##c_phone').show();
						$('##c_phone').append("<p>#translations[langID]['chat']['val_phone']#</p>");
						scrolldown();
				        istyping(false);
				    }, 3000);

				}

			});


		</cfoutput>
		});

	</script>

</body>
</html>