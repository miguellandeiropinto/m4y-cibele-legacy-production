<cfprocessingdirective pageencoding="utf-8">
<cfinclude template="../cfc/config.cfc">

<CFSTOREDPROC procedure="dbo.getLeads">
  <CFPROCRESULT name="rsLeads" resultset="1">
  <CFPROCRESULT name="rsCampaigns" resultset="2">
</CFSTOREDPROC>

<!DOCTYPE HTML>
<html>
<head>
<style>
	body { font-family: 'PT Sans', sans-serif!important; font-weight: 400; font-size: 13px; }
	tr {border:1px solid black;}
	td {border:1px solid black;}
</style>
</head>
<body>
	<table>
		<tr>
			<td>leadID</td>
			<td>lead_tstamp</td>
			<td>sessionID</td>
			<td>statusID</td>
			<td>platformID</td>
			<td>sourceID</td>
			<td>mediumID</td>
			<td>campaignID</td>
			<td>lead_country</td>
			<td>lead_ip</td>
			<td>lead_optin</td>
			<td>lead_email</td>
			<td>lead_name</td>
			<td>lead_surname</td>
			<td>lead_gender</td>
			<td>lead_birthdate</td>
			<td>lead_zip</td>
			<td>lead_phone</td>
			<td>lead_interest</td>
			<td>conversion</td>
			<td>lead_cost</td>
			<td>lead_keyword</td>
			<td>integrations</td>
			<td>integrate</td>
		</tr>
		<cfoutput query="rsLeads" group="leadID">
		<tr>
			<td>#leadID#</td>
			<td>#lead_tstamp#</td>
			<td>#sessionID#</td>
			<td>#statusID#</td>
			<td>#platformID#</td>
			<td>#sourceID#</td>
			<td>#mediumID#</td>
			<td>#campaignID#</td>
			<td>#lead_country#</td>
			<td>#lead_ip#</td>
			<td>#lead_optin#</td>
			<td>#lead_email#</td>
			<td>#lead_name#</td>
			<td>#lead_surname#</td>
			<td>#lead_gender#</td>
			<td>#lead_birthdate#</td>
			<td>#lead_zip#</td>
			<td>#lead_phone#</td>
			<td>#lead_interest#</td>
			<td>#conversion#</td>
			<td>#lead_cost#</td>
			<td>#lead_keyword#</td>
			<td><cfoutput>#ccID# > #result#<br><br></cfoutput></td>
			<td>
				<cfloop query="rsCampaigns">
				<a href="../cfc/integration.cfc?method=do_integration&leadID=#rsLeads.leadID#&ccID=#rsCampaigns.campaignID#">#rsCampaigns.campaignID#</a><br><br>
				</cfloop>
			</td>
		</tr>
		</cfoutput>
	</table>


</body>
</html>