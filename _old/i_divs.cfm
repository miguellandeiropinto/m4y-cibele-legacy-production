<cfprocessingdirective pageencoding="utf-8">

<div id="popup_privacy" class="mfp-hide popup">
    <cfoutput>
    #translations[langID]['terms']['privacy']#
    </cfoutput>
</div>


<div id="popup_cookies" class="mfp-hide popup">

    <cfoutput>
    #translations[langID]['terms']['cookies']#
    </cfoutput>
    
</div>


<div id="popup_terms" class="mfp-hide popup">

    <cfoutput>
    #translations[langID]['terms']['terms']#
    </cfoutput>
    
</div>
