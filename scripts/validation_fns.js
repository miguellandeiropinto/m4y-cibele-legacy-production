// V.20170326

$(document).ready(function(){


	// don't allow spaces
	jQuery.validator.addMethod("noSpace", function(value, element) { 
		return value.indexOf(" ") < 0 && value != ""; 
	}, "");

	// proper email
	jQuery.validator.addMethod("properEmail", function(value, element) {
	    return this.optional(element) ||  /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);

	    return isNaN(value) 
	},  "");

	
	// proper time
	jQuery.validator.addMethod("properTime", function(value, element) {
		return this.optional(element) || /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/.test(value);
		
		return isNaN(value) 
	}, "");


	// isodate
	jQuery.validator.addMethod("isoDate", function(value, element) {
        return this.optional(element) || /^((((19|20)(([02468][048])|([13579][26]))-02-29))|((20[0-9][0-9])|(19[0-9][0-9]))-((((0[1-9])|(1[0-2]))-((0[1-9])|(1\d)|(2[0-8])))|((((0[13578])|(1[02]))-31)|(((0[1,3-9])|(1[0-2]))-(29|30)))))$/.test(value);
		
		return isNaN(value) 
    }, "");

	// date greater than
	jQuery.validator.addMethod("dateGreaterThan", function(value, element, params) {
	
		if (!/Invalid|NaN/.test(new Date(value))) {
			return new Date(value) >= new Date($(params).val());
		}
	
		return isNaN(value) && isNaN($(params).val()) 
			|| (Number(value) > Number($(params).val())) || (value=='' && $(params).val()==''); 
	},'');


	// date smaller than
	jQuery.validator.addMethod("dateSmallerThan", function(value, element, params) {
	
		if (!/Invalid|NaN/.test(new Date(value))) {
			return new Date(value) <= new Date($(params).val());
		}
	
		return isNaN(value) && isNaN($(params).val()) 
			|| (Number(value) < Number($(params).val())) || (value=='' && $(params).val()==''); 
	},'');
	
		
	// time greater than
	jQuery.validator.addMethod("timeGreaterThan", function(value, element, params) {
     //   if ( parseInt(value) === 0 && parseInt($(params).val()) === 0 )  {
			
			//console.log('h2: ' + value);
			//console.log('h1: ' + $(params).val());
		if (!/Invalid|NaN/.test(new Date(1900,01,01,parseInt(value.slice(0,2)),parseInt(value.slice(3,5))))) {
			 if ( value == '00:00'  &&  !/Invalid|NaN/.test(new Date(1900,01,01,parseInt($(params).val().slice(0,2)),parseInt($(params).val().slice(3,5)))) ) {
				return true
			 } else {
			 
			 return new Date(1900,01,01,parseInt(value.slice(0,2)),parseInt(value.slice(3,5))) >= new Date(1900,01,01,parseInt($(params).val().slice(0,2)),parseInt($(params).val().slice(3,5)))
			
			 }
		}
	//	}
		
		return ( isNaN(value) && isNaN($(params).val()) ) || (Number(value) > Number($(params).val())) || (value=='' && $(params).val()=='' ); 
	},'');


	// at least 2 words
	var tagCheckRE = new RegExp("(\\w+)(\\s+)(\\w+)");
	jQuery.validator.addMethod("twoWords", function(value, element) { 
	    return tagCheckRE.test(value);
	}, "");

});


