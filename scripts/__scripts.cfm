
	<cfoutput>
	
	/* reset input boxes */
		$('##lead_birthyear').val("");
		$('##lead_birthmonth').val("");
		$('##lead_birthday').val("");
		$('##lead_gender').val("");
		$('##lead_name').val("");
		$('##lead_surname').val("");
		$('##lead_interest').val("");
		$('##lead_email').val("");
		$('##lead_phone').val("");
		$('##lead_zip').val("");

	/* optouts */
		$(".partner").change(function() {
            var ischecked= $(this).is(':checked');
            if (ischecked) {
				var v=1;
				// alert($(this).val() + v);
            }
            if(!ischecked) {
              	var v=0;
			  	// alert($(this).val() + v);
			 }
		 	$.ajax({
				method:'get',
				url:'tracking_actions.cfc?method=store_partners&p=' + $(this).val() +'&v=' + v,
				success: function(data){}
			});	
        });;


	/* check time of the day and welcome */
		var now = new Date();

		setTimeout(function(){ 
		

			$('##c_birthdate').show();	
			istyping(true);

			if (now.getHours() >= 0 && now.getHours() < 6) {

				$('##c_birthdate').append("<p>#translations[langID]['chat']['good_night']#</p>" );
			
			} else {

				if (now.getHours() >= 6 && now.getHours() < 12) {

					$('##c_birthdate').append("<p>#translations[langID]['chat']['good_day']#</p>");

				} else {

					$('##c_birthdate').append("<p>#translations[langID]['chat']['good_afternoon']#</p>");

				}
			}
			scrolldown();

			setTimeout(function(){ 

				$('##c_birthdate').append("<p>#translations[langID]['chat']['welcome1']#</p>");

				setTimeout(function(){ 

					$('##c_birthdate').append("<p>#translations[langID]['chat']['welcome2']#</p>");

					setTimeout(function(){ 

						$('##c_birthdate').append("<p>#translations[langID]['chat']['welcome3']#</p>");

						setTimeout(function(){ 

							$('##c_birthdate').append("<p>#translations[langID]['chat']['welcome4']#</p>");
							
							setTimeout(function(){ 

								$('##c_birthdate').append("<p>#translations[langID]['chat']['question_birthdate']#</p>");
								$('##birthdatecanvas').show();
								
								istyping(false);

							}, 3000);

						}, 2000);

					}, 2000);

				}, 3000);

			}, 3000);

		}, 2000);



	/* define birthdate and ask gender */
		$('##submit_birthdate').click(function() {


			if ( $('##lead_birthyear').val() =='' || $('##lead_birthmonth').val() =='' || $('##lead_birthday').val() =='' ) {

			istyping(true);
			setTimeout(function(){
				$('##c_birthdate').append("<p>#translations[langID]['chat']['val_birthdate_blanks']#</p>");
				scrolldown();
				istyping(false);
			}, 2000);

			} else {

				var d = $('##lead_birthday').val() ;
				var m = $('##lead_birthmonth').val() ;
				var y = $('##lead_birthyear').val() ;
				var sign_key = $('##lead_sign').val();
				var date = new Date(y,m-1,d);

				if (date.getFullYear() == y && date.getMonth() + 1 == m && date.getDate() == d) {

					$('##u_birthdate').show();	
					$('##u_birthdate').append("<p>" + d + "-" + m + "-" + y + "</p>");
					scrolldown();
				 	$.ajax({
						method:'get',
						url:'../cfc/content_actions.cfc?method=getSign&langID=#langID#&day=' + d +'&month=' + m + '&year=' + y,
						success: function(data){
				        	JSON.stringify(data);
				        	//console.log(JSON.parse(data).SIGN_KEY);
				        	//console.log(JSON.parse(data).SIGN);
				        	$('##lead_sign').val(JSON.parse(data).SIGN_KEY);
				        	//console.log(sign_key);
				        	$('##birthdatecanvas').hide();
				        	istyping(true);
				        	setTimeout(function(){
				        		$('##c_gender').show();
				        		$('##c_gender').append("<p>#translations[langID]['chat']['your_sign']# " + JSON.parse(data).SIGN +"</p>");
				        		scrolldown();
				        			setTimeout(function(){
				        				$('##c_gender').append("<p>#translations[langID]['chat']['question_gender']#</p>");
				        				scrolldown();
				        				$('##gendercanvas').show();
				        				istyping(false);
				        			}, 3000);
				        	}, 2000);
				        	
						 	$.ajax({
								method:'get',
								url:'tracking_actions.cfc?method=store_birthdate&day=' + d +'&month=' + m + '&year=' + y + '&sign=' + $('##lead_sign').val(),
								success: function(data){}
							});						

						}
					});





				} else {
				  
				  
					istyping(true);
					setTimeout(function(){
						$('##c_birthdate').append("<p>#translations[langID]['chat']['val_birthdate_invalid']#</p>");
						scrolldown();
						istyping(false);
					}, 3000);
				  	//istyping(true);

				}

			}

		});


	/* define gender and ask zip */
		
		var gender='';
		$('.submit_gender').click(function() {

			$('##u_gender').show();
			if ($(this).attr('id') == 'gender_male') {
				var gender='M';
				$('##u_gender').append("<p>#translations[langID]['chat']['btn_male']#</p>");
				$('##lead_gender').val(gender);
			} else {
				var gender='F';
				$('##u_gender').append("<p>#translations[langID]['chat']['btn_female']#</p>");
				$('##lead_gender').val(gender);
			};
			scrolldown();

			var d = $('##lead_birthday').val() ;
			var m = $('##lead_birthmonth').val() ;
			var y = $('##lead_birthyear').val() ;
			var date = new Date(y,m-1,d);

			if (date.getFullYear() == y && date.getMonth() + 1 == m && date.getDate() == d) {

			 	$.ajax({
					method:'get',
					url:'../cfc/content_actions.cfc?method=getSign&langID=#langID#&day=' + d +'&month=' + m + '&year=' + y,
					success: function(data){
			        	JSON.stringify(data);
			        	$('##gendercanvas').hide();
			        	istyping(true);
			        	setTimeout(function(){
			        		$('##c_zip').show(); 
			        		$('##c_zip').append("<p>" + JSON.parse(data).SHORT_DESC + "</p>");
				        	scrolldown();
			        		setTimeout(function(){
			        			$('##c_zip').append("<p>#translations[langID]['chat']['question_zip1']#</p>");
			        			scrolldown();
			        			setTimeout(function(){
									$('##c_zip').append("<p>#translations[langID]['chat']['question_zip2']#</p>");
									scrolldown();
									$('##zipcanvas').show();
			        			}, 2000);
			        			istyping(false);
			        		}, 4000);   		
			        	}, 4000);
			        	
					}
				});

			 	$.ajax({
					method:'get',
					url:'tracking_actions.cfc?method=store_gender&gender=' + gender,
					success: function(data){}
				});

			}

		});


	/* define zip and ask name */

		$('.submit_zip').click(function() {


			if ( $('##lead_zip').val() == '' ) {

				
				istyping(true);
				setTimeout(function(){
					$('##c_zip').append("<p>#translations[langID]['chat']['val_zip_blanks']#</p>");
					scrolldown();
					istyping(false);
				}, 2000);


			} else {


				if ( $('##lead_zip').valid() ) {

					$('##zipcanvas').hide();
					$('##u_zip').show();
					$('##u_zip').append("<p>" + $('##lead_zip').val() );
					scrolldown();
					istyping(true);
					setTimeout(function(){

						
						scrolldown();
						setTimeout(function(){
							$('##c_name').show();
							$('##c_name').append("<p>#translations[langID]['chat']['question_name']#</p>");
							scrolldown();
							$('##namecanvas').show();
							istyping(false);
						}, 3000); 


					}, 2000);

					$.ajax({
						method:'get',
						url:'tracking_actions.cfc?method=store_zip&zip=' + $('##lead_zip').val(),
						success: function(data){}
					});  


				} else {

					istyping(true);
					setTimeout(function(){
						$('##c_zip').append("<p>#translations[langID]['chat']['val_zip_format']#</p>");
						scrolldown();
						istyping(false);
					}, 2000);

				}	

			}

		});




	/* define name and ask interest */
		$('##submit_name').click(function() {


			if ( $('##lead_name').val() == '' ) {

				
				istyping(true);
				setTimeout(function(){
					$('##c_name').append("<p>#translations[langID]['chat']['val_name_blanks']#</p>");
					scrolldown();
					istyping(false);
				}, 2000);
			  	istyping(true);

			} else {

				if ( $('##lead_surname').val() == '' ) {
				
					istyping(true);
					setTimeout(function(){
						$('##c_name').append("<p>#translations[langID]['chat']['val_surname_blanks']#</p>");
						scrolldown();
						istyping(false);
					}, 2000);
				  	istyping(true);


				} else {

					$('##namecanvas').hide();
					$('##u_name').show();
					$('##u_name').append("<p>" + $('##lead_name').val() + " " + $('##lead_surname').val() );
					scrolldown();
					istyping(true);
					setTimeout(function(){

						$('##c_interest').show();
						var gender= $('##lead_gender').val();
						if (gender == 'M') {
							$('##c_interest').append("<p>" +  $('##lead_name').val() + ", #translations[langID]['chat']['meet_male']#</p>");
						} else {
							$('##c_interest').append("<p>" +  $('##lead_name').val() + ", #translations[langID]['chat']['meet_female']#</p>");
						}
						scrolldown();
						setTimeout(function(){
							$('##c_interest').append("<p>#translations[langID]['chat']['question_interest']#</p>");
							scrolldown();
							$('##interestcanvas').show();
							istyping(false);
						}, 3000); 
					}, 2000);

					$.ajax({
						method:'get',
						url:'tracking_actions.cfc?method=store_name&name=' + $('##lead_name').val() + '&surname=' + $('##lead_surname').val(),
						success: function(data){}
					});  

				}				

			}

		});



		/* define interest and ask email */
		var interest='';
		$('.submit_interest').click(function() {

			$('##u_interest').show();
			if ($(this).attr('id') == 'love') {
				var interest='love';
				$('##u_interest').append("<p>#translations[langID]['interests']['love']#</p>");
			};
			if ($(this).attr('id') == 'money') {
				var interest='money';
				$('##u_interest').append("<p>#translations[langID]['interests']['money']#</p>");
			};
			if ($(this).attr('id') == 'health') {
				var interest='health';
				$('##u_interest').append("<p>#translations[langID]['interests']['health']#</p>");
			};
			if ($(this).attr('id') == 'family') {
				var interest='family';
				$('##u_interest').append("<p>#translations[langID]['interests']['family']#</p>");
			};
			if ($(this).attr('id') == 'career') {
				var interest='career';
				$('##u_interest').append("<p>#translations[langID]['interests']['career']#</p>");
			};
			if ($(this).attr('id') == 'luck') {
				var interest='luck';
				$('##u_interest').append("<p>#translations[langID]['interests']['luck']#</p>");
			};
			scrolldown();

		 	$.ajax({
				method:'get',
				url:'../cfc/content_actions.cfc?method=getForecast&langID=#langID#&int=' + interest,
				success: function(data){
		        	JSON.stringify(data);
		        	$('##interestcanvas').hide();
	        		scrolldown();
	        		istyping(true);
	        		setTimeout(function(){
	        			$('##c_emaili').show();
	        			$('##c_emaili').append("<p>" + JSON.parse(data).FORECAST + "</p>");
	        			scrolldown();
		        		setTimeout(function(){ 
	        				$('##c_emaili').append("<p>#translations[langID]['chat']['forecast_intro']#</p>");
	        				scrolldown();
			        		setTimeout(function(){	
			        			$('##c_emaili').append("<p>#translations[langID]['chat']['email_optin_intro']#</p>");
			        			scrolldown();
			        			setTimeout(function(){
			        				$('##c_emaili').append("<p>#translations[langID]['chat']['question_email_optinA']#</p>");
			        				scrolldown();
			        				$('##emailintrocanvas').show();
			        				istyping(false);
			        			}, 2000); 
		        			}, 2000); 
		        		}, 4000);
	        		}, 3000);   				
				}
			});

			$.ajax({
				method:'get',
				url:'tracking_actions.cfc?method=store_interest&interest=' + interest,
				success: function(data){}
			});  

		});


		/* ask email */
		$('.submit_emailintro').click(function() {


			if ($(this).attr('id') == 'email_no') {
				
				istyping(true);
				setTimeout(function(){ 
					var gender= $('##lead_gender').val();
					if (gender == 'M') {

						$('##c_emaili').append("<p>#translations[langID]['chat']['email_optin_fail_male']#</p>");

					
					} else {

						$('##c_emaili').append("<p>#translations[langID]['chat']['email_optin_fail_female']#</p>");

					}
					scrolldown();
	    			setTimeout(function(){
	    				$('##c_emaili').append("<p>#translations[langID]['chat']['question_email_optinB']#</p>");
	    				scrolldown();
	    				istyping(false);
	    			}, 2000); 	
				}, 4000);

			} else {

				$('##u_emaili').show();
				$('##u_emaili').append("<p>#translations[langID]['chat']['yes']#</p>");
				scrolldown();
				istyping(true);
				setTimeout(function(){ 
						$('##c_email').show();
		        		$('##c_email').append("<p>#translations[langID]['chat']['question_email']#</p>");
		        		scrolldown();
		        		$('##emailintrocanvas').hide();
		        		$('##emailcanvas').show();
		        		istyping(false);
		        }, 2000);

		        $.ajax({
					method:'get',
					url:'tracking_actions.cfc?method=store_optin_email&optin_email=1',
					success: function(data){}
				});  

			}

		});


		/* validate email and ask phone*/
		$('##submit_email').click(function() {

			if ( $('##lead_email').valid() && $('##lead_email').val() !== '' ) {

				istyping(true);
				$('##emailcanvas').hide();
				$('##u_email').show();
				$('##u_email').append( $('##lead_email').val() );
				scrolldown();
				$.ajax({
					method:'get',
					url:'tracking_actions.cfc?method=store_email&email=' + $('##lead_email').val(),
					success: function(data){
						/* insert lead */
						$.ajax({
							method:'get',
							url:'tracking_actions.cfc?method=insert_lead',
							success: function(data){
								JSON.stringify(data);
								// console.log(JSON.parse(data).LEADID);
								$('##leadID').val(JSON.parse(data).LEADID);
								ga('send', 'pageview', '/#langID#-successemail');

								$.ajax({
									method:'get',
									url:'../cfc/integration_actions.cfc?method=doIntegrationEgoi&leadID=' + JSON.parse(data).LEADID,
									success: function(data){
									}
								});

							}
						});
					}
				});
				  
				setTimeout(function(){ 
					$('##c_phonei').show();
					$('##c_phonei').append("<p>#translations[langID]['chat']['email_conclusion']#</p>");
					scrolldown();
					setTimeout(function(){
						$('##c_phonei').append("<p>#translations[langID]['chat']['email_conclusion2']#</p>");
						scrolldown();
						setTimeout(function(){
							var gender = $('##lead_gender').val();
							if (gender == 'M') {
								$('##c_phonei').append("<p>#translations[langID]['chat']['question_phone_male']#</p>");
							} else {
								$('##c_phonei').append("<p>#translations[langID]['chat']['question_phone_female']#</p>");
							}
							scrolldown();
							istyping(false);
							$('##phoneintrocanvas').show();
						}, 5000);
					}, 4000);
				}, 5000);

				 

			} else {

				istyping(true);
				setTimeout(function(){ 
					$('##c_email').append("<p>#translations[langID]['chat']['val_email']#</p>");
					scrolldown();
			        istyping(false);
			    }, 3000);

			}

		});


		/* ask phone */
		$('.submit_phoneintro').click(function() {

			$('##phoneintrocanvas').hide();

			if ($(this).attr('id') == 'phone_no') {
				$('##u_phonei').show();
				$('##u_phonei').append("<p>#translations[langID]['chat']['no']#</p>");
				scrolldown();
				istyping(true);
				setTimeout(function(){ 
					$('##c_goodbye').show();
					$('##c_goodbye').append("<p>#translations[langID]['chat']['phone_conclusionB']#</p>");
					scrolldown();
					setTimeout(function(){ 
						
						var gender= $('##lead_gender').val();
						if (gender == 'M') {

							$('##c_goodbye').append("<p>#translations[langID]['chat']['goodbye_compliment_male']#</p>");

						} else {

							$('##c_goodbye').append("<p>#translations[langID]['chat']['goodbye_compliment_female']#</p>");

						}
						scrolldown();
	        			setTimeout(function(){
	        				var gender= $('##lead_gender').val();
							if (gender == 'M') {

								$('##c_goodbye').append("<p>#translations[langID]['chat']['goodbye_male']#</p>");

							} else {

								$('##c_goodbye').append("<p>#translations[langID]['chat']['goodbye_female']#</p>");

							}
	        				scrolldown();
	        				istyping(false);
	        			}, 2000); 	
					}, 2000);
				}, 2000);
			} else {

				$('##phonecanvas').show();
				$('##u_phonei').show();
				$('##u_phonei').append("<p>#translations[langID]['chat']['yes']#</p>");
				scrolldown();

				$.ajax({
					method:'get',
					url:'tracking_actions.cfc?method=store_optin_phone&optin_phone=1',
					success: function(data){}
				});  

			}

		});

		/* validate phone and say goodbye*/
		$('##submit_phone').click(function() {

			if ( $('##lead_phone').valid() && $('##lead_phone').val() !== '' &&  $('##lead_phone_dd').valid() ) {

				istyping(true);
				$('##phonecanvas').hide();
				$('##u_phone').show();
				$('##u_phone').append( $('##lead_phone_dd').val() + $('##lead_phone').val() );
				scrolldown();
				$.ajax({
					method:'get',
					url:'tracking_actions.cfc?method=store_phone&phone=' + $('##lead_phone_dd').val() + $('##lead_phone').val(),
					success: function(data){
						/* update lead */
						$.ajax({
							method:'get',
							url:'tracking_actions.cfc?method=update_lead&leadID=' + $('##leadID').val(),
							success: function(data){
								ga('send', 'pageview', '/#langID#-successphone');
							}
						});
					}
				});
				
				setTimeout(function(){ 
					$('##c_goodbye').show();
					$('##c_goodbye').append("<p>#translations[langID]['chat']['phone_conclusionA']#</p>");
					scrolldown();
					setTimeout(function(){ 
						var gender= $('##lead_gender').val();
						if (gender == 'M') {

							$('##c_goodbye').append("<p>#translations[langID]['chat']['goodbye_compliment_male']#</p>");

						} else {

							$('##c_goodbye').append("<p>#translations[langID]['chat']['goodbye_compliment_female']#</p>");

						}
						scrolldown();
	        			setTimeout(function(){
	        				var gender= $('##lead_gender').val();
							if (gender == 'M') {

								$('##c_goodbye').append("<p>#translations[langID]['chat']['goodbye_male']#</p>");

							} else {

								$('##c_goodbye').append("<p>#translations[langID]['chat']['goodbye_female']#</p>");

							}
	        				scrolldown();
	        				istyping(false);
	        			}, 2000); 	
					}, 2000);
				}, 2000);
			} else {

				istyping(true);
				setTimeout(function(){ 
					$('##c_phone').show();
					$('##c_phone').append("<p>#translations[langID]['chat']['val_phone']#</p>");
					scrolldown();
			        istyping(false);
			    }, 3000);

			}

		});

	</cfoutput>

