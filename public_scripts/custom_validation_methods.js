	
	// time
	jQuery.validator.addMethod("thetime", function(value, element) {
		return this.optional(element) || /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/.test(value);
		
		return isNaN(value) 
	}, "Insira uma hora no formato HH:MM");


	// date
	jQuery.validator.addMethod("dateFormat", function(value, element) {
        return this.optional(element) || /^((((19|20)(([02468][048])|([13579][26]))-02-29))|((20[0-9][0-9])|(19[0-9][0-9]))-((((0[1-9])|(1[0-2]))-((0[1-9])|(1\d)|(2[0-8])))|((((0[13578])|(1[02]))-31)|(((0[1,3-9])|(1[0-2]))-(29|30)))))$/.test(value);
		
		return isNaN(value) 
    }, "Insira a data no formato AAAA-MM-DD");

	// date greater than
	jQuery.validator.addMethod("greaterThan", function(value, element, params) {
	
		if (!/Invalid|NaN/.test(new Date(value))) {
			return new Date(value) >= new Date($(params).val());
		}
	
		return isNaN(value) && isNaN($(params).val()) 
			|| (Number(value) > Number($(params).val())) || (value=='' && $(params).val()==''); 
	},'Uma das datas de fim é maior do que uma das datas de início');
	

	// date greater than
	jQuery.validator.addMethod("greaterThanNow", function(value, element, params) {
	
		if (!/Invalid|NaN/.test(new Date(value))) {
			return new Date(value) >= new Date($(params).val());
		}
	
		return isNaN(value) && isNaN($(params).val()) 
			|| (Number(value) > Number($(params).val())) || (value==''); 
	},'Uma das datas de fim é maior do que uma das datas de início');
	
		
	
	// time greater than
	jQuery.validator.addMethod("TimegreaterThan", function(value, element, params) {
     //   if ( parseInt(value) === 0 && parseInt($(params).val()) === 0 )  {
			
			
		if (!/Invalid|NaN/.test(new Date(1900,01,01,parseInt(value.slice(0,2)),parseInt(value.slice(3,5))))) {
			 return new Date(1900,01,01,parseInt(value.slice(0,2)),parseInt(value.slice(3,5))) >= new Date(1900,01,01,parseInt($(params).val().slice(0,2)),parseInt($(params).val().slice(3,5)))
		}
	//	}
		//alert(value); alert($(params).val());
		return ( isNaN(value) && isNaN($(params).val()) ) || (Number(value) > Number($(params).val())) || (value=='' && $(params).val()==''); 
	},'A hora de fim deve ser superior à hora de início');