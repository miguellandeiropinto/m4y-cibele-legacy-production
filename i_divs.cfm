<cfprocessingdirective pageencoding="utf-8">

<div id="popup_privacy" class="mfp-hide popup">
    <cfoutput>
    #translations[langID]['terms']['privacy']#
    </cfoutput>
</div>


<div id="popup_cookies" class="mfp-hide popup">

    <cfoutput>
    #translations[langID]['terms']['cookies']#
    </cfoutput>
    
</div>


<div id="popup_terms" class="mfp-hide popup">

    <cfoutput>
    #translations[langID]['terms']['terms']#
    </cfoutput>
    
</div>


<div id="popup_partners" class="mfp-hide popup">

    <cfoutput>
    #translations[langID]['partners']['text_intro']#<br>
    </cfoutput>
    <cflock timeout=20 scope="Session" type="Exclusive"> 
    <cfform name="formpartners">
		<cfoutput query="rsPartners">
			<div class="row">
				<div class="row-height">
                    <div class="col-xs-4 col-height">
                        <img src="../media/partners/#partner_logo#"><br>
                        <a href="#partner_privacy#" target="_blank">#translations[langID]['partners']['text_privacy']#</a>
                    </div>
                    <div class="col-xs-8 col-height col-middle">
                        <cfif #StructKeyExists(session.partners, partnerID)#>
                            <cfif session.partners[partnerID] eq 1>
                                <cfinput type="checkbox" name="partner" class="partner" id="partner#partnerID#" value="#partnerID#" checked>&nbsp;#translations[langID]['partners']['accept']#
                            <cfelse>
                                <cfinput type="checkbox" name="partner" class="partner" id="partner#partnerID#" value="#partnerID#">&nbsp;#translations[langID]['partners']['accept']#
                            </cfif>
                        <cfelse>
                            <cfinput type="checkbox" name="partner" class="partner" id="partner#partnerID#" value="#partnerID#">&nbsp;#translations[langID]['partners']['accept']#
                        </cfif>

                    </div>
				</div>
			</div>
			<hr>
		</cfoutput>		    
    </cfform>
	</cflock>
</div>


<div id="popup_coregs" class="mfp-hide popup">
<cfoutput>
        <script type="application/json" id="coregsJson">#serializeJSON(rsCoregs)#</script>
</cfoutput>
    <cflock timeout=20 scope="Session" type="Exclusive">
                        <cfform name="formpartners_coregs">
                    		<cfoutput query="rsCoregs">
                    			<div class="row">
                    				<div class="row-height">
                    				<div class="col-xs-4 col-height">
                    				    <!--- passar imagem para pasta --->
                    					<img src="../media/partners/#partner_logo#"><br>
                    				</div>
                    				<div class="col-xs-8 col-height col-middle">
                                <!---
                    					<cfif StructKeyExists(session.partners, partnerID)>--->
                    					    <p>#partner_txt#<p>
                    						<cfinput type="checkbox" name="partner" class="coreg" id="partner#partnerID#" value="#partnerID#">&nbsp;#translations[langID]['partners']['accept']#</br>
                    						<cfif isNull(partner_txt)>

                                            <cfelse>
                                                <a href="#partner_privacy#" target="_blank">#translations[langID]['partners']['text_privacy']#</a>
                                            </cfif>
<!---
                    					<cfelse>
                    						<cfinput type="checkbox" name="partner" class="coreg" id="partner#partnerID#" value="#partnerID#">&nbsp;#translations[langID]['partners']['accept']#

                                        </cfif>
                                        --->

                    				</div>
                    				</div>
                    			</div>
                    			<hr>
                    		</cfoutput>
                        </cfform>
                    </cflock>
</div>

