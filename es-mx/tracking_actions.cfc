<cfcomponent>
<cfprocessingdirective pageencoding="utf-8">


	<cffunction name="store_birthdate" access="remote">
		<cflock timeout=20 scope="Session" type="Exclusive"> 
		<cfset session.lead_birthdate=DateFormat( CreateDate(URL.year, URL.month, URL.day), "YYYY-MM-DD" )>
		<cfset session.lead_sign=URL.sign>
		</cflock>
	</cffunction>

	<cffunction name="store_gender" access="remote">
		<cflock timeout=20 scope="Session" type="Exclusive"> 
		<cfset session.lead_gender=URL.gender>
		</cflock>
	</cffunction>

	<cffunction name="store_zip" access="remote">
		<cflock timeout=20 scope="Session" type="Exclusive"> 
		<cfset session.lead_zip=URL.zip>
		</cflock>
	</cffunction>

	<cffunction name="store_name" access="remote">
		<cflock timeout=20 scope="Session" type="Exclusive"> 
		<cfset session.lead_name=URL.name>
		<cfset session.lead_surname=URL.surname>
		</cflock>
	</cffunction>

	<cffunction name="store_interest" access="remote">
		<cflock timeout=20 scope="Session" type="Exclusive"> 
		<cfset session.lead_interest=URL.interest>
		</cflock>
	</cffunction>

	<cffunction name="store_optin_email" access="remote">
		<cflock timeout=20 scope="Session" type="Exclusive"> 
		<cfset session.lead_optin_email=URL.optin_email>
		</cflock>
	</cffunction>

	<cffunction name="store_email" access="remote">
		<cflock timeout=20 scope="Session" type="Exclusive"> 
		<cfset session.lead_email=URL.email>
		</cflock>
	</cffunction>

	<cffunction name="store_optin_phone" access="remote">
		<cflock timeout=20 scope="Session" type="Exclusive"> 
		<cfset session.lead_optin_phone=URL.optin_phone>
		</cflock>
	</cffunction>

	<cffunction name="store_phone" access="remote">
		<cflock timeout=20 scope="Session" type="Exclusive"> 
		<cfset session.lead_phone=URL.phone>
		</cflock>
	</cffunction>

	<cffunction name="store_partners" access="remote">
		<cflock timeout=20 scope="Session" type="Exclusive"> 
		<cfif URL.v eq 1>
			<cfset StructUpdate(session.partners,  URL.p, 1)>
		<cfelseif URL.v eq 0>
			<cfset StructUpdate(session.partners, URL.p, 0)>
		</cfif>
		</cflock>
	</cffunction>

	<cffunction name="checkall" access="remote">
		<cfdump var="#session#">	
	</cffunction>

	<cffunction name="store_coregs" access="remote">

		<cfparam name="URL.p" default="">
		<cfparam name="URL.v" default="">

		<CFSTOREDPROC procedure="dbo.update_optins">
			<CFPROCPARAM type="IN" value="#URL.p#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.lead_email#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#URL.v#" cfsqltype="CF_SQL_INTEGER">
		</CFSTOREDPROC>

	</cffunction>




	<cffunction name="insert_lead" access="remote" returnformat="json">

		<cflock timeout=20 scope="Session" type="Exclusive"> 

		<CFSTOREDPROC procedure="dbo.insert_lead">
			<CFPROCPARAM type="IN" value="#session.urltoken#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.platformID#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.sourceID#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.mediumID#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.campaignID#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.lead_country#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.lead_ip#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.lead_optin_email#" cfsqltype="CF_SQL_INTEGER">
			<CFPROCPARAM type="IN" value="#session.lead_email#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.lead_name#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.lead_surname#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.lead_gender#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.lead_birthdate#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.lead_sign#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.lead_phone#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.lead_zip#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.lead_interest#" cfsqltype="CF_SQL_VARCHAR">	
			<CFPROCPARAM type="OUT" variable="leadID" cfsqltype="CF_SQL_VARCHAR">
		</CFSTOREDPROC>

		<cfloop collection="#session.partners#" item="partnerID">
			<CFSTOREDPROC procedure="dbo.update_optins">
				<CFPROCPARAM type="IN" value="#partnerID#" cfsqltype="CF_SQL_VARCHAR">
				<CFPROCPARAM type="IN" value="#session.lead_email#" cfsqltype="CF_SQL_VARCHAR">
				<CFPROCPARAM type="IN" value="#session.partners[partnerID]#" cfsqltype="CF_SQL_INTEGER">
			</CFSTOREDPROC>
		</cfloop>

		</cflock>

    	<cfset result.leadID=leadID>

    	<cfreturn result>

	</cffunction>


	<cffunction name="update_lead" access="remote">

		<cflock timeout=20 scope="Session" type="Exclusive"> 

		<CFSTOREDPROC procedure="dbo.update_lead">
			<CFPROCPARAM type="IN" value="#URL.leadID#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.urltoken#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.platformID#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.sourceID#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.mediumID#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.campaignID#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.lead_country#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.lead_ip#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.lead_optin_email#" cfsqltype="CF_SQL_INTEGER">
			<CFPROCPARAM type="IN" value="#session.lead_email#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.lead_name#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.lead_surname#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.lead_gender#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.lead_birthdate#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.lead_sign#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.lead_phone#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.lead_zip#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#session.lead_interest#" cfsqltype="CF_SQL_VARCHAR">	
		</CFSTOREDPROC>

		</cflock>

	</cffunction>


</cfcomponent>