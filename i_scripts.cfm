<!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->

<script type="text/javascript" src="../public_scripts/jquery-2.1.3.min.js"></script>
<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../public_scripts/bootstrap-select.min.js"></script>
<script type="text/javascript" src="../scripts/validation_fns.js"></script>
<script type="text/javascript" src="../public_scripts/jquery.validate.min.js"></script>
<script type="text/javascript" src="../public_scripts/additional-methods.min.js"></script>
<script type="text/javascript" src="../public_scripts/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="../public_scripts/cookieconsent.min.js"></script>
<script type="text/javascript" src="../scripts/functions.js"></script>


<cfoutput>
<script type="text/javascript">


    window.cookieconsent_options = {"message":"#translations[langID]['cookies']['text']#","dismiss":"#translations[langID]['cookies']['dismiss']#","learnMore":"#translations[langID]['cookies']['info']#","link":"","theme":"light-floating"};
</script>
</cfoutput>
<noscript>
	
	<div class="noscript">
        <cfoutput>
        <div class="noscript_msg">#translations[langID]['platform']['noscript']#</div>
        </cfoutput>
    </div>

</noscript>