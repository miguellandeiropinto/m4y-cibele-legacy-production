<cfcomponent displayname="Application" output="true" hint="Handle the application.">

	<!--- Set up the application. --->
	<cfset THIS.Name = "CIBELLE.WAPP" />
	<cfset THIS.ApplicationTimeout = CreateTimeSpan( 0, 0, 45, 0 ) />
	<cfset THIS.SessionManagement = true />
	<cfset THIS.SetClientCookies = true />
	<cfset THIS.SessionTimeout= CreateTimeSpan( 0, 0, 30, 0 ) /> 

	<cfset THIS.serialization.serializequeryas='struct'>
	<cfset THIS.serialization.preserveCaseForQueryColumn=true>

<!---
	<cfset THIS.clientManagement="False"> 
	<cfset THIS.loginStorage="Cookie"> 
	
	<cfset THIS.setDomainCookies="False"> 
--->


	
	<!--- Define the page request properties. --->
	<cfsetting requestTimeout="20" showDebugOutput="false" enableCFOutputOnly="false" />

	
	<cfset THIS.datasource="leaddam_wapp">


	<cffunction name="OnApplicationStart" access="public" returntype="boolean" output="false" hint="Fires when the application is first created.">
		
		<cflog file="#This.Name#" type="Information" text="Application Started">
		
		
		<cfreturn true />
	</cffunction>


	<cffunction name="OnSessionStart" access="public" returntype="void" output="false" hint="Fires when the session is first created.">


		<cfreturn />
	</cffunction>


	<cffunction name="OnRequestStart" access="public" returntype="boolean" output="false" hint="Fires at first part of page processing.">

		<cfargument name="TargetPage" type="string" required="true" />

		<cfreturn true />
	</cffunction>


	<cffunction name="OnRequest" access="public" returntype="void" output="true" hint="Fires after pre page processing is complete.">

		<cfargument name="TargetPage" type="string" required="true" />

		<!--- Include the requested page. --->
		<cfinclude template="#ARGUMENTS.TargetPage#" />

		
		<cfreturn />
	</cffunction>


	<cffunction name="OnRequestEnd" access="public" returntype="void" output="true" hint="Fires after the page processing is complete.">


		<cfreturn />
	</cffunction>


	<cffunction name="OnSessionEnd" access="public" returntype="void" output="false" hint="Fires when the session is terminated.">

		<!--- Define arguments. --->
		<cfargument name="SessionScope" type="struct" required="true"/>
		<cfargument name="ApplicationScope" type="struct" required="false" default="#StructNew()#" />

		<cfreturn />
	</cffunction>


	<cffunction name="OnApplicationEnd" access="public" returntype="void" output="false" hint="Fires when the application is terminated.">

		<!--- Define arguments. --->
		<cfargument name="ApplicationScope" type="struct" required="false" default="#StructNew()#" />

		<cfreturn />
	</cffunction>


	<cffunction name="OnError" access="public" returntype="void" output="true" hint="Fires when an exception occures that is not caught by a try/catch.">

		<!--- Define arguments. --->
		<cfargument name="Exception" type="any" required="true" />
		<cfargument name="EventName" type="string" required="false" default=""/>

		<cflog file="#This.Name#" type="error" text="Event Name: #Arguments.Eventname#" > 
		<cflog file="#This.Name#" type="error" text="Message: #Arguments.Exception.message#"> 
		<cfif isdefined("Arguments.Exception.rootcause")> 
        <cflog file="#This.Name#" type="error"  text="Root Cause Message: #Arguments.Exception.rootcause.message#"> 
    	</cfif>
		<!--- Display an error message if there is a page context. ---> 
		<cfif NOT (Arguments.EventName IS "onSessionEnd") OR  (Arguments.EventName IS "onApplicationEnd")> 
	        <cfoutput>
	        
	        <!---<cflocation url="../error.cfm?token=#key#"> <cfinclude template="cfc/queries.cfc">--->
	        </cfoutput>
	        <cfoutput> 
	            <h2>An unexpected error occurred.</h2> 
	            <p>Please provide the following information to technical support:"</p> 
	            <p>Error Event: #Arguments.EventName#</p> 
	            <p>Error details:<br> 
	            <cfdump var=#Arguments.Exception#></p> 
	        </cfoutput> 
    	</cfif>

		<cfreturn />
	</cffunction>

</cfcomponent>