<cfprocessingdirective pageencoding="utf-8">


<CFSTOREDPROC procedure="dbo.getLeads">
  <CFPROCRESULT name="rsLeads" resultset="1">
  <CFPROCRESULT name="rsCampaigns" resultset="2">
</CFSTOREDPROC>


<cfset myBooks='<?xml version="1.0" encoding="UTF-8"?> <Egoi_Api generator="zend" version="1.0"><checklogin><RESULT>OK</RESULT><user_id>190282</user_id><cliente_id>167769</cliente_id><username>mccm4y@m4yweb.com</username><email>mccm4y@m4yweb.com</email><nome>Mystify</nome><ultimo_nome></ultimo_nome><idioma>pt</idioma><telefone>+351-</telefone><telemovel>+351-938957411</telemovel><fax>+351-</fax><status>1</status><apikey>3ec783f68e3d17031051201e3a844606575a7a26</apikey></checklogin></Egoi_Api>'>

<cfset objXml = createObject("component","xml2Struct")>

<!--- append the returned structure to some existing structure--->
<cfset str = structnew()>
<cfset str.existingElement = "some text here">
<cfset str = objXml.ConvertXmlToStruct(myBooks, str)>


<!--- create a new structure --->
<cfset newStr = objXml.ConvertXmlToStruct(myBooks, StructNew())>
<cfdump var = "#newStr#">

<!DOCTYPE HTML>
<html>
<head>
<style>
	body { font-family: 'PT Sans', sans-serif!important; font-weight: 400; font-size: 13px; }
	tr {border:1px solid black;}
	td {border:1px solid black;}
</style>
</head>
<body>
	<table>
		<tr>
			<td>Session INFO</td>
			<td>Campaign INFO</td>
			<td>LEAD INFO</td>
			<td>Status</td>
		</tr>
		<cfoutput query="rsLeads" group="leadID">
		<tr>
			<td>
				#lead_tstamp#<br>
				---<br>
				LeadID: #leadID#<br>
				---<br>
				Session: #sessionID#<br>
				Platform: #platformID#<br>
			</td>
			<td>
				#sourceID#<br>
				#mediumID#<br>
				#campaignID#<br>
			</td>
			<td>
				#lead_name# #lead_surname#<br>
				--<br>
				#lead_gender#<br>
				#lead_birthdate#<br>
				--<br>
				#lead_interest#<br>
				---<br>
				#lead_ip# - #lead_country#<br>
				---<br>
				#lead_email#<br>
				#lead_phone#<br>
				#lead_zip#<br>
			</td>
			<td>
				<cfoutput>
					#ccID# > #result#<br><br>
				</cfoutput>
			</td>
			<!---
			<td>
				<cfloop query="rsCampaigns">
				<a href="../cfc/integration.cfc?method=do_integration&leadID=#rsLeads.leadID#&ccID=#rsCampaigns.campaignID#">#rsCampaigns.campaignID#</a><br><br>
				</cfloop>
			</td>
			--->
		</tr>
		</cfoutput>
	</table>


</body>
</html>