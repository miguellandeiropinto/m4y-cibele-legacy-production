<cfprocessingdirective pageencoding="utf-8">

		<cfswitch expression="#URL.page#">
		<cfcase value="list">
			
			<cfoutput>
			<div class="container navigation">
			
				<div class="row">
					<div class="col-sm-6">
						<span class="h6">LEADS <span class="fa fa-chevron-right"></span> 
						<cfif statusID neq ''>
						STATUS <span class="fa fa-chevron-right"></span> #URL.statusID#
						<cfelseif ccampaignID neq ''>
						CAMPAIGN <span class="fa fa-chevron-right"></span> #ucase(URL.ccampaignID)#
						<cfelseif country neq ''> 
						COUNTRY	<span class="fa fa-chevron-right"></span> #URL.country#
						<cfelse>
						ALL
						</cfif>
					</div>
					<div class="col-sm-6">
						<cfform action="home.cfm" method="post">
							<cfinput type="datefield" name="date_ini" value="#Dateformat(date_ini,'YYYY-MM-DD')#" mask="YYYY-MM-DD">
							<cfinput type="datefield" name="date_end" value="#Dateformat(date_end,'YYYY-MM-DD')#" mask="YYYY-MM-DD">
							<cfinput type="submit" name="filter" value="Submit">
						</cfform>
					</div>
				</div>
			
			</div>
			</cfoutput>

		</cfcase>
		<cfcase value="edit">

			<cfoutput>
			<div class="container navigation">
			
				<div class="row">
					<div class="col-sm-12">
						<span class="h6">LEAD <span class="fa fa-chevron-right"></span> EDIT
						<h3><a class="stickyarrow" href="home.cfm?#CGI.QUERY_STRING#"><span class="fa fa-arrow-circle-o-left"></span></a> #rsLead.leadID#</h3>
					</div>
				</div>
			
			</div>
			</cfoutput>

		</cfcase>
		<cfcase value="new">


			<cfoutput>
			<div class="container navigation">
			
				<div class="row">
					<div class="col-sm-12">
						<span class="h6">LEAD <span class="fa fa-chevron-right"></span> NOVA
					</div>
				</div>
			
			</div>
			</cfoutput>
			
		</cfcase>
		<cfcase value="stats">


			<cfoutput>
			<div class="container navigation">
			
				<div class="row">
					<div class="col-sm-12">
						<span class="h6">STATISTICS <!---<span class="fa fa-chevron-right"></span> NOVO--->
					</div>
				</div>
			
			</div>
			</cfoutput>
			
		</cfcase>
		</cfswitch>


