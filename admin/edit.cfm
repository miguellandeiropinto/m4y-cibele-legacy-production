<cfprocessingdirective pageencoding="utf-8">
<cfcontent type="text/html; charset=utf-8">
<cfset setEncoding("URL", "utf-8")>
<cfset setEncoding("Form", "utf-8")>


<cfparam name="URL.page" default="edit">
<cfparam name="URL.filter" default="all">
<cfparam name="URL.participantID" default="-999">
<cfinclude template="cfc/queries.cfc">


<!DOCTYPE HTML>
<html>
<head>
<!-- META -->
<cfinclude template="i_meta.cfm">
<!-- CSS -->
<cfinclude template="i_css.cfm">
<!-- IE8 -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!---<cfinclude template="i_analytics.cfm">--->
</head>
<body class="bggreylight">
<cfinclude template="i_header.cfm">

<div class="container-fluid">
	

	<section id="contents">

	<!--- HEADER --->
	<cfinclude template="home_header.cfm">
	<!--- HEADER --->

		<div class="container bgwhite padding-tb margin-tb">

			<div class="row">
				<div class="col-sm-12">

					<div class="form-header-cont">

					<!--- TABS --->
					<cfinclude template="home_header_tabs.cfm">
					<!--- TABS --->

					</div>


					<div class="row">
						
						<div class="col-sm-6">
							
							<h3>LEAD DATA</h3>

							<cfset tabindex=1>
							<cfform id="mform" name="mform" action="cfc/main.cfc?method=bo_update_lead" method="post">
	                    	<cfinput type="hidden" name="leadID" value="#rsLead.leadID#">
	                    	<cfinput type="hidden" name="qs" value="#CGI.QUERY_STRING#">
	                        <!--- <cfinput type="hidden" name="usr" value="#rsUser.usr#"> --->
	                        <hr>
	                        <cfoutput>
							<h5><strong>#DateFormat(rsLead.lead_tstamp,"YYYY-MM-DD")# #TimeFormat(rsLead.lead_tstamp,"HH:MM")#</strong></h5>
							<h5>#rsLead.platformID# | #rsLead.campaignID# | #rsLead.sourceID# | #rsLead.mediumID#</h5>
							<h5>#rsLead.lead_ip#</h5>
							<h5>wish: #rsLead.lead_interest#</h5>
							<h5>sign: #rsLead.lead_sign#</h5>
							<h5><strong>EGOI-UID: #rsLead.egoiID#</strong></h5>
							</cfoutput>
	                        <hr>
	                        <div class="row padding-tb-form">
								<div class="col-sm-6">
								<label class="label-control" for="lead_gender">country</label>
	                            <cfselect name="lead_country" class="form-control" tabindex="#tabindex#" query="rsCountries" display="lead_country" value="lead_country" selected="#rsLead.lead_country#">
	                            </cfselect>
	                            <cfset tabindex=tabindex+1>
								</div>
								<div class="col-sm-6">
								<label class="label-control" for="statusID">status</label>
	                            <cfselect name="statusID" class="form-control" tabindex="#tabindex#" query="rsStatus" display="lead_status" value="statusID" selected="#rsLead.statusID#">
	                            </cfselect>
	                            <cfset tabindex=tabindex+1>
								</div>
							</div>
	                        <div class="row padding-tb-form">
								<div class="col-sm-6">
								<label class="label-control" for="lead_name">name</label>
	                            <cfinput type="text" name="lead_name" value="#rsLead.lead_name#" class="form-control" validate="noblanks" validateat="onsubmit" required="yes" message="insert name" maxlength="255" tabindex="#tabindex#">
	                            <cfset tabindex=tabindex+1>
								</div>
								<div class="col-sm-6">
								<label class="label-control" for="lead_surname">surname</label>
	                            <cfinput type="text" name="lead_surname" value="#rsLead.lead_surname#" class="form-control " validate="noblanks" validateat="onsubmit" required="yes" message="insert surname" maxlength="255" tabindex="#tabindex#">
	                            <cfset tabindex=tabindex+1>
								</div>
							</div>
	                        <div class="row padding-tb-form">
								<div class="col-sm-4">
								<label class="label-control" for="lead_gender">gender</label>
	                            <cfselect name="lead_gender" class="form-control" tabindex="#tabindex#">
									<option value="M" <cfif rsLead.lead_gender eq 'M'>selected</cfif>>M</option>
									<option value="F" <cfif rsLead.lead_gender eq 'F'>selected</cfif>>F</option>
	                            </cfselect>
	                            <cfset tabindex=tabindex+1>
								</div>
								<div class="col-sm-4">
								<label class="label-control" for="lead_birthdate">birthdate</label>
	                            <cfinput type="datefield" name="lead_birthdate" value="#rsLead.lead_birthdate#" class="form-control" validate="noblanks" validateat="onsubmit" required="yes" message="insert birthdate" maxlength="10" tabindex="#tabindex#" mask="YYYY-MM-DD">
	                            <cfset tabindex=tabindex+1>
								</div>
								<div class="col-sm-4">
								<label class="label-control" for="lead_zip">zip</label>
	                            <cfinput type="text" name="lead_zip" value="#rsLead.lead_zip#" class="form-control" validate="noblanks" validateat="onsubmit" required="no" message="insert zip" maxlength="10" tabindex="#tabindex#">
	                            <cfset tabindex=tabindex+1>
								</div>
							</div>
							<div class="row padding-tb-form">
								<div class="col-sm-8">
								<label class="label-control" for="lead_email">email</label>
	                            <cfinput type="text" name="lead_email" value="#rsLead.lead_email#" class="form-control" validate="email" validateat="onsubmit" required="yes" message="insert email" maxlength="255" tabindex="#tabindex#">
	                            <cfset tabindex=tabindex+1>
								</div>
								<div class="col-sm-4">
								<label class="label-control" for="lead_phone">phone</label>
	                            <cfinput type="text" name="lead_phone" value="#rsLead.lead_phone#" class="form-control" validate="noblanks" validateat="onsubmit" required="no" message="insert phone" maxlength="255" tabindex="#tabindex#">
	                            <cfset tabindex=tabindex+1>
								</div>
							</div>   
							
							<div class="row padding-tb-form">
								<div class="col-sm-12">
								<cfinput type="submit" name="Submit" id="Submit" class="btn btn-default margin" value="Save">

	                            <cfoutput>
								<button href="##popup_dialogbox_delete" role="button" class="btn btn-default btn_popup_dialogbox margin" data-var1="cfc/main.cfc?method=bo_delete_lead" data-var2="Tem a certeza que deseja eliminar esta lead?" data-var3="#rsLead.leadID#">Delete</button>		
								</cfoutput>

								</div>
							</div>	
	                        </cfform>

						</div>

						<div class="col-sm-6">
							<cfoutput>
								<a href="../cfc/integration_actions.cfc?method=doIntegrationClients&leadID=#rsLead.leadID#&show=yes" role="button" class="btn btn-default margin" data-var1="">Integrate with Clients</a>
								<a href="../cfc/integration_actions.cfc?method=doIntegrationEgoi&leadID=#rsLead.leadID#&show=yes" role="button" class="btn btn-default margin" data-var1="">Integrate with E-goi</a>
							</cfoutput>
							<hr>
							<h4>INTEGRATIONS</h4>
							<table id="table_int" class="table">
							<thead>
								<tr>
									<th>CAMPAIGN</th>
									<th>TS</th>
									<th>STATUS</th>
								</tr>
							</thead>
							<tbody>

								<cfoutput query="rsIntegrations">
								<tr>
									<td>#campaignID#</td>
									<td>#DateFormat(tstamp,"YYYY-MM-DD")# #TimeFormat(tstamp,"HH:MM")#<br></td>
									<td>#lead_integration_status#</td>
								</tr>
								</cfoutput>

							</tbody>
							</table>
	                        <hr>
	                        <h4>ATTEMPTS</h4>
							<table id="table_int" class="table">
							<thead>
								<tr>
									<th>CAMPAIGN</th>
									<th>TS</th>
									<th>STATUS</th>
									<th>RESULT</th>
								</tr>
							</thead>
							<tbody>

								<cfoutput query="rsAttempts">
								<tr>
									<td>#campaignID#</td>
									<td>#DateFormat(tstamp,"YYYY-MM-DD")# #TimeFormat(tstamp,"HH:MM")#<br></td>
									<td>#statusID#</td>
									<td>#result#</td>
								</tr>
								</cfoutput>

							</tbody>
							</table>
	                        <!---
	                        <hr>
								<cfoutput>
								<a href="cfc/integration.cfc?method=do_integration&leadID=#rsLead.leadID#&ccampaignID=tara-coreg-pt-0001" role="button" class="btn btn-default margin" data-var1="">Integrate w/ Tara PT</a>
								<a href="cfc/integration.cfc?method=do_integration&leadID=#rsLead.leadID#&ccampaignID=tara-coreg-br-0002" role="button" class="btn btn-default margin" data-var1="">Integrate w/ Tara BR</a>
								<hr>
								<a href="cfc/integration.cfc?method=do_integration&leadID=#rsLead.leadID#&ccampaignID=maria-coreg-pt-0003" role="button" class="btn btn-default margin" data-var1="">Integrate w/ Maria PT</a>
								<a href="cfc/integration.cfc?method=do_integration&leadID=#rsLead.leadID#&ccampaignID=maria-coreg-br-0004" role="button" class="btn btn-default margin" data-var1="">Integrate w/ Maria BR</a>
								<hr>
								<a href="cfc/integration.cfc?method=do_integration&leadID=#rsLead.leadID#&ccampaignID=egoi-0000" role="button" class="btn btn-default margin" data-var1="">Integrate w/ E-goi</a>
								</cfoutput>
							--->
						</div>
						
					</div>
							
				</div>
			
			</div>

		</div>
	

	</section>

</div>


<cfinclude template="i_footer.cfm">

	
<cfinclude template="i_divs.cfm">

    <!-- JS -->
    <cfinclude template="i_scripts.cfm">
	
	<script>

	jQuery(document).ready(function(){
	
		// TABLES

	    $('#table_clients').DataTable({
	    	"language": {
                "url": "../public_scripts/localization/portuguese.json"
            }
	    });


	    $('.selectpicker').selectpicker({
			  size: 5
		});

	});




	</script>

</body>
</html>

<cfinclude template="i_alert.cfm">