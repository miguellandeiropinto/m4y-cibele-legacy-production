<cfprocessingdirective pageencoding="utf-8">
<cfcontent type="text/html; charset=utf-8">
<cfset setEncoding("URL", "utf-8")>
<cfset setEncoding("Form", "utf-8")>

<cfparam name="URL.page" default="list">
<cfparam name="URL.sid" default="0">


<cfinclude template="cfc/queries.cfc">

<cfif cgi.request_method eq "POST" and isDefined("form.campID")>
    <cfset SID = ""/>
    <cftransaction>
        <cfquery name="insertS">
            UPdate campaign_sends SET
                campaign_historyID = #FORM.campID#,
                revenue = #FORM.revenue#,
                sourceID ='#FORM.sid#',
                mediumID ='#FORM.mid#',
                sender = '#FORM.sender#',
                sents = 1,
                start_tstamp = '#FORM.ststamp#',
                end_tstamp = '#FORM.etstamp#',
                url = '#FORM.url#',
                send_id = '#FORM.sendid#',
                externalID = '#FORM.extid#'
            WHERE id = #FORM.seid#
        </cfquery>
        <cfquery name="s">
            SELECT SCOPE_IDENTITY() as id
        </cfquery>
        <cfset SID = #s.id#>
    </cftransaction>
</cfif>

<!DOCTYPE HTML>
<html>
<head>
    <!-- META -->
<cfinclude template="i_meta.cfm">
    <!-- CSS -->
<cfinclude template="i_css.cfm">
    <!-- IE8 -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<!---<cfinclude template="i_analytics.cfm">--->
</head>
<body class="body">
<cfinclude template="i_header.cfm">

<div class="container-fluid">
<section id="contents">

<!--- HEADER --->

<!--- HEADER --->

<div class="container bgwhite padding-tb margin-tb">
    <style>

        .zone {
            margin-bottom: 25px;
        }
        form div label {
            display: block;
        }
        table {
            width: 100%;
            border: #ccc 1px solid;
        }
        table th, table td {
            padding: 7px 10px;
            border-right: #ccc 1px solid;
        }
        table tr td:last-of-type {
            border-right: none;
        }
        table tbody {
            border-top: #ccc 1px solid;
        }

        table tbody tr {
            border-bottom: #ccc 1px solid;
        }

        form label {
            display: block;
        }

        * {
            word-wrap: break-word;
        }

        form input[type="date"] {
            padding: 4px 10px;
        }

        form#updSend input {
            width: 100%;
            display: block !important;
            padding: 4px 10px;
        }

        a.go-back {
            display: block;
        }

        form#chartForm input.btn {
            margin-top: -2.5px;
        }

    </style>
<div class="row">
<div class="col-sm-12">
    <cfoutput>
        <!---
    <table>
            <thead>
            <th>Country</th>
            <th>Cmpaings</th>
            <th>Send ID</th>
            <th>Sender</th>
            <th>Source</th>
            <th>Medium</th>
            <th>Sents</th>
            <th>Opens</th>
            <th>Clicks</th>
            </thead>
        <cfquery name="rsCountries">
            SELECT DISTINCT country FROM campaigns_history GROUp BY country
        </cfquery>
        <cfloop query="rsCountries">
            <cfquery name="rsCampaigns">
                SELECT * FROM campaigns_history WHERE country = '#rsCountries.country#'
            </cfquery>
            <tr><td rowspan="#rsCampaigns.recordcount#">#rsCountries.country#</td>
            <cfset ctC = 0>
            <cfloop query="rsCampaigns">
                <cfquery name="rsCSends">
                        SELECT * FROM campaign_sends WHERE campaign_historyID = #rsCampaigns.id#
                </cfquery>
                <cfif ctC lte 1>
                        <td rowspan="#rsCSends.recordcount#">
                </cfif>
                <cfset ctC=ctC+1>

                   #rsCampaigns.campaignID#</td>
                    <cfset ct = 0>
                    <cfloop query="rsCSends">

                                <td>#rsCSends.send_id#</td>
                                <td>#rsCSends.sender#</td>
                                <td>#rsCSends.sourceID#</td>
                                <td>#rsCSends.mediumID#</td>

                                <td> #rsCSends.sents#</td>
                                <td> #rsCSends.opens#</td>
                                <td> #rsCSends.clicks#</td>
        </tr>
                        <cfset ct = ct+1>
                    </cfloop>
                </td>
            </cfloop>
            </td></tr>
        </cfloop>
    </table>

--->

    <table>
            <thead>
            <th>Country</th>
            <th>Cmpaings</th>
            <th>Send ID</th>
            <th>Sender</th>
            <th>Source</th>
            <th>Medium</th>
            <th>Sents</th>
            <th>Opens</th>
            <th>Clicks</th>
            <th>Conversions</th>
            <th>Revenue</th>
            </thead>

            <cfquery name="rsCountries">
                SELECT DISTINCT country FROM campaigns_history GROUp BY country
            </cfquery>
            <cfset countCampaigns=0>

            <cfloop query="rsCountries">
                <cfset countSends=0>
                <cfquery name="rsCampaigns">
                    SELECT * FROM campaigns_history WHERE country = '#rsCountries.country#'
                </cfquery>
                <cfset countCampaigns=countCampaigns+#rsCampaigns.recordcount#>

                <cfloop query="rsCampaigns">
                    <cfset countSendsCampaign=0>
                    <cfquery name="rsCSends">
                                SELECT * FROM campaign_sends WHERE campaign_historyID = #rsCampaigns.id#
                    </cfquery>
                    <cfloop query="rsCSends">
                        <cfset countSendsCampaign=countSendsCampaign+1>
                    </cfloop>
                    <cfset countSends = countSends + #rsCSends.recordcount#>
                </cfloop>
                <tr>
                    <td rowspan="#countSends#">#rsCountries.country#</td>
                <cfloop query="rsCampaigns">
                    <td rowspan="#countSendsCampaign#">#rsCampaigns.campaignID#</td>
                    <cfquery name="rsCSends">
                                SELECT * FROM campaign_sends WHERE campaign_historyID = #rsCampaigns.id#
                    </cfquery>
                    <cfloop query="rsCSends">
                        <td>#rsCSends.send_id#</td>
                        <td>#rsCSends.sender#</td>
                        <td>#rsCSends.sourceID#</td>
                        <td>#rsCSends.mediumID#</td>
                        <td>#rsCSends.sents#</td>
                        <td>#rsCSends.opens#</td>
                        <td>#rsCSends.clicks#</td>
                        <cfquery name="rsStats">
                            SELECT
                            count(C.subid) as [total_conversions],
                            count(C.subid) * CS.revenue as [estimated_revenue]
                            FROM campaign_sends CS
                            LEFT OUTER JOIN c C ON C.subid = cast(CS.id as nchar(255))
                            WHERE CS.id = #rsCSends.id#
                            GROUP BY CS.revenue
                        </cfquery>
                        <td>#rsStats.total_conversions#</td>
                        <td>#rsStats.estimated_revenue#</td>
                    </tr>
                    </cfloop>
                    </tr>
                </cfloop>
                </tr>
            </cfloop>

        </table>
    </cfoutput>
</div>
</div>
</section></div>
<cfinclude template="i_scripts.cfm">
</body>
</html>