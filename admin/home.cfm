<cfprocessingdirective pageencoding="utf-8">
<cfcontent type="text/html; charset=utf-8">
<cfset setEncoding("URL", "utf-8")>
<cfset setEncoding("Form", "utf-8")>
<cfparam name="URL.page" default="list">
<cfinclude template="cfc/queries.cfc">

 

<!DOCTYPE HTML>
<html>
<head>
<!-- META -->
<cfinclude template="i_meta.cfm">
<!-- CSS -->
<cfinclude template="i_css.cfm">
<!-- IE8 -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!---<cfinclude template="i_analytics.cfm">--->
</head>
<body class="body">
<cfinclude template="i_header.cfm">
<div class="container-fluid">
	

	<section id="contents">

		<!--- HEADER --->
		<cfinclude template="home_header.cfm">
		<!--- HEADER --->

		<div class="bgwhite padding-tb margin-tb">

			<div class="row">
				<div class="col-sm-12">

					<table id="table_home" class="table table-hover table-striped" >
					<thead>
						<tr>
							<th style="font-size:12px;">ID</th>
							<th style="font-size:12px;">TS</th>
							<th style="font-size:12px;">CPI</th>
							<th style="font-size:12px;">STATUS</th>
							<th style="font-size:12px;">NAME</th>
							<th style="font-size:12px;">G</th>
							<th style="font-size:12px;">AGE</th>
							<th style="font-size:12px;">CT</th>
							<th style="font-size:12px;">DATA</th>
							<th style="font-size:12px;">MAR-PT</th>
							<th style="font-size:12px;">TAR-PT</th>
							<th style="font-size:12px;">PAD-PT</th>
							<th style="font-size:12px;">WEB-PT</th>
							<th style="font-size:12px;">BON-PT</th>
							<th style="font-size:12px;">MAR-BR</th>
							<th style="font-size:12px;">TAR-BR</th>
							<th style="font-size:12px;">PAD-BR</th>
							<th style="font-size:12px;">EST-BR</th>
							<th style="font-size:12px;">EGO</th>
						</tr>
					</thead>
					<tbody>

						<cfoutput query="rsLeads">
						<tr onClick="window.location='edit.cfm?leadID=#rsLeads.leadID#&ccampaingID=#URL.ccampaignID#&statusID=#URL.statusID#&country=#URL.country#'">
							<td>#left(rsLeads.leadID,10)#...</td>
							<td>#DateFormat(rsLeads.lead_tstamp,"YYYY-MM-DD")# #TimeFormat(rsLeads.lead_tstamp,"HH:MM")#<br></td>
							<td>#rsLeads.platformID#<br>#rsLeads.campaignID#<br>#rsLeads.sourceID#<br>#rsLeads.mediumID#</td>
							<td><strong>#rsLeads.lead_status#</strong></td>
							<td>#rsLeads.lead_name#<br>#rsLeads.lead_surname#</td>
							<td>#rsLeads.lead_gender#</td>
							<td>#rsLeads.lead_age#</td>
							<td>#rsLeads.lead_country#</td>
							<td>#rsLeads.lead_email#<br>#rsLeads.lead_phone#<br>#rsLeads.lead_zip#</td>
							<td>#rsLeads.marpt#</td>
							<td>#rsLeads.tarpt#</td>
							<td>#rsLeads.padpt#</td>
							<td>#rsLeads.wpipt#</td>
							<td>#rsLeads.bntpt#</td>
							<td>#rsLeads.marbr#</td>
							<td>#rsLeads.tarbr#</td>
							<td>#rsLeads.padbr#</td>
							<td>#rsLeads.estbr#</td>
							<td>#rsLeads.ego#</td>
						</tr>
						</cfoutput>

					</tbody>
					</table>
				

				</div>
			</div>

		</div>
	</section>

</div>

<cfinclude template="i_footer.cfm" >

	
<cfinclude template="i_divs.cfm">

    <!-- JS -->
    <cfinclude template="i_scripts.cfm">
	<script type="text/javascript">

	$(document).ready( function () {
	    $('#table_home').DataTable({
	    	"language": {
                "url": "../public_scripts/localization/portuguese.json"
            },
            "order": [[ 1, "desc" ]] //,
            // "autoWidth": false
	    });
	} );

	</script>

</body>
</html>

<cfinclude template="i_alert.cfm">