<cfoutput>
<cfheader name="content-type" value="text/css" />
@charset "utf-8";

<!--- /* Extra small devices (smartphones, min 768px) */--->
	
	body, h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 { 
		font-family: "Open Sans", Helvetica, Arial, sans-serif!important;
		font-weight:400;
		color:##2d2d2d;
		font-size:12px;
	}

	.container-fluid { calc(height: 100%-50px) !important; }
	##footer {height:20px; position: fixed; bottom:0; width:100%; z-index:50;}


	section {margin-top:60px;}
	
	.section_break {
		margin-bottom:15px;
	}
	
	.navigation {margin-bottom:20px;}


	.nopadding {
	   padding: 0 !important;
	   margin: 0 !important;
	}
	
	.padding-tb-form {
		padding-top:10px !important;
		padding-bottom:10px !important;
	}

	.padding-tb-form-input {
		padding-bottom:5px !important;
	}

	.form-break {
		border-bottom: 1px solid ##F1f1f1;
	}		

	a.stickyarrow {color:##2d2d2d;}
	a.stickyarrow:hover {color:##424143;}

	.body_text {
		font-family: "Open Sans", Helvetica, Arial, sans-serif!important;
		font-weight:400;
  		line-height: 1.42857143;
	}

	
	h1, .h1 {  
		font-family: "Open Sans", Helvetica, Arial, sans-serif!important;

	}
	
	h2, .h2 {  
		font-family: "Open Sans", Helvetica, Arial, sans-serif!important;

	}
	
	<!--- POP UP --->
	h3, .h3 {  
		font-family: "Open Sans", Helvetica, Arial, sans-serif!important;

	}
	
	
	h4, .h4 {  
		font-family: "Open Sans", Helvetica, Arial, sans-serif!important;

	}
	
	h5, .h5 {  
		font-family: "Open Sans", Helvetica, Arial, sans-serif!important;

	}

	h6, .h6 {  
		font-family: "Open Sans", Helvetica, Arial, sans-serif!important;
		font-weight: 600;
	}

	
	
	.table th, .table td { 
     	border-top: none !important; border-bottom: none !important;
	 }
	 
	 .table td { 
	 	font-family: "Open Sans", Helvetica, Arial, sans-serif!important;
	 	font-size: 13px;
	 	font-weight:300;
	 }

	 .table th {
	 	font-family: "Open Sans", Helvetica, Arial, sans-serif!important;
	 	font-size: 15px;
	 	font-weight:400;
	 }	
	<!---
	.header {
		font-family: "Open Sans", Helvetica, Arial, sans-serif!important;
		font-size:16px;
		color:##CCCBCB;
		font-weight:300;
	}
	

	a.header-text {
		color:##CCCBCB;
		text-decoration:none;
	}
	
	a.header-text:hover {
		color:##424143;
		text-decoration:none;
	}
	
	a.header-text:active {color:##8B169C; text-decoration:none;}

	.logo_container {
		padding-top:50px;
	}

	.label-control {
		font-family: "Regular", Helvetica, Arial, sans-serif!important;
	 	font-size: 14px;
	 	color:##8B169C;
	 	font-weight:400;
	}

	.form-control {
	  color: ##424143;
	  background-color: ##fff;
	  border: 1px solid ##CCCBCB;
	  border-radius: 4px;
	}

	.form-control:focus {
	  border-color: ##8B169C;
  	  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(139, 22, 156, .6);
      box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(139, 22, 156, .6);
	}

	.form-control::-moz-placeholder {
	  color: ##CCCBCB;
	  opacity: 1;
	}
	.form-control:-ms-input-placeholder {
	  color: ##CCCBCB;
	}
	.form-control::-webkit-input-placeholder {
	  color: ##CCCBCB;
	}

	.btn {border-radius: 0px 0px 0px 0px; }
	
	.btn-default {
		font-family: "Black", Helvetica, Arial, sans-serif!important;
		font-size: 14px;
		color: ##8B169C;
		background-color: ##FFFFFF;
		border: ##8B169C 1px solid;
	}
	
	.btn-default.inv {background-color: ##8B169C;}
	
	.btn-default:focus, .btn-default.focus {
		  color: ##FFFFFF;
		  background-color: ##8B169C;
		  border:  ##8B169C 1px solid;
	}
	
	.btn-default:hover {
		  color: ##FFFFFF;
		  background-color: ##8B169C;
		  border:  ##8B169C 1px solid;
	}

	.btn-image {
	  color: ##FFF;
	  background-color: ##fff;
	  border-color: ##ccc;
	  padding: 5px 0px 5px 0px !important;
	  margin: 0px 4px 0px 4px !important;
	  width:150px;
	}
	
	.btn-image.active { 
		background-color: ##F1F1F2;
	}

	.btn-file {
	  position: relative;
	  overflow: hidden;
	}
	.btn-file input[type=file] {
	  position: absolute;
	  top: 0;
	  right: 0;
	  min-width: 100%;
	  min-height: 100%;
	  font-size: 100px;
	  text-align: right;
	  filter: alpha(opacity=0);
	  opacity: 0;
	  cursor: inherit;
	  display: block;
	}
	
	
	input[readonly] {
	  margin-left:1px;
	  background-color: white !important;
	  cursor: text !important;
	}

	
	.img_template {
		width:120px; height:auto;
	}
	--->
	.popup_msgbox {
	  position: relative;
	  background: ##FFF;
	  padding: 20px;
	  width:auto;
	  max-width: 400px;
	  margin: 20px auto;
	}
	
	
	##errplace {color:##C90000;}
	
	
	.verbosity_counters {
		color:##CCC;
		font-size:12px;
	}

<!--- /* Small devices (tablets, 768px and up) */ --->
@media (min-width: 768px) { 
	

	
}

<!--- /* Medium devices (desktops, 992px and up) */ --->
@media (min-width: 992px) { 


	
}

<!--- /* Large devices (large desktops, 1200px and up) */ --->
@media (min-width: 1200px) {
	

	
}


</cfoutput>
