<cfprocessingdirective pageencoding="utf-8">
<cfcontent type="text/html; charset=utf-8">
<cfset setEncoding("URL", "utf-8")>
<cfset setEncoding("Form", "utf-8")>

<cfparam name="URL.page" default="stats">
<cfparam name="URL.filter" default="all">
<cfparam name="URL.participantID" default="all">
<cfinclude template="cfc/queries.cfc">


<!DOCTYPE HTML>
<html>
<head>
<!-- META -->
<cfinclude template="i_meta.cfm">
<!-- CSS -->
<cfinclude template="i_css.cfm">
<!-- IE8 -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!---<cfinclude template="i_analytics.cfm">--->
</head>
<body class="body">
<cfinclude template="i_header.cfm">

<div class="container-fluid">
	

	<section id="contents">

		<!--- HEADER --->
		<cfinclude template="home_header.cfm">
		<!--- HEADER --->

		<div class="container bgwhite padding-tb margin-tb">

			<div class="row">
				<div class="col-sm-12">

					
					<h3>30th March</h3>
					<table id="table_stats" class="table table-hover table-striped">
					<thead>
						<tr>
							<th>Seats Avaliable</th>
							<th>Invited</th>
							<th>Registred</th>
							<th>Waiting List</th>
							<th>Parking</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>

						<cfoutput query="rsStatsDay1">
						<tr>
							<td>#rsDay1.seats#</td>
							<td>#rsStatsDay1.invited#</td>
							<td>#rsStatsDay1.confirmed#</td>
							<td>#rsStatsDay1.waiting#</td>
							<td>#rsStatsDay1.parking#</td>
							<th>&nbsp;</th>
						</tr>
						</cfoutput>

					</tbody>
					</table>
				

				</div>
			</div>

			<div class="row">
				<div class="col-sm-12">

					
					<h3>31st March</h3>
					<table id="table_stats" class="table table-hover table-striped">
					<thead>
						<tr>
							<th>Seats Avaliable</th>
							<th>Invited</th>
							<th>Registred</th>
							<th>Waiting List</th>
							<th>Bus</th>
							<th>Lunch</th>
						</tr>
					</thead>
					<tbody>

						<cfoutput>
						<tr>
							<td>#rsDay2.seats#</td>
							<td>#rsStatsDay2.invited#</td>
							<td>#rsStatsDay2.confirmed#</td>
							<td>#rsStatsDay2.waiting#</td>
							<td>#rsStatsDay2.bus#</td>
							<td>#rsStatsDay2.lunch#</td>
						</tr>
						</cfoutput>

					</tbody>
					</table>
				

				</div>
			</div>

			<div class="row">
				<div class="col-sm-12">

					
					<h3>duplicated emails</h3>
					<table id="table_stats" class="table table-hover table-striped">
					<thead>
						<tr>
							<th>Email</th>
						</tr>
					</thead>
					<tbody>

						<cfoutput query="rsDuplicated">
						<tr>
							<td>#rsDuplicated.participant_email#</td>
						</tr>
						</cfoutput>

					</tbody>
					</table>
				

				</div>
			</div>

			<div class="row">
				<div class="col-sm-12">

					
					<h3>email problems</h3>
					<table id="table_stats" class="table table-hover table-striped">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th>Problem</th>
						</tr>
					</thead>
					<tbody>

						<cfoutput query="rsEP">
						<tr>
							<td>#rsEP.participant_name#</td>
							<td>#rsEP.participant_email#</td>
							<td>#rsEP.email_problem#</td>
						</tr>
						</cfoutput>

					</tbody>
					</table>
				

				</div>
			</div>
			

		</div>
	</section>

</div>

<cfinclude template="i_footer.cfm" >

	
<cfinclude template="i_divs.cfm">

    <!-- JS -->
    <cfinclude template="i_scripts.cfm">
	<script type="text/javascript">

	$(document).ready( function () {
	    $('#table_participants').DataTable({
	    	"language": {
                "url": "../public_scripts/localization/portuguese.json"
            }
	    });
	} );

	</script>

</body>
</html>

<cfinclude template="i_alert.cfm">