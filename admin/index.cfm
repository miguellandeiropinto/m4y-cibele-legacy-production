<cfprocessingdirective pageencoding="utf-8">
<cfcontent type="text/html; charset=utf-8">
<cfset setEncoding("URL", "utf-8")>
<cfset setEncoding("Form", "utf-8")>

<cfparam name="URL.page" default="list">
<cfparam name="URL.filter" default="all">
<cfparam name="URL.participantID" default="999999999">
<cfinclude template="cfc/queries.cfc">

<!DOCTYPE HTML>
<html>
<head>
<!-- META -->
<cfinclude template="i_meta.cfm">
<!-- CSS -->
<cfinclude template="i_css.cfm">



<!-- IE8 -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!---<cfinclude template="i_analytics.cfm">--->
</head>
<body>

<cflocation url="home.cfm">

<div class="container-fluid">


    <cfoutput> 
        <h2>Welcome #Session.usrname#!</h2> 
        <br>#Application.pp_global.platform_name#
        <h3>#session.user_roles#</h3>
    </cfoutput> 
     
    ALL Logged-in Users see this message.<br> 
    <br>

    <cfscript> 
        if (IsUserInRole("Accountant")) 
            WriteOutput("managers see this message.<br><br>"); 
        if (IsUserInRole("Admin")) 
            WriteOutput("Admins see this message.<br><br>"); 
    </cfscript> 	

</div>

<cfdump var="#Application.pp_global#">




</body>
</html>
