<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,700,700italic,600,600italic,800,800italic' rel='stylesheet' type='text/css'>

<!---
<link href="../common_css/fonts.cfm" rel="stylesheet" type="text/css">
--->



<link href="../bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="../bootstrap/css/bootstrap-select.min.css" rel="stylesheet" type="text/css">
<link href="../bootstrap/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<link href="../common_css/bootstrap_overide.css" rel="stylesheet" type="text/css">
<link href="../common_css/common_styles.cfm" rel="stylesheet" type="text/css">
<link href="css/common_styles.css" rel="stylesheet" type="text/css">		

<link href="css/app_styles.cfm" rel="stylesheet" type="text/css">

<link href="../public_css/magnific-popup.css" rel="stylesheet">
<link href="../public_css/animate.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/s/bs/jq-2.1.4,dt-1.10.10,af-2.1.0,b-1.1.0,b-colvis-1.1.0,cr-1.3.0,fc-3.2.0,fh-3.1.0,r-2.0.0,rr-1.1.0,sc-1.4.0,se-1.1.0/datatables.min.css"/>
<!---

<link href="../public_css/jquery.bxslider.css" rel="stylesheet" type="text/css">
<link href="../public_css/gray.min.css" rel="stylesheet" type="text/css">
--->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>