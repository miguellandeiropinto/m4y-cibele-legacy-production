<cfprocessingdirective pageencoding="utf-8">

    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="home.cfm">M4YBO</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <!--- <li class="active"><a href="#">Home</a></li> --->
            <!--- LEADS --->
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Leads <span class="caret"></span></a>
              <ul class="dropdown-menu">
                
                <li class="dropdown-header">filter by lead status</li>
                <cfoutput query="rsStatus">
                <li><a href="home.cfm?statusID=#rsStatus.statusID#&date_ini=#date_ini#&date_end=#date_end#">#rsStatus.lead_status#</a></li>
                </cfoutput>
                <cfoutput>
                <li><a href="home.cfm?statusID=&date_ini=#date_ini#&date_end=#date_end#">ALL</a></li>
                </cfoutput>
                <li role="separator" class="divider"></li>
                <li class="dropdown-header">filter by client campaign</li>
                <cfoutput query="rsCampaigns">
                <li><a href="home.cfm?ccampaignID=#rsCampaigns.campaignID#&date_ini=#date_ini#&date_end=#date_end#">#rsCampaigns.campaign_name#</a></li>
                </cfoutput>                
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Countries <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li role="separator" class="divider"></li>
                <li class="dropdown-header">filter by country</li>
                <cfoutput query="rsCountries">
                <li><a href="home.cfm?country=#rsCountries.lead_country#&date_ini=#date_ini#&date_end=#date_end#">#rsCountries.lead_country#</a></li>
                </cfoutput>
                <!--- <li><a href="home.cfm?all=1">ALL LEADS</a></li> --->
                
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Errors <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <cfoutput>
                <li class="dropdown-header">filters</li>
                <li><a href="home.cfm?statusID=-1&date_ini=#date_ini#&date_end=#date_end#">with Lead Error</a></li>
                <li><a href="home.cfm?int_statusID=0&date_ini=#date_ini#&date_end=#date_end#">with Integration Error</a></li>
                <li><a href="home.cfm?country=NO&date_ini=#date_ini#&date_end=#date_end#">No country</a></li>
                </cfoutput>
              </ul>
            </li>
              <li><a href="campaigns.cfm">Campaigns</a></li>
            <!--- INT 
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Integrations <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li class="dropdown-header">integrate 10 new leads with</li>
                <cfoutput query="rsCampaigns">
                <li><a href="cfc/integration.cfc?method=do_integration&ccampaignID=#rsCampaigns.campaignID#">#rsCampaigns.campaign_name#</a></li>
                </cfoutput>
              </ul>
            </li>
            --->
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <!--- USER --->
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><cfoutput>#session.usrname#</cfoutput> <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <!---
                <li><a href="#">dados</a></li>
                <li><a href="users_times.cfm?smid=ss">registo horas</a></li>
                --->
              </ul>
            </li>
            <li><a href="index.cfm?logout=1">LOGOUT</a></li>
            <!--<li class="active"><a href="./">Fixed top <span class="sr-only">(current)</span></a></li>-->
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
