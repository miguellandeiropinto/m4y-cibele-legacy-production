<cfprocessingdirective pageencoding="utf-8">

<div class="" style="width:100%;">
<section id="footer" class="bggreydark">

      <div class="container text-center">
        <span class="white">&copy; <cfoutput>#year(now())#</cfoutput> mad4ideas</span>
      </div>

</section>
</div>