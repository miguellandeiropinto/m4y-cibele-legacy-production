<cfcomponent>
<cfprocessingdirective pageencoding="utf-8">


	<cffunction name="do_integration" access="remote">


		<cfparam name="URL.leadID" default="">
		<cfparam name="URL.ccampaignID" default="">


		<CFSTOREDPROC procedure="dbo.getLeadsforIntegration">
		  <CFPROCPARAM type="IN" value="#URL.leadID#" cfsqltype="CF_SQL_VARCHAR">
		  <CFPROCPARAM type="IN" value="#URL.ccampaignID#" cfsqltype="CF_SQL_VARCHAR">
		  <CFPROCPARAM type="IN" value="20" cfsqltype="CF_SQL_VARCHAR">
		  <CFPROCRESULT name="rsLead" resultset="1">
		</CFSTOREDPROC>

		<CFSTOREDPROC procedure="dbo.getClientCampaign">
		  <CFPROCPARAM type="IN" value="#URL.ccampaignID#" cfsqltype="CF_SQL_VARCHAR">
		  <CFPROCRESULT name="rsCampaign" resultset="1">
		  <CFPROCRESULT name="rsRules" resultset="2">
		  <CFPROCRESULT name="rsFields" resultset="3">
		</CFSTOREDPROC>

		<!---
		<CFSTOREDPROC procedure="dbo.checkCampaignRules">
		  <CFPROCPARAM type="IN" value="#URL.ccampaignID#" cfsqltype="CF_SQL_VARCHAR">
		  <CFPROCPARAM type="IN" value="#URL.leadID#" cfsqltype="CF_SQL_VARCHAR">
		  <CFPROCRESULT name="rsLeadRules" resultset="1">
		</CFSTOREDPROC>
		--->


		<cfset ftranslate = StructNew() >

		<cfloop query="rsFields">
			<cfset ftranslate["#rsFields.field_name#"]["#rsFields.field_value#"] = rsFields.field_value_client >
		</cfloop>

		Lista de leads para tentativa de integração:
		<cfdump var="#rsLead#"><br>

		<cfloop query="#rsLead#">

			<cfswitch expression="#URL.ccampaignID#">


					<cfcase value="egoi-0000">
					
						<cfif rsLead.recordcount gte 1>
					

							<cfset apikey="46099444df9915f368e9b525fa3d87b525cb0c85">
							<cfset listID='2'>
							<cfset tags= ''>
							<cfset theurl='http://api.e-goi.com/v2/rest.php?method=addSubscriber'>
							<cfset qs='&functionOptions[apikey]=' & apikey>
							<cfset qs=qs & '&functionOptions[listID]=' & listID>
							<cfset qs=qs & '&functionOptions[status]=' & '1'>
							<cfset qs=qs & '&functionOptions[lang]=' & lcase(rsLead.lead_country)>
							<cfset qs=qs & '&functionOptions[email]=' & rsLead.lead_email>
							<cfset qs=qs & '&functionOptions[telephone]=' & rsLead.lead_phone>
							<cfset qs=qs & '&functionOptions[validate_phone]=' & '0'>
							<cfset qs=qs & '&functionOptions[first_name]=' & rsLead.lead_name>
							<cfset qs=qs & '&functionOptions[last_name]=' & rsLead.lead_surname>
							<cfset qs=qs & '&functionOptions[birth_date]=' & rsLead.lead_birthdate>
							<cfset qs=qs & '&functionOptions[extra_1]=' & rsLead.lead_country>
							<cfset qs=qs & '&functionOptions[extra_2]=' & rsLead.lead_zip>
							<cfset qs=qs & '&functionOptions[extra_15]=' & rsLead.lead_interest>
							<cfset qs=qs & '&functionOptions[extra_16]=' & rsLead.lead_gender>
							<cfset qs=qs & '&functionOptions[extra_6]=' & rsLead.leadID>
							<cfset qs=qs & '&functionOptions[extra_10]=' & DateFormat(rsLead.lead_tstamp, "YYYY-MM-DD")>
							<cfset qs=qs & '&functionOptions[extra_19]=' & rsLead.lead_sign>
							<cfif rsLead.mpt eq 1>
							<cfset tags=tags & '&functionOptions[tags][]=2'>
							</cfif>
							<cfif rsLead.mbr eq 1>
							<cfset tags=tags & '&functionOptions[tags][]=3'>
							</cfif>
							<cfif rsLead.tpt eq 1>
							<cfset tags=tags & '&functionOptions[tags][]=4'>
							</cfif>
							<cfif rsLead.tbr eq 1>
							<cfset tags=tags & '&functionOptions[tags][]=4'>
							</cfif>
							<cfset qs=qs & tags>
							<cfset theurl=theurl & qs>
							Pedido:<br>
							<cfoutput>#theurl#</cfoutput>
							<br>
							<!---  (0 = unconfirmed; 1 = active; 2 = removed; 4 = inactive).  --->

							<cfhttp url="#theurl#" method="get" result="result" charset="utf-8"></cfhttp>


							<cfset egoiresponse='#result.fileContent#'>


							<cfset objXml = createObject("component","xml2Struct")>

							<!--- append the returned structure to some existing structure--->
							<cfset str = structnew()>
							<cfset str.existingElement = "some text here">
							<cfset str = objXml.ConvertXmlToStruct(egoiresponse, str)>


							<!--- create a new structure --->
							<cfset newStr = objXml.ConvertXmlToStruct(egoiresponse, StructNew())>
							<cfdump var = "#newStr#">

							<cfif isDefined("newStr.addSubscriber.error")>
								<cfset theresult=newStr.addSubscriber.error>
							<cfelse>
								<cfset theresult='ok'>
							</cfif>

							<CFSTOREDPROC procedure="dbo.integrate_lead">
								<CFPROCPARAM type="IN" value="#rsCampaign.campaignID#" cfsqltype="CF_SQL_VARCHAR">
								<CFPROCPARAM type="IN" value="#rsLead.leadID#" cfsqltype="CF_SQL_VARCHAR">
								<CFPROCPARAM type="IN" value="#theresult#" cfsqltype="CF_SQL_VARCHAR">
								<!---<CFPROCPARAM type="OUT" variable="leadID" cfsqltype="CF_SQL_VARCHAR">--->
							</CFSTOREDPROC>

							<cfif theresult eq 'ok'>
								
								<cfoutput>lead #rsLead.leadID# integrada com sucesso na campanha #rsCampaign.campaignID#<br><br></cfoutput>

							<cfelse>

								<cfoutput>lead #rsLead.leadID# não integrada na campanha #rsCampaign.campaignID#. Motivo:
								#theresult#<br><br>
								</cfoutput>
							
							</cfif>

						<cfelse>

							<cfoutput>lead #rsLead.leadID# não integrada na campanha #rsCampaign.campaignID#. Motivo
							não satisfaz os critérios (país ou idade)<br><br>
							</cfoutput>

						</cfif>


					</cfcase>

					<cfcase value="tara-coreg-pt-0001">
					
						<cfif rsLead.recordcount gte 1>
						
							<cfhttp url="http://www.tara-vidente.com/remote-registration.php" method="post" result="result" charset="utf-8"> 
							    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
							    <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#"> 
							    <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#"> 
							    <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#"> 
							    <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
							    <cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> 
							    <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#"> 
							    <cfhttpparam type="url" name="campaignarea" value="PT"> 
							    <cfhttpparam type="url" name="media" value="coreg">
							    <cfhttpparam type="url" name="partner" value="Yours">  
							    <cfhttpparam type="url" name="campaign" value="coreg2016">  
							</cfhttp>

							<cfdump var="#result#">

							<CFSTOREDPROC procedure="dbo.integrate_lead">
								<CFPROCPARAM type="IN" value="#rsCampaign.campaignID#" cfsqltype="CF_SQL_VARCHAR">
								<CFPROCPARAM type="IN" value="#rsLead.leadID#" cfsqltype="CF_SQL_VARCHAR">
								<CFPROCPARAM type="IN" value="#result.filecontent#" cfsqltype="CF_SQL_VARCHAR">
								<!---<CFPROCPARAM type="OUT" variable="leadID" cfsqltype="CF_SQL_VARCHAR">--->
							</CFSTOREDPROC>


							<cfif result.filecontent eq trim('ok')>
								
								<cfoutput>lead #rsLead.leadID# integrada com sucesso na campanha #rsCampaign.campaignID#<br><br></cfoutput>

							<cfelse>

								<cfoutput>lead #rsLead.leadID# não integrada na campanha #rsCampaign.campaignID#. Motivo:
								#result.filecontent#<br><br>
								</cfoutput>
							
							</cfif>

						<cfelse>

							<cfoutput>lead #rsLead.leadID# não integrada na campanha #rsCampaign.campaignID#. Motivo
							não satisfaz os critérios (país ou idade)<br><br>
							</cfoutput>

						</cfif>


					</cfcase>

					<cfcase value="tara-coreg-br-0002">
						
						<cfif rsLead.recordcount gte 1>
						
							<cfhttp url="http://www.tara-clarividencia.com/remote-registration.php" method="get" result="result" charset="utf-8"> 
							    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
							    <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#"> 
							    <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#"> 
							    <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#"> 
							    <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
							    <cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> 
							    <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#"> 
							    <cfhttpparam type="url" name="campaignarea" value="PT"> 
							    <cfhttpparam type="url" name="media" value="coreg">
							    <cfhttpparam type="url" name="partner" value="Yours">  
							    <cfhttpparam type="url" name="campaign" value="coreg2016">  
							</cfhttp>

							<cfdump var="#result#">

							<CFSTOREDPROC procedure="dbo.integrate_lead">
								<CFPROCPARAM type="IN" value="#rsCampaign.campaignID#" cfsqltype="CF_SQL_VARCHAR">
								<CFPROCPARAM type="IN" value="#rsLead.leadID#" cfsqltype="CF_SQL_VARCHAR">
								<CFPROCPARAM type="IN" value="#result.filecontent#" cfsqltype="CF_SQL_VARCHAR">
								<!---<CFPROCPARAM type="OUT" variable="leadID" cfsqltype="CF_SQL_VARCHAR">--->
							</CFSTOREDPROC>


							<cfif result.filecontent eq trim('ok')>
								
								<cfoutput>lead #rsLead.leadID# integrada com sucesso na campanha #rsCampaign.campaignID#<br><br></cfoutput>

							<cfelse>

								<cfoutput>lead #rsLead.leadID# não integrada na campanha #rsCampaign.campaignID#. Motivo:
								#result.filecontent#<br><br>
								</cfoutput>
							
							</cfif>

						<cfelse>

							<cfoutput>lead #rsLead.leadID# não integrada na campanha #rsCampaign.campaignID#. Motivo
							não satisfaz os critérios (país ou idade)<br><br>
							</cfoutput>

						</cfif>


					</cfcase>
					
					<cfcase value="maria-coreg-pt-0003">
						
						<cfif rsLead.recordcount gte 1>
						
							<cfhttp url="http://www.vidente-maria.com/remote-registration.php" method="get" result="result" charset="utf-8"> 
							    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
							    <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#"> 
							    <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#"> 
							    <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#"> 
							    <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
							    <!---<cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> --->
							    <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#">
							    <cfhttpparam type="url" name="optin" value="#rsLead.lead_optin#">
							    <cfhttpparam type="url" name="campaignarea" value="PT"> 
							    <cfhttpparam type="url" name="media" value="coreg">
							    <cfhttpparam type="url" name="partner" value="Yours">  
							    <cfhttpparam type="url" name="campaign" value="coreg2016">  
							</cfhttp>

							<cfdump var="#result#">

							<CFSTOREDPROC procedure="dbo.integrate_lead">
								<CFPROCPARAM type="IN" value="#rsCampaign.campaignID#" cfsqltype="CF_SQL_VARCHAR">
								<CFPROCPARAM type="IN" value="#rsLead.leadID#" cfsqltype="CF_SQL_VARCHAR">
								<CFPROCPARAM type="IN" value="#result.filecontent#" cfsqltype="CF_SQL_VARCHAR">
								<!---<CFPROCPARAM type="OUT" variable="leadID" cfsqltype="CF_SQL_VARCHAR">--->
							</CFSTOREDPROC>


							<cfif result.filecontent eq trim('ok')>
								
								<cfoutput>lead #rsLead.leadID# integrada com sucesso na campanha #rsCampaign.campaignID#<br><br></cfoutput>

							<cfelse>

								<cfoutput>lead #rsLead.leadID# não integrada na campanha #rsCampaign.campaignID#. Motivo:
								#result.filecontent#<br><br>
								</cfoutput>
							
							</cfif>

						<cfelse>

							<cfoutput>lead #rsLead.leadID# não integrada na campanha #rsCampaign.campaignID#. Motivo
							não satisfaz os critérios (país ou idade)<br><br>
							</cfoutput>

						</cfif>

					</cfcase>
					
					<cfcase value="maria-coreg-br-0004">

						<cfif rsLead.recordcount gte 1>
						
							<cfhttp url="http://www.maria-medium-numerologa.com/remote-registration.php" method="get" result="result" charset="utf-8"> 
							    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
							    <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#"> 
							    <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#"> 
							    <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#"> 
							    <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
							    <!---<cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> --->
							    <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#">
							    <cfhttpparam type="url" name="optin" value="#rsLead.lead_optin#">
							    <cfhttpparam type="url" name="campaignarea" value="PT"> 
							    <cfhttpparam type="url" name="media" value="coreg">
							    <cfhttpparam type="url" name="partner" value="Yours">  
							    <cfhttpparam type="url" name="campaign" value="coreg2016">  
							</cfhttp>

							<cfdump var="#result#">

							<CFSTOREDPROC procedure="dbo.integrate_lead">
								<CFPROCPARAM type="IN" value="#rsCampaign.campaignID#" cfsqltype="CF_SQL_VARCHAR">
								<CFPROCPARAM type="IN" value="#rsLead.leadID#" cfsqltype="CF_SQL_VARCHAR">
								<CFPROCPARAM type="IN" value="#result.filecontent#" cfsqltype="CF_SQL_VARCHAR">
								<!---<CFPROCPARAM type="OUT" variable="leadID" cfsqltype="CF_SQL_VARCHAR">--->
							</CFSTOREDPROC>


							<cfif result.filecontent eq trim('ok')>
								
								<cfoutput>lead #rsLead.leadID# integrada com sucesso na campanha #rsCampaign.campaignID#<br><br></cfoutput>

							<cfelse>

								<cfoutput>lead #rsLead.leadID# não integrada na campanha #rsCampaign.campaignID#. Motivo:
								#result.filecontent#<br><br>
								</cfoutput>
							
							</cfif>

						<cfelse>

							<cfoutput>lead #rsLead.leadID# não integrada na campanha #rsCampaign.campaignID#. Motivo
							não satisfaz os critérios (país ou idade)<br><br>
							</cfoutput>

						</cfif>				


					</cfcase>

			</cfswitch>

		</cfloop>

	</cffunction>



</cfcomponent>