<cfcomponent>
<cfprocessingdirective pageencoding="utf-8">
<cfcontent type="text/html; charset=utf-8">
<cfset setEncoding("URL", "utf-8")>
<cfset setEncoding("Form", "utf-8")>


	<cffunction name="bo_update_lead" access="remote">


		<CFSTOREDPROC procedure="dbo.bo_update_lead">
			<CFPROCPARAM type="IN" value="#FORM.leadID#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#FORM.statusID#" cfsqltype="CF_SQL_VARCHAR">
			<!---
			<CFPROCPARAM type="IN" value="#FORM.urltoken#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#FORM.platformID#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#FORM.sourceID#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#FORM.mediumID#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#FORM.campaignID#" cfsqltype="CF_SQL_VARCHAR">--->
			<CFPROCPARAM type="IN" value="#FORM.lead_country#" cfsqltype="CF_SQL_VARCHAR">
			<!---
			<CFPROCPARAM type="IN" value="#FORM.lead_ip#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#FORM.lead_optin_email#" cfsqltype="CF_SQL_INTEGER"> --->
			<CFPROCPARAM type="IN" value="#FORM.lead_email#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#FORM.lead_name#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#FORM.lead_surname#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#FORM.lead_gender#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#FORM.lead_birthdate#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#FORM.lead_phone#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#FORM.lead_zip#" cfsqltype="CF_SQL_VARCHAR">
			<!--- <CFPROCPARAM type="IN" value="#FORM.lead_interest#" cfsqltype="CF_SQL_VARCHAR">	--->
		</CFSTOREDPROC>

		<cflocation url="../edit.cfm?#FORM.QS#">

	</cffunction>



	<cffunction name="bo_delete_lead" access="remote">


		<CFSTOREDPROC procedure="dbo.bo_delete_lead">
			<CFPROCPARAM type="IN" value="#FORM.var3#" cfsqltype="CF_SQL_VARCHAR">
		</CFSTOREDPROC>

		<cflocation url="../home.cfm">

	</cffunction>

</cfcomponent>