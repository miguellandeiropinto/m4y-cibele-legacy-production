<cfcomponent>
<cfprocessingdirective pageencoding="utf-8">
<cfparam name="URL.filter" default="">
<cfparam name="URL.statusID" default="">
<cfparam name="URL.int_statusID" default="">
<cfparam name="URL.leadID" default="999999999">
<cfparam name="URL.country" default="">
<cfparam name="URL.ccampaignID" default="">
<cfparam name="URL.all" default="0">
<cfif isDefined("FORM.date_ini")>
    <cfset date_ini=FORM.date_ini>
<cfelseif isDefined("URL.date_ini")>
    <cfset date_ini=URL.date_ini>
<cfelse>
    <cfset date_ini=Dateformat(now(),'YYYY-MM-DD')>
</cfif>
<cfif isDefined("FORM.date_end")>
    <cfset date_end=FORM.date_end>
<cfelseif isDefined("URL.date_end")>
    <cfset date_end=URL.date_end>
<cfelse>
    <cfset date_end=Dateformat(now(),'YYYY-MM-DD')>
</cfif>


  <CFSTOREDPROC procedure="dbo.getLeads">
    <CFPROCPARAM type="IN" value="#URL.statusID#" cfsqltype="CF_SQL_VARCHAR">
    <CFPROCPARAM type="IN" value="#URL.int_statusID#" cfsqltype="CF_SQL_VARCHAR">
    <CFPROCPARAM type="IN" value="#URL.country#" cfsqltype="CF_SQL_VARCHAR">
    <CFPROCPARAM type="IN" value="#URL.ccampaignID#" cfsqltype="CF_SQL_VARCHAR">
    <CFPROCPARAM type="IN" value="#URL.leadID#" cfsqltype="CF_SQL_VARCHAR">
    <CFPROCPARAM type="IN" value="#date_ini#" cfsqltype="CF_SQL_VARCHAR">
    <CFPROCPARAM type="IN" value="#date_end#" cfsqltype="CF_SQL_VARCHAR">
    <CFPROCRESULT name="rsLeads" resultset="1">
    <CFPROCRESULT name="rsCampaigns" resultset="2">
    <CFPROCRESULT name="rsStatus" resultset="3">
    <CFPROCRESULT name="rsCountries" resultset="4">
    <CFPROCRESULT name="rsLead" resultset="5">
    <CFPROCRESULT name="rsAttempts" resultset="6">
    <CFPROCRESULT name="rsIntegrations" resultset="7">
  </CFSTOREDPROC>

</cfcomponent>