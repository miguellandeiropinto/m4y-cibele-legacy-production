<cfprocessingdirective pageencoding="utf-8">
<cfcontent type="text/html; charset=utf-8">
<cfset setEncoding("URL", "utf-8")>
<cfset setEncoding("Form", "utf-8")>

<cfparam name="URL.page" default="list">
<cfinclude template="cfc/queries.cfc">

<cfif cgi.request_method eq "POST" and isDefined("form.cid")>
    <cfset CID = ''>
    <cftransaction>
        <cfquery name="insertC">
            INSERT INTO campaigns_history (campaignID, currency, vertical, externalID, tstamp, country, is_active, supplier)
            VALUES ('#FORM.cid#', '#FORM.cur#', '#FORM.vertical#', '#FORM.extid#', getdate(), '#FORM.country#', 1, '#FORM.supplier#');
        </cfquery>
        <cfquery name="c">
            SELECT SCOPE_IDENTITY() as id
        </cfquery>
        <cfset CID = #c.id#>
    </cftransaction>
    <cflocation url="campaign.cfm?cid=#CID#">
</cfif>

<cfquery name="rsVerticals">
    SELECT * FROM verticals ORDER BY id DESC
</cfquery>

<!DOCTYPE HTML>
<html>
<head>
    <!-- META -->
<cfinclude template="i_meta.cfm">
    <!-- CSS -->
<cfinclude template="i_css.cfm">
    <!-- IE8 -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<!---<cfinclude template="i_analytics.cfm">--->
</head>
<body class="body">
<cfinclude template="i_header.cfm">

<div class="container-fluid">
<section id="contents">

<!--- HEADER --->
<cfinclude template="campaigns_header.cfm">
<!--- HEADER --->

<div class="container bgwhite padding-tb margin-tb">
<style>
    form div label {
        display: block;
    }
</style>
    <cfoutput>
<div class="row">
    <div class="col-sm-12">
        <cfform method="post">
            <div class="form-group">
                <label>Campaing ID</label>
                <cfinput type="text" name="cid" />
            </div>
            <div class="form-group">
                    <label>Vertical</label>


                <cfselect name="vertical">
                    <cfloop query="rsVerticals">
                        <option value="#rsVerticals.id#">#rsVerticals.vertical#</option>
                    </cfloop>
                </cfselect>
            </div>
            <div class="form-group">
                <label>Currency</label>
            <cfselect name="cur">
                    <option value="0">Choose a currency for the campaign...</option>
                    <option value="1">Euro</option>
                    <option value="2">Real</option>
                    <option value="3">Dolár</option>
                    <option value="4">Peso</option>
            </cfselect>
            </div>
            <div class="form-group">
                    <label>Supplier</label>
                <cfselect name="supplier">
                    <option value="0">Choose a supplier for the campaign...</option>
                    <option value="1">Blue Butterfly</option>
                    <option value="2">Montargency</option>
                    <option value="3">DGMAX</option>
                    <option value="4">Net Affiliation</option>
                </cfselect>
            </div>
            <div class="form-group">
                    <label>Country Code</label>
                <cfinput type="text" name="country"/>
            </div>
            <div class="form-group">
                    <label>External ID</label>
                <cfinput type="text" name="extid"/>
            </div>
                <div class="form-group">
                    <cfinput type="submit" name="submit" value="Save Campaign" class="btn btn-md btn-primary">
            </div>
        </cfform>
    </div>
</div>
    </cfoutput>
</div>
</section></div>
<cfinclude template="i_scripts.cfm">
</body>
</html>