<cfprocessingdirective pageencoding="utf-8">
<cfcontent type="text/html; charset=utf-8">
<cfset setEncoding("URL", "utf-8")>
<cfset setEncoding("Form", "utf-8")>

<cfset section="login">




<!DOCTYPE HTML>
<html>
<head>
<!-- META -->
<cfinclude template="i_meta.cfm">
<!-- CSS -->
<cfoutput>

</cfoutput>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,700,700italic,600,600italic,800,800italic' rel='stylesheet' type='text/css'>
<link href="../bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="../bootstrap/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<link href="../common_css/bootstrap_overide.css" rel="stylesheet" type="text/css">
<link href="../common_css/common_styles.cfm" rel="stylesheet" type="text/css">
<link href="../common_css/common_styles.css" rel="stylesheet" type="text/css">		


<link href="../public_css/magnific-popup.css" rel="stylesheet">
<link href="../public_css/animate.min.css" rel="stylesheet" type="text/css">

	
<!-- IE8 -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!---<cfinclude template="i_analytics.cfm">--->
</head>
<body class="bggreylight">

<div class="container-fluid">
	
<section id="header">

	<div class="container">
	<div class="row">
		<div class="row-height">
		<div class="col-sm-12 col-height col-middle header">
			<div class="logo_container text-center">
			<cfoutput>
			<!---<img src="media/images/#application.pp_global.logo#">--->
			</cfoutput>
			</div>
		</div>
		</div>
	</div>
	</div>
	
</section>
	
<section id="contents">
	<div class="container">
	<div class="row" style="padding-top:100px;">
		<div class="col-xs-8 col-xs-offset-2 col-md-4 col-md-offset-4">
		<h4>M4YWEB BACKOFFICE</h4>
		<h5>LOGIN</h5><br><br>
		<cfoutput> 
		    <cfform action="#CGI.script_name#?#CGI.query_string#" method="post"> 
		        <label class="label-control" for="j_username">username:</label> 
		        <cfinput type="text" name="j_username" id="j_username" class="form-control"  maxlength="25">
		        <br>
		        <label class="label-control" for="j_password">password:</label>
		        <cfinput type="password" name="j_password" id="j_password" class="form-control"  maxlength="25"> 
				<br>
				#loginmessage#
				<br><br>
		        <button type="submit" class="btn btn-default">Enter</button>
		    </cfform> 
		</cfoutput>
		</div>
	</div>
	</div>

</section>

</div>




	
	<!---<cfinclude template="i_footer.cfm" ><cfinclude template="i_divs.cfm">--->

    <!-- JS -->
    <cfinclude template="i_scripts.cfm">
</body>
</html>

<cfinclude template="i_alert.cfm">