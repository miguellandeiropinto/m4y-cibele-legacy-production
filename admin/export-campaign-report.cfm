<cfprocessingdirective pageencoding="utf-8">
<cfcontent type="text/html; charset=utf-8">
<cfset setEncoding("URL", "utf-8")>
<cfset setEncoding("Form", "utf-8")>

<cfparam name="URL.cid" default="">


<cfif #URL.cid# neq "">

    <cfquery name="rsSendings">
        SELECT * FROM campaign_sends WHERE campaign_historyID = #URL.cid#
    </cfquery>

    <cfsavecontent variable="report">
        <cfoutput>
            <table>
                <thead>
                    <tr>
                        <th>id</th>
                        <th>campaign_historyID</th>
                        <th>revenue</th>
                        <th>sourceID</th>
                        <th>mediumID</th>
                        <th>sender</th>
                        <th>opens</th>
                        <th>clicks</th>
                        <th>sents</th>
                        <th>start_tstamp</th>
                        <th>end_tstamp</th>
                        <th>url</th>
                        <th>generated_url</th>
                        <th>send_id</th>
                        <th>externalID</th>
                    </tr>
                </thead>
                <cfloop query="#rsSendings#">
                    <tr>
                        <th>#id#</th>
                        <th>#campaign_historyID#</th>
                        <th>#revenue#</th>
                        <th>#sourceID#</th>
                        <th>#mediumID#</th>
                        <th>#sender#</th>
                        <th>#opens#</th>
                        <th>#clicks#</th>
                        <th>#sents#</th>
                        <th>#start_tstamp#</th>
                        <th>#end_tstamp#</th>
                        <th>#url#</th>
                        <th>#generated_url#</th>
                        <th>#send_id#</th>
                        <th>#externalID#</th>
                    </tr>
                </cfloop>
            </table>
        </cfoutput>
    </cfsavecontent>

    <cfset filename = "sends_report_#DateFormat(Now(),'ddmmyyyy')#_#LSTimeFormat(Now(),'HHMMSS')#.txt">
    <cfdump var=#GetDirectoryFromPath(
            GetCurrentTemplatePath()
            )&filename#>
    <cffile action="write" file="#GetDirectoryFromPath(
        GetCurrentTemplatePath()
            )&filename#" output="#report#">


</cfif>