<cfprocessingdirective pageencoding="utf-8">

<cfif isDefined("URL.msg")>
	<cfif URL.msg neq ''>
	<cfoutput>
	<div id="popup_msgbox" class="popup_msgbox mfp-hide animated fadeIn">
		<div class="row">
			<div class="row-height bb">
				<div class="col-sm-12 col-height col-middle">
					<span class="h3 greydark">MENSAGEM</span>
				</div>
			</div>
		</div>
		<div class="row" >
			<div class="row-height" style="height:100px;">
				<div class="col-sm-12 col-height col-middle" >
					<p class="body_text">#URL.msg#</p>
				</div>
			</div>
		</div>
		<!---
		<div class="row">
			<div class="row-height">
				<div class="col-sm-12 col-height col-middle">
					<button type="submit" class="btn btn-default">Confirmar</button>
				</div>
			</div>
		</div>
		--->
	</div>
	<script>jQuery.magnificPopup.open({items: {src: '##popup_msgbox'},type: 'inline'}, 0);</script>	
    </cfoutput>
    </cfif>
</cfif>

<!---
<div id="popup_msgboxadd" class="popup_msgbox mfp-hide animated fadeIn">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="alignB1 bb"><h3>AVISO!</h3></td>
  </tr>
  <tr>
    <td id="alertamais" class="alignB1 popup_text" height="100"></td>
  </tr>
</table>
</div>
--->
