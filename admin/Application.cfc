<cfcomponent displayname="Application" output="true" hint="Handle the application.">



	<!--- Set up the application. --->
	<cfset THIS.Name = "MYSTIFY.BO" />
	<cfset THIS.ApplicationTimeout = CreateTimeSpan( 0, 2, 0, 0 ) />
	<cfset This.SessionManagement="True"> 
	<cfset This.loginStorage="session">
	<cfset THIS.SessionTimeout = CreateTimeSpan( 0, 1, 0, 0 ) />
	<cfset THIS.SetClientCookies = true />

<!---
	<cfset THIS.SetClientCookies = false />
	<cfset THIS.clientManagement="False"> 
	<cfset THIS.sessionTimeout="#createtimespan(0,0,10,0)#"> 
	<cfset THIS.setDomainCookies="False"> 
--->


	
	<!--- Define the page request properties. --->
	<cfsetting requestTimeout="20" showDebugOutput="false" enableCFOutputOnly="false" />

	
	<cfset THIS.datasource="leaddam_wapp">


	<cffunction name="OnApplicationStart" access="public" returntype="boolean" output="false" hint="Fires when the application is first created.">
		
		<cflog file="#This.Name#" type="Information" text="Application Started">
		

		<cfset SetLocale("Portuguese (Standard)")>

		<cfreturn true />
	</cffunction>


	<cffunction name="OnSessionStart" access="public" returntype="void" output="false" hint="Fires when the session is first created.">
		
		
		<cfscript>

		</cfscript>
		

		<cfreturn />
	</cffunction>


	<cffunction name="OnRequestStart" access="public" returntype="boolean" output="false" hint="Fires at first part of page processing.">
	
		<!---<cfargument name="TargetPage" type="string" required="true" />---> 
	    <cfargument name="request" required="true"/>
		<cfscript>
			
		SetEncoding("form","utf-8");
		SetEncoding("url","utf-8");

		</cfscript>


	    <cfif IsDefined("URL.logout") or IsDefined("FORM.logout")> 
	        <script>
	        StructClear(session);
			StructClear(cookie);	
	        </script>
	        <cflogout> 
	    </cfif> 
	 	<cflogin applicationToken="#THIS.Name#" allowconcurrent="false" idletimeout="3600"> 
	        <cfif NOT IsDefined("cflogin")> 
	        	<cfset loginmessage="">  
	            <cfinclude template="login.cfm">
	            <cfabort> 
	        <cfelse> 
	            <cfif cflogin.name IS "" OR cflogin.password IS "">
	            	<cfset loginmessage="Preencha o user e a password">  
	                <cfinclude template="login.cfm"> 
	                <cfabort> 
	            <cfelse> 
	                <CFSTOREDPROC procedure="dbo.doLogin">
				      <CFPROCPARAM type="IN" value="#cflogin.name#" cfsqltype="CF_SQL_VARCHAR">
				      <CFPROCPARAM type="IN" value="#cflogin.password#" cfsqltype="CF_SQL_VARCHAR">
				      <CFPROCRESULT name="rsLoginData" resultset="1">
	            	</CFSTOREDPROC>
	                <cfif rsLoginData.roles NEQ ""> 
	                    <cfloginuser name="#cflogin.name#" Password = "#cflogin.password#" roles="#rsLoginData.roles#"> 
	                <cfelse> 
	                    <cfset loginmessage='Login invalido. Tente novamente.'>     
	                    <cfinclude template="login.cfm">
	                    <cfabort> 
	                </cfif> 
	            </cfif>     
	        </cfif> 
	    </cflogin> 
	 
	    <cfif GetAuthUser() NEQ ""> 
			
			<CFSTOREDPROC procedure="dbo.getUserbyUsr">
		      <CFPROCPARAM type="IN" value="#GetAuthUser()#" cfsqltype="CF_SQL_VARCHAR">
		      <CFPROCRESULT name="rsUser" resultset="1">
		    </CFSTOREDPROC>
		    <cfset Session.userIDv = rsUser.userID>
			<cfset Session.usrname = rsUser.usrname>
			<cfset Session.user_roles = rsUser.roles>
			
	        <cfoutput>
	            <form action="index.cfm" method="Post"> 
	                <input type="submit" Name="Logout" value="Logout"> 
	            </form>
	        </cfoutput> 
	    </cfif> 
	    
		<cfreturn true />
	</cffunction>


	<cffunction name="OnRequest" access="public" returntype="void" output="true" hint="Fires after pre page processing is complete.">

		<cfargument name="TargetPage" type="string" required="true" />

		<!--- Include the requested page. --->
		<cfinclude template="#ARGUMENTS.TargetPage#" />


		<cfreturn />
	</cffunction>


	<cffunction name="OnRequestEnd" access="public" returntype="void" output="true" hint="Fires after the page processing is complete.">


		<cfreturn />
	</cffunction>


	<cffunction name="OnSessionEnd" access="public" returntype="void" output="false" hint="Fires when the session is terminated.">

		<!--- Define arguments. --->
		<cfargument name="SessionScope" type="struct" required="true"/>
		<cfargument name="ApplicationScope" type="struct" required="false" default="#StructNew()#" />

		<cfreturn />
	</cffunction>


	<cffunction name="OnApplicationEnd" access="public" returntype="void" output="false" hint="Fires when the application is terminated.">

		<!--- Define arguments. --->
		<cfargument name="ApplicationScope" type="struct" required="false" default="#StructNew()#" />

		<cfreturn />
	</cffunction>


	<cffunction name="OnError" access="public" returntype="void" output="true" hint="Fires when an exception occures that is not caught by a try/catch.">

		<!--- Define arguments. --->
		<cfargument name="Exception" type="any" required="true" />
		<cfargument name="EventName" type="string" required="false"default=""/>

			<cflog file="#This.Name#" type="error" text="Event Name: #Arguments.Eventname#" > 
		    <cflog file="#This.Name#" type="error" text="Message: #Arguments.Exception.message#"> 
		    <!---<cflog file="#This.Name#" type="error" text="Root Cause Message: #Arguments.Exception.rootcause.message#">--->
		    <!--- Display an error message if there is a page context. ---> 
		    <cfif NOT (Arguments.EventName IS "onSessionEnd") OR  
		            (Arguments.EventName IS "onApplicationEnd")> 
		        <cfoutput> 
		            <h2>An unexpected error occurred.</h2> 
		            <p>Please provide the following information to technical support:</p> 
		            <p>Error Event: #Arguments.EventName#</p> 
		            <p>Error details:<br> 
		            <cfdump var=#Arguments.Exception#></p> 
		        </cfoutput> 
    		</cfif>

		<cfreturn />
	</cffunction>

</cfcomponent>