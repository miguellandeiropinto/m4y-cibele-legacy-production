<!DOCTYPE>
<html>
<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js" type="application/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment-with-locales.min.js"></script>
</head>
<body>
<script>

    (function($){

        function setupLead( lead )
        {
            //YYYY-MM-DD HH:mm:ss
            var leadDict = {
                "status": 0,
                "lang": lead.lead_country,
                "email": lead.lead_email,
                "firstName": lead.lead_name,
                "lastName": lead.lead_surname,
                "birth_date": moment(lead.lead_birthdate).format("YYYY-MM-DD"),
                "validate_telephone": 0,
                "telephone": lead.lead_phone
                /*"extra_1": lead['lead_country'],
                "extra_2": lead['lead_zip'],
                "extra_6": lead['leadid'],
                "extra_10": lead['lead_tstamp'],
                "extra_15": lead['lead_interest'],
                "extra_16": lead['lead_gender'],
                "extra_17": lead['sign_element'],
                "extra_19": lead['lead_sign'],
                "extra_20": lead['characteristics1'],
                "extra_21": lead['characteristics2'],
                "extra_22": lead['sign_planet'],
                "extra_23": lead['sign_incense'],
                "extra_24": lead['sign_stone'],
                "extra_25": lead['sign_metal'],
                "extra_26": lead['sing_number'],
                "extra_27": lead['sign_color']*/
            };

            return leadDict;
        }

        $(document).ready(function(){
            $.get("../cfc/leadset.cfc?method=getLeadset", function(response){

                var apKey = "f79fd059186e1533d4369031f5112e9fd2eb8eee";
                var egoiRequestParams = {
                    apiKey: apKey,
                    listID: 1,
                    compareField: "email",
                    subscribers: []
                };
                JSON.parse(response).forEach(function(lead){
                    egoiRequestParams.subscribers.push(setupLead(lead));
                });

                $.ajax({
                    method: "POST",
                    data: egoiRequestParams,
                    url: 'http://api.e-goi.com/v2/rest.php?method=addSubscriberBulk',
                    success: function(response){
                        console.log("success");
                        console.log(response);
                    },
                    error: function(response){
                        console.log("error");
                        console.log(response);
                    }
                });

            });
        });
    })(jQuery)
</script>
</body>
</html>