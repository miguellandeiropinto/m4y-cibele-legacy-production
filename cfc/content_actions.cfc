<cfcomponent>
	<cfsetting requesttimeout="9000">
<cfprocessingdirective pageencoding="utf-8">


<cfinclude template="contents.cfc">


	<cffunction name="getSign" access="remote" returnformat="json">
      	
		<!---<cfloop query="rsZodiac">
			<cfoutput>#rsZodiac.sign_key#</cfoutput><br>--->
			<cfswitch expression="#URL.month#">
				<cfcase value="1">
					<cfif URL.day gte 21>
						<cfset signID=11>
						<cfset sign_key='aquarius'>
					<cfelse>
						<cfset signID=10>
						<cfset sign_key='capricorn'>
					</cfif>
				</cfcase>
				<cfcase value="2">
					<cfif URL.day gte 20>
						<cfset signID=12>
						<cfset sign_key='pisces'>
					<cfelse>
						<cfset signID=11>
						<cfset sign_key='aquarius'>
					</cfif>
				</cfcase>
				<cfcase value="3">
					<cfif URL.day gte 21>
						<cfset signID=1>
						<cfset sign_key='aries'>
					<cfelse>
						<cfset signID=12>
						<cfset sign_key='pisces'>
					</cfif>
				</cfcase>
				<cfcase value="4">
					<cfif URL.day gte 21>
						<cfset signID=2>
						<cfset sign_key='taurus'>
					<cfelse>
						<cfset signID=1>
						<cfset sign_key='aries'>
					</cfif>
				</cfcase>
				<cfcase value="5">
					<cfif URL.day gte 21>
						<cfset signID=3>
						<cfset sign_key='gemini'>
					<cfelse>
						<cfset signID=2>
						<cfset sign_key='taurus'>
					</cfif>
				</cfcase>
				<cfcase value="6">
					<cfif URL.day gte 22>
						<cfset signID=4>
						<cfset sign_key='cancer'>
					<cfelse>
						<cfset signID=3>
						<cfset sign_key='gemini'>
					</cfif>
				</cfcase>
				<cfcase value="7">
					<cfif URL.day gte 23>
						<cfset signID=5>
						<cfset sign_key='leo'>
					<cfelse>
						<cfset signID=4>
						<cfset sign_key='cancer'>
					</cfif>
				</cfcase>
				<cfcase value="8">
					<cfif URL.day gte 24>
						<cfset signID=6>
						<cfset sign_key='virgo'>
					<cfelse>
						<cfset signID=5>
						<cfset sign_key='leo'>
					</cfif>
				</cfcase>
				<cfcase value="9">
					<cfif URL.day gte 24>
						<cfset signID=7>
						<cfset sign_key='libra'>
					<cfelse>
						<cfset signID=6>
						<cfset sign_key='virgo'>
					</cfif>
				</cfcase>
				<cfcase value="10">
					<cfif URL.day gte 24>
						<cfset signID=8>
						<cfset sign_key='scorpio'>
					<cfelse>
						<cfset signID=7>
						<cfset sign_key='libra'>
					</cfif>
				</cfcase>
				<cfcase value="11">
					<cfif URL.day gte 23>
						<cfset signID=9>
						<cfset sign_key='sagittarius'>
					<cfelse>
						<cfset signID=8>
						<cfset sign_key='scorpio'>
					</cfif>
				</cfcase>
				<cfcase value="12">
					<cfif URL.day gte 22>
						<cfset signID=10>
						<cfset sign_key='capricorn'>
					<cfelse>
						<cfset signID=9>
						<cfset sign_key='sagittarius'>
					</cfif>
				</cfcase>
			</cfswitch>
			<!---
			<cfif rsZodiac.sign_month_start eq 1>
				<cfset the_ini_date=CreateDate(URL.year-1, rsZodiac.sign_month_start, rsZodiac.sign_day_start)>
				<cfset the_end_date=CreateDate(URL.year, rsZodiac.sign_month_finish, rsZodiac.sign_day_finish)>
				<cfoutput>#the_ini_date# #the_end_date#<br></cfoutput>
			<cfelseif rsZodiac.sign_month_start eq 12>
				<cfset the_ini_date=CreateDate(URL.year, rsZodiac.sign_month_start, rsZodiac.sign_day_start)>
				<cfset the_end_date=CreateDate(URL.year, rsZodiac.sign_month_finish, rsZodiac.sign_day_finish)>
				<cfoutput>#the_ini_date# #the_end_date#<br></cfoutput>
			<cfelse>
				<cfset the_ini_date=CreateDate(URL.year, rsZodiac.sign_month_start, rsZodiac.sign_day_start)>
				<cfset the_end_date=CreateDate(URL.year, rsZodiac.sign_month_finish, rsZodiac.sign_day_finish)>
				<cfoutput>#the_ini_date# #the_end_date#<br></cfoutput>
			</cfif>
			--->
			<cfset the_date=CreateDate(URL.year, URL.month, URL.day)>
			<!---<cfoutput>#the_date# | #the_ini_date# | #the_end_date#</cfoutput><br>--->
			<!---
			<cfif the_date gte the_ini_date and the_date lte the_end_date>
				SIM<br>
				<cfset signID=rsZodiac.signID>
				<cfset sign_key=rsZodiac.sign_key>
			<cfelse>
				NAO<br>
			</cfif>

		<br></cfloop>
		--->
		
    	<cfset result.sign_key=sign_key>
    	<cfset result.sign=translations[URL.langID]['zodiac'][sign_key]>
    	<cfset result.short_desc=translations[URL.langID]['zodiac_short'][sign_key]>
    	<!---
    	<cfset result.characteristics1=translations[APPLICATION.langID]['characteristics'][zodiac[signID]['characteristics1']]>
    	<cfset result.characteristics2=translations[APPLICATION.langID]['characteristics'][zodiac[signID]['characteristics2']]>
    	--->

    <cfreturn result>
    
    </cffunction>


	<cffunction name="getForecast" access="remote" returnformat="json">
      	
		<CFSTOREDPROC procedure="dbo.getForecasts">
		  <CFPROCPARAM type="IN" value="#URL.langID#" cfsqltype="CF_SQL_VARCHAR">
		  <CFPROCPARAM type="IN" value="#URL.int#" cfsqltype="CF_SQL_VARCHAR">
		  <CFPROCRESULT name="rsForecasts" resultset="1">
		</CFSTOREDPROC>
		

    	<cfset result.forecast=rsForecasts.sentence>
    
    <cfreturn result>
    
    </cffunction>

</cfcomponent>