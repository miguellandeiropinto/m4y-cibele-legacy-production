<cfprocessingdirective pageencoding="utf-8">


<cfif isDefined("URL.utm_source")>
	<cflock timeout=20 scope="Session" type="Exclusive"> 
		<cfset session.sourceID=URL.utm_source>
	</cflock>
<cfelse>
	<cflock timeout=20 scope="Session" type="Exclusive"> 
		<cfset session.sourceID="direct">
	</cflock>
</cfif>


<cfif isDefined("URL.utm_medium")>
	<cflock timeout=20 scope="Session" type="Exclusive"> 
		<cfset session.mediumID=URL.utm_medium>
	</cflock>
</cfif>


<cfif isDefined("URL.utm_campaign")>
	<cflock timeout=20 scope="Session" type="Exclusive"> 
		<cfset session.campaignID=URL.utm_campaign>
	</cflock>
</cfif>
