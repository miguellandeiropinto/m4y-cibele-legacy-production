<cfprocessingdirective pageencoding="utf-8">
<cfset requestMethod = #cgi.request_method#/>
<!DOCTYPE html>
<html>
<head>
    <title>Prepare For Campaign</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.7.1/clipboard.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>

        body {
            font-family: 'Arial';
            width: 100%;
            background: #ededed;
        }

        form::after {
            display: block;
            content: '';
            clear: both;
        }

        form p span {
            color: #4d4d4d;
            display: block;
            margin: 3px 0px;
            font-size: 9pt;
        }
        span.info {
            color: #333;
            font-size: 10pt;
            font-weight: bold;
            float: right;
            margin-top: 5px;
        }

        form p span::before {
            content: "Ex: ";
        }

        form p input {
            width: 400px;
            padding: 5px 10px;
            width: 100%;
        }

        form p label {
            font-weight: bold;
            display: block;
            margin: 3px 0px;
        }

        form a {
            display: inline-block;
            max-width: 100%;
        }

        form input[type="submit"] {
            width: auto;
        }

        h2 {
            margin-bottom: 30px;
            font-weight: bold;
        }

        label {
            display: block;
            font-weight: bold;
            margin-bottom: 3px;
        }


        .result a {
            display: block;
            max-width: 100%;
            width: 100%;
            margin: 0px;
            padding: 0px;
        }

        .result p {
            word-wrap: break-word;
        }
        .table {
            display: table;
            height: 100VH;
        }
        .table-cell {
            display: table-cell;
            width: 100%;
            vertical-align: middle;
        }
        span.info::before {
            content: '* ';
            color: darkred;
            font-weight: bold;
            font-size: 13pt;
        }
        table {
            width: 100%;
        }
        table th {
            background-color: #337ab7;
            color: #fff;
        }
        table th, table td{
            padding: 5px 20px;
            border-bottom: #ccc 1px solid;
            vertical-align: middle;
        }

        button.remove {
            background-color: darkred;
            color: #fff;
            font-weight: bold;
            border: none;
        }
        input[type="number"] {
            width: 80px;
            display: inline-block;
            margin: 0px 10px;

        }
        span.per_page {
            font-weight: bold;
            display: inline-block;

        }
        h1 {
            font-weight: bold;
        }
        h1 small {
            font-size: 14pt;
        }
        h1 small a {
            text-decoration: none;
            display: inline-block;
            padding: 8px;
            border-radius: 5px;
            border: #fff 2px solid;
            color: #fff;
            font-weight: bold;
            background: #337ab7;
        }

        .table-container {
            margin: 30px 0px;
        }

        #rsTable td {
            background: #fff;
        }

        span.found {float: right; font-size: 9pt;}

        .c-info {
            margin: 30px 0px;
            padding: 0px;
        }
        .c-info li {
            list-style: none;
            border-right: #ccc 1px solid;
            margin-bottom: 15px;
            background: #fff;
            min-height: 95px;
            padding: 0px;
        }

        .c-info li label {
            background: #ccc;
            padding: 5px 10px;

        }
        .c-info li p {
            padding: 5px 10px;

        }

        p {
         word-wrap: break-word;

        }

        #conversion_filter_form input, #conversion_filter_form select, #conversion_filter_form span
        {
            display: inline-block;
        }
        #conversion_filter_form input, #conversion_filter_form select{
            width: 160px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <cfquery name="rsCampaign">
            SELECT * FROM campaigns_history WHERE id= #URL.cid#
        </cfquery>
<cfquery name="rsReport">
            -- get conversions volume filtered where fields...
            SELECT CH.campaignID as campaignID,
                    count(*) as conversions,
                    sum(CK.opens) as opens,
                    sum(CK.clicks) as clicks,
                    CH.currency as currency,
                    sum(CH.revenue) as profit,
                    CH.sourceID as source,
                    CH.mediumID as medium
            FROM lead_conversions LC
            INNER JOIN conversions_keygen CK ON CK.genkey = LC.leadID
            INNER JOIN campaigns_history CH ON CH.id = CK.campaignID
            -- filter where...
            WHERE
                CH.id = #URL.cid#
                and IsNumeric(LC.leadID) = 1
            GROUP BY CH.campaignID, CH.currency, CH.sourceID, CH.mediumID
        </cfquery>
        <cfoutput>
            <h1>Campaign History | <small>###URL.cid# #rsCampaign.campaignID#</small></h1>
        </cfoutput>

        <div class="table-container">
            <ul class="c-info">
                <cfoutput>
                    <li class="col-sm-2">
                        <label>Internal ID</label>
                        <p>#rsCampaign.id#</p>
                    </li>

                    <li class="col-sm-3">
                        <label>Campaign ID</label>
                        <p>#rsCampaign.campaignID#</p>
                    </li>

                    <li class="col-sm-3">
                        <label>Vertical</label>
                        <p>#rsCampaign.vertical#</p>
                    </li>
                    <li class="col-sm-2">
                        <label>Currency</label>
                        <p>#rsCampaign.currency#</p>
                    </li>

                    <li class="col-sm-2">
                        <label>Revenue P/Conversion</label>
                        <p>#rsCampaign.revenue#</p>
                    </li>
                    <li class="col-sm-1">
                        <label>Source</label>
                        <p>#rsCampaign.sourceID#</p>
                    </li>
                    <li class="col-sm-1">
                        <label>Medium</label>
                         <p>#rsCampaign.mediumID#</p>
                    </li>

                    <li class="col-sm-5">
                        <label>Campaing URL</label>
                    <p>#rsCampaign.generatedURL#</p>
                    </li>

                    <li class="col-sm-5">
                        <label>Client URL</label>
                    <p>#rsCampaign.campaignURL#</p>
                    </li>

                    <li class="col-sm-1">
                        <label>Opens</label>
                    <p>#rsReport.opens#</p>
                    </li>

                    <li class="col-sm-1">
                        <label>Clicks</label>
                        <p>#rsReport.clicks#</p>
                    </li>

                    <li class="col-sm-2">
                    <label>Profit</label>
                <p>#rsReport.profit# #rsCampaign.currency#</p>
                </li>
                </cfoutput>
            </ul>
        </div>
    </div>
    <div class="row">
        <h2>Conversions Report</h2>

        <cfquery name="rsLeads">
            SELECT LF.leadID, LF.lead_interest, LF.lead_tstamp, LC.conversion_date, LC.baseID
            FROM campaigns_history CH
            INNER JOIN conversions_keygen CK ON CK.campaignID = CH.id
            INNER JOIN lead_conversions LC ON LC.leadID = CK.genkey
            INNER JOIN lead_feed LF ON LF.leadID = CK.leadID
            WHERE CH.id = #URL.cid#
        </cfquery>
<form method="post" id="conversion_filter_form">
    <span>From: </span>
    <input class="form-control" type="date" name="from">
    <span>To: </span>
    <input class="form-control" type="date" name="to">
    <button class="btn btn-primary">Filtrar</button>
</form>
    <cfoutput>
        <div class="table-container">
        <span class="found">#rsLeads.recordcount# conversions found.</span>
        <table id="rsLeads">
            <thead>
                <th>Lead ID</th>
                <th>Conversion Date</th>
                <th>Interesse</th>
                <th>Fornecedor</th>
            </thead>
            <tbody>
            <cfloop query="rsLeads">
                <tr>
                    <td>#rs.leadID#</td>
                    <td>#rs.conversion_date#</td>
                    <td>#rs.lead_interest#</td>
                    <td>#rs.baseID#</td>
                </tr>
            </cfloop>
            </tbody>
        </table>
            </div>
    </cfoutput>

<cfdump var=#rsReport#>
    </div>
</div>
<script>
    $(function () {
        $(document).on("click", "#addFilterBtn", function (e) {
            var txt = $("#filter_row").html();
            var html = txt.slice(4, txt.length - 3);
            $("#filter_table").append($(html));
        });

        $(document).on("click", "button.remove", function (e) {
            $(this).closest("tr").remove();
        });
    });
</script>
</form>
</body>
</html>
