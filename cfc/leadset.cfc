<cfcomponent>
	<cfsetting requesttimeout="9000">
<cfprocessingdirective pageencoding="utf-8">

	<cffunction name="insertEmailPingLog" access="remote">
		<cfparam name="apikey" default="" />
		<cfif #URL.apikey# eq "117def1af97b25ff73e1e786e10e82fb">
			<CFSTOREDPROC procedure="dbo.InsertEmailPingLog">
				<CFPROCPARAM type="IN" value="#URL.leadID#" cfsqltype="CF_SQL_VARCHAR">
				<CFPROCPARAM type="IN" value="#URL.status#" cfsqltype="CF_SQL_VARCHAR">
				<CFPROCPARAM type="IN" value="#URL.accuracy#" cfsqltype="CF_SQL_VARCHAR">
				<CFPROCPARAM type="IN" value="#URL.suggestion#" cfsqltype="CF_SQL_VARCHAR">
				<CFPROCPARAM type="IN" value="#URL.result#" cfsqltype="CF_SQL_VARCHAR">
				<CFPROCPARAM type="IN" value="#URL.rawResponse#" cfsqltype="CF_SQL_VARCHAR">
				<CFPROCRESULT name="rsLead" resultset="1">
			</CFSTOREDPROC>
		</cfif>
	</cffunction>


	<cffunction name="getLeadById" access="remote" returnformat="json">
		<cfparam name="URL.leadID" default="" />
		<cfif #URL.leadID# neq "">
			<cfquery name="rsLead">
				SELECT
					leadID,
					lead_email,
					lead_name,
					lead_surname,
					lead_gender,
					lead_zip,
					lead_phone,
					lead_country,
					lead_ip,
					convert(varchar(25), lead_tstamp, 120) as lead_tstamp,
					convert(varchar(25), lead_tstamp_upd, 120) as lead_tstamp_upd,
					lead_interest,
					lead_sign,
					convert(varchar(25), lead_birthdate, 120) as lead_birthdate
				FROM lead_feed WHERE leadID = '#URL.leadID#'
			</cfquery>
		</cfif>
		<cfset result2.lead=rsLead/>
		<cfreturn result2/>
	</cffunction>




	<cffunction name="setMailchimpID" access="remote" returnformat="json">
		<cfparam name="URL.leadID" default="" />
		<cfparam name="URL.mailchimpID" default="" />
		<cfif #URL.leadID# neq "">
			<cfquery name="rsMcID">
				UPDATE lead_feed SET mailchimpID = '#URL.mailchimpID#' WHERE leadID = '#URL.leadID#'
			</cfquery>
			<cfquery name="rsAttempt">
				insert into lead_integration_attempts (campaignID, leadID, tstamp, statusID, result)
				values ('mailchimp-0025', '#URL.leadID#', getdate(), 1, 'OK')
			</cfquery>
			<cfquery name="rsInt">
				insert into lead_integration (campaignID, leadID, tstamp, statusID)
				values ('mailchimp-0025', '#URL.leadID#', getdate(), 1)
			</cfquery>
		</cfif>
		<cfset result2.leadID=#URL.leadID#/>
		<cfreturn result2/>
	</cffunction>



    <cffunction name="getLeadset" access="remote" returnformat="json">
		<cfparam name="apikey" default="" />
		<cfif #URL.apikey# eq "117def1af97b25ff73e1e786e10e82fb">
			<cfquery name="rsLeads">
				SELECT TOP #URL.nof# leadID, lead_email, egoiID  FROM lead_feed ORDER BY NEWID()
			</cfquery>
		</cfif>

        <cfset result=rsLeads>

    <cfreturn result>
    
    </cffunction>

</cfcomponent>