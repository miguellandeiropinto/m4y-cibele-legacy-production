<cfcomponent>
<cfprocessingdirective pageencoding="utf-8">


	<cffunction name="pco" access="remote">

		<cfparam name="URL.p" default=""> <!--- lead / clickID --->
		<cfparam name="URL.c" default=""> <!--- our campaign ID --->
		<cfparam name="URL.r" default=""> <!--- revenue ID --->
		<cfparam name="URL.b" default=""> <!--- base ID / partner / supplier --->
		<cfparam name="URL.t" default=""> <!--- type --->

		<!--- STORE CONVERSION ON DB CONVERSION LOGS --->
		<CFSTOREDPROC procedure="dbo.conversionlog">
			<CFPROCPARAM type="IN" value="#URL.p##URL.c##URL.r##URL.b##URL.t#" cfsqltype="CF_SQL_VARCHAR">
		</CFSTOREDPROC>

		<!--- GET MATCH OF CONVERSION ON KEYGENS TABLE --->
		<cfif isNumeric(#URL.p#)>
			<cfquery name="rsConversion">
				SELECT * FROM conversions_keygen WHERE genkey = #URL.p#
			</cfquery>
			<!--- CHECK MEDIUM AND TRIGGER CONVERSION ON PLATFORM --->
			<cfif #rsConversion.recordcount# gt 0>
				<cfloop query="rsConversion">
					<cfset m = #replace(rsConversion.mediumID, " ", "", "all")#>
					<cfif m eq "aw">
						<cfoutput>Adwords Conversion</cfoutput>
						<cfelseif m eq "fb">
						<cfoutput>Facebook Conversion</cfoutput>
					</cfif>
				</cfloop>
			</cfif>
		<cfelse>
			<!---
			<cfquery name="rsConversion">
				SELECT * FROM conversions_keygen WHERE genkey = '#URL.p#'
			</cfquery>
			--->
		</cfif>





      	<cfset hashH=""/>

		<cfif ( #URL.b# eq "directo" AND FindNoCase("display", URL.p) AND FindNoCase("fb", URL.p) ) OR
			(#URL.b# eq "BB" and FindNoCase("display", URL.p) and FindNoCase("fb", URL.p)) OR
				(#URL.b# eq "ISPC" and FindNoCase("display", URL.p) and FindNoCase("fb", URL.p))>
			<cfoutput>
                <img height="1" width="1" style="display:none"
                        src="https://www.facebook.com/tr?id=1388974591180294&ev=#URL.c#"
				/>
				FB pixel fired!
			</cfoutput>
			<cfset hashH=CreateUUID()/>
		</cfif>

		<!--- STORE CONVERSION ON dco_conversions TABLE --->
		<CFSTOREDPROC procedure="dbo.DirectClientConversion">
			<CFPROCPARAM type="IN" value="#ToString(URL.p)##hashH#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#URL.c#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#URL.b#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#ToString(URL.r)#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#ToString(URL.t)#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="NOT_DEFINED" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="NOT_DEFINED" cfsqltype="CF_SQL_VARCHAR">
		</CFSTOREDPROC>
		
		<!--- STORE CONVERSION ON lead_conversions TABLE --->
		<CFSTOREDPROC procedure="dbo.pconversion">
		  <CFPROCPARAM type="IN" value="#ToString(URL.p)##hashH#" cfsqltype="CF_SQL_VARCHAR">
		  <CFPROCPARAM type="IN" value="#URL.c#" cfsqltype="CF_SQL_VARCHAR">
		  <CFPROCPARAM type="IN" value="#ToString(URL.r)#" cfsqltype="CF_SQL_VARCHAR">
		  <CFPROCPARAM type="IN" value="#URL.b#" cfsqltype="CF_SQL_VARCHAR">
		  <CFPROCPARAM type="IN" value="#URL.t#" cfsqltype="CF_SQL_VARCHAR">
		</CFSTOREDPROC>

    </cffunction>



</cfcomponent>