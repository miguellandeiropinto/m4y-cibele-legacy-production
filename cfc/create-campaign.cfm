<cfprocessingdirective pageencoding="utf-8">
<cfset requestMethod = #cgi.request_method#/>
<!DOCTYPE html>
<html>
<head>
    <title>Prepare For Campaign</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.7.1/clipboard.min.js"></script>
    <style>

        body {
            font-family: 'Arial';
            width: 100%;
            background: #ededed;
        }

        form {
            padding: 20px;
            background-color: #ccc;
            margin: 10px;
        }

        form::after {
            display: block;
            content: '';
            clear: both;
        }

        form p span {
            color: #4d4d4d;
            display: block;
            margin: 3px 0px;
            font-size: 9pt;
        }
        span.info {
            color: #333;
            font-size: 10pt;
            font-weight: bold;
            float: right;
            margin-top: 5px;
        }

        form p span::before {
            content: "Ex: ";
        }

        form p input {
            width: 400px;
            padding: 5px 10px;
            width: 100%;
        }

        form p label {
            font-weight: bold;
            display: block;
            margin: 3px 0px;
        }

        form a {
            display: inline-block;
            max-width: 100%;
        }

        form input[type="submit"] {
            width: auto;
        }

        h2 {
            text-align: center;
            margin-bottom: 30px;
            font-weight: bold;
            color: #337ab7;
        }

        label {
            display: block;
            font-weight: bold;
            margin-bottom: 3px;
        }


        .result a {
            display: block;
            max-width: 100%;
            width: 100%;
            margin: 0px;
            padding: 0px;
        }

        .result p {
            word-wrap: break-word;
        }
        .table {
            display: table;
            height: 100VH;
        }
        .table-cell {
            display: table-cell;
            width: 100%;
            vertical-align: middle;
        }
        span.info::before {
            content: '* ';
            color: darkred;
            font-weight: bold;
            font-size: 13pt;
        }
    </style>
</head>
<body>
<div class="table"><div class="table-cell">
<cfform method="post" class="col-sm-6 col-sm-offset-3" preservedata="yes">
    <h2>Prepare URL For Campaign</h2>
    <div class="col-sm-12">
    <div class="col-sm-12">
    <p>
        <label>Campaign ID</label>
    <cfinput class="form-control" type="text" name="cid">
    </p>
    </div>
    <div class="col-sm-4">
    <p>
        <label>Revenue P/Conversion</label>
    <cfinput class="form-control" type="text" name="r">
    </p>
    </div>
    <div class="col-sm-2">
    <p>
        <label>Currency</label>
        <cfselect class="form-control" type="text" name="rcurr">
            <option value="EUR">€</option>
            <option value="REAL">$ - BR</option>
            <option value="ARS">$ - AR</option>
            <option value="MXN">$ - MX</option>
            <option value="CLP">$ - CL</option>
        </cfselect>
    </p>
    </div>
    <div class="col-sm-6">
    <p>
        <label>Vertical</label>
        <cfinput class="form-control" type="text" name="v">
    </p>
    </div>
    <div class="col-sm-6">
    <p>
        <label>Source</label>
    <cfinput class="form-control" type="text" name="sid">
        <span>email / social / organic ...</span>
    </p>
    </div>
    <div class="col-sm-6">
    <p>
        <label>Medium</label>
    <cfinput class="form-control" type="text" name="mid">
        <span>aw / fb / bing ...</span>
    </p>
    </div>
    <div class="col-sm-12">
    <p>
        <label>Campaign / Conversion Page URL</label>
    <cfinput class="form-control" type="url" name="uri">
    </p>
    </div>
    <div class="col-sm-6">
    <p>
        <label>External ID for Campaign</label>
        <cfinput class="form-control" type="text" name="extid">
        <span>Egoi campaign ID / Mailchimp campaign ID ...</span>
    </p>
    </div>
        <div class="col-sm-12">
            <p>
                <input class="btn btn-primary" type="Submit" name="Submeter">
                <span class="info">Replace the string __LEADID__ on the generated URL with the valid lead id.
                    If a lead id isn't available (display campaign on social media) replace with "unknown" or remove the parameter "lid".</span>
            </p>
        </div>
    </div>
</cfform>
<div class="col-sm-6 col-sm-offset-3 result">

<cfif requestMethod eq 'POST'>

    <cfstoredproc procedure="dbo.CreateCampaignHistory">
        <cfprocparam type="in" value="#FORM.cid#" cfsqltype="CF_SQL_VARCHAR">
        <cfprocparam type="in" value="#FORM.r#" cfsqltype="CF_SQL_FLOAT">
        <cfprocparam type="in" value="#FORM.rcurr#" cfsqltype="CF_SQL_VARCHAR">
        <cfprocparam type="in" value="#FORM.v#" cfsqltype="CF_SQL_VARCHAR">
        <cfprocparam type="in" value="#FORM.sid#" cfsqltype="CF_SQL_VARCHAR">
        <cfprocparam type="in" value="#FORM.mid#" cfsqltype="CF_SQL_VARCHAR">
        <cfprocparam type="in" value="#FORM.uri#" cfsqltype="CF_SQL_VARCHAR">
        <cfprocparam type="in" value="#FORM.extid#" cfsqltype="CF_SQL_VARCHAR">
        <cfprocparam type="in" value="aaa" cfsqltype="CF_SQL_VARCHAR">
        <cfprocparam type="out" variable="cKey" cfsqltype="CF_SQL_INTEGER">
    </cfstoredproc>



    <cfset genurl = "http://www.videntecibele.com/cfc/trackingKeygen.cfc?method=generate&lid=__LEADID__&cid=#cKey#">
    <cfquery name="insertUrl">
        UPDATE campaigns_history SET generatedURL = '#genurl#' WHERE id = #cKey#
    </cfquery>
        <p>
            <label>Generated URL:
                <button class="btn btn-xs btn-default" id="copyto">Copy to clipboard</button>
            </label>
            <a id="linkgen">
                <cfoutput>#genurl#</cfoutput>
            </a>
        </p>
        <script>

            var clipboard = new Clipboard('#copyto', {
                target: function() {
                    return document.querySelector('#linkgen');
                }
            });

            clipboard.on('success', function(e) {
                alert("Copied to clipboard!")
            });

        </script>
</cfif>
</div>
</div></div>
</body>
</html>