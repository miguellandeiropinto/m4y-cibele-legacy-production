<cfcomponent>
	<cfsetting requesttimeout="15000">
<cfprocessingdirective pageencoding="utf-8">


	<cffunction name="leadset4autointegration" access="remote">

		<cfparam name="URL.nof" default="1">
		<cfparam name="URL.order" default="ASC">
		<cfparam name="URL.campaign" default="">
		<cfparam name="URL.ccampaignID" default="">
		<cfparam name="URL.year" default="">
		<cfparam name="URL.show" default="">

		<cfset thecampaigns=StructNew()>
		<cfset StructInsert(thecampaigns, 'tara-coreg-pt-0001', 	1)>
		<cfset StructInsert(thecampaigns, 'tara-coreg-br-0002', 	1)>
		<cfset StructInsert(thecampaigns, 'maria-coreg-pt-0003', 	1)>
		<cfset StructInsert(thecampaigns, 'maria-coreg-br-0004', 	0)>
		<cfset StructInsert(thecampaigns, 'estrela-coreg-br-0005', 	0)>
		<cfset StructInsert(thecampaigns, 'webpilots-coreg-pt-0006',0)>
		<cfset StructInsert(thecampaigns, 'bonotour-coreg-pt-0007', 0)>
		<cfset StructInsert(thecampaigns, 'padre-coreg-pt-0008', 	1)>
		<cfset StructInsert(thecampaigns, 'padre-coreg-br-0009', 	0)>
		<cfset StructInsert(thecampaigns, 'tara-coreg-es-0010', 	1)>
		<cfset StructInsert(thecampaigns, 'tara-coreg-ar-0011', 	1)>
		<cfset StructInsert(thecampaigns, 'maria-coreg-es-0012', 	1)>
		<cfset StructInsert(thecampaigns, 'maria-coreg-ar-0013', 	1)>
		<cfset StructInsert(thecampaigns, 'padre-coreg-es-0014', 	0)>
		<cfset StructInsert(thecampaigns, 'padre-coreg-ar-0015', 	0)>
		<cfset StructInsert(thecampaigns, 'easyvoyage-spons-es-0016', 1)>
		<cfset StructInsert(thecampaigns, 'adeslas-coreg-es-0017', 1)>
		<cfset StructInsert(thecampaigns, 'rcf-coreg-es-0018', 1)>
		<cfset StructInsert(thecampaigns, 'maria-coreg-cl-0020', 1)>
		<cfset StructInsert(thecampaigns, 'tara-coreg-cl-0022', 0)>
		<cfset StructInsert(thecampaigns, 'padre-coreg-cl-0024', 0)>
		<cfset StructInsert(thecampaigns, 'maria-coreg-mx-0019', 1)>
		<cfset StructInsert(thecampaigns, 'tara-coreg-mx-0021', 0)>
		<cfset StructInsert(thecampaigns, 'padre-coreg-mx-0023', 0)>
		<cfset StructInsert(thecampaigns, 'abanca-coreg-es-0025', 1)>

		<cfquery name="rsCCaps">
			SELECT campaign_qtt as qty FROM client_campaigns WHERE campaignID='#URL.ccampaignID#'
		</cfquery>

		<cfquery name="rsCap">
			SELECT count(*) as total FROM lead_integration WHERE statusID = 1 AND
				campaignID = '#URL.ccampaignID#'
				and tstamp >= '2017-07-31'
		</cfquery>

		<cfif #rsCap.total# gte #rsCCaps.qty#>
			<cfoutput>
				#URL.ccampaignID# Caps DONE!
			</cfoutput>
			<cfquery name="integrationCapDone">
				INSERT INTO lead_integration_logs (campaignID, recordCount, int_log, tstamp)
				VALUES ('#URL.ccampaignID#', 0, 'CAPPED', GETDATE())
			</cfquery>
			<cfexit>
		</cfif>

		<cfquery name="rsLeadSet">
		
			<cfif thecampaigns['#URL.ccampaignID#'] eq 1>
				select top #URL.nof# * 
				from lead_feed 
				where 
				leadID in (select leadID from lead_integration where campaignID='egoi-0000' and statusID=1)
				and egoiID is not null
				and leadID not in (select leadID from lead_integration where campaignID='#URL.ccampaignID#')
				and lead_country=(select campaign_country from client_campaigns where campaignID='#URL.ccampaignID#')
            	and leadID not in (select leadID from lead_integration WHERE statusID=1 and
					campaignID not in ('egoi-0001', 'egoi-0000', 'adeslas-coreg-es-0017', 'easyvoyage-spons-es-0016', 'rcf-coreg-es-0018') AND DATEDIFF(day, tstamp, getdate()) = 0)
				and rtrim(ltrim(lead_name))<>'' and  rtrim(ltrim(lead_surname))<>'' and rtrim(ltrim(lead_gender))<>''
				and rtrim(ltrim(lead_interest))<>''
				<cfif URL.year neq ''>
				and year(lead_tstamp)='#URL.year#'	
				</cfif>
				order by lead_tstamp #URL.order#
			<cfelse>
				select * from lead_feed where 0=1
			</cfif>
		
		</cfquery>


		<cfif ListFind('padre-coreg-pt-0008,padre-coreg-br-0009,padre-coreg-es-0014,padre-coreg-ar-0015', URL.ccampaignID) gt 0>
			
			<!--- GET TOKEN FOR PADRE --->
			<cfset loginFields = {'email': 'andre.amorim@yoursporto.com','password': '**Yours2016'} >   

			<cfhttp url="https://api.mensagens-dos-anjos.com/api/Partners/login" method="post" result="cresult" timeout="60">
			    <cfhttpparam type="header" name="Content-Type" value="application/json" />
			    <cfhttpparam type="body" value="#serializeJSON(loginFields)#">
			</cfhttp>

			<cfset tempvar=deserializeJSON(cresult.filecontent)>
			<cfset login_token=tempvar.id>


		<cfelse>

			<cfset login_token="">

		</cfif>

		<cfif URL.show eq 'yes'>
		<cfdump var="#rsLeadSet#">
		</cfif>

		<cfquery name="integrationLog">
			INSERT INTO lead_integration_logs (campaignID, recordCount, int_log, tstamp)
			VALUES ('#URL.ccampaignID#', #rsLeadSet.recordcount#, 'TRY', GETDATE())
		</cfquery>

		<cfloop query="rsLeadSet">


			<cfif 
				rsLeadSet.lead_country eq 'PT' or
				rsLeadSet.lead_country eq 'BR' or
				rsLeadSet.lead_country eq 'ES' or
				rsLeadSet.lead_country eq 'AR' or
				rsLeadSet.lead_country eq 'CL' or
				rsLeadSet.lead_country eq 'MX'>

				<cfinvoke component="#rsLeadSet.lead_country#" method="doIntegrationClients" leadID="#rsLeadSet.leadID#" ccampaignID="#URL.ccampaignID#" token="#login_token#" returnvariable="invokeresult">

				<cfif StructKeyExists(invokeresult, "statusCode")>
					
	            <!--- get results --->

	                <cfif invokeresult.statuscode neq ''>
	                    
	                    <cfif URL.show eq 'yes'>
	                    <cfoutput>RESULT FROM #UCASE(ccampaignID)#<br></cfoutput>
	                    <cfdump var="#invokeresult#">
	                    </cfif>

	                    <cfif trim(invokeresult.filecontent) eq 'ok' or (FindNoCase('OK', invokeresult.filecontent) gt 0 and FindNoCase('book', invokeresult.filecontent) eq 0) or FindNoCase('"errorCode":0,', invokeresult.filecontent) gt 0 or FindNoCase('created', invokeresult.filecontent) gt 0 >
	                        <cfset statusID=10>
	                        <cfset int_statusID=1>
	                        <cfif URL.show eq 'yes'>
	                        <cfoutput>LEAD SUCCESSFULY INTEGRATED ON #URL.ccampaignID#</cfoutput>
	                        </cfif>
	                    <cfelseif FindNoCase('já está', invokeresult.filecontent) gt 0 or FindNoCase('já estão', invokeresult.filecontent) gt 0 or FindNoCase('already exist', invokeresult.filecontent) gt 0 or FindNoCase('"errorCode":10,', invokeresult.filecontent) gt 0 or FindNoCase('"errorCode":20,', invokeresult.filecontent) gt 0 or FindNoCase('"errorCode":30,', invokeresult.filecontent) gt 0 or FindNoCase('duplicate', invokeresult.filecontent) gt 0 or FindNoCase('KO - 0001', invokeresult.filecontent) gt 0 or FindNoCase('Repetido', invokeresult.filecontent) gt 0 or FindNoCase('L345', invokeresult.filecontent) gt 0 or FindNoCase('L054', invokeresult.filecontent) gt 0 or FindNoCase('Err. 004', invokeresult.filecontent) gt 0>
	                        <!--- <cfif rsOthSuccessInt.recordcount eq 0><cfset statusID=5><cfelse><cfset statusID=rsLeadSet.statusID></cfif> --->
	                        <cfset int_statusID=0>
	                        <cfif URL.show eq 'yes'>
	                        <cfoutput>LEAD NOT INTEGRATED ON #URL.ccampaignID# >#invokeresult.filecontent#</cfoutput>
	                        </cfif>
	                    <cfelseif FindNoCase('"email"', invokeresult.filecontent) gt 0 or FindNoCase('email validation', invokeresult.filecontent) gt 0 or FindNoCase('bad', invokeresult.filecontent) gt 0 or FindNoCase('mailbox', invokeresult.filecontent) gt 0 or FindNoCase('"errorCode":103,', invokeresult.filecontent) gt 0 or FindNoCase('"errorCode":104,', invokeresult.filecontent) gt 0 or FindNoCase('KO - 0002', invokeresult.filecontent) gt 0  or FindNoCase('L351', invokeresult.filecontent) gt 0 or FindNoCase('L801', invokeresult.filecontent) gt 0 or FindNoCase('Err. 003', invokeresult.filecontent) gt 0>
	                        <!--- <cfif rsOthSuccessInt.recordcount eq 0><cfset statusID=-1><cfelse><cfset statusID=rsLeadSet.statusID></cfif> --->
	                        <cfset int_statusID=-1>
	                        <cfif URL.show eq 'yes'>
	                        <cfoutput>LEAD NOT INTEGRATED ON #URL.ccampaignID# >#invokeresult.filecontent#</cfoutput>
	                        </cfif>
	                    <cfelse>
	                        <!--- <cfif rsOthSuccessInt.recordcount eq 0><cfset statusID=-1><cfelse><cfset statusID=rsLeadSet.statusID></cfif>--->
	                        <cfset int_statusID=-2>
	                        <cfif URL.show eq 'yes'>
	                        <cfoutput>LEAD NOT INTEGRATED ON #URL.ccampaignID# >#invokeresult.filecontent#</cfoutput>
	                        </cfif>
	                    </cfif>
	                    <br>

	                <cfelse>

	                    <!--- doesn't satisfy campaign 2nd+ rule --->
	                    <!---<cfset cresult=StructNew()>--->
	                    <cfset statusID=rsLeadSet.statusID>
	                    <cfset int_statusID=-3>
	                    <cfset invokeresult.filecontent='it does not satisfy campaign 2nd+ rule'>
	                    <cfif URL.show eq 'yes'>
	                    <cfoutput>DOES NOT SATISFY CAMPAIGN 2nd RULES FOR #UCASE(ccampaignID)#<br></cfoutput>
	                    </cfif>

	                </cfif>
	                

	            <!--- save actions --->
	                <cfquery name="int_p1">
	                    insert into lead_integration_attempts (campaignID, leadID, tstamp, statusID, result)
	                    values ('#URL.ccampaignID#', '#rsLeadSet.leadID#', getdate(), #int_statusID#, '#invokeresult.filecontent#')
	                </cfquery>

	                <cftry>
	                    <cfquery name="int_p2">
	                        insert into lead_integration (campaignID, leadID, tstamp, statusID)
	                        values ('#URL.ccampaignID#', '#rsLeadSet.leadID#', getdate(), #int_statusID#)
	                    </cfquery>
	                <cfcatch>
	                    Integration Error
	                </cfcatch>  
	                </cftry>


	                <cfquery name="int_p3">
	                    update lead_feed set statusID=#statusID# where leadID='#rsLeadSet.leadID#'
	                </cfquery>

				</cfif>

			</cfif>


		</cfloop>


	</cffunction>

</cfcomponent>