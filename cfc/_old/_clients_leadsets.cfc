<cfcomponent>
<cfprocessingdirective pageencoding="utf-8">


<!---
'tara-coreg-pt-0001', 
'tara-coreg-br-0002', 
'maria-coreg-pt-0003', 
'webpilots-coreg-pt-0006',
'padre-coreg-pt-0008',
'padre-coreg-br-0009',
'tara-coreg-es-0010',
'tara-coreg-ar-0011', 
'maria-coreg-es-0012',
'maria-coreg-ar-0013',
'padre-coreg-es-0014',
'padre-coreg-ar-0015', 
'easyvoyage-spons-es-0016'
<!--- 'maria-coreg-br-0004', 'bonotour-coreg-pt-0007' 'estrela-coreg-br-0005', --->
--->

	<cffunction name="leadset4autointegration" access="remote">

		<cfparam name="URL.nof" default="1">
		<cfparam name="URL.order" default="ASC">
		<cfparam name="URL.campaign" default="">
		<cfparam name="URL.ccampaignID" default="">

		<cfset thecampaigns=StructNew()>
		<cfset StructInsert(thecampaigns, 'tara-coreg-pt-0001', 	1)>
		<cfset StructInsert(thecampaigns, 'tara-coreg-br-0002', 	1)>
		<cfset StructInsert(thecampaigns, 'maria-coreg-pt-0003', 	1)>
		<cfset StructInsert(thecampaigns, 'maria-coreg-br-0004', 	1)>
		<cfset StructInsert(thecampaigns, 'estrela-coreg-br-0005', 	0)>
		<cfset StructInsert(thecampaigns, 'webpilots-coreg-pt-0006',0)>
		<cfset StructInsert(thecampaigns, 'bonotour-coreg-pt-0007', 0)>
		<cfset StructInsert(thecampaigns, 'padre-coreg-pt-0008', 	1)>
		<cfset StructInsert(thecampaigns, 'padre-coreg-br-0009', 	1)>
		<cfset StructInsert(thecampaigns, 'tara-coreg-es-0010', 	1)>
		<cfset StructInsert(thecampaigns, 'tara-coreg-ar-0011', 	1)>
		<cfset StructInsert(thecampaigns, 'maria-coreg-es-0012', 	1)>
		<cfset StructInsert(thecampaigns, 'maria-coreg-ar-0013', 	1)>
		<cfset StructInsert(thecampaigns, 'padre-coreg-es-0014', 	1)>
		<cfset StructInsert(thecampaigns, 'padre-coreg-ar-0015', 	1)>
		<cfset StructInsert(thecampaigns, 'easyvoyage-spons-es-0016', 1)>

		<cfquery name="rsLeadSet">
		
			<cfif thecampaigns['#URL.ccampaignID#'] eq 1>
				select top #URL.nof# * 
				from lead_feed 
				where 
				leadID in (select leadID from lead_integration where campaignID='egoi-0000' and statusID=1)
				and egoiID is not null
				and leadID not in (select leadID from lead_integration where campaignID='#URL.ccampaignID#')
				and lead_country=(select campaign_country from client_campaigns where campaignID='#URL.ccampaignID#')
				and rtrim(ltrim(lead_name))<>'' and  rtrim(ltrim(lead_surname))<>'' and rtrim(ltrim(lead_gender))<>''
				and rtrim(ltrim(lead_interest))<>''
				and year(lead_tstamp)>=2017
				order by lead_tstamp #URL.order#
			<cfelse>
				select * from lead_feed where 0=1
			</cfif>

		
		</cfquery>	


		<cfif ListFind('padre-coreg-pt-0008,padre-coreg-br-0009,padre-coreg-es-0014,padre-coreg-ar-0015', URL.ccampaignID) gt 0>
			
			<!--- GET TOKEN FOR PADRE --->
			<cfset loginFields = {'email': 'andre.amorim@yoursporto.com','password': '**Yours2016'} >   

			<cfhttp url="https://api.mensagens-dos-anjos.com/api/Partners/login" method="post" result="cresult" timeout="60">
			    <cfhttpparam type="header" name="Content-Type" value="application/json" />
			    <cfhttpparam type="body" value="#serializeJSON(loginFields)#">
			</cfhttp>

			<cfset tempvar=deserializeJSON(cresult.filecontent)>
			<cfset login_token=tempvar.id>


		<cfelse>

			<cfset login_token="">

		</cfif>

		<cfloop query="rsLeadSet">

			<cfinvoke component="integration_actions" method="doIntegrationClients" leadID="#rsLeadSet.leadID#" ccampaignID="#URL.ccampaignID#" token="#login_token#">

		</cfloop>


	</cffunction>

</cfcomponent>