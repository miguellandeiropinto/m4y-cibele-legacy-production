<cfcomponent>
<cfprocessingdirective pageencoding="utf-8">


	<cffunction name="doIntegrationClients" access="remote" returnformat="json">
		<cfargument name="leadID" type="any" required="false" /> 

		<cfparam name="URL.leadID" default="">
		<cfparam name="URL.ccampaignID" default="">
		<cfparam name="URL.show" default="yes">
		<cfparam name="URL.token" default="">

		<cfif ARGUMENTS.leadID neq "">
			<cfset URL.leadID=ARGUMENTS.leadID>
		</cfif>

		<CFSTOREDPROC procedure="dbo.getLeadsforIndividualIntegration">
		  <CFPROCPARAM type="IN" value="#URL.leadID#" cfsqltype="CF_SQL_VARCHAR">
		  <CFPROCPARAM type="IN" value="#URL.ccampaignID#" cfsqltype="CF_SQL_VARCHAR">
		  <CFPROCRESULT name="rsLead" resultset="1">
		</CFSTOREDPROC>

		<cfquery name="rsCCampaigns">
			select * from client_campaigns where 
			campaignID 
			in ('tara-coreg-pt-0001', 'tara-coreg-br-0002', 
				'maria-coreg-pt-0003', 'maria-coreg-br-0004',
				'estrela-coreg-br-0005','webpilots-coreg-pt-0006',
				'padre-coreg-pt-0008','padre-coreg-br-0009',
				'tara-coreg-es-0010', 'tara-coreg-ar-0011', 
				'maria-coreg-es-0012', 'maria-coreg-ar-0013',
				'padre-coreg-es-0014','padre-coreg-ar-0015', 'easyvoyage-spons-es-0016'
				) <!--- , 'bonotour-coreg-pt-0007' --->
			<cfif URL.ccampaignID neq ''>
			and campaignID='#URL.ccampaignID#'
			</cfif>
		</cfquery>

		<cfif URL.show eq 'yes'>
		<cfoutput>LEAD FOR INTEGRATION:<br></cfoutput>
		<cfdump var="#rsLead#">
		</cfif>

		<!--- INTEGRATE WITH CLIENTS --->
		<cfloop query="rsCCampaigns">

			<!--- set campaign --->
			<cfset ccampaignID=rsCCampaigns.campaignID>

			<!--- get data --->
			<CFSTOREDPROC procedure="dbo.getLeadsforIndividualIntegration">
			  <CFPROCPARAM type="IN" value="#leadID#" cfsqltype="CF_SQL_VARCHAR">
			  <CFPROCPARAM type="IN" value="#ccampaignID#" cfsqltype="CF_SQL_VARCHAR">
			  <CFPROCRESULT name="rsIntegrations" resultset="2">
			  <CFPROCRESULT name="rsOthSuccessInt" resultset="3">
			  <CFPROCRESULT name="rsBonoTour" resultset="4">
			  <CFPROCRESULT name="rsEasyVoyage" resultset="5">
			</CFSTOREDPROC>

			<CFSTOREDPROC procedure="dbo.getClientCampaign">
			  <CFPROCPARAM type="IN" value="#ccampaignID#" cfsqltype="CF_SQL_VARCHAR">
			  <CFPROCRESULT name="rsCampaign" resultset="1">
			  <CFPROCRESULT name="rsRules" resultset="2">
			  <CFPROCRESULT name="rsFields" resultset="3">
			</CFSTOREDPROC>

			<!--- transform data --->
			<cfset ftranslate = StructNew() >

			<cfloop query="rsFields">
				<cfset ftranslate["#rsFields.field_name#"]["#rsFields.field_value#"] = rsFields.field_value_client >
			</cfloop>
	
			<!--- check if it was already integrated --->
			<cfif rsIntegrations.recordcount eq 0>
			<!-- first time --->

				<cfif URL.show eq 'yes'>
				<cfoutput>1st ATTEMPT WITH #UCASE(ccampaignID)#<br></cfoutput>
				</cfif>

				<cfif rsLead.lead_country eq rsCCampaigns.campaign_country>
				<!--- satisfies campaing country --->
					<cfif URL.show eq 'yes'>
					<cfoutput>SATISFIES CAMPAIGN COUNTRY<br></cfoutput>
					</cfif>

					<cfset int_try_flag="">
					
					<cfswitch expression="#ccampaignID#">

						<cfcase value="tara-coreg-pt-0001">

							<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
								
								<cfset int_try_flag=1>

								<cfif URL.show eq 'yes'>
								<cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
								</cfif>

								<cfhttp url="http://www.tara-vidente.com/remote-registration.php" method="post" result="cresult" charset="utf-8"> 
								    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
								    <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#"> 
								    <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#"> 
								    <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#"> 
								    <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
								    <cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> 
								    <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#"> 
								    <cfhttpparam type="url" name="campaignarea" value="PT"> 
								    <cfhttpparam type="url" name="media" value="coreg">
								    <cfhttpparam type="url" name="partner" value="Yours">  
								    <cfhttpparam type="url" name="campaign" value="coreg2016">  
								</cfhttp>

							<cfelse>

								<cfset int_try_flag=0>

							</cfif>
								
						</cfcase>

						<cfcase value="tara-coreg-br-0002">

							<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
								
								<cfset int_try_flag=1>

								<cfif URL.show eq 'yes'>
								<cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
								</cfif>

								<cfhttp url="http://www.tara-clarividencia.com/remote-registration.php" method="get" result="cresult" charset="utf-8"> 
								    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
								    <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#"> 
								    <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#"> 
								    <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#"> 
								    <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
								    <cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> 
								    <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#"> 
								    <cfhttpparam type="url" name="campaignarea" value="BR"> 
								    <cfhttpparam type="url" name="media" value="coreg">
								    <cfhttpparam type="url" name="partner" value="Yours">  
								    <cfhttpparam type="url" name="campaign" value="coreg2016">  
								</cfhttp>

							<cfelse>

									<cfset int_try_flag=0>

							</cfif>

						</cfcase>

						<cfcase value="maria-coreg-pt-0003">

							<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
								
								<cfset int_try_flag=1>

								<cfif URL.show eq 'yes'>
								<cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
								</cfif>

								<cfhttp url="http://www.vidente-maria.com/remote-registration.php" method="get" result="cresult" charset="utf-8"> 
								    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
								    <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#"> 
								    <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#"> 
								    <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#"> 
								    <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
								    <!---<cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> --->
								    <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#">
								    <cfhttpparam type="url" name="optin" value="#rsLead.lead_optin#">
								    <cfhttpparam type="url" name="campaignarea" value="PT"> 
								    <cfhttpparam type="url" name="media" value="coreg">
								    <cfhttpparam type="url" name="partner" value="Yours">  
								    <cfhttpparam type="url" name="campaign" value="coreg2016">  
								</cfhttp>


							<cfelse>

									<cfset int_try_flag=0>

							</cfif>
								
						</cfcase>

						<cfcase value="maria-coreg-br-0004">

							<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
								
								<cfset int_try_flag=1>

								<cfif URL.show eq 'yes'>
								<cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
								</cfif>
								
								<cfhttp url="http://www.maria-medium-numerologa.com/remote-registration.php" method="get" result="cresult" charset="utf-8"> 
								    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
								    <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#"> 
								    <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#"> 
								    <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#"> 
								    <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
								    <!---<cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> --->
								    <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#">
								    <cfhttpparam type="url" name="optin" value="#rsLead.lead_optin#">
								    <cfhttpparam type="url" name="campaignarea" value="BR"> 
								    <cfhttpparam type="url" name="media" value="coreg">
								    <cfhttpparam type="url" name="partner" value="Yours">  
								    <cfhttpparam type="url" name="campaign" value="coreg2016">  
								</cfhttp>

							<cfelse>

									<cfset int_try_flag=0>

							</cfif>

						</cfcase>

						<cfcase value="estrela-coreg-br-0005">

							<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 30>
								
								<cfif now() lte DateAdd("h", 6, rsLead.lead_tstamp) and len(rsLead.lead_phone) gte 9 and rsLead.lead_gender eq 'F'>
									<cfif (hour(now()) lte 3 or hour(now()) gte 12)> <!--- duvida: para o intervalo de horas, devo avaliar o now ou o lead_tstamp?--->

										<cfset int_try_flag=1>
										<cfset lead_phone='55' & rsLead.lead_phone>
										<cfif URL.show eq 'yes'>
										<cfoutput>SATISFIES CAMPAIGN AGE, GENDER, PHONE AND TIMING<br></cfoutput>
										</cfif>
										<!---?civilite=X&emailAddress=XXXXX@XXXXX&nom=XXXX&prenom=XXXX&dob=XX-XX-XXXX&ipAddress=XXX.XXX.XXX.XX&telephoneNumber=55XXXXXXXXXX&origin=egentic&c=227&s=ca_astra_br&conversation=1&countryCode=BR&telCountry=BR--->
										<cfhttp url="http://www.estrelafone.com.br/pt/api/createClientPublic.htm" method="get" result="cresult" charset="utf-8"> 
										    <cfhttpparam type="url" name="civilite" value="#rsLead.lead_gender#"> 
										    <cfhttpparam type="url" name="emailAddress" value="#rsLead.lead_email#"> 
										    <cfhttpparam type="url" name="prenom" value="#rsLead.lead_name#"> 
										    <cfhttpparam type="url" name="nom" value="#rsLead.lead_surname#"> 
										    <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
										    <cfhttpparam type="url" name="dob" value="#DateFormat(rsLead.lead_birthdate,"DD-MM-YYYY")#">
										    <cfhttpparam type="url" name="ipAddress" value="#rsLead.lead_ip#">
										    <cfhttpparam type="url" name="telephoneNumber" value="#lead_phone#">
										    <cfhttpparam type="url" name="origin" value="rsLead.leadID"> 
										    <cfhttpparam type="url" name="c" value="227">
										    <cfhttpparam type="url" name="s" value="ca_astra_br">  
										    <cfhttpparam type="url" name="conversation" value="1"> 
										    <cfhttpparam type="url" name="countryCode" value="BR"> 
										    <cfhttpparam type="url" name="telCountry" value="BR"> 
										</cfhttp>

									<cfelse>
										<cfset int_try_flag=0>	
									</cfif>
								<cfelse>
									<cfset int_try_flag=0>
								</cfif>

							<cfelse>

									<cfset int_try_flag=0>

							</cfif>

						</cfcase>

						<cfcase value="webpilots-coreg-pt-0006">

							<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 18>
								
								<cfif len(rsLead.lead_phone) gte 9 and len(rsLead.lead_zip) gte 4>
									
										<cfif len(rsLead.lead_zip) eq 7>
											<cfset lz=left(rsLead.lead_zip,4) & "-" & mid(rsLead.lead_zip,5,3)>
										<cfelse>
											<cfset lz=rsLead.lead_zip + "-000">
										</cfif>

										<cfset int_try_flag=1>
										<cfif URL.show eq 'yes'>
										<cfoutput>SATISFIES CAMPAIGN AGE, PHONE AND ZIP<br></cfoutput>
										</cfif>
										<cfoutput>
										<!--- http://coreg.webpilots.com/wscampaign.php?realtime=1&country=pt&companyid=9&publisherid=28&campaignid=103&type=S&subid=YOURSPORTO&birthday=[BIRTHDAY]&gender=[GENDER]&firstname=[FIRSTNAME]&lastname=[LASTNAME]&mobile=[MOBILE]&email=[EMAIL]&zip=[ZIP]&country_residence=[COUNTRY_RESIDENCE]&nationality=[NATIONALITY]&data1=[GAME_DESCRIPTION]&ipenduser=[xxx.xxx.xxx.xxx] --->
										<!--- http://coreg.webpilots.com/wscampaign.php?realtime=1&country=pt&companyid=9&publisherid=28&campaignid=103&type=S&subid=YOURSPORTO&birthday=#DateFormat(rsLead.lead_birthdate,"YYYY-MM-DD")#&gender=#rsLead.lead_gender#&firstname=#rsLead.lead_name#&lastname=#rsLead.lead_surname#&mobile=#rsLead.lead_phone#&email=#rsLead.lead_email#&zip=#rsLead.lead_zip#&country_residence=#rsLead.lead_country#&nationality=#rsLead.lead_country#&data1=&ipenduser=#rsLead.lead_ip#--->	
										<br>
										</cfoutput>
										
										<cfhttp url="http://coreg.webpilots.com/wscampaign.php" method="get" result="cresult" charset="utf-8"> 
										    <cfhttpparam type="url" name="realtime" value="1"> 
										    <cfhttpparam type="url" name="country" value="pt"> 
										    <cfhttpparam type="url" name="companyid" value="9"> 
										    <cfhttpparam type="url" name="publisherid" value="28"> 
										    <cfhttpparam type="url" name="campaignid" value="103">
										    <cfhttpparam type="url" name="type" value="S">
										    <cfhttpparam type="url" name="subid" value="YOURSPORTO">
										    <cfhttpparam type="url" name="birthday" value="#DateFormat(rsLead.lead_birthdate,"YYYY-MM-DD")#">
										    <cfhttpparam type="url" name="gender" value="#rsLead.lead_gender#">
										    <cfhttpparam type="url" name="firstname" value="#rsLead.lead_name#">
										    <cfhttpparam type="url" name="lastname" value="#rsLead.lead_surname#">
										    <cfhttpparam type="url" name="mobile" value="#rsLead.lead_phone#">
										    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#">
										    <cfhttpparam type="url" name="zip" value="#lz#">
										    <cfhttpparam type="url" name="country_residence" value="#rsLead.lead_country#">
										    <cfhttpparam type="url" name="nationality" value="#rsLead.lead_country#">
										    <cfhttpparam type="url" name="data1" value="#rsLead.platformID#">	
										    <cfhttpparam type="url" name="ipenduser" value="#rsLead.lead_ip#">
										</cfhttp>

								<cfelse>
									<cfset int_try_flag=0>
								</cfif>

							<cfelse>

									<cfset int_try_flag=0>

							</cfif>

						</cfcase>

						<cfcase value="bonotour-coreg-pt-0007">

							<cfif rsBonoTour.lead_optin eq 1>
							
								<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 25>
									
									<cfif len(rsLead.lead_zip) gte 4>
										
										<cfset int_try_flag=1>
										<cfif URL.show eq 'yes'>
										<cfoutput>SATISFIES CAMPAIGN AGE AND ZIP<br></cfoutput>
										</cfif>
										<cfoutput>
										<!--- http://new.hotelbono.com/process/receptor/receptor.php?pk_camp=MTM2MDk%3Dk9x&fecha_alta=[fecha_alta]&url_refered=[url_refered]&ip=[ip]&email=[email]&nombre=[nombre]&apellidos=[apellidos]&cp=[cp]&sexo=[sexo]&fecha_nac=[fecha_nac]&fk_pais=[fk_pais]&supermercado=[supermercado] --->
										<!---http://new.hotelbono.com/process/receptor/receptor.php?pk_camp=MTM2MDk%3Dk9x&fecha_alta=#DateFormat(rsLead.lead_tstamp,"YYYY-MM-DD")# #TimeFormat(rsLead.lead_tstamp,"HH:MM:SS")#&url_refered=http://www.videntecibele.com&ip=#rsLead.lead_ip#&email=#rsLead.lead_email#&nombre=#rsLead.lead_name#&
										apellidos=#rsLead.lead_surname#&cp=#rsLead.lead_zip#&sexo=#ftranslate['lead_gender'][rsLead.lead_gender]#&fecha_nac=#DateFormat(rsLead.lead_birthdate,"YYYY-MM-DD")#&fk_pais=72&supermercado=#rsLead.platformID#--->
										<br>
										</cfoutput>
										
										<cfhttp url="http://new.hotelbono.com/process/receptor/receptor.php" method="get" result="cresult" charset="utf-8"> 
										    <cfhttpparam type="url" name="pk_camp" value="MTM2MDk%3Dk9x"> 
										    <cfhttpparam type="url" name="fecha_alta" value="#DateFormat(rsLead.lead_tstamp,"YYYY-MM-DD")# #TimeFormat(rsLead.lead_tstamp,"HH:MM:SS")#"> 
										    <cfhttpparam type="url" name="url_refered" value="http://www.videntecibele.com/pt-pt/"> 
										    <cfhttpparam type="url" name="ip" value="#rsLead.lead_ip#"> 
										    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#">
										    <cfhttpparam type="url" name="nombre" value="#rsLead.lead_name#">
										    <cfhttpparam type="url" name="apellidos" value="#rsLead.lead_surname#">
										    <cfhttpparam type="url" name="cp" value="#rsLead.lead_zip#">
										    <cfhttpparam type="url" name="sexo" value="#ftranslate['lead_gender'][rsLead.lead_gender]#">
										    <cfhttpparam type="url" name="fecha_nac" value="#DateFormat(rsLead.lead_birthdate,"YYYY-MM-DD")#">
										    <cfhttpparam type="url" name="fk_pais" value="72">
										    <cfhttpparam type="url" name="supermercado" value="#rsLead.platformID#">
										</cfhttp>

									<cfelse>
										<cfset int_try_flag=0>
									</cfif>

								<cfelse>

										<cfset int_try_flag=0>

								</cfif>

							</cfif>

						</cfcase>

						<cfcase value="padre-coreg-pt-0008">
							
							<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
								
									
								<cfset int_try_flag=1>
								<cfif URL.show eq 'yes'>
								<cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
								</cfif>
								<cfoutput>
								
								<br>
								</cfoutput>

								<cfif URL.token eq ''>
								
									<!--- GET TOKEN FOR PADRE --->
									<cfset loginFields = {'email': 'andre.amorim@yoursporto.com','password': '**Yours2016'} >   

									<cfhttp url="https://api.mensagens-dos-anjos.com/api/Partners/login" method="post" result="cresult" timeout="60">
									    <cfhttpparam type="header" name="Content-Type" value="application/json" />
									    <cfhttpparam type="body" value="#serializeJSON(loginFields)#">
									</cfhttp>

									<cfset tempvar=deserializeJSON(cresult.filecontent)>
									<cfset login_token=tempvar.id>

									<cfset URL.token=login_token>

								</cfif>

								<cfset intFields = {
								"email": "#rsLead.lead_email#",
								"birthdate": "#DateFormat(rsLead.lead_birthdate,'YYYY-MM-DD')#",
								"language" : "pt-pt",
								"timezone" : 0,
								"gender": "#ftranslate['lead_gender'][rsLead.lead_gender]#",
								"firstName": "#rsLead.lead_name#",
								"lastName": "#rsLead.lead_surname#",
								"originDetail": {
								"partner" : "YOURS",
								"referer": "http://www.videntecibele.com/pt-pt",
								"subid": "#rsLead.platformID#",
								"media": "COREG",
								"theme": "angel",
								"campaign": "campaign_2016",
								"campaignarea": "PT"
								},
								"location":{
								"registrationIpV4": "#rsLead.lead_ip#",
								"registrationIpV6": "",
								"number": "",
								"street": "",
								"street2": "",
								"street3": "",
								"zip": "",
								"city": "",
								"state": "",
								"country": "PRT"
								}
								}>
								
								<cfhttp url="https://api.mensagens-dos-anjos.com/api/Customers" method="post" result="cresult" timeout="60">
								    <cfhttpparam type="url" name="access_token" value="#URL.token#">
								    <cfhttpparam type="header" name="Content-Type" value="application/json" />
								    <cfhttpparam type="body" value="#serializeJSON(intFields)#">
								</cfhttp>

							<cfelse>

								<cfset int_try_flag=0>

							</cfif>
							
						</cfcase>

						<cfcase value="padre-coreg-br-0009">
							
							<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
								
									
								<cfset int_try_flag=1>
								<cfif URL.show eq 'yes'>
								<cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
								</cfif>
								<cfoutput>
								
								<br>
								</cfoutput>

								<cfif URL.token eq ''>

									<!--- GET TOKEN FOR PADRE --->
									<cfset loginFields = {'email': 'andre.amorim@yoursporto.com','password': '**Yours2016'} >   

									<cfhttp url="https://api.mensagens-dos-anjos.com/api/Partners/login" method="post" result="cresult" timeout="60">
									    <cfhttpparam type="header" name="Content-Type" value="application/json" />
									    <cfhttpparam type="body" value="#serializeJSON(loginFields)#">
									</cfhttp>

									<cfset tempvar=deserializeJSON(cresult.filecontent)>
									<cfset login_token=tempvar.id>

									<cfset URL.token=login_token>

								</cfif>

								<cfset intFields = {
								"email": "#rsLead.lead_email#",
								"birthdate": "#DateFormat(rsLead.lead_birthdate,'YYYY-MM-DD')#",
								"language" : "pt-pt",
								"timezone" : -3,
								"gender": "#ftranslate['lead_gender'][rsLead.lead_gender]#",
								"firstName": "#rsLead.lead_name#",
								"lastName": "#rsLead.lead_surname#",
								"originDetail": {
								"partner" : "YOURS",
								"referer": "http://www.videntecibele.com/pt-br",
								"subid": "#rsLead.platformID#",
								"media": "COREG",
								"theme": "angel",
								"campaign": "campaign_2016",
								"campaignarea": "BR"
								},
								"location":{
								"registrationIpV4": "#rsLead.lead_ip#",
								"registrationIpV6": "",
								"number": "",
								"street": "",
								"street2": "",
								"street3": "",
								"zip": "",
								"city": "",
								"state": "",
								"country": "BRA"
								}
								}>
								
								<cfhttp url="https://api.mensagens-dos-anjos.com/api/Customers" method="post" result="cresult" timeout="60">
								    <cfhttpparam type="url" name="access_token" value="#URL.token#">
								    <cfhttpparam type="header" name="Content-Type" value="application/json" />
								    <cfhttpparam type="body" value="#serializeJSON(intFields)#">
								</cfhttp>

							<cfelse>

								<cfset int_try_flag=0>

							</cfif>
							
						</cfcase>

						<cfcase value="tara-coreg-es-0010">

							<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
								
								<cfset int_try_flag=1>

								<cfif URL.show eq 'yes'>
								<cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
								</cfif>

								<cfhttp url="http://www.tara-astrologia.com/remote-registration.php" method="post" result="cresult" charset="utf-8"> 
								    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
								    <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#"> 
								    <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#"> 
								    <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#"> 
								    <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
								    <cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> 
								    <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#"> 
								    <cfhttpparam type="url" name="campaignarea" value="ES"> 
								    <cfhttpparam type="url" name="media" value="coreg">
								    <cfhttpparam type="url" name="partner" value="Yours">  
								    <cfhttpparam type="url" name="campaign" value="coreg2017">  
								</cfhttp>

							<cfelse>

								<cfset int_try_flag=0>

							</cfif>
								
						</cfcase>

						<cfcase value="tara-coreg-ar-0011">

							<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
								
								<cfset int_try_flag=1>

								<cfif URL.show eq 'yes'>
								<cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
								</cfif>

								<cfhttp url="http://www.tara-videncia.com/remote-registration.php" method="get" result="cresult" charset="utf-8"> 
								    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
								    <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#"> 
								    <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#"> 
								    <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#"> 
								    <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
								    <cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> 
								    <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#"> 
								    <cfhttpparam type="url" name="campaignarea" value="AR"> 
								    <cfhttpparam type="url" name="media" value="coreg">
								    <cfhttpparam type="url" name="partner" value="Yours">  
								    <cfhttpparam type="url" name="campaign" value="coreg2017">  
								</cfhttp>

							<cfelse>

									<cfset int_try_flag=0>

							</cfif>

						</cfcase>

						<cfcase value="maria-coreg-es-0012">

							<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
								
								<cfset int_try_flag=1>

								<cfif URL.show eq 'yes'>
								<cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
								</cfif>

								<cfhttp url="http://www.videncia-maria.com/remote-registration.php" method="get" result="cresult" charset="utf-8"> 
								    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
								    <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#"> 
								    <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#"> 
								    <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#"> 
								    <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
								    <!---<cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> --->
								    <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#">
								    <cfhttpparam type="url" name="optin" value="#rsLead.lead_optin#">
								    <cfhttpparam type="url" name="campaignarea" value="ES"> 
								    <cfhttpparam type="url" name="media" value="coreg">
								    <cfhttpparam type="url" name="partner" value="Yours">  
								    <cfhttpparam type="url" name="campaign" value="coreg2017">  
								</cfhttp>


							<cfelse>

									<cfset int_try_flag=0>

							</cfif>
								
						</cfcase>

						<cfcase value="maria-coreg-ar-0013">

							<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
								
								<cfset int_try_flag=1>

								<cfif URL.show eq 'yes'>
								<cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
								</cfif>
								
								<cfhttp url="http://www.maria-clarividencia.com/remote-registration.php" method="get" result="cresult" charset="utf-8"> 
								    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
								    <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#"> 
								    <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#"> 
								    <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#"> 
								    <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
								    <!---<cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> --->
								    <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#">
								    <cfhttpparam type="url" name="optin" value="#rsLead.lead_optin#">
								    <cfhttpparam type="url" name="campaignarea" value="AR"> 
								    <cfhttpparam type="url" name="media" value="coreg">
								    <cfhttpparam type="url" name="partner" value="Yours">  
								    <cfhttpparam type="url" name="campaign" value="coreg2017">  
								</cfhttp>

							<cfelse>

									<cfset int_try_flag=0>

							</cfif>

						</cfcase>

						<cfcase value="padre-coreg-es-0014">
							
							<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
								
									
								<cfset int_try_flag=1>
								<cfif URL.show eq 'yes'>
								<cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
								</cfif>
								<cfoutput>
								
								<br>
								</cfoutput>

								<cfif URL.token eq ''>
								
									<!--- GET TOKEN FOR PADRE --->
									<cfset loginFields = {'email': 'andre.amorim@yoursporto.com','password': '**Yours2016'} >   

									<cfhttp url="https://api.mensagens-dos-anjos.com/api/Partners/login" method="post" result="cresult" timeout="60">
									    <cfhttpparam type="header" name="Content-Type" value="application/json" />
									    <cfhttpparam type="body" value="#serializeJSON(loginFields)#">
									</cfhttp>

									<cfset tempvar=deserializeJSON(cresult.filecontent)>
									<cfset login_token=tempvar.id>

									<cfset URL.token=login_token>

								</cfif>

								<cfset intFields = {
								"email": "#rsLead.lead_email#",
								"birthdate": "#DateFormat(rsLead.lead_birthdate,'YYYY-MM-DD')#",
								"language" : "es",
								"timezone" : 0,
								"gender": "#ftranslate['lead_gender'][rsLead.lead_gender]#",
								"firstName": "#rsLead.lead_name#",
								"lastName": "#rsLead.lead_surname#",
								"originDetail": {
								"partner" : "YOURS",
								"referer": "http://www.videntecibele.com/es-es",
								"subid": "#rsLead.platformID#",
								"media": "COREG",
								"theme": "angel",
								"campaign": "campaign_2016",
								"campaignarea": "ES"
								},
								"location":{
								"registrationIpV4": "#rsLead.lead_ip#",
								"registrationIpV6": "",
								"number": "",
								"street": "",
								"street2": "",
								"street3": "",
								"zip": "",
								"city": "",
								"state": "",
								"country": "ESP"
								}
								}>
								
								<cfhttp url="https://api.mensagens-dos-anjos.com/api/Customers" method="post" result="cresult" timeout="60">
								    <cfhttpparam type="url" name="access_token" value="#URL.token#">
								    <cfhttpparam type="header" name="Content-Type" value="application/json" />
								    <cfhttpparam type="body" value="#serializeJSON(intFields)#">
								</cfhttp>

							<cfelse>

								<cfset int_try_flag=0>

							</cfif>
							
						</cfcase>

						<cfcase value="padre-coreg-ar-0015">
							
							<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
								
									
								<cfset int_try_flag=1>
								<cfif URL.show eq 'yes'>
								<cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
								</cfif>
								<cfoutput>
								
								<br>
								</cfoutput>

								<cfif URL.token eq ''>

									<!--- GET TOKEN FOR PADRE --->
									<cfset loginFields = {'email': 'andre.amorim@yoursporto.com','password': '**Yours2016'} >   

									<cfhttp url="https://api.mensagens-dos-anjos.com/api/Partners/login" method="post" result="cresult" timeout="60">
									    <cfhttpparam type="header" name="Content-Type" value="application/json" />
									    <cfhttpparam type="body" value="#serializeJSON(loginFields)#">
									</cfhttp>

									<cfset tempvar=deserializeJSON(cresult.filecontent)>
									<cfset login_token=tempvar.id>

									<cfset URL.token=login_token>

								</cfif>

								<cfset intFields = {
								"email": "#rsLead.lead_email#",
								"birthdate": "#DateFormat(rsLead.lead_birthdate,'YYYY-MM-DD')#",
								"language" : "es",
								"timezone" : -3,
								"gender": "#ftranslate['lead_gender'][rsLead.lead_gender]#",
								"firstName": "#rsLead.lead_name#",
								"lastName": "#rsLead.lead_surname#",
								"originDetail": {
								"partner" : "YOURS",
								"referer": "http://www.videntecibele.com/es-ar",
								"subid": "#rsLead.platformID#",
								"media": "COREG",
								"theme": "angel",
								"campaign": "campaign_2016",
								"campaignarea": "AR"
								},
								"location":{
								"registrationIpV4": "#rsLead.lead_ip#",
								"registrationIpV6": "",
								"number": "",
								"street": "",
								"street2": "",
								"street3": "",
								"zip": "",
								"city": "",
								"state": "",
								"country": "ARG"
								}
								}>
								
								<cfhttp url="https://api.mensagens-dos-anjos.com/api/Customers" method="post" result="cresult" timeout="60">
								    <cfhttpparam type="url" name="access_token" value="#URL.token#">
								    <cfhttpparam type="header" name="Content-Type" value="application/json" />
								    <cfhttpparam type="body" value="#serializeJSON(intFields)#">
								</cfhttp>

							<cfelse>

								<cfset int_try_flag=0>

							</cfif>
							
						</cfcase>

						<cfcase value="easyvoyage-spons-es-0016">

							<cfif rsEasyVoyage.lead_optin eq 1>

								<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 18>
									
									<cfif len(rsLead.lead_zip) eq 5>
										
										<cfif FindNoCase('@gmail.com', rsLead.lead_email) eq 0>

											<cfset int_try_flag=1>
											<cfif URL.show eq 'yes'>
											<cfoutput>SATISFIES CAMPAIGN AGE, ZIP AND NO GMAIL<br></cfoutput>
											</cfif>
											<cfoutput>
											<!--- ?id=314&email=[EMAIL]&nombre=[NOMBRE]&apellidos=[APELLIDOS]&dob=[DIA_DE_NACIMIENTO]&mob=[MES_DE_NACIMIENTO]&yob=[ANYO_DE_NACIMIENTO]&sexo=[SEXO_M_O_F]&cp=[COD_POSTAL]&ip=[IP_DE_REGISTRO]&ref=[WEB_ASOCIADA_AL_WEBMASTER] --->
							
											<br>
											</cfoutput>
											
											<cfhttp url="http://www.mailingcp.com/corregistros/pasarelacrg.php" method="get" result="cresult" charset="utf-8"> 
											    <cfhttpparam type="url" name="id" value="441"> 
											    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
											    <cfhttpparam type="url" name="nombre" value="#rsLead.lead_name#"> 
											    <cfhttpparam type="url" name="apellidos" value="#rsLead.lead_surname#"> 
											    <cfhttpparam type="url" name="dob" value="#day(rsLead.lead_birthdate)#">
											    <cfhttpparam type="url" name="mob" value="#month(rsLead.lead_birthdate)#">
											    <cfhttpparam type="url" name="yob" value="#year(rsLead.lead_birthdate)#">
											    <cfhttpparam type="url" name="sexo" value="#rsLead.lead_gender#">
											    <cfhttpparam type="url" name="cp" value="#rsLead.lead_zip#">
											    <cfhttpparam type="url" name="ip" value="#rsLead.lead_ip#">
											    <cfhttpparam type="url" name="ref" value="#rsLead.platformID#">
											</cfhttp>

										<cfelse>
											<cfset int_try_flag=0>
										</cfif>

									<cfelse>
										<cfset int_try_flag=0>
									</cfif>

								<cfelse>

										<cfset int_try_flag=0>

								</cfif>

							<cfelse>
								
								<cfset int_try_flag=0>

							</cfif>

						</cfcase>

					</cfswitch>
					

					<cfif int_try_flag eq 1>
						
						<cfif URL.show eq 'yes'>
						<cfoutput>RESULT FROM #UCASE(ccampaignID)#<br></cfoutput>
						<cfdump var="#cresult#">
						</cfif>

						<cfif trim(cresult.filecontent) eq 'ok' or (FindNoCase('OK', cresult.filecontent) gt 0 and FindNoCase('book', cresult.filecontent) eq 0) or FindNoCase('"errorCode":0,', cresult.filecontent) gt 0 or FindNoCase('created', cresult.filecontent) gt 0 >
							<cfset statusID=10>
							<cfset int_statusID=1>
							<cfif URL.show eq 'yes'>
							<cfoutput>LEAD SUCCESSFULY INTEGRATED ON #rsCampaign.campaignID#</cfoutput>
							</cfif>
						<cfelseif FindNoCase('já está', cresult.filecontent) gt 0 or FindNoCase('já estão', cresult.filecontent) gt 0 or FindNoCase('already exist', cresult.filecontent) gt 0 or FindNoCase('"errorCode":10,', cresult.filecontent) gt 0 or FindNoCase('"errorCode":20,', cresult.filecontent) gt 0 or FindNoCase('"errorCode":30,', cresult.filecontent) gt 0 or FindNoCase('duplicate', cresult.filecontent) gt 0 or FindNoCase('KO - 0001', cresult.filecontent) gt 0 or FindNoCase('Repetido', cresult.filecontent) gt 0 or FindNoCase('L345', cresult.filecontent) gt 0 or FindNoCase('L054', cresult.filecontent) gt 0 or FindNoCase('Err. 004', cresult.filecontent) gt 0>
							<cfif rsOthSuccessInt.recordcount eq 0><cfset statusID=5><cfelse><cfset statusID=rsLead.statusID></cfif>
							<cfset int_statusID=0>
							<cfif URL.show eq 'yes'>
							<cfoutput>LEAD NOT INTEGRATED ON #rsCampaign.campaignID# >#cresult.filecontent#</cfoutput>
							</cfif>
						<cfelseif FindNoCase('"email"', cresult.filecontent) gt 0 or FindNoCase('email validation', cresult.filecontent) gt 0 or FindNoCase('bad', cresult.filecontent) gt 0 or FindNoCase('mailbox', cresult.filecontent) gt 0 or FindNoCase('"errorCode":103,', cresult.filecontent) gt 0 or FindNoCase('"errorCode":104,', cresult.filecontent) gt 0 or FindNoCase('KO - 0002', cresult.filecontent) gt 0  or FindNoCase('L351', cresult.filecontent) gt 0 or FindNoCase('L801', cresult.filecontent) gt 0 or FindNoCase('Err. 003', cresult.filecontent) gt 0>
							<cfif rsOthSuccessInt.recordcount eq 0><cfset statusID=-1><cfelse><cfset statusID=rsLead.statusID></cfif>
							<cfset int_statusID=-1>
							<cfif URL.show eq 'yes'>
							<cfoutput>LEAD NOT INTEGRATED ON #rsCampaign.campaignID# >#cresult.filecontent#</cfoutput>
							</cfif>
						<cfelse>
							<cfif rsOthSuccessInt.recordcount eq 0><cfset statusID=-1><cfelse><cfset statusID=rsLead.statusID></cfif>
							<cfset int_statusID=-2>
							<cfif URL.show eq 'yes'>
							<cfoutput>LEAD NOT INTEGRATED ON #rsCampaign.campaignID# >#cresult.filecontent#</cfoutput>
							</cfif>
						</cfif>
						<br>

					<cfelse>

						<!--- doesn't satisfy campaign 2nd+ rule --->
						<cfset cresult=StructNew()>
						<cfset statusID=rsLead.statusID>
						<cfset int_statusID=-3>
						<cfset cresult.filecontent='it does not satisfy campaign 2nd+ rule'>
						<cfif URL.show eq 'yes'>
						<cfoutput>DOES NOT SATISFY CAMPAIGN 2nd RULES FOR #UCASE(ccampaignID)#<br></cfoutput>
						</cfif>

					</cfif>

				<cfelse>	
				<!--- doesn't satisfy campaign country --->
					
					<cfset cresult=StructNew()>
					<cfset statusID=rsLead.statusID>
					<cfset int_statusID=-3>
					<cfset cresult.filecontent='it does not satisfy campaign country'>
					<cfif URL.show eq 'yes'>
					<cfoutput>DOES NOT SATISFY CAMPAIGN COUNTRY FOR #UCASE(ccampaignID)#<br></cfoutput>
					</cfif>

				</cfif>

				<!--- save actions --->
				<cfquery name="int_p1">
					insert into lead_integration_attempts (campaignID, leadID, tstamp, statusID, result)
					values ('#rsCampaign.campaignID#', '#rsLead.leadID#', getdate(), #int_statusID#, '#cresult.filecontent#')
				</cfquery>

				<cfquery name="int_p2">
					insert into lead_integration (campaignID, leadID, tstamp, statusID)
					values ('#rsCampaign.campaignID#', '#rsLead.leadID#', getdate(), #int_statusID#)
				</cfquery>

				<cfquery name="int_p3">
					update lead_feed set statusID=#statusID# where leadID='#rsLead.leadID#'
				</cfquery>

			<cfelse>
			<!-- there was already an attempt -->

				<cfif rsIntegrations.statusID eq 1>

					<cfif URL.show eq 'yes'>
					<cfoutput>INTEGRATION WITH #UCASE(ccampaignID)# WAS ALREADY DONE SUCCESSFULY (1)<br></cfoutput>
					<cfdump var="#rsIntegrations#">
					</cfif>

				<cfelse>

					<cfif rsLead.statusID neq 1>
						
						<cfif URL.show eq 'yes'>
						<cfswitch expression="#rsIntegrations.statusID#">
							<cfcase value="0">
								<cfoutput>INTEGRATION WITH #UCASE(ccampaignID)# WAS ALREADY TRIED AND EMAIL EXISTED (0)<br></cfoutput>
							</cfcase>
							<cfcase value="-1">
								<cfoutput>INTEGRATION WITH #UCASE(ccampaignID)# WAS ALREADY TRIED AND THERE WAS A PROBLEM WITH THE EMAIL (-1)<br></cfoutput>
							</cfcase>
							<cfcase value="-2">
								<cfoutput>INTEGRATION WITH #UCASE(ccampaignID)# WAS ALREADY TRIED AND THERE WAS SOME OTHER PROBLEM (-2)<br></cfoutput>
							</cfcase>
							<cfcase value="-3">
								<cfoutput>INTEGRATION WITH #UCASE(ccampaignID)# WAS ALREADY TRIED AND DIDN'T SATISFY THE CAMPAIGN CRITERIA (-3)<br></cfoutput>
							</cfcase>
						</cfswitch>
						<cfdump var="#rsIntegrations#">
						</cfif>

					<cfelse>
					<!--- retry, because the status say it so --->

						<cfif URL.show eq 'yes'>
						<cfoutput>FORCE ANOTHER INTEGRATION WITH #UCASE(ccampaignID)#<br></cfoutput>
						</cfif>

						<cfif rsLead.lead_country eq rsCCampaigns.campaign_country>
						<!--- satisfies campaing country --->
							<cfif URL.show eq 'yes'>
							<cfoutput>SATISFIES CAMPAIGN COUNTRY<br></cfoutput>
							</cfif>

							<cfswitch expression="#ccampaignID#">
								<cfcase value="tara-coreg-pt-0001">

									<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
										
										<cfset int_try_flag=1>

										<cfif URL.show eq 'yes'>
										<cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
										</cfif>

										<cfhttp url="http://www.tara-vidente.com/remote-registration.php" method="post" result="cresult" charset="utf-8"> 
										    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
										    <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#"> 
										    <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#"> 
										    <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#"> 
										    <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
										    <cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> 
										    <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#"> 
										    <cfhttpparam type="url" name="campaignarea" value="PT"> 
										    <cfhttpparam type="url" name="media" value="coreg">
										    <cfhttpparam type="url" name="partner" value="Yours">  
										    <cfhttpparam type="url" name="campaign" value="coreg2016">  
										</cfhttp>

									<cfelse>

										<cfset int_try_flag=0>

									</cfif>
										
								</cfcase>

								<cfcase value="tara-coreg-br-0002">

									<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
										
										<cfset int_try_flag=1>

										<cfif URL.show eq 'yes'>
										<cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
										</cfif>

										<cfhttp url="http://www.tara-clarividencia.com/remote-registration.php" method="get" result="cresult" charset="utf-8"> 
										    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
										    <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#"> 
										    <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#"> 
										    <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#"> 
										    <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
										    <cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> 
										    <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#"> 
										    <cfhttpparam type="url" name="campaignarea" value="BR"> 
										    <cfhttpparam type="url" name="media" value="coreg">
										    <cfhttpparam type="url" name="partner" value="Yours">  
										    <cfhttpparam type="url" name="campaign" value="coreg2016">  
										</cfhttp>

									<cfelse>

											<cfset int_try_flag=0>

									</cfif>

								</cfcase>

								<cfcase value="maria-coreg-pt-0003">

									<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
										
										<cfset int_try_flag=1>

										<cfif URL.show eq 'yes'>
										<cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
										</cfif>

										<cfhttp url="http://www.vidente-maria.com/remote-registration.php" method="get" result="cresult" charset="utf-8"> 
										    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
										    <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#"> 
										    <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#"> 
										    <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#"> 
										    <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
										    <!---<cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> --->
										    <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#">
										    <cfhttpparam type="url" name="optin" value="#rsLead.lead_optin#">
										    <cfhttpparam type="url" name="campaignarea" value="PT"> 
										    <cfhttpparam type="url" name="media" value="coreg">
										    <cfhttpparam type="url" name="partner" value="Yours">  
										    <cfhttpparam type="url" name="campaign" value="coreg2016">  
										</cfhttp>


									<cfelse>

											<cfset int_try_flag=0>

									</cfif>
										
								</cfcase>

								<cfcase value="maria-coreg-br-0004">

									<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
										
										<cfset int_try_flag=1>

										<cfif URL.show eq 'yes'>
										<cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
										</cfif>
										
										<cfhttp url="http://www.maria-medium-numerologa.com/remote-registration.php" method="get" result="cresult" charset="utf-8"> 
										    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
										    <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#"> 
										    <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#"> 
										    <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#"> 
										    <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
										    <!---<cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> --->
										    <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#">
										    <cfhttpparam type="url" name="optin" value="#rsLead.lead_optin#">
										    <cfhttpparam type="url" name="campaignarea" value="BR"> 
										    <cfhttpparam type="url" name="media" value="coreg">
										    <cfhttpparam type="url" name="partner" value="Yours">  
										    <cfhttpparam type="url" name="campaign" value="coreg2016">  
										</cfhttp>

									<cfelse>

											<cfset int_try_flag=0>

									</cfif>

								</cfcase>

								<cfcase value="estrela-coreg-br-0005">

									<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 30>
										
										<cfif now() lte DateAdd("h", 6, rsLead.lead_tstamp) and len(rsLead.lead_phone) gte 9 and rsLead.lead_gender eq 'F'>
											<cfif (hour(now()) lte 3 or hour(now()) gte 12)> <!--- duvida: para o intervalo de horas, devo avaliar o now ou o lead_tstamp?--->

												<cfset int_try_flag=1>
												<cfset lead_phone='55' & rsLead.lead_phone>
												<cfif URL.show eq 'yes'>
												<cfoutput>SATISFIES CAMPAIGN AGE, GENDER, PHONE AND TIMING<br></cfoutput>
												</cfif>
												<!---?civilite=X&emailAddress=XXXXX@XXXXX&nom=XXXX&prenom=XXXX&dob=XX-XX-XXXX&ipAddress=XXX.XXX.XXX.XX&telephoneNumber=55XXXXXXXXXX&origin=egentic&c=227&s=ca_astra_br&conversation=1&countryCode=BR&telCountry=BR--->
												<cfhttp url="http://www.estrelafone.com.br/pt/api/createClientPublic.htm" method="get" result="cresult" charset="utf-8"> 
												    <cfhttpparam type="url" name="civilite" value="#rsLead.lead_gender#"> 
												    <cfhttpparam type="url" name="emailAddress" value="#rsLead.lead_email#"> 
												    <cfhttpparam type="url" name="prenom" value="#rsLead.lead_name#"> 
												    <cfhttpparam type="url" name="nom" value="#rsLead.lead_surname#"> 
												    <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
												    <cfhttpparam type="url" name="dob" value="#DateFormat(rsLead.lead_birthdate,"DD-MM-YYYY")#">
												    <cfhttpparam type="url" name="ipAddress" value="#rsLead.lead_ip#">
												    <cfhttpparam type="url" name="telephoneNumber" value="#lead_phone#">
												    <cfhttpparam type="url" name="origin" value="rsLead.leadID"> 
												    <cfhttpparam type="url" name="c" value="227">
												    <cfhttpparam type="url" name="s" value="ca_astra_br">  
												    <cfhttpparam type="url" name="conversation" value="1"> 
												    <cfhttpparam type="url" name="countryCode" value="BR"> 
												    <cfhttpparam type="url" name="telCountry" value="BR"> 
												</cfhttp>

											<cfelse>
												<cfset int_try_flag=0>	
											</cfif>
										<cfelse>
											<cfset int_try_flag=0>
										</cfif>

									<cfelse>

											<cfset int_try_flag=0>

									</cfif>

								</cfcase>

								<cfcase value="webpilots-coreg-pt-0006">

									<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 18>
										
										<cfif len(rsLead.lead_phone) gte 9 and len(rsLead.lead_zip) gte 4>
											
												<cfif len(rsLead.lead_zip) eq 7>
													<cfset lz=left(rsLead.lead_zip,4) & "-" & mid(rsLead.lead_zip,5,3)>
												<cfelse>
													<cfset lz=rsLead.lead_zip + "-000">
												</cfif>

												<cfset int_try_flag=1>
												<cfif URL.show eq 'yes'>
												<cfoutput>SATISFIES CAMPAIGN AGE, PHONE AND ZIP<br></cfoutput>
												</cfif>
												<cfoutput>
												<!--- http://coreg.webpilots.com/wscampaign.php?realtime=1&country=pt&companyid=9&publisherid=28&campaignid=103&type=S&subid=YOURSPORTO&birthday=[BIRTHDAY]&gender=[GENDER]&firstname=[FIRSTNAME]&lastname=[LASTNAME]&mobile=[MOBILE]&email=[EMAIL]&zip=[ZIP]&country_residence=[COUNTRY_RESIDENCE]&nationality=[NATIONALITY]&data1=[GAME_DESCRIPTION]&ipenduser=[xxx.xxx.xxx.xxx] --->
												<!--- http://coreg.webpilots.com/wscampaign.php?realtime=1&country=pt&companyid=9&publisherid=28&campaignid=103&type=S&subid=YOURSPORTO&birthday=#DateFormat(rsLead.lead_birthdate,"YYYY-MM-DD")#&gender=#rsLead.lead_gender#&firstname=#rsLead.lead_name#&lastname=#rsLead.lead_surname#&mobile=#rsLead.lead_phone#&email=#rsLead.lead_email#&zip=#rsLead.lead_zip#&country_residence=#rsLead.lead_country#&nationality=#rsLead.lead_country#&data1=&ipenduser=#rsLead.lead_ip#--->	
												<br>
												</cfoutput>
												
												<cfhttp url="http://coreg.webpilots.com/wscampaign.php" method="get" result="cresult" charset="utf-8"> 
												    <cfhttpparam type="url" name="realtime" value="1"> 
												    <cfhttpparam type="url" name="country" value="pt"> 
												    <cfhttpparam type="url" name="companyid" value="9"> 
												    <cfhttpparam type="url" name="publisherid" value="28"> 
												    <cfhttpparam type="url" name="campaignid" value="103">
												    <cfhttpparam type="url" name="type" value="S">
												    <cfhttpparam type="url" name="subid" value="YOURSPORTO">
												    <cfhttpparam type="url" name="birthday" value="#DateFormat(rsLead.lead_birthdate,"YYYY-MM-DD")#">
												    <cfhttpparam type="url" name="gender" value="#rsLead.lead_gender#">
												    <cfhttpparam type="url" name="firstname" value="#rsLead.lead_name#">
												    <cfhttpparam type="url" name="lastname" value="#rsLead.lead_surname#">
												    <cfhttpparam type="url" name="mobile" value="#rsLead.lead_phone#">
												    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#">
												    <cfhttpparam type="url" name="zip" value="#lz#">
												    <cfhttpparam type="url" name="country_residence" value="#rsLead.lead_country#">
												    <cfhttpparam type="url" name="nationality" value="#rsLead.lead_country#">
												    <cfhttpparam type="url" name="data1" value="#rsLead.platformID#">	
												    <cfhttpparam type="url" name="ipenduser" value="#rsLead.lead_ip#">
												</cfhttp>

										<cfelse>
											<cfset int_try_flag=0>
										</cfif>

									<cfelse>

											<cfset int_try_flag=0>

									</cfif>

								</cfcase>

								<cfcase value="bonotour-coreg-pt-0007">

									<cfif rsBonoTour.lead_optin eq 1>
									
										<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 25>
											
											<cfif len(rsLead.lead_zip) gte 4>
												
												<cfset int_try_flag=1>
												<cfif URL.show eq 'yes'>
												<cfoutput>SATISFIES CAMPAIGN AGE AND ZIP<br></cfoutput>
												</cfif>
												<cfoutput>
												<!--- http://new.hotelbono.com/process/receptor/receptor.php?pk_camp=MTM2MDk%3Dk9x&fecha_alta=[fecha_alta]&url_refered=[url_refered]&ip=[ip]&email=[email]&nombre=[nombre]&apellidos=[apellidos]&cp=[cp]&sexo=[sexo]&fecha_nac=[fecha_nac]&fk_pais=[fk_pais]&supermercado=[supermercado] --->
												<!---http://new.hotelbono.com/process/receptor/receptor.php?pk_camp=MTM2MDk%3Dk9x&fecha_alta=#DateFormat(rsLead.lead_tstamp,"YYYY-MM-DD")# #TimeFormat(rsLead.lead_tstamp,"HH:MM:SS")#&url_refered=http://www.videntecibele.com&ip=#rsLead.lead_ip#&email=#rsLead.lead_email#&nombre=#rsLead.lead_name#&
												apellidos=#rsLead.lead_surname#&cp=#rsLead.lead_zip#&sexo=#ftranslate['lead_gender'][rsLead.lead_gender]#&fecha_nac=#DateFormat(rsLead.lead_birthdate,"YYYY-MM-DD")#&fk_pais=72&supermercado=#rsLead.platformID#--->
												<br>
												</cfoutput>
												
												<cfhttp url="http://new.hotelbono.com/process/receptor/receptor.php" method="get" result="cresult" charset="utf-8"> 
												    <cfhttpparam type="url" name="pk_camp" value="MTM2MDk%3Dk9x"> 
												    <cfhttpparam type="url" name="fecha_alta" value="#DateFormat(rsLead.lead_tstamp,"YYYY-MM-DD")# #TimeFormat(rsLead.lead_tstamp,"HH:MM:SS")#"> 
												    <cfhttpparam type="url" name="url_refered" value="http://www.videntecibele.com/pt-pt/"> 
												    <cfhttpparam type="url" name="ip" value="#rsLead.lead_ip#"> 
												    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#">
												    <cfhttpparam type="url" name="nombre" value="#rsLead.lead_name#">
												    <cfhttpparam type="url" name="apellidos" value="#rsLead.lead_surname#">
												    <cfhttpparam type="url" name="cp" value="#rsLead.lead_zip#">
												    <cfhttpparam type="url" name="sexo" value="#ftranslate['lead_gender'][rsLead.lead_gender]#">
												    <cfhttpparam type="url" name="fecha_nac" value="#DateFormat(rsLead.lead_birthdate,"YYYY-MM-DD")#">
												    <cfhttpparam type="url" name="fk_pais" value="72">
												    <cfhttpparam type="url" name="supermercado" value="#rsLead.platformID#">
												</cfhttp>

											<cfelse>
												<cfset int_try_flag=0>
											</cfif>

										<cfelse>

												<cfset int_try_flag=0>

										</cfif>

									</cfif>

								</cfcase>

								<cfcase value="padre-coreg-pt-0008">
									
									<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
										
											
										<cfset int_try_flag=1>
										<cfif URL.show eq 'yes'>
										<cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
										</cfif>
										<cfoutput>
										
										<br>
										</cfoutput>

										<cfif URL.token eq ''>
										
											<!--- GET TOKEN FOR PADRE --->
											<cfset loginFields = {'email': 'andre.amorim@yoursporto.com','password': '**Yours2016'} >   

											<cfhttp url="https://api.mensagens-dos-anjos.com/api/Partners/login" method="post" result="cresult" timeout="60">
											    <cfhttpparam type="header" name="Content-Type" value="application/json" />
											    <cfhttpparam type="body" value="#serializeJSON(loginFields)#">
											</cfhttp>

											<cfset tempvar=deserializeJSON(cresult.filecontent)>
											<cfset login_token=tempvar.id>

											<cfset URL.token=login_token>

										</cfif>

										<cfset intFields = {
										"email": "#rsLead.lead_email#",
										"birthdate": "#DateFormat(rsLead.lead_birthdate,'YYYY-MM-DD')#",
										"language" : "pt-pt",
										"timezone" : 0,
										"gender": "#ftranslate['lead_gender'][rsLead.lead_gender]#",
										"firstName": "#rsLead.lead_name#",
										"lastName": "#rsLead.lead_surname#",
										"originDetail": {
										"partner" : "YOURS",
										"referer": "http://www.videntecibele.com/pt-pt",
										"subid": "#rsLead.platformID#",
										"media": "COREG",
										"theme": "angel",
										"campaign": "campaign_2016",
										"campaignarea": "PT"
										},
										"location":{
										"registrationIpV4": "#rsLead.lead_ip#",
										"registrationIpV6": "",
										"number": "",
										"street": "",
										"street2": "",
										"street3": "",
										"zip": "",
										"city": "",
										"state": "",
										"country": "PRT"
										}
										}>
										
										<cfhttp url="https://api.mensagens-dos-anjos.com/api/Customers" method="post" result="cresult" timeout="60">
										    <cfhttpparam type="url" name="access_token" value="#URL.token#">
										    <cfhttpparam type="header" name="Content-Type" value="application/json" />
										    <cfhttpparam type="body" value="#serializeJSON(intFields)#">
										</cfhttp>

									<cfelse>

										<cfset int_try_flag=0>

									</cfif>
									
								</cfcase>

								<cfcase value="padre-coreg-br-0009">
									
									<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
										
											
										<cfset int_try_flag=1>
										<cfif URL.show eq 'yes'>
										<cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
										</cfif>
										<cfoutput>
										
										<br>
										</cfoutput>

										<cfif URL.token eq ''>

											<!--- GET TOKEN FOR PADRE --->
											<cfset loginFields = {'email': 'andre.amorim@yoursporto.com','password': '**Yours2016'} >   

											<cfhttp url="https://api.mensagens-dos-anjos.com/api/Partners/login" method="post" result="cresult" timeout="60">
											    <cfhttpparam type="header" name="Content-Type" value="application/json" />
											    <cfhttpparam type="body" value="#serializeJSON(loginFields)#">
											</cfhttp>

											<cfset tempvar=deserializeJSON(cresult.filecontent)>
											<cfset login_token=tempvar.id>

											<cfset URL.token=login_token>

										</cfif>

										<cfset intFields = {
										"email": "#rsLead.lead_email#",
										"birthdate": "#DateFormat(rsLead.lead_birthdate,'YYYY-MM-DD')#",
										"language" : "pt-pt",
										"timezone" : -3,
										"gender": "#ftranslate['lead_gender'][rsLead.lead_gender]#",
										"firstName": "#rsLead.lead_name#",
										"lastName": "#rsLead.lead_surname#",
										"originDetail": {
										"partner" : "YOURS",
										"referer": "http://www.videntecibele.com/pt-br",
										"subid": "#rsLead.platformID#",
										"media": "COREG",
										"theme": "angel",
										"campaign": "campaign_2016",
										"campaignarea": "BR"
										},
										"location":{
										"registrationIpV4": "#rsLead.lead_ip#",
										"registrationIpV6": "",
										"number": "",
										"street": "",
										"street2": "",
										"street3": "",
										"zip": "",
										"city": "",
										"state": "",
										"country": "BRA"
										}
										}>
										
										<cfhttp url="https://api.mensagens-dos-anjos.com/api/Customers" method="post" result="cresult" timeout="60">
										    <cfhttpparam type="url" name="access_token" value="#URL.token#">
										    <cfhttpparam type="header" name="Content-Type" value="application/json" />
										    <cfhttpparam type="body" value="#serializeJSON(intFields)#">
										</cfhttp>

									<cfelse>

										<cfset int_try_flag=0>

									</cfif>
									
								</cfcase>

								<cfcase value="tara-coreg-es-0010">

									<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
										
										<cfset int_try_flag=1>

										<cfif URL.show eq 'yes'>
										<cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
										</cfif>

										<cfhttp url="http://www.tara-astrologia.com/remote-registration.php" method="post" result="cresult" charset="utf-8"> 
										    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
										    <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#"> 
										    <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#"> 
										    <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#"> 
										    <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
										    <cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> 
										    <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#"> 
										    <cfhttpparam type="url" name="campaignarea" value="ES"> 
										    <cfhttpparam type="url" name="media" value="coreg">
										    <cfhttpparam type="url" name="partner" value="Yours">  
										    <cfhttpparam type="url" name="campaign" value="coreg2017">  
										</cfhttp>

									<cfelse>

										<cfset int_try_flag=0>

									</cfif>
										
								</cfcase>

								<cfcase value="tara-coreg-ar-0011">

									<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
										
										<cfset int_try_flag=1>

										<cfif URL.show eq 'yes'>
										<cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
										</cfif>

										<cfhttp url="http://www.tara-videncia.com/remote-registration.php" method="get" result="cresult" charset="utf-8"> 
										    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
										    <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#"> 
										    <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#"> 
										    <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#"> 
										    <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
										    <cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> 
										    <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#"> 
										    <cfhttpparam type="url" name="campaignarea" value="AR"> 
										    <cfhttpparam type="url" name="media" value="coreg">
										    <cfhttpparam type="url" name="partner" value="Yours">  
										    <cfhttpparam type="url" name="campaign" value="coreg2017">  
										</cfhttp>

									<cfelse>

											<cfset int_try_flag=0>

									</cfif>

								</cfcase>

								<cfcase value="maria-coreg-es-0012">

									<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
										
										<cfset int_try_flag=1>

										<cfif URL.show eq 'yes'>
										<cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
										</cfif>

										<cfhttp url="http://www.videncia-maria.com/remote-registration.php" method="get" result="cresult" charset="utf-8"> 
										    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
										    <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#"> 
										    <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#"> 
										    <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#"> 
										    <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
										    <!---<cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> --->
										    <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#">
										    <cfhttpparam type="url" name="optin" value="#rsLead.lead_optin#">
										    <cfhttpparam type="url" name="campaignarea" value="ES"> 
										    <cfhttpparam type="url" name="media" value="coreg">
										    <cfhttpparam type="url" name="partner" value="Yours">  
										    <cfhttpparam type="url" name="campaign" value="coreg2017">  
										</cfhttp>


									<cfelse>

											<cfset int_try_flag=0>

									</cfif>
										
								</cfcase>

								<cfcase value="maria-coreg-ar-0013">

									<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
										
										<cfset int_try_flag=1>

										<cfif URL.show eq 'yes'>
										<cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
										</cfif>
										
										<cfhttp url="http://www.maria-clarividencia.com/remote-registration.php" method="get" result="cresult" charset="utf-8"> 
										    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
										    <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#"> 
										    <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#"> 
										    <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#"> 
										    <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
										    <!---<cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> --->
										    <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#">
										    <cfhttpparam type="url" name="optin" value="#rsLead.lead_optin#">
										    <cfhttpparam type="url" name="campaignarea" value="AR"> 
										    <cfhttpparam type="url" name="media" value="coreg">
										    <cfhttpparam type="url" name="partner" value="Yours">  
										    <cfhttpparam type="url" name="campaign" value="coreg2017">  
										</cfhttp>

									<cfelse>

											<cfset int_try_flag=0>

									</cfif>

								</cfcase>

								<cfcase value="padre-coreg-es-0014">
									
									<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
										
											
										<cfset int_try_flag=1>
										<cfif URL.show eq 'yes'>
										<cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
										</cfif>
										<cfoutput>
										
										<br>
										</cfoutput>

										<cfif URL.token eq ''>
										
											<!--- GET TOKEN FOR PADRE --->
											<cfset loginFields = {'email': 'andre.amorim@yoursporto.com','password': '**Yours2016'} >   

											<cfhttp url="https://api.mensagens-dos-anjos.com/api/Partners/login" method="post" result="cresult" timeout="60">
											    <cfhttpparam type="header" name="Content-Type" value="application/json" />
											    <cfhttpparam type="body" value="#serializeJSON(loginFields)#">
											</cfhttp>

											<cfset tempvar=deserializeJSON(cresult.filecontent)>
											<cfset login_token=tempvar.id>

											<cfset URL.token=login_token>

										</cfif>

										<cfset intFields = {
										"email": "#rsLead.lead_email#",
										"birthdate": "#DateFormat(rsLead.lead_birthdate,'YYYY-MM-DD')#",
										"language" : "es",
										"timezone" : 0,
										"gender": "#ftranslate['lead_gender'][rsLead.lead_gender]#",
										"firstName": "#rsLead.lead_name#",
										"lastName": "#rsLead.lead_surname#",
										"originDetail": {
										"partner" : "YOURS",
										"referer": "http://www.videntecibele.com/es-es",
										"subid": "#rsLead.platformID#",
										"media": "COREG",
										"theme": "angel",
										"campaign": "campaign_2016",
										"campaignarea": "ES"
										},
										"location":{
										"registrationIpV4": "#rsLead.lead_ip#",
										"registrationIpV6": "",
										"number": "",
										"street": "",
										"street2": "",
										"street3": "",
										"zip": "",
										"city": "",
										"state": "",
										"country": "ESP"
										}
										}>
										
										<cfhttp url="https://api.mensagens-dos-anjos.com/api/Customers" method="post" result="cresult" timeout="60">
										    <cfhttpparam type="url" name="access_token" value="#URL.token#">
										    <cfhttpparam type="header" name="Content-Type" value="application/json" />
										    <cfhttpparam type="body" value="#serializeJSON(intFields)#">
										</cfhttp>

									<cfelse>

										<cfset int_try_flag=0>

									</cfif>
									
								</cfcase>

								<cfcase value="padre-coreg-ar-0015">
									
									<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
										
											
										<cfset int_try_flag=1>
										<cfif URL.show eq 'yes'>
										<cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
										</cfif>
										<cfoutput>
										
										<br>
										</cfoutput>

										<cfif URL.token eq ''>

											<!--- GET TOKEN FOR PADRE --->
											<cfset loginFields = {'email': 'andre.amorim@yoursporto.com','password': '**Yours2016'} >   

											<cfhttp url="https://api.mensagens-dos-anjos.com/api/Partners/login" method="post" result="cresult" timeout="60">
											    <cfhttpparam type="header" name="Content-Type" value="application/json" />
											    <cfhttpparam type="body" value="#serializeJSON(loginFields)#">
											</cfhttp>

											<cfset tempvar=deserializeJSON(cresult.filecontent)>
											<cfset login_token=tempvar.id>

											<cfset URL.token=login_token>

										</cfif>

										<cfset intFields = {
										"email": "#rsLead.lead_email#",
										"birthdate": "#DateFormat(rsLead.lead_birthdate,'YYYY-MM-DD')#",
										"language" : "es",
										"timezone" : -3,
										"gender": "#ftranslate['lead_gender'][rsLead.lead_gender]#",
										"firstName": "#rsLead.lead_name#",
										"lastName": "#rsLead.lead_surname#",
										"originDetail": {
										"partner" : "YOURS",
										"referer": "http://www.videntecibele.com/es-ar",
										"subid": "#rsLead.platformID#",
										"media": "COREG",
										"theme": "angel",
										"campaign": "campaign_2016",
										"campaignarea": "AR"
										},
										"location":{
										"registrationIpV4": "#rsLead.lead_ip#",
										"registrationIpV6": "",
										"number": "",
										"street": "",
										"street2": "",
										"street3": "",
										"zip": "",
										"city": "",
										"state": "",
										"country": "ARG"
										}
										}>
										
										<cfhttp url="https://api.mensagens-dos-anjos.com/api/Customers" method="post" result="cresult" timeout="60">
										    <cfhttpparam type="url" name="access_token" value="#URL.token#">
										    <cfhttpparam type="header" name="Content-Type" value="application/json" />
										    <cfhttpparam type="body" value="#serializeJSON(intFields)#">
										</cfhttp>

									<cfelse>

										<cfset int_try_flag=0>

									</cfif>
									
								</cfcase>

								<cfcase value="easyvoyage-spons-es-0016">

									<cfif rsEasyVoyage.lead_optin eq 1>

										<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 18>
											
											<cfif len(rsLead.lead_zip) eq 5>
												
												<cfif FindNoCase('@gmail.com', rsLead.lead_email) eq 0>

													<cfset int_try_flag=1>
													<cfif URL.show eq 'yes'>
													<cfoutput>SATISFIES CAMPAIGN AGE, ZIP AND NO GMAIL<br></cfoutput>
													</cfif>
													<cfoutput>
													<!--- ?id=314&email=[EMAIL]&nombre=[NOMBRE]&apellidos=[APELLIDOS]&dob=[DIA_DE_NACIMIENTO]&mob=[MES_DE_NACIMIENTO]&yob=[ANYO_DE_NACIMIENTO]&sexo=[SEXO_M_O_F]&cp=[COD_POSTAL]&ip=[IP_DE_REGISTRO]&ref=[WEB_ASOCIADA_AL_WEBMASTER] --->
									
													<br>
													</cfoutput>
													
													<cfhttp url="http://www.mailingcp.com/corregistros/pasarelacrg.php" method="get" result="cresult" charset="utf-8"> 
													    <cfhttpparam type="url" name="id" value="441"> 
													    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
													    <cfhttpparam type="url" name="nombre" value="#rsLead.lead_name#"> 
													    <cfhttpparam type="url" name="apellidos" value="#rsLead.lead_surname#"> 
													    <cfhttpparam type="url" name="dob" value="#day(rsLead.lead_birthdate)#">
													    <cfhttpparam type="url" name="mob" value="#month(rsLead.lead_birthdate)#">
													    <cfhttpparam type="url" name="yob" value="#year(rsLead.lead_birthdate)#">
													    <cfhttpparam type="url" name="sexo" value="#rsLead.lead_gender#">
													    <cfhttpparam type="url" name="cp" value="#rsLead.lead_zip#">
													    <cfhttpparam type="url" name="ip" value="#rsLead.lead_ip#">
													    <cfhttpparam type="url" name="ref" value="#rsLead.platformID#">
													</cfhttp>

												<cfelse>
													<cfset int_try_flag=0>
												</cfif>

											<cfelse>
												<cfset int_try_flag=0>
											</cfif>

										<cfelse>

												<cfset int_try_flag=0>

										</cfif>

									<cfelse>
										
										<cfset int_try_flag=0>

									</cfif>

								</cfcase>

							</cfswitch>

							<cfif int_try_flag eq 1>
								
								<cfif URL.show eq 'yes'>
								<cfoutput>RESULT FROM #UCASE(ccampaignID)#<br></cfoutput>
								<cfdump var="#cresult#">
								</cfif>

								<cfif trim(cresult.filecontent) eq 'ok' or (FindNoCase('OK', cresult.filecontent) gt 0 and FindNoCase('book', cresult.filecontent) eq 0) or FindNoCase('"errorCode":0,', cresult.filecontent) gt 0 or FindNoCase('created', cresult.filecontent) gt 0 >
									<cfset statusID=10>
									<cfset int_statusID=1>
									<cfif URL.show eq 'yes'>
									<cfoutput>LEAD SUCCESSFULY INTEGRATED ON #rsCampaign.campaignID#</cfoutput>
									</cfif>
								<cfelseif FindNoCase('já está', cresult.filecontent) gt 0 or FindNoCase('já estão', cresult.filecontent) gt 0 or FindNoCase('already exist', cresult.filecontent) gt 0 or FindNoCase('"errorCode":10,', cresult.filecontent) gt 0 or FindNoCase('"errorCode":20,', cresult.filecontent) gt 0 or FindNoCase('"errorCode":30,', cresult.filecontent) gt 0 or FindNoCase('duplicate', cresult.filecontent) gt 0 or FindNoCase('KO - 0001', cresult.filecontent) gt 0 or FindNoCase('Repetido', cresult.filecontent) gt 0 or FindNoCase('L345', cresult.filecontent) gt 0 or FindNoCase('L054', cresult.filecontent) gt 0 or FindNoCase('Err. 004', cresult.filecontent) gt 0>
									<cfif rsOthSuccessInt.recordcount eq 0><cfset statusID=5><cfelse><cfset statusID=rsLead.statusID></cfif>
									<cfset int_statusID=0>
									<cfif URL.show eq 'yes'>
									<cfoutput>LEAD NOT INTEGRATED ON #rsCampaign.campaignID# >#cresult.filecontent#</cfoutput>
									</cfif>
								<cfelseif FindNoCase('"email"', cresult.filecontent) gt 0 or FindNoCase('email validation', cresult.filecontent) gt 0 or FindNoCase('bad', cresult.filecontent) gt 0 or FindNoCase('mailbox', cresult.filecontent) gt 0 or FindNoCase('"errorCode":103,', cresult.filecontent) gt 0 or FindNoCase('"errorCode":104,', cresult.filecontent) gt 0 or FindNoCase('KO - 0002', cresult.filecontent) gt 0  or FindNoCase('L351', cresult.filecontent) gt 0 or FindNoCase('L801', cresult.filecontent) gt 0 or FindNoCase('Err. 003', cresult.filecontent) gt 0>
									<cfif rsOthSuccessInt.recordcount eq 0><cfset statusID=-1><cfelse><cfset statusID=rsLead.statusID></cfif>
									<cfset int_statusID=-1>
									<cfif URL.show eq 'yes'>
									<cfoutput>LEAD NOT INTEGRATED ON #rsCampaign.campaignID# >#cresult.filecontent#</cfoutput>
									</cfif>
								<cfelse>
									<cfif rsOthSuccessInt.recordcount eq 0><cfset statusID=-1><cfelse><cfset statusID=rsLead.statusID></cfif>
									<cfset int_statusID=-2>
									<cfif URL.show eq 'yes'>
									<cfoutput>LEAD NOT INTEGRATED ON #rsCampaign.campaignID# >#cresult.filecontent#</cfoutput>
									</cfif>
								</cfif>
								<br>

							<cfelse>
								<!--- doesn't satisfy campaign 2nd+ rule --->
								<cfset cresult=StructNew()>
								<cfset statusID=rsLead.statusID>
								<cfset int_statusID=-3>
								<cfset cresult.filecontent='it does not satisfy campaign 2nd+ rule'>
								<cfif URL.show eq 'yes'>
								<cfoutput>DOES NOT SATISFY CAMPAIGN 2nd RULES FOR #UCASE(ccampaignID)#<br></cfoutput>
								</cfif>

							</cfif>

						<cfelse>	
						<!--- doesn't satisfy campaign country --->
							
							<cfset cresult=StructNew()>
							<cfset statusID=rsLead.statusID>
							<cfset int_statusID=-3>
							<cfset cresult.filecontent='it does not satisfy campaign country'>
							<cfif URL.show eq 'yes'>
							<cfoutput>DOES NOT SATISFY CAMPAIGN COUNTRY FOR #UCASE(ccampaignID)#<br></cfoutput>
							</cfif>

						</cfif>

						<!--- save actions --->
						<cfquery name="int_p1">
							insert into lead_integration_attempts (campaignID, leadID, tstamp, statusID, result)
							values ('#rsCampaign.campaignID#', '#rsLead.leadID#', getdate(), #int_statusID#, '#cresult.filecontent#')
						</cfquery>

						<cfquery name="int_p2">
							delete from lead_integration where campaignID='#rsCampaign.campaignID#' and leadID='#rsLead.leadID#'
						</cfquery>

						<cfquery name="int_p3">
							insert into lead_integration (campaignID, leadID, tstamp, statusID)
							values ('#rsCampaign.campaignID#', '#rsLead.leadID#', getdate(), #int_statusID#)
						</cfquery>

						<cfquery name="int_p4">
							update lead_feed set statusID=#statusID# where leadID='#rsLead.leadID#'
						</cfquery>

					</cfif>

				</cfif>
		
			</cfif>

		</cfloop>


		<br><br>
		<cfif URL.show eq 'yes'>
		<cfoutput>
		<a href="../admin/edit.cfm?leadID=#rsLead.leadID#" role="button" class="btn btn-default margin" data-var1="">Back to lead</a>
		</cfoutput>
		</cfif>


    	<cfset result.leadID=URL.leadID>

    	<cfreturn result>

	</cffunction>














	<cffunction name="doIntegrationEgoi" access="remote" returnformat="json">
		<cfargument name="leadID" type="any" required="false" /> 

		<cfparam name="URL.leadID" default="">
		<cfparam name="URL.ccampaignID" default="">
		<cfparam name="URL.show" default="no">

		<cfif ARGUMENTS.leadID neq "">
			<cfset URL.leadID=ARGUMENTS.leadID>
		</cfif>

		<CFSTOREDPROC procedure="dbo.getLeadsforIndividualIntegration">
		  <CFPROCPARAM type="IN" value="#URL.leadID#" cfsqltype="CF_SQL_VARCHAR">
		  <CFPROCPARAM type="IN" value="#URL.ccampaignID#" cfsqltype="CF_SQL_VARCHAR">
		  <CFPROCRESULT name="rsLead" resultset="1">
		</CFSTOREDPROC>

		<cfif URL.show eq 'yes'>
		<cfoutput>LEAD FOR INTEGRATION:<br></cfoutput>
		<cfdump var="#rsLead#">
		</cfif>


		<!--- INTEGRATE WITH E-GOI --->
			<!--- set campaign --->
			<cfset URL.ccampaignID='egoi-0000'>

			<!--- get data --->
			<CFSTOREDPROC procedure="dbo.getLeadsforIndividualIntegration">
			  <CFPROCPARAM type="IN" value="#URL.leadID#" cfsqltype="CF_SQL_VARCHAR">
			  <CFPROCPARAM type="IN" value="#URL.ccampaignID#" cfsqltype="CF_SQL_VARCHAR">
			  <CFPROCRESULT name="rsIntegrations" resultset="2">
			  <CFPROCRESULT name="rsOthSuccessInt" resultset="3">
			</CFSTOREDPROC>

			<CFSTOREDPROC procedure="dbo.getClientCampaign">
			  <CFPROCPARAM type="IN" value="#URL.ccampaignID#" cfsqltype="CF_SQL_VARCHAR">
			  <CFPROCRESULT name="rsCampaign" resultset="1">
			  <CFPROCRESULT name="rsRules" resultset="2">
			  <CFPROCRESULT name="rsFields" resultset="3">
			</CFSTOREDPROC>

			<!--- transform data --->
			<cfset ftranslate = StructNew() >

			<cfloop query="rsFields">
				<cfset ftranslate["#rsFields.field_name#"]["#rsFields.field_value#"] = rsFields.field_value_client >
			</cfloop>
	
			<cfif URL.show eq 'yes'>
			<cfoutput>ATTEMPT WITH #UCASE(URL.ccampaignID)#<br></cfoutput>
			</cfif>

			<cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 0 and (rsLead.lead_country eq 'PT' or rsLead.lead_country eq 'BR' or rsLead.lead_country eq 'ES' or rsLead.lead_country eq 'AR')>
			<!--- satisfies campaing rules --->
				<cfif URL.show eq 'yes'>
				<cfoutput>SATISFIES CAMPAIGN RULES<br></cfoutput>
				</cfif>
				
				<cfset apikey="46099444df9915f368e9b525fa3d87b525cb0c85">
				<cfset listID='2'>
				<cfset tags= ''>
				<cfset theurl='http://api.e-goi.com/v2/rest.php?method=addSubscriber'>
				<cfset qs='&functionOptions[apikey]=' & apikey>
				<cfset qs=qs & '&functionOptions[listID]=' & listID>
				<cfset qs=qs & '&functionOptions[status]=' & '1'>
				<cfset qs=qs & '&functionOptions[lang]=' & lcase(rsLead.lead_country)>
				<cfset qs=qs & '&functionOptions[email]=' & rsLead.lead_email>
				<cfset qs=qs & '&functionOptions[telephone]=' & rsLead.lead_phone>
				<cfset qs=qs & '&functionOptions[validate_phone]=' & '0'>
				<cfset qs=qs & '&functionOptions[first_name]=' & rsLead.lead_name>
				<cfset qs=qs & '&functionOptions[last_name]=' & rsLead.lead_surname>
				<cfset qs=qs & '&functionOptions[birth_date]=' & rsLead.lead_birthdate>
				<cfset qs=qs & '&functionOptions[extra_1]=' & rsLead.lead_country>
				<cfset qs=qs & '&functionOptions[extra_2]=' & rsLead.lead_zip>
				<cfset qs=qs & '&functionOptions[extra_15]=' & rsLead.lead_interest>
				<cfset qs=qs & '&functionOptions[extra_16]=' & rsLead.lead_gender>
				<cfset qs=qs & '&functionOptions[extra_6]=' & rsLead.leadID>
				<cfset qs=qs & '&functionOptions[extra_10]=' & DateFormat(rsLead.lead_tstamp, "YYYY-MM-DD")>
				<cfset qs=qs & '&functionOptions[extra_19]=' & rsLead.lead_sign>
				<cfset qs=qs & '&functionOptions[extra_17]=' & rsLead.sign_element>
				<cfset qs=qs & '&functionOptions[extra_20]=' & rsLead.sign_characteristics1>
				<cfset qs=qs & '&functionOptions[extra_21]=' & rsLead.sign_characteristics2>
				<cfset qs=qs & '&functionOptions[extra_22]=' & rsLead.sign_planet1>
				<cfset qs=qs & '&functionOptions[extra_23]=' & rsLead.sign_incense>
				<cfset qs=qs & '&functionOptions[extra_24]=' & rsLead.sign_stone>
				<cfset qs=qs & '&functionOptions[extra_25]=' & rsLead.sign_metal>
				<cfset qs=qs & '&functionOptions[extra_26]=' & rsLead.sign_number>
				<cfset qs=qs & '&functionOptions[extra_27]=' & rsLead.sign_color>
				<cfif rsLead.mpt eq 1>
				<cfset tags=tags & '&functionOptions[tags][]=2'>
				</cfif>
				<cfif rsLead.mbr eq 1>
				<cfset tags=tags & '&functionOptions[tags][]=3'>
				</cfif>
				<cfif rsLead.tpt eq 1>
				<cfset tags=tags & '&functionOptions[tags][]=4'>
				</cfif>
				<cfif rsLead.tbr eq 1>
				<cfset tags=tags & '&functionOptions[tags][]=5'>
				</cfif>
				<cfif rsLead.abr eq 1>
				<cfset tags=tags & '&functionOptions[tags][]=8'>
				</cfif>
				<cfset qs=qs & tags>
				<cfset theurl=theurl & qs>
				<cfif URL.show eq 'yes'>
				CALL TO EGOI:<br>
				<cfoutput>#theurl#</cfoutput>
				<br>
				</cfif>
				<!---  (0 = unconfirmed; 1 = active; 2 = removed; 4 = inactive).  --->

				<cfhttp url="#theurl#" method="get" result="eresult" charset="utf-8"></cfhttp>


				<cfset egoiresponse='#eresult.fileContent#'>


				<cfset objXml = createObject("component","xml2Struct")>

				<!--- append the returned structure to some existing structure--->
				<cfset str = structnew()>
				<cfset str.existingElement = "some text here">
				<cfset str = objXml.ConvertXmlToStruct(egoiresponse, str)>


				<!--- create a new structure --->
				<cfset newStr = objXml.ConvertXmlToStruct(egoiresponse, StructNew())>
				<cfif URL.show eq 'yes'>
				<cfoutput>RESULT FROM #UCASE(URL.ccampaignID)#<br></cfoutput>
				<cfdump var = "#newStr#">
				</cfif>

				<cfif isDefined("newStr.addSubscriber.error")>
					<cfset theresult=newStr.addSubscriber.error>
				<cfelse>
					<cfset theresult='ok'>
				</cfif>

				<cfif trim(theresult) eq 'ok'>
					<cfset statusID=10>
					<cfset int_statusID=1>

					<cfquery name="int_p0">
						update lead_feed set egoiID='#newStr.addSubscriber.UID#' where leadID='#rsLead.leadID#'
					</cfquery>

					<cfif URL.show eq 'yes'>
					<cfoutput>LEAD SUCCESSFULY INTEGRATED ON #rsCampaign.campaignID#</cfoutput> 
					</cfif>
				<cfelseif FindNoCase('ALREADY_EXISTS', eresult.filecontent) gt 0>
					<cfif rsOthSuccessInt.recordcount eq 0><cfset statusID=5><cfelse><cfset statusID=rsLead.statusID></cfif>
					<cfset int_statusID=0>
					<cfif URL.show eq 'yes'>
					<cfoutput>LEAD NOT INTEGRATED ON #rsCampaign.campaignID# >#eresult.filecontent#</cfoutput>
					</cfif>
				<cfelseif FindNoCase('ADDRESS_INVALID', eresult.filecontent) gt 0>
					<cfif rsOthSuccessInt.recordcount eq 0><cfset statusID=-1><cfelse><cfset statusID=rsLead.statusID></cfif>
					<cfset int_statusID=-1>
					<cfif URL.show eq 'yes'>
					<cfoutput>LEAD NOT INTEGRATED ON #rsCampaign.campaignID# >#eresult.filecontent#</cfoutput>
					</cfif>
				<cfelse>
					<cfif rsOthSuccessInt.recordcount eq 0><cfset statusID=-1><cfelse><cfset statusID=rsLead.statusID></cfif>
					<cfset int_statusID=-2>
					<cfif URL.show eq 'yes'>
					<cfoutput>LEAD NOT INTEGRATED ON #rsCampaign.campaignID# >#eresult.filecontent#</cfoutput>
					</cfif>
				</cfif>

			<cfelse>	
			<!--- doesn't satisfy campaign rules --->
				
				<cfset statusID=rsLead.statusID>
				<cfset int_statusID=-3>
				<cfset theresult='it does not satisfy campaign rules'>
				<cfif URL.show eq 'yes'>
				<cfoutput>DOES NOT SATISFY CAMPAIGN RULES FOR #UCASE(URL.ccampaignID)#<br></cfoutput>
				</cfif>

			</cfif>

			<cfquery name="int_p1">
				insert into lead_integration_attempts (campaignID, leadID, tstamp, statusID, result)
				values ('#rsCampaign.campaignID#', '#rsLead.leadID#', getdate(), #int_statusID#, '#theresult#')
			</cfquery>

			<cfif rsIntegrations.recordcount eq 0>
			
				<cfquery name="int_p2">
					insert into lead_integration (campaignID, leadID, tstamp, statusID)
					values ('#rsCampaign.campaignID#', '#rsLead.leadID#', getdate(), #int_statusID#)
				</cfquery>
			
			<cfelse>

				<cfquery name="int_p2">
					update lead_integration 
					set 
					statusID='#int_statusID#',
					tstamp=getdate()
					where campaignID='#rsCampaign.campaignID#' and leadID='#rsLead.leadID#'
				</cfquery>
			
			</cfif>

			<cfquery name="int_p3">
				update lead_feed set statusID=#statusID# where leadID='#rsLead.leadID#'
			</cfquery>

			<br><br>
			<cfif URL.show eq 'yes'>
			<cfoutput>
			<a href="../admin/edit.cfm?leadID=#rsLead.leadID#" role="button" class="btn btn-default margin" data-var1="">Back to lead</a>
			</cfoutput>
			</cfif>

    	<cfset result.leadID=URL.leadID>

    	<cfreturn result>

	</cffunction>





</cfcomponent>