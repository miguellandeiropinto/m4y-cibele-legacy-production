<cfcomponent>
    <cfprocessingdirective pageencoding="utf-8">

            <cffunction name="doReintegration">

                <cfparam name="URL.campaign" default=""/>
                <cfparam name="URL.country" default=""/>
                <cfparam name="URL.nof" default=""/>

                <cfif URL.c neq ="">
                    <cfquery name="rsLeads">
                        SELECT top(50) LF.* FROM lead_feed LF
                            INNER JOIN lead_integration LI ON LI.leadID = LF.leadID
                        WHERE
                            LF.lead_country = #URL.country# AND
                            LI.campaignID = #URL.campaign# AND
                            LI.statusID != 1 AND
                            (
                                LF.lead_tstamp > '2017-05-31 23:59:59.999' OR
                                LF.lead_tstamp_upd > '2017-05-31 23:59:59.999'
                            )
                    </cfquery>
                </cfif>
            </cffunction>

</cfcomponent>