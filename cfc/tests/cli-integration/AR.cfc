<cfcomponent name="AR_client_integration">
    <cfprocessingdirective pageencoding="utf-8">
    <cffunction name="integrate" access="remote">

        <cfparam name="URL.leadID" default="-1"/>

        <cfif #URL.leadID# neq "-1">


            <cfquery name="rsLead">
                SELECT * FROM lead_feed WHERE leadID = '#URL.leadID#'
            </cfquery>

            <!--- transform data --->
            <cfset ftranslate = StructNew() >

            <cfloop query="rsFields">
                <cfset ftranslate["#rsFields.field_name#"]["#rsFields.field_value#"] = rsFields.field_value_client />
            </cfloop>

            <cfloop query="rsLead">
                <!--- START TARA --->
                <cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
                    <cfset int_try_flag=1>
                    <!---
                    <cfhttp url="http://www.tara-videncia.com/remote-registration.php" method="get" result="cresult" charset="utf-8">
                        <cfhttpparam type="url" name="email" value="#rsLead.lead_email#">
                        <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#">
                        <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#">
                        <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#">
                        <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
                        <cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#">
                        <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#">
                        <cfhttpparam type="url" name="campaignarea" value="AR">
                        <cfhttpparam type="url" name="media" value="coreg">
                        <cfhttpparam type="url" name="partner" value="Yours">
                        <cfhttpparam type="url" name="campaign" value="coreg2017">
                    </cfhttp>--->
                <cfelse>
                    <cfset int_try_flag=0>
                </cfif>
                <!--- END TARA AR --->
                <!--- START MARIA --->
                <cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
                    <cfset int_try_flag=1>
                    <cfhttp url="http://www.maria-clarividencia.com/remote-registration.php" method="get" result="cresult" charset="utf-8">
                        <cfhttpparam type="url" name="email" value="#rsLead.lead_email#">
                        <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#">
                        <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#">
                        <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#">
                        <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
                        <!---<cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> --->
                        <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#">
                        <cfhttpparam type="url" name="optin" value="#rsLead.lead_optin#">
                        <cfhttpparam type="url" name="campaignarea" value="AR">
                        <cfhttpparam type="url" name="media" value="coreg">
                        <cfhttpparam type="url" name="partner" value="Yours">
                        <cfhttpparam type="url" name="campaign" value="coreg2017">
                    </cfhttp>
                <cfelse>
                    <cfset int_try_flag=0>
                </cfif>
                <!--- END MARIA --->
                <!--- START PADRE --->
                <cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
                    <cfset int_try_flag=1>
                    <cfif URL.show eq 'yes'>
                        <cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
                    </cfif>
                    <cfif URL.token eq ''>
                        <!--- GET TOKEN FOR PADRE --->
                        <cfset loginFields = {'email': 'andre.amorim@yoursporto.com','password': '**Yours2016'} >
                        <cfhttp url="https://api.mensagens-dos-anjos.com/api/Partners/login" method="post" result="cresult" timeout="60">
                            <cfhttpparam type="header" name="Content-Type" value="application/json" />
                            <cfhttpparam type="body" value="#serializeJSON(loginFields)#">
                        </cfhttp>

                        <cfset tempvar=deserializeJSON(cresult.filecontent)>
                        <cfset login_token=tempvar.id>
                        <cfset URL.token=login_token>
                    </cfif>
                    <cfset intFields = {
                            "email": "#rsLead.lead_email#",
                            "birthdate": "#DateFormat(rsLead.lead_birthdate,'YYYY-MM-DD')#",
                            "language" : "es",
                            "timezone" : -3,
                            "gender": "#ftranslate['lead_gender'][rsLead.lead_gender]#",
                            "firstName": "#rsLead.lead_name#",
                            "lastName": "#rsLead.lead_surname#",
                            "originDetail": {
                                "partner" : "YOURS",
                                "referer": "http://www.videntecibele.com/es-ar",
                                "subid": "#rsLead.platformID#",
                                "media": "COREG",
                                "theme": "angel",
                                "campaign": "campaign_2016",
                                "campaignarea": "AR"
                            },
                            "location":{
                                "registrationIpV4": "#rsLead.lead_ip#",
                                "registrationIpV6": "",
                                "number": "",
                                "street": "",
                                "street2": "",
                                "street3": "",
                                "zip": "",
                                "city": "",
                                "state": "",
                                "country": "ARG"
                            }
                        }/>
                        <cfhttp url="https://api.mensagens-dos-anjos.com/api/Customers" method="post" result="cresult" timeout="60">
                            <cfhttpparam type="url" name="access_token" value="#URL.token#">
                            <cfhttpparam type="header" name="Content-Type" value="application/json" />
                            <cfhttpparam type="body" value="#serializeJSON(intFields)#">
                        </cfhttp>

                <cfelse>
                    <cfset int_try_flag=0>
                </cfif>
                <!--- END PADRE --->
    </cfloop>
        </cfif>
        <cfoutput>AR Country clients</cfoutput>
    </cffunction>
</cfcomponent>