<cfcomponent>
    <cfprocessingdirective pageencoding="utf-8">

    <cffunction name="insertLead" access="remote">

        <cfparam name="URL.lead_email" default="">
        <cfparam name="URL.lead_fname" default="">
        <cfparam name="URL.lead_surname" default="">
        <cfparam name="URL.lead_gender" default="">
        <cfparam name="URL.lead_sign" default="">
        <cfparam name="URL.lead_interest" default="">
        <cfparam name="URL.lead_birthdate" default="">
        <cfparam name="URL.lead_ip" default="">
        <cfparam name="URL.lead_phone" default="">
        <cfparam name="URL.lead_country" default="">
        <cfparam name="URL.lead_zip" default="">
        <cfparam name="URL.lead_tstamp_upd" default="">
        <cfparam name="URL.mailchimpID" default="">
        <cfparam name="URL.leadID" default="">

        <CFSTOREDPROC procedure="dbo.insert_lead">
            <CFPROCPARAM type="IN" value="n/a" cfsqltype="CF_SQL_VARCHAR">
            <CFPROCPARAM type="IN" value="Imperdivel" cfsqltype="CF_SQL_VARCHAR">
            <CFPROCPARAM type="IN" value="imperdivel.eu" cfsqltype="CF_SQL_VARCHAR">
            <CFPROCPARAM type="IN" value="EmailNewsletter" cfsqltype="CF_SQL_VARCHAR">
            <CFPROCPARAM type="IN" value="MCBot#URL.lead_country#" cfsqltype="CF_SQL_VARCHAR">
            <CFPROCPARAM type="IN" value="#URL.lead_country#" cfsqltype="CF_SQL_VARCHAR">
            <CFPROCPARAM type="IN" value="#URL.lead_ip#" cfsqltype="CF_SQL_VARCHAR">
            <CFPROCPARAM type="IN" value="1" cfsqltype="CF_SQL_INTEGER">
            <CFPROCPARAM type="IN" value="#URL.lead_email#" cfsqltype="CF_SQL_VARCHAR">
            <CFPROCPARAM type="IN" value="#URL.lead_fname#" cfsqltype="CF_SQL_VARCHAR">
            <CFPROCPARAM type="IN" value="#URL.lead_surname#" cfsqltype="CF_SQL_VARCHAR">
            <CFPROCPARAM type="IN" value="#URL.lead_gender#" cfsqltype="CF_SQL_VARCHAR">
            <CFPROCPARAM type="IN" value="#URL.lead_birthdate#" cfsqltype="CF_SQL_VARCHAR">
            <CFPROCPARAM type="IN" value="#URL.lead_sign#" cfsqltype="CF_SQL_VARCHAR">
            <CFPROCPARAM type="IN" value="#URL.lead_phone#" cfsqltype="CF_SQL_VARCHAR">
            <CFPROCPARAM type="IN" value="#URL.lead_zip#" cfsqltype="CF_SQL_VARCHAR">
            <CFPROCPARAM type="IN" value="#URL.lead_interest#" cfsqltype="CF_SQL_VARCHAR">
            <CFPROCPARAM type="OUT" variable="leadID" cfsqltype="CF_SQL_VARCHAR">
        </CFSTOREDPROC>

		<CFSTOREDPROC procedure="dbo.update_optins">
			<CFPROCPARAM type="IN" value="1" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#URL.lead_email#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="1" cfsqltype="CF_SQL_INTEGER">
		</CFSTOREDPROC>

		<CFSTOREDPROC procedure="dbo.update_optins">
			<CFPROCPARAM type="IN" value="2" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="#URL.lead_email#" cfsqltype="CF_SQL_VARCHAR">
			<CFPROCPARAM type="IN" value="1" cfsqltype="CF_SQL_INTEGER">
		</CFSTOREDPROC>

    </cffunction>

    <cffunction name="unsubscribeLead" access="remote">
            <cfparam name="URL.lead_email" default=""/>
            <CFSTOREDPROC procedure="dbo.update_optins">
                <CFPROCPARAM type="IN" value="1" cfsqltype="CF_SQL_VARCHAR">
                <CFPROCPARAM type="IN" value="#URL.lead_email#" cfsqltype="CF_SQL_VARCHAR">
                <CFPROCPARAM type="IN" value="0" cfsqltype="CF_SQL_INTEGER">
            </CFSTOREDPROC>

            <CFSTOREDPROC procedure="dbo.update_optins">
                <CFPROCPARAM type="IN" value="2" cfsqltype="CF_SQL_VARCHAR">
                <CFPROCPARAM type="IN" value="#URL.lead_email#" cfsqltype="CF_SQL_VARCHAR">
                <CFPROCPARAM type="IN" value="0" cfsqltype="CF_SQL_INTEGER">
            </CFSTOREDPROC>
    </cffunction>

</cfcomponent>