<cfcomponent>
    <cfprocessingdirective pageencoding="utf-8">
        <cfsetting requesttimeout="9000">
            <cffunction name="integrate" access="remote">
                <cfparam name="URL.leadID" default="-1"/>
                <cfparam name="URL.show" default="no"/>

                <cfif #URL.leadID# neq "-1">
                    <cfquery name="rsLead">
                        SELECT top(1) LF.*, Z.* FROM lead_feed LF
                        INNER JOIN cmys_zodiac Z ON Z.sign_key = LF.lead_sign
                        WHERE LF.leadID = '#URL.leadID#'
                    </cfquery>

                    <cfset apikey="46099444df9915f368e9b525fa3d87b525cb0c85">
                    <cfset listID='2'>
                    <cfloop query="rsLead">
                            <cfset theurl='http://api.e-goi.com/v2/rest.php?method=addSubscriber'>
                            <cfset qs='&functionOptions[apikey]=' & apikey>
                            <cfset qs=qs & '&functionOptions[listID]=' & listID>
                            <cfset qs=qs & '&functionOptions[status]=' & '1'>
                            <cfset qs=qs & '&functionOptions[lang]=' & lcase(rsLead.lead_country)>
                            <cfset qs=qs & '&functionOptions[email]=' & rsLead.lead_email>
                            <cfset qs=qs & '&functionOptions[telephone]=' & rsLead.lead_phone>
                            <cfset qs=qs & '&functionOptions[validate_phone]=' & '0'>
                            <cfset qs=qs & '&functionOptions[first_name]=' & rsLead.lead_name>
                            <cfset qs=qs & '&functionOptions[last_name]=' & rsLead.lead_surname>
                            <cfset qs=qs & '&functionOptions[birth_date]=' & rsLead.lead_birthdate>
                            <cfset qs=qs & '&functionOptions[extra_1]=' & rsLead.lead_country>
                            <cfset qs=qs & '&functionOptions[extra_2]=' & rsLead.lead_zip>
                            <cfset qs=qs & '&functionOptions[extra_15]=' & rsLead.lead_interest>
                            <cfset qs=qs & '&functionOptions[extra_16]=' & rsLead.lead_gender>
                            <cfset qs=qs & '&functionOptions[extra_6]=' & rsLead.leadID>
                            <cfset qs=qs & '&functionOptions[extra_10]=' & DateFormat(rsLead.lead_tstamp, "YYYY-MM-DD")>
                            <cfset qs=qs & '&functionOptions[extra_19]=' & rsLead.lead_sign>
                            <cfset qs=qs & '&functionOptions[extra_17]=' & rsLead.sign_element>
                            <cfset qs=qs & '&functionOptions[extra_20]=' & rsLead.sign_characteristics1>
                            <cfset qs=qs & '&functionOptions[extra_21]=' & rsLead.sign_characteristics2>
                            <cfset qs=qs & '&functionOptions[extra_22]=' & rsLead.sign_planet1>
                            <cfset qs=qs & '&functionOptions[extra_23]=' & rsLead.sign_incense>
                            <cfset qs=qs & '&functionOptions[extra_24]=' & rsLead.sign_stone>
                            <cfset qs=qs & '&functionOptions[extra_25]=' & rsLead.sign_metal>
                            <cfset qs=qs & '&functionOptions[extra_26]=' & rsLead.sign_number>
                            <cfset qs=qs & '&functionOptions[extra_27]=' & rsLead.sign_color>
                            <cfset theurl=theurl & qs & "&type=json">
                            <cfif #URL.show# eq 'yes'>
                                <cfoutput>
                                    <label>Build Egoi API request URL:</label>
                                    <p>#theurl#</p>
                                </cfoutput>
                            </cfif>

                            <cfhttp url="#theurl#" method="get" result="eresponse" charset="utf-8"/>

                            <cfset response="#deserializeJSON(eresponse.filecontent)#"/>
                            <cfif #URL.show# eq 'yes'>
                                <cfoutput>
                                    <label>The Egoi API response was:</label>
                                    <cfdump var="#response#"/>
                                </cfoutput>
                            </cfif>
                            <cfif StructKeyExists(response.Egoi_Api.addSubscriber, "ERROR")>
                                <cfset error="#response.Egoi_Api.addSubscriber.error#"/>
                                <cfif #URL.show# eq 'yes'>
                                    <cfoutput>
                                        <label>An error occurred:</label>
                                        <p>#error#</p>
                                    </cfoutput>
                                </cfif>
                                <cfset statusID = -10>
                                <cfswitch expression="#error#">
                                    <cfcase value="EMAIL_ADDRESS_INVALID">
                                        <cfset statusID = -1>
                                        <cfquery>
                                            insert into lead_integration_attempts (campaignID, leadID, tstamp, statusID, result)
                                            values ('egoi-0000', '#rsLead.leadID#', getdate(), #statusID#, '#error#')
                                        </cfquery>
                                        <cfquery>
                                            BEGIN

                                            DECLARE @leadCount INT
                                            SET @leadCount = (
                                                                SELECT count(*)
                                                                FROM lead_integration
                                                                WHERE leadID = '#rsLead.leadID#'
                                                                AND campaignID='egoi-0000'
                                                              )

                                            IF @leadCount = 0
                                                INSERT INTO lead_integration (leadID, campaignID, statusID, tstamp)
                                                VALUES ('#rsLead.leadID#', 'egoi-0000', #integrationStatusID#, getdate())
                                            ELSE
                                                UPDATE lead_integration
                                                    SET statusID = #statusID#
                                                WHERE
                                                    leadID = '#rsLead.leadID#' AND
                                                    campaignID = 'egoi-0000'

                                            END
                                        </cfquery>
                                    </cfcase>
                                    <cfcase value="EMAIl_ALREADY_EXISTS">
                                        <cfset statusID = 1>
                                        <cfquery>
                                            insert into lead_integration_attempts (campaignID, leadID, tstamp, statusID, result)
                                            values ('egoi-0000', '#rsLead.leadID#', getdate(), #statusID#, '#error#')
                                        </cfquery>
                                    </cfcase>
                                </cfswitch>
                            <cfelse>
                                <cfif #URL.show# eq 'yes'>
                                    <cfoutput>
                                        <label>Lead integrated successfully with E-goi! egoiID generated is: #response.Egoi_Api.addSubscriber.UID#</label>
                                    </cfoutput>
                                </cfif>
                                <cfset statusID = 10>
                                <cfset integrationStatusID = 1>

                                <cfset apikey="46099444df9915f368e9b525fa3d87b525cb0c85">
                                <cfset listID='3'>
                                <cfset tags= ''>
                                <cfset theurl='http://api.e-goi.com/v2/rest.php?method=addSubscriber'>
                                <cfset qs='&functionOptions[apikey]=' & apikey>
                                <cfset qs=qs & '&functionOptions[listID]=' & listID>
                                <cfset qs=qs & '&functionOptions[status]=' & '1'>
                                <cfset qs=qs & '&functionOptions[lang]=' & lcase(rsLead.lead_country)>
                                <cfset qs=qs & '&functionOptions[email]=' & rsLead.lead_email>
                                <cfset qs=qs & '&functionOptions[telephone]=' & rsLead.lead_phone>
                                <cfset qs=qs & '&functionOptions[validate_phone]=' & '0'>
                                <cfset qs=qs & '&functionOptions[first_name]=' & rsLead.lead_name>
                                <cfset qs=qs & '&functionOptions[last_name]=' & rsLead.lead_surname>
                                <cfset qs=qs & '&functionOptions[birth_date]=' & rsLead.lead_birthdate>
                                <cfset qs=qs & '&functionOptions[extra_32]=' & rsLead.lead_country>
                                <cfset qs=qs & '&functionOptions[extra_30]=' & rsLead.lead_zip>
                                <cfset qs=qs & '&functionOptions[extra_31]=' & rsLead.lead_interest>
                                <cfset qs=qs & '&functionOptions[extra_29]=' & rsLead.lead_gender>
                                <cfset qs=qs & '&functionOptions[extra_36]=' & rsLead.leadID>
                                <cfset qs=qs & '&functionOptions[extra_33]=' & rsLead.lead_sign>
                                <cfset qs=qs & '&functionOptions[extra_35]=' & rsLead.lead_tstamp>
                                <cfset qs=qs & '&functionOptions[extra_37]=CIBELE'>
                                <cfset theurl=theurl & qs>
                                <cfhttp url="#theurl#" method="get" result="eresult" charset="utf-8"></cfhttp>

                                <cfquery>

                                    BEGIN

                                        DECLARE @leadCount INT
                                        DECLARE @leadCount2 INT
                                        SET @leadCount = (
                                                            SELECT count(*)
                                                            FROM lead_integration
                                                            WHERE leadID = '#rsLead.leadID#'
                                                            AND campaignID='egoi-0000'
                                                          )

                                        SET @leadCount2 = (
                                                            SELECT count(*)
                                                            FROM lead_integration
                                                            WHERE leadID = '#rsLead.leadID#'
                                                            AND campaignID='egoi-0001'
                                                          )

                                        IF @leadCount = 0
                                            BEGIN
                                            INSERT INTO lead_integration (leadID, campaignID, statusID, tstamp)
                                            VALUES ('#rsLead.leadID#', 'egoi-0000', #integrationStatusID#, getdate())
                                            END
                                        ELSE
                                            BEGIN
                                            UPDATE lead_integration
                                            SET statusID = #integrationStatusID#
                                            WHERE
                                                leadID = '#rsLead.leadID#' AND
                                                campaignID = 'egoi-0000'
                                            END

                                        IF @leadCount2 = 0
                                            BEGIN
                                            INSERT INTO lead_integration (leadID, campaignID, statusID, tstamp)
                                            VALUES ('#rsLead.leadID#', 'egoi-0001', #integrationStatusID#, getdate())
                                            END
                                        ELSE
                                            BEGIN
                                            UPDATE lead_integration
                                            SET statusID = #integrationStatusID#
                                            WHERE
                                                leadID = '#rsLead.leadID#' AND
                                                campaignID = 'egoi-0001'
                                            END

                                        UPDATE lead_feed SET statusID=#statusID#, egoiID='#response.Egoi_Api.addSubscriber.UID#' WHERE leadID = '#rsLead.leadID#'

                                     END
                                </cfquery>

                                <!---
                                <cfhttp
                                    url="http://www.videntecibele.com/cfc/tests/cli-integration/#rsLead.lead_country#.cfc?method=integrate&leadID=#rsLead.leadID#"
                                    method="get"
                                    result="clientsIntegration"
                                />
                                --->

                                <cfheader
                                    name="Content-Type"
                                    value="application/json"/>

                                <cfoutput>
                                    #serializeJSON(
                                        {
                                            "status": "OK",
                                            "message": "integrated"
                                        }
                                    )#
                                </cfoutput>

                            </cfif>
                    </cfloop>
                </cfif>
            </cffunction>
</cfcomponent>