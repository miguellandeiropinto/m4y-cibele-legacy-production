<cfparam name="URL.cid" default="-1">
<cfif #URL.cid# neq "-1">
    <!DOCTYPE html>
    <html>
    <head>
    <cfoutput>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-80714780-1', 'auto');
            ga('send', 'pageview', '#URL.cid#-success');
        </script>
    </cfoutput>
    </head>
    <body></body>
    </html>
</cfif>