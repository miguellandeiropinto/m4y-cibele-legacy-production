<cfcomponent>
<cfprocessingdirective pageencoding="utf-8">
<cfsetting requestTimeOut = "9000" />

<cffunction name="c" access="remote">

    <cfparam name="URL.sid" default="">
    <cfparam name="URL.cid" default="">

    <cfquery name="conversionLog">
        INSERT INTO conversion_logs (data, test_date)
        VALUES ('#URL.sid#_#URL.cid#', getdate())
    </cfquery>

    <cfquery name="insertConversion">
        INSERT INTO c (subid, clickid, tstamp)
        VALUES ('#URL.sid#', '#URL.cid#', getdate())
    </cfquery>

    <cfquery name="rsCampaign">
        SELECT CH.campaignID, CS.* FROM campaign_sends CS
        INNER JOIN campaigns_history CH ON CH.id = CS.campaign_historyID
        WHERE CS.id = #URL.sid#
    </cfquery>

    <cfloop query="rsCampaign">

        <cfset source="#rsCampaign.sourceID#">
        <cfset medium="#rsCampaign.mediumID#">
        <cfset campaign="#rsCampaign.campaignID#">
        <cfset sending="#rsCampaign.send_id#">

        <cfswitch expression="#medium#">
            <cfcase value="facebook">
                <!--- trigger facebook pixel --->
                <cfhttp result="result" method="GET" charset="utf-8" url="fb-pixel-trigger.cfm">
                    <cfhttpparam name="cid" type="formfield" value="#campaign#">
                </cfhttp>
            </cfcase>
            <cfcase value="adwords">
                <!--- trigger google analytics goal --->
                <cfhttp result="result" method="GET" charset="utf-8" url="google-pixel-trigger.cfm">
                    <cfhttpparam name="cid" type="formfield" value="#campaign#">
                </cfhttp>
            </cfcase>
            <cfcase value="bing">
                <!--- trigger bing goal --->
            </cfcase>
            <cfcase value="yahoo">
                <!--- trigger yahoo goal --->
            </cfcase>
        </cfswitch>

    </cfloop>

    <!---
        -- Example:
            <img width="1" height="1"
                src="http://www.videntecibele.com/cfc/tests/pixel.cfc?method=c&sid=10&cid=3c59faa7-6416-47a4-b5d3-3e4aef6bb52a">

        -- Implementation (Direct | MA):
            <img width="1" height="1"
            src="http://www.videntecibele.com/cfc/tests/pixel.cfc?method=c&sid=__SUBID__&cid=__CLICKID__">
    --->

</cffunction>

</cfcomponent>