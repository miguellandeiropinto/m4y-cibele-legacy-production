<cfcomponent>
    <cfprocessingdirective pageencoding="utf-8">

    <cffunction name="integrate" access="remote">

        <cfparam name="URL.nof" default="5"/>

        <cfquery name="rsLead">
            SELECT TOP #URL.nof# * FROM lead_feed LF
            WHERE LF.leadID not in (SELECT leadID FROM lead_integration WHERE leadID = LF.leadID and campaignID='egoi-0001')
            AND leadID in (SELECT leadID from lead_optin WHERE leadID = LF.leadID and partnerID=2)
            and LF.lead_tstamp > '2017-04-07' and LF.statusID = 10 ORDER BY LF.lead_tstamp DESC
        </cfquery>

        <cfdump var=#rsLead#/>

        <cfset apikey = "46099444df9915f368e9b525fa3d87b525cb0c85">
        <cfset listID = '3'>
        <cfset tags = ''>
        <cfset theurl = 'http://api.e-goi.com/v2/rest.php?method=addSubscriber'>


        <cfloop query="rsLead">
            <cfset qs = '&functionOptions[apikey]=' & apikey>
            <cfset qs = qs & '&functionOptions[listID]=' & listID>
            <cfset qs = qs & '&functionOptions[status]=' & '1'>
            <cfset qs = qs & '&functionOptions[lang]=' & lcase(rsLead.lead_country)>
            <cfset qs = qs & '&functionOptions[email]=' & rsLead.lead_email>
            <cfset qs = qs & '&functionOptions[telephone]=' & rsLead.lead_phone>
            <cfset qs = qs & '&functionOptions[validate_phone]=' & '0'>
            <cfset qs = qs & '&functionOptions[first_name]=' & rsLead.lead_name>
            <cfset qs = qs & '&functionOptions[last_name]=' & rsLead.lead_surname>
            <cfset qs = qs & '&functionOptions[birth_date]=' & rsLead.lead_birthdate>
            <cfset qs = qs & '&functionOptions[extra_32]=' & rsLead.lead_country>
            <cfset qs = qs & '&functionOptions[extra_30]=' & rsLead.lead_zip>
            <cfset qs = qs & '&functionOptions[extra_31]=' & rsLead.lead_interest>
            <cfset qs = qs & '&functionOptions[extra_29]=' & rsLead.lead_gender>
            <cfset qs = qs & '&functionOptions[extra_36]=' & rsLead.leadID>
            <cfset qs = qs & '&functionOptions[extra_33]=' & rsLead.lead_sign>
            <cfset qs = qs & '&functionOptions[extra_35]=' & rsLead.lead_tstamp>
            <cfset qs = qs & '&functionOptions[extra_37]=CIBELE&type=json'>
            <cfset theurl = theurl & qs>

            <cfhttp url="#theurl#" method="get" result="eresponse" charset="utf-8"></cfhttp>
            <cfset response = "#deserializeJSON(eresponse.filecontent)#"/>

            <cfif #URL.show# eq 'yes'>
                <cfoutput>
                    <label>The Egoi API response was:</label>
                    <cfdump var="#response#"/>
                </cfoutput>
            </cfif>
            <cfif StructKeyExists(response.Egoi_Api.addSubscriber, "ERROR")>
            <cfset error = "#response.Egoi_Api.addSubscriber.error#"/>
            <cfif #URL.show# eq 'yes'>
                <cfoutput>
                    <label>An error occurred:</label>
                    <p>#error#</p>
                </cfoutput>
            </cfif>
            <cfset statusID = - 10>

            <cfswitch expression="#error#">
                <cfcase value="EMAIL_ADDRESS_INVALID">
                    <cfset statusID = - 1>
                    <cfquery>
                                            insert into lead_integration_attempts (campaignID, leadID, tstamp, statusID, result)
                                            values ('egoi-0001', '#rsLead.leadID#', getdate(), #statusID#, '#error#')
                    </cfquery>
                    <cfquery>
                                            BEGIN

                                            DECLARE @leadCount INT
                                            SET @leadCount = (
                                                                SELECT count(*)
                                                                FROM lead_integration
                                                                WHERE leadID = '#rsLead.leadID#'
                                                                AND campaignID='egoi-0001'
                                                              )

                                            IF @leadCount = 0
                                                INSERT INTO lead_integration (leadID, campaignID, statusID, tstamp)
                                                VALUES ('#rsLead.leadID#', 'egoi-0001', #integrationStatusID#, getdate())
                                            ELSE
                                                UPDATE lead_integration
                                                    SET statusID = #statusID#
                                                WHERE
                                                    leadID = '#rsLead.leadID#' AND
                                                    campaignID = 'egoi-0001'

                                            END
                                        </cfquery>
                </cfcase>
                <cfcase value="EMAIl_ALREADY_EXISTS">
                    <cfset statusID = 1>
                    <cfquery>
                                            insert into lead_integration_attempts (campaignID, leadID, tstamp, statusID, result)
                                            values ('egoi-0001', '#rsLead.leadID#', getdate(), #statusID#, '#error#')
                    </cfquery>
                </cfcase>
            </cfswitch>
            <cfelse>
                <cfset integrationStatusID = 1>

                <cfquery>

                                    BEGIN


                                        DECLARE @leadCount2 INT


                                        SET @leadCount2 = (
                                                            SELECT count(*)
                                                            FROM lead_integration
                                                            WHERE leadID = '#rsLead.leadID#'
                                                            AND campaignID='egoi-0001'
                                                          )



                                        IF @leadCount2 = 0
                                            BEGIN
                                            INSERT INTO lead_integration (leadID, campaignID, statusID, tstamp)
                                            VALUES ('#rsLead.leadID#', 'egoi-0001', #integrationStatusID#, getdate())
                                            END
                                        ELSE
                                            BEGIN
                                            UPDATE lead_integration
                                            SET statusID = #integrationStatusID#
                                            WHERE
                                                leadID = '#rsLead.leadID#' AND
                                                campaignID = 'egoi-0001'
                                            END

                                    END
                                </cfquery>


            </cfif>

        </cfloop>

    </cffunction>

</cfcomponent>
