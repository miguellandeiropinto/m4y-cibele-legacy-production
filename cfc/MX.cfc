<cfcomponent>
    <cfsetting requesttimeout="9000">
    <cfprocessingdirective pageencoding="utf-8">

        <cffunction name="doIntegrationClients" access="remote">
            <cfargument name="leadID" type="any" required="false"/>

            <cfparam name="URL.leadID" default="-1"/>
            <cfparam name="URL.ccampaignID" default=""/>
            <cfparam name="URL.show" default="yes"/>
            <cfparam name="URL.token" default=""/>

            <cfset cresult= StructNew()/>
            <cfset cresult.statuscode=""/>

            <cfif ARGUMENTS.leadID neq "">
                <cfset URL.leadID= ARGUMENTS.leadID/>
            </cfif>

            <cfif #URL.leadID# neq "-1">

                <CFSTOREDPROC procedure="dbo.getLeadsforIndividualIntegration">
                    <CFPROCPARAM type="IN" value="#URL.leadID#" cfsqltype="CF_SQL_VARCHAR"/>
                    <CFPROCPARAM type="IN" value="#URL.ccampaignID#" cfsqltype="CF_SQL_VARCHAR"/>
                    <CFPROCRESULT name="rsLead" resultset="1"/>
                    <CFPROCRESULT name="rsIntegrations" resultset="2"/>
                    <CFPROCRESULT name="rsOthSuccessInt" resultset="3"/>
                    <CFPROCRESULT name="rsBonoTour" resultset="4"/>
                    <CFPROCRESULT name="rsEasyVoyage" resultset="5"/>
                    <CFPROCRESULT name="rsAdeslas" resultset="6"/>
                    <CFPROCRESULT name="rsRCF" resultset="7"/>
                </CFSTOREDPROC>

                <CFSTOREDPROC procedure="dbo.getClientCampaign">
                    <CFPROCPARAM type="IN" value="#URL.ccampaignID#" cfsqltype="CF_SQL_VARCHAR"/>
                    <!---
                    <CFPROCRESULT name="rsCampaign" resultset="1">
                    <CFPROCRESULT name="rsRules" resultset="2">
                    --->
                    <CFPROCRESULT name="rsFields" resultset="3"/>
                </CFSTOREDPROC>

                <!--- START Process --->

                <!--- transform data --->
                <cfset ftranslate= StructNew()/>

                <cfloop query="rsFields">
                    <cfset ftranslate["#rsFields.field_name#"]["#rsFields.field_value#"] = rsFields.field_value_client >
                </cfloop>

                <cfdump var="#ftranslate#"/>


                <!-- attempt --->

                <cfif URL.show eq 'yes'>
                    <cfoutput>1st ATTEMPT WITH #UCASE(ccampaignID)#
                        <br>
                    </cfoutput>
                </cfif>

            <!-- integrate --->

                <cfswitch expression="#URL.ccampaignID#">

                    <cfcase value="maria-coreg-mx-0019">

                        <cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>

                            <cfset int_try_flag=1>

                            <cfif URL.show eq 'yes'>
                                <cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
                            </cfif>

                            <cfhttp url="http://www.maria-clarividencia.com/remote-registration.php" method="get" result="cresult" charset="utf-8">
                                <cfhttpparam type="url" name="email" value="#rsLead.lead_email#">
                                <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#">
                                <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#">
                                <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#">
                                <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
                                <!---<cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> --->
                                <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#">
                                <cfhttpparam type="url" name="campaignarea" value="MX">
                                <cfhttpparam type="url" name="media" value="coreg">
                                <cfhttpparam type="url" name="partner" value="Yours">
                                <cfhttpparam type="url" name="campaign" value="coreg2017">
                            </cfhttp>

                        <cfelse>

                            <cfset int_try_flag=0>

                        </cfif>

                    </cfcase>


                    <cfcase value="tara-coreg-mx-0021">

                        <cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>

                            <cfset int_try_flag=1>

                            <cfif URL.show eq 'yes'>
                                <cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
                            </cfif>

                            <cfhttp url="http://www.tara-videncia.com/remote-registration.php" method="get" result="cresult" charset="utf-8">
                                <cfhttpparam type="url" name="email" value="#rsLead.lead_email#">
                                <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#">
                                <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#">
                                <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#">
                                <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
                                <cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#">
                                <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#">
                                <cfhttpparam type="url" name="campaignarea" value="MX">
                                <cfhttpparam type="url" name="media" value="coreg">
                                <cfhttpparam type="url" name="partner" value="Yours">
                                <cfhttpparam type="url" name="campaign" value="coreg2017">
                            </cfhttp>

                        <cfelse>

                            <cfset int_try_flag=0>

                        </cfif>

                    </cfcase>

                    <cfcase value="padre-coreg-mx-0023">

                        <cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>


                        <cfset int_try_flag=1>
                        <cfif URL.show eq 'yes'>
                            <cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
                        </cfif>
                        <cfoutput>
                                                <br>
                        </cfoutput>

                        <cfif URL.token eq ''>

                                <!--- GET TOKEN FOR PADRE --->
                            <cfset loginFields = {'email': 'andre.amorim@yoursporto.com','password': '**Yours2016'} >

                            <cfhttp url="https://api.mensagens-dos-anjos.com/api/Partners/login" method="post" result="cresult" timeout="60">
                            <cfhttpparam type="header" name="Content-Type" value="application/json" />
                            <cfhttpparam type="body" value="#serializeJSON(loginFields)#">
                            </cfhttp>

                            <cfset tempvar=deserializeJSON(cresult.filecontent)>
                            <cfset login_token=tempvar.id>

                            <cfset URL.token=login_token>

                        </cfif>

                        <cfset intFields = {
                        "email": "#rsLead.lead_email#",
                        "birthdate": "#DateFormat(rsLead.lead_birthdate,'YYYY-MM-DD')#",
                        "language" : "es",
                        "timezone" : 0,
                        "gender": "#ftranslate['lead_gender'][rsLead.lead_gender]#",
                        "firstName": "#rsLead.lead_name#",
                        "lastName": "#rsLead.lead_surname#",
                        "originDetail": {
                        "partner" : "YOURS",
                        "referer": "http://www.videntecibele.com/es-mx",
                        "subid": "#rsLead.platformID#",
                        "media": "COREG",
                        "theme": "angel",
                        "campaign": "campaign_2017",
                        "campaignarea": "MX"
                        },
                        "location":{
                        "registrationIpV4": "#rsLead.lead_ip#",
                        "registrationIpV6": "",
                        "number": "",
                        "street": "",
                        "street2": "",
                        "street3": "",
                        "zip": "",
                        "city": "",
                        "state": "",
                        "country": "MEX"
                        }
                        }>

                        <cfhttp url="https://api.mensagens-dos-anjos.com/api/Customers" method="post" result="cresult" timeout="60">
                            <cfhttpparam type="url" name="access_token" value="#URL.token#">
                            <cfhttpparam type="header" name="Content-Type" value="application/json" />
                            <cfhttpparam type="body" value="#serializeJSON(intFields)#">
                        </cfhttp>

                        <cfelse>

                            <cfset int_try_flag=0>

                        </cfif>

                    </cfcase>

                </cfswitch>

            </cfif>

            <cfreturn cresult/>

        </cffunction>
</cfcomponent>