<cfcomponent>
<cfprocessingdirective pageencoding="utf-8">

        <cffunction name="track" access="remote">
            <cfparam name="URL.lid" default="unknown">
            <cfparam name="URL.csid" default="">

            <cfif #len(URL.csid)# gt 0 >
                <cfquery name="trackGen">

                </cfquery>
            </cfif>

        </cffunction>

        <!--- GENERATE CONVERSION ID FOR LEAD --->
        <cffunction access="remote" name="generate">

            <cfparam name="URL.lid" default="unknown">
            <cfparam name="URL.csid" default="NOT_DEFINED">
            <cfparam name="URL.debug" default="">

            <CFSTOREDPROC procedure="dbo.GenerateConversionId">
                <CFPROCPARAM type="IN" value="#URL.lid#" cfsqltype="CF_SQL_VARCHAR">
                <CFPROCPARAM type="IN" value="#URL.csid#" cfsqltype="CF_SQL_INTEGER">
                <cfprocresult name="genkey" resultset="1">
                <cfprocresult name="targetURL" resultset="2">
            </CFSTOREDPROC>

            <cfoutput>#targetURL.url#</cfoutput>
            <cfset urls="#replace(targetURL.url, "__0__", URL.csid)#"/>
            <cfset urls="#replace(urls, "__1__", genkey.id)#"/>
            <cfif #URL.debug# eq "yes">
                <cfoutput> --- #urls#</cfoutput>
                <cfelse>
                <cflocation url= "#urls#" addToken = "no">
            </cfif>




        </cffunction>

        <!--- STORE LEAD EMAIL OPENING --->
        <cffunction access="remote" name="trackOpen">

            <cfparam name="URL.lid" default="unknown">
            <cfparam name="URL.cid" default="NOT_DEFINED">

            <CFSTOREDPROC procedure="dbo.LeadTrackOpening">
                <CFPROCPARAM type="IN" value="#URL.lid#" cfsqltype="CF_SQL_VARCHAR">
                <CFPROCPARAM type="IN" value="#URL.csid#" cfsqltype="CF_SQL_VARCHAR">
                <cfprocparam type="OUT" variable="genkey" cfsqltype="CF_SQL_NUMERIC">
            </CFSTOREDPROC>

        </cffunction>


        <!--- STORE LEAD EMAIL OPENING --->
        <cffunction access="remote" name="trackClick">

            <cfparam name="URL.lid" default="unknown">
            <cfparam name="URL.cid" default="NOT_DEFINED">

            <CFSTOREDPROC procedure="dbo.LeadTrackingClicks">
                <CFPROCPARAM type="IN" value="#URL.lid#" cfsqltype="CF_SQL_VARCHAR">
                <CFPROCPARAM type="IN" value="#URL.csid#" cfsqltype="CF_SQL_VARCHAR">
                <cfprocparam type="OUT" variable="genkey" cfsqltype="CF_SQL_NUMERIC">
            </CFSTOREDPROC>

        </cffunction>


</cfcomponent>