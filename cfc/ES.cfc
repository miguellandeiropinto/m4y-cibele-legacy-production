<cfcomponent>
    <cfsetting requesttimeout="15000">
    <cfprocessingdirective pageencoding="utf-8">

        <cffunction name="doIntegrationClients" access="remote">
            <cfargument name="leadID" type="any" required="false"/>

            <cfparam name="URL.leadID" default="-1"/>
            <cfparam name="URL.ccampaignID" default=""/>
            <cfparam name="URL.show" default="yes"/>
            <cfparam name="URL.token" default=""/>

            <cfset cresult= StructNew()/>
            <cfset cresult.statuscode=""/>

            <cfif ARGUMENTS.leadID neq "">
                <cfset URL.leadID= ARGUMENTS.leadID/>
            </cfif>

            <cfif #URL.leadID# neq "-1">

                <CFSTOREDPROC procedure="dbo.getLeadsforIndividualIntegration">
                    <CFPROCPARAM type="IN" value="#URL.leadID#" cfsqltype="CF_SQL_VARCHAR"/>
                    <CFPROCPARAM type="IN" value="#URL.ccampaignID#" cfsqltype="CF_SQL_VARCHAR"/>
                    <CFPROCRESULT name="rsLead" resultset="1"/>
                    <CFPROCRESULT name="rsIntegrations" resultset="2"/>
                    <CFPROCRESULT name="rsOthSuccessInt" resultset="3"/>
                    <CFPROCRESULT name="rsBonoTour" resultset="4"/>
                    <CFPROCRESULT name="rsEasyVoyage" resultset="5"/>
                    <CFPROCRESULT name="rsAdeslas" resultset="6"/>
                    <CFPROCRESULT name="rsRCF" resultset="7"/>
                    <CFPROCRESULT name="rsAbanca" resultset="8"/>
                    <CFPROCRESULT name="rsAldeas" resultset="9"/>
                    <CFPROCRESULT name="rsAldeasDi" resultset="10"/>
                </CFSTOREDPROC>

                <CFSTOREDPROC procedure="dbo.getClientCampaign">
                    <CFPROCPARAM type="IN" value="#URL.ccampaignID#" cfsqltype="CF_SQL_VARCHAR"/>
                    <!---
                    <CFPROCRESULT name="rsCampaign" resultset="1">
                    <CFPROCRESULT name="rsRules" resultset="2">
                    --->
                    <CFPROCRESULT name="rsFields" resultset="3"/>
                </CFSTOREDPROC>

                    <!--- START Process --->

                    <!--- transform data --->
                    <cfset ftranslate= StructNew()/>

                    <cfloop query="rsFields">
                        <cfset ftranslate["#rsFields.field_name#"]["#rsFields.field_value#"] = rsFields.field_value_client >
                    </cfloop>

                    <cfdump var="#ftranslate#"/>


                    <!-- attempt --->

                    <cfif URL.show eq 'yes'>
                        <cfoutput>1st ATTEMPT WITH #UCASE(ccampaignID)#
                            <br>
                        </cfoutput>
                    </cfif>

                <!-- integrate --->

                    <cfswitch expression="#URL.ccampaignID#">


                        <cfcase value="tara-coreg-es-0010">

                            <cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>

                            <cfset int_try_flag=1>

                            <cfif URL.show eq 'yes'>
                                <cfoutput>SATISFIES CAMPAIGN AGE
                                    <br>
                                </cfoutput>
                            </cfif>

                        <cfhttp url="http://www.tara-astrologia.com/remote-registration.php" method="post" result="cresult"
                                charset="utf-8">
                            <cfhttpparam type="url" name="email" value="#rsLead.lead_email#">
                            <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#">
                            <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#">
                            <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#">
                            <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
                            <cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#">
                            <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#">
                            <cfhttpparam type="url" name="campaignarea" value="ES">
                            <cfhttpparam type="url" name="media" value="coreg">
                            <cfhttpparam type="url" name="partner" value="Yours">
                            <cfhttpparam type="url" name="campaign" value="coreg2017">
                        </cfhttp>

                        <cfelse>

                            <cfset int_try_flag=0>

                        </cfif>

                    </cfcase>


                    <cfcase value="maria-coreg-es-0012">

                        <cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>

                            <cfset int_try_flag=1>

                            <cfif URL.show eq 'yes'>
                                <cfoutput>SATISFIES CAMPAIGN AGE
                                    <br>
                                </cfoutput>
                            </cfif>

                            <cfhttp url="http://www.videncia-maria.com/remote-registration.php" method="get" result="cresult"
                                    charset="utf-8">
                                <cfhttpparam type="url" name="email" value="#rsLead.lead_email#">
                                <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#">
                                <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#">
                                <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#">
                                <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
                                <!---<cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> --->
                                <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#">
                                <cfhttpparam type="url" name="optin" value="#rsLead.lead_optin#">
                                <cfhttpparam type="url" name="campaignarea" value="ES">
                                <cfhttpparam type="url" name="media" value="coreg">
                                <cfhttpparam type="url" name="partner" value="Yours">
                                <cfhttpparam type="url" name="campaign" value="coreg2017">
                            </cfhttp>


                        <cfelse>

                            <cfset int_try_flag=0>

                        </cfif>

                    </cfcase>


                    <cfcase value="padre-coreg-es-0014">

                        <cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>

                            <cfset int_try_flag=1/>
                            <cfif URL.show eq 'yes'>
                                <cfoutput>SATISFIES CAMPAIGN AGE
                                    <br>
                                </cfoutput>
                            </cfif>
                            <cfoutput>

                            <br>
                            </cfoutput>

                            <cfif URL.token eq ''>

                                    <!--- GET TOKEN FOR PADRE --->
                                <cfset loginFields= {'email': 'andre.amorim@yoursporto.com','password': '**Yours2016'} >

                                <cfhttp url="https://api.mensagens-dos-anjos.com/api/Partners/login" method="post" result="cresult" timeout="60">
                                    <cfhttpparam type="header" name="Content-Type" value="application/json"/>
                                    <cfhttpparam type="body" value="#serializeJSON(loginFields)#">
                                </cfhttp>

                                <cfset tempvar= deserializeJSON(cresult.filecontent)/>
                                <cfset login_token= tempvar.id/>

                                <cfset URL.token= login_token/>

                            </cfif>

                            <cfset intFields={
                                    "email": "#rsLead.lead_email#",
                                    "birthdate": "#DateFormat(rsLead.lead_birthdate,'YYYY-MM-DD')#",
                                    "language" : "es",
                                    "timezone" : 0,
                                    "gender": "#ftranslate['lead_gender'][rsLead.lead_gender]#",
                                    "firstName": "#rsLead.lead_name#",
                                    "lastName": "#rsLead.lead_surname#",
                                    "originDetail": {
                                    "partner" : "YOURS",
                                    "referer": "http://www.videntecibele.com/es-es",
                                    "subid": "#rsLead.platformID#",
                                    "media": "COREG",
                                    "theme": "angel",
                                    "campaign": "campaign_2016",
                                    "campaignarea": "ES"
                                    },
                                    "location":{
                                    "registrationIpV4": "#rsLead.lead_ip#",
                                    "registrationIpV6": "",
                                    "number": "",
                                    "street": "",
                                    "street2": "",
                                    "street3": "",
                                    "zip": "",
                                    "city": "",
                                    "state": "",
                                    "country": "ESP"
                                    }
                                    }/>

                            <cfhttp url="https://api.mensagens-dos-anjos.com/api/Customers" method="post" result="cresult" timeout="60">
                            <cfhttpparam type="url" name="access_token" value="#URL.token#"/>
                            <cfhttpparam type="header" name="Content-Type" value="application/json"/>
                            <cfhttpparam type="body" value="#serializeJSON(intFields)#"/>
                            </cfhttp>

                        <cfelse>

                            <cfset int_try_flag=0/>

                        </cfif>

                    </cfcase>


                    <cfcase value="easyvoyage-spons-es-0016">

                        <cfif rsEasyVoyage.lead_optin eq 1>

                            <cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 18>

                                <cfif len(rsLead.lead_zip) eq 5>

                                    <cfif FindNoCase('@gmail.com', rsLead.lead_email) eq 0>

                                        <cfset int_try_flag=1/>
                                        <cfif URL.show eq 'yes'>
                                            <cfoutput>SATISFIES CAMPAIGN AGE, ZIP AND NO GMAIL
                                                <br>
                                            </cfoutput>
                                        </cfif>
                                        <cfoutput>
                                        <!--- ?id=314&email=[EMAIL]&nombre=[NOMBRE]&apellidos=[APELLIDOS]&dob=[DIA_DE_NACIMIENTO]&mob=[MES_DE_NACIMIENTO]&yob=[ANYO_DE_NACIMIENTO]&sexo=[SEXO_M_O_F]&cp=[COD_POSTAL]&ip=[IP_DE_REGISTRO]&ref=[WEB_ASOCIADA_AL_WEBMASTER] --->

                                        <br>
                                        </cfoutput>

                                        <cfhttp url="http://www.mailingcp.com/corregistros/pasarelacrg.php" method="get" result="cresult" charset="utf-8">
                                            <cfhttpparam type="url" name="id" value="441"/>
                                            <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"/>
                                            <cfhttpparam type="url" name="nombre" value="#rsLead.lead_name#"/>
                                            <cfhttpparam type="url" name="apellidos" value="#rsLead.lead_surname#"/>
                                            <cfhttpparam type="url" name="dob" value="#day(rsLead.lead_birthdate)#"/>
                                            <cfhttpparam type="url" name="mob" value="#month(rsLead.lead_birthdate)#"/>
                                            <cfhttpparam type="url" name="yob" value="#year(rsLead.lead_birthdate)#"/>
                                            <cfhttpparam type="url" name="sexo" value="#rsLead.lead_gender#"/>
                                            <cfhttpparam type="url" name="cp" value="#rsLead.lead_zip#"/>
                                            <cfhttpparam type="url" name="ip" value="#rsLead.lead_ip#"/>
                                            <cfhttpparam type="url" name="ref" value="#rsLead.platformID#"/>
                                        </cfhttp>

                                    <cfelse>
                                        <cfset int_try_flag=0/>
                                    </cfif>

                                <cfelse>
                                    <cfset int_try_flag=0/>
                                </cfif>

                            <cfelse>

                                <cfset int_try_flag=0/>

                            </cfif>

                        <cfelse>

                            <cfset int_try_flag=0/>

                        </cfif>

                    </cfcase>

                    <cfcase value="adeslas-coreg-es-0017">

                        <cfif rsAdeslas.lead_optin eq 1>

                            <cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 30 or DATEDIFF('yyyy',rsLead.lead_birthdate, now()) lte 55>

                                <cfif len(rsLead.lead_zip) eq 5 and (left(rsLead.lead_zip,2) eq '01' or left(rsLead.lead_zip,2) eq '31' or left(rsLead.lead_zip,2) eq '33' or left(rsLead.lead_zip,2) eq '39' or left(rsLead.lead_zip,2) eq '20' or left(rsLead.lead_zip,2) eq '48')>

                                    <cfif len(rsLead.lead_phone) eq 9>

                                        <cfset int_try_flag=1/>
                                        <cfif URL.show eq 'yes'>
                                            <cfoutput>SATISFIES CAMPAIGN AGE, ZIP AND HAS PHONE
                                                <br>
                                            </cfoutput>
                                        </cfif>
                                        <cfoutput>
                                        <!--- ?id=314&email=[EMAIL]&nombre=[NOMBRE]&apellidos=[APELLIDOS]&dob=[DIA_DE_NACIMIENTO]&mob=[MES_DE_NACIMIENTO]&yob=[ANYO_DE_NACIMIENTO]&sexo=[SEXO_M_O_F]&cp=[COD_POSTAL]&ip=[IP_DE_REGISTRO]&ref=[WEB_ASOCIADA_AL_WEBMASTER] --->

                                        <br>
                                        </cfoutput>

                                        <cfhttp url="http://www.mailingcp.com/corregistros/pasarelacrg.php" method="get" result="cresult" charset="utf-8">
                                            <cfhttpparam type="url" name="id" value="964"/>
                                            <cfhttpparam type="url" name="nombre" value="#rsLead.lead_name# #rsLead.lead_surname#"/>
                                            <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"/>
                                            <cfhttpparam type="url" name="telf" value="#rsLead.lead_phone#"/>
                                            <cfhttpparam type="url" name="dob" value="#day(rsLead.lead_birthdate)#"/>
                                            <cfhttpparam type="url" name="mob" value="#month(rsLead.lead_birthdate)#"/>
                                            <cfhttpparam type="url" name="yob" value="#year(rsLead.lead_birthdate)#"/>
                                            <cfhttpparam type="url" name="cp" value="#rsLead.lead_zip#"/>
                                            <cfhttpparam type="url" name="ip" value="#rsLead.lead_ip#"/>
                                            <cfhttpparam type="url" name="ref" value="#rsLead.platformID#"/>
                                        </cfhttp>

                                    <cfelse>
                                        <cfset int_try_flag=0/>
                                    </cfif>

                                <cfelse>
                                    <cfset int_try_flag=0/>
                                </cfif>

                            <cfelse>

                                <cfset int_try_flag=0/>

                            </cfif>

                        <cfelse>

                            <cfset int_try_flag=0/>

                        </cfif>

                    </cfcase>

                    <cfcase value="rcf-coreg-es-0018">

                        <cfif rsRCF.lead_optin eq 1>

                            <cfif len(rsLead.lead_phone) eq 9>

                                <cfset int_try_flag=1/>
                                <cfif URL.show eq 'yes'>
                                    <cfoutput>SATISFIES CAMPAIGN PHONE
                                        <br>
                                    </cfoutput>
                                </cfif>
                                <cfoutput>
                                <!--- ?id=314&email=[EMAIL]&nombre=[NOMBRE]&apellidos=[APELLIDOS]&dob=[DIA_DE_NACIMIENTO]&mob=[MES_DE_NACIMIENTO]&yob=[ANYO_DE_NACIMIENTO]&sexo=[SEXO_M_O_F]&cp=[COD_POSTAL]&ip=[IP_DE_REGISTRO]&ref=[WEB_ASOCIADA_AL_WEBMASTER] --->

                                <br>
                                </cfoutput>

                                <cfhttp url="http://www.mailingcp.com/corregistros/pasarelacrg.php" method="get" result="cresult" charset="utf-8">
                                    <cfhttpparam type="url" name="id" value="966"/>
                                    <cfhttpparam type="url" name="movil" value="#rsLead.lead_phone#"/>
                                    <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"/>
                                    <cfhttpparam type="url" name="nombre" value="#rsLead.lead_name# #rsLead.lead_surname#"/>
                                    <cfhttpparam type="url" name="ip" value="#rsLead.lead_ip#"/>
                                    <cfhttpparam type="url" name="ref" value="#rsLead.platformID#"/>
                                </cfhttp>

                            <cfelse>
                                <cfset int_try_flag=0/>
                            </cfif>


                        <cfelse>

                            <cfset int_try_flag=0/>

                        </cfif>

                    </cfcase>

                     <cfcase value="abanca-coreg-es-0025">

                         <cfif rsAbanca.lead_optin eq 1>
                             <cfset int_try_flag=1/>
                             <!--- has optin --->
                         <cfoutput>Has OPTIN</cfoutput>
                             <cfif rsLead.lead_phone neq "" and len(rsLead.lead_phone) eq 9>
                                 <!--- has valid phone --->
                                <cfoutput>has phone</cfoutput>
                                <!--- get json of accepted zip codes for integration --->
                                 <cffile file="#GetDirectoryFromPath(GetCurrentTemplatePath())#/abanca_zipcodes.json" action="read" variable="ZipList">
                                 <cfset zlist=#deserializeJSON(ZipList)#>

                                <!--- check if lead_zip is in array of accepted zipcodes --->
                                 <cfif #ArrayContains(zlist.zipcodes, rsLead.lead_zip)#>
                                     <!--- valid lead_zip for integration --->
                                     <cfoutput>Accepted Zip</cfoutput>
                                     <!---
                                        https://lmt.timeonegroup.com/get/import/8096/?country=ES&last_name=test&first_name=test&zipcode=75002
                                        &email=test20170807183207@pdl.com&phone_number=0627382536&ip_address=127.0.0.1&area=test&
                                        origin=[optional]your_sub_id&user_agent=[optional]user_agent&id_delivery=23265
                                      --->
                                     
                                     <cfhttp url="http://lmt.timeonegroup.com/get/import/8096/" method="get" result="cresult" charset="utf-8">
                                         <cfhttpparam type="url" name="id_delivery" value="23265"/>
                                         <cfhttpparam type="url" name="country" value="#rsLead.lead_country#"/>
                                         <cfhttpparam type="url" name="last_name" value="#rsLead.lead_surname#"/>
                                         <cfhttpparam type="url" name="first_name" value="#rsLead.lead_name#"/>
                                         <cfhttpparam type="url" name="zipcode" value="#rsLead.lead_zip#"/>
                                         <cfhttpparam type="url" name="ip_address" value="#rsLead.lead_ip#"/>
                                         <cfhttpparam type="url" name="phone_number" value="#rsLead.lead_phone#"/>
                                         <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"/>
                                         <cfhttpparam type="url" name="area" value="#rsLead.lead_country#"/>
                                     </cfhttp>


                                 <cfelse>

                                     <!--- not accepted lead_zip --->
                                     <cfset int_try_flag=0/>

                                 </cfif>

                             <cfelse>

                                 <!--- doesnt have phone --->
                                 <cfset int_try_flag=0/>

                             </cfif>

                         <cfelse>

                             <!--- doesnt have optin --->
                             <cfset int_try_flag=0/>

                         </cfif>
                     </cfcase>

                     <cfcase value="aldeasinfantiles-coreg-es-0026">
                         <cfif rsAldeas.lead_optin eq 1>
                             <cfset int_try_flag=1/>
                             <!--- valid optin --->
                            <cfoutput>valid optin</cfoutput>
                             <cfif rsLead.lead_phone neq "">
                                 <!--- valid phone --->
                                 <cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 30>
                                     <!--- valid birthdate--->

                                     <cfset webserviceURL = "http://lmt.timeonegroup.com/get/import/8096/?">
                                        <!---
                                            Webservice URL
                                            https://lmt.timeonegroup.com/get/import/8096/?
                                            country=ES&
                                            reference_civility=1&
                                            last_name=test&
                                            first_name=test&city=test&
                                            birth_date=1975-09-18&
                                            email=test20170918120050@pdl.com&
                                            phone_number=0627382536&
                                            ip_address=127.0.0.1&
                                            origin=[optional]your_sub_id&
                                            user_agent=[optional]user_agent&
                                            id_delivery=23842
                                        --->

                                     <cfset webserviceURL &=  "country=ES&">
                                     <cfset webserviceURL &= "last_name="&rsLead.lead_surname&"&">
                                     <cfset webserviceURL &=  "first_name="&rsLead.lead_name&"&">
                                     <cfset webserviceURL &=  "reference_civility="&#ftranslate['lead_gender'][rsLead.lead_gender]#&"&">
                                     <cfset webserviceURL &= "birth_date="&rsLead.lead_birthdate&"&">
                                     <cfset webserviceURL &= "email="&rsLead.lead_email&"&">
                                     <cfset webserviceURL &= "city="&rsLead.lead_zip&"&">
                                     <cfset webserviceURL &="phone_number="&rsLead.lead_phone&"&">
                                     <cfset webserviceURL &= "ip_address="&rsLead.lead_ip&"&">
                                     <cfset webserviceURL &= "origin=cibele&">
                                     <cfset webserviceURL &= "id_delivery=23842">

                                     <cfdump var=#webserviceURL#>

                                     <cfhttp url="#webserviceURL#" method="get" result="cresult" charset="utf-8"></cfhttp>

                                 <cfelse>
                                     <cfset int_try_flag=0/>
                                 </cfif>
                             <cfelse>
                                 <cfset int_try_flag=0/>
                             </cfif>
                         <cfelse>
                             <cfset int_try_flag=0/>
                         </cfif>
                     </cfcase>

                        <cfcase value="aldeasinfantiles-dediee-coreg-es-0027">

                            <cfif rsAldeasDi.lead_optin eq 1>
                                <cfset int_try_flag=1/>
<!--- valid optin --->
                                <cfoutput>valid optin</cfoutput>

                                <cfif rsLead.lead_phone neq "">
<!--- valid phone --->
                                    <cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 30>
<!--- valid birthdate--->

                                        <cfset webserviceURL = "http://lmt.timeonegroup.com/get/import/8096/?">
<!---
    Webservice URL
    https://lmt.timeonegroup.com/get/import/8096/?
    country=ES&
    reference_civility=1&
    last_name=test&
    first_name=test&city=test&
    birth_date=1975-09-18&
    email=test20170918120050@pdl.com&
    phone_number=0627382536&
    ip_address=127.0.0.1&
    origin=[optional]your_sub_id&
    user_agent=[optional]user_agent&
    id_delivery=23841
--->

                                        <cfset webserviceURL &=  "country=ES&">
                                        <cfset webserviceURL &= "last_name="&rsLead.lead_surname&"&">
                                        <cfset webserviceURL &=  "first_name="&rsLead.lead_name&"&">
                                        <cfset webserviceURL &=  "reference_civility="&#ftranslate['lead_gender'][rsLead.lead_gender]#&"&">
                                        <cfset webserviceURL &= "birth_date="&rsLead.lead_birthdate&"&">
                                        <cfset webserviceURL &= "email="&rsLead.lead_email&"&">
                                        <cfset webserviceURL &= "city="&rsLead.lead_zip&"&">
                                        <cfset webserviceURL &="phone_number="&rsLead.lead_phone&"&">
                                        <cfset webserviceURL &= "ip_address="&rsLead.lead_ip&"&">
                                        <cfset webserviceURL &= "origin=cibele&">
                                        <cfset webserviceURL &= "id_delivery=23841">

                                        <cfdump var=#webserviceURL#>

                                        <cfhttp url="#webserviceURL#" method="get" result="cresult" charset="utf-8"></cfhttp>

                                    <cfelse>
                                        <cfset int_try_flag=0/>
                                    </cfif>
                                <cfelse>
                                    <cfset int_try_flag=0/>
                                </cfif>
                            <cfelse>
                                <cfset int_try_flag=0/>
                            </cfif>
                        </cfcase>

                </cfswitch>

            </cfif>

        <cfreturn cresult/>

    </cffunction>
</cfcomponent>