<cfcomponent>
    <cfsetting requesttimeout="15000">
<cfprocessingdirective pageencoding="utf-8">
    
    <cffunction name="doIntegrationClients" access="remote">
        <cfargument name="leadID" type="any" required="false" /> 

        <cfparam name="URL.leadID" default="-1">
        <cfparam name="URL.ccampaignID" default="">
        <cfparam name="URL.show" default="yes">
        <cfparam name="URL.token" default="">

        <cfset cresult=StructNew()>
        <cfset cresult.statuscode="">

        <cfif ARGUMENTS.leadID neq "">
            <cfset URL.leadID=ARGUMENTS.leadID>
        </cfif>
        
        <cfif #URL.leadID# neq "-1">
            
            <CFSTOREDPROC procedure="dbo.getLeadsforIndividualIntegration">
                <CFPROCPARAM type="IN" value="#URL.leadID#" cfsqltype="CF_SQL_VARCHAR">
                <CFPROCPARAM type="IN" value="#URL.ccampaignID#" cfsqltype="CF_SQL_VARCHAR">
                <CFPROCRESULT name="rsLead" resultset="1">
                <CFPROCRESULT name="rsIntegrations" resultset="2">
                <CFPROCRESULT name="rsOthSuccessInt" resultset="3">
                <CFPROCRESULT name="rsBonoTour" resultset="4">
                <CFPROCRESULT name="rsEasyVoyage" resultset="5">
                <CFPROCRESULT name="rsAdeslas" resultset="6">
                <CFPROCRESULT name="rsRCF" resultset="7">
            </CFSTOREDPROC>

            <CFSTOREDPROC procedure="dbo.getClientCampaign">
                <CFPROCPARAM type="IN" value="#URL.ccampaignID#" cfsqltype="CF_SQL_VARCHAR">
                <!---
                <CFPROCRESULT name="rsCampaign" resultset="1">
                <CFPROCRESULT name="rsRules" resultset="2">
                --->   
                <CFPROCRESULT name="rsFields" resultset="3">
            </CFSTOREDPROC>


            <!--- START Process --->

            <!--- transform data --->
                <cfset ftranslate = StructNew() >

                <cfloop query="rsFields">
                    <cfset ftranslate["#rsFields.field_name#"]["#rsFields.field_value#"] = rsFields.field_value_client >
                </cfloop>

                <cfdump var="#ftranslate#"/>


            <!-- attempt --->

                <cfif URL.show eq 'yes'>
                <cfoutput>1st ATTEMPT WITH #UCASE(ccampaignID)#<br></cfoutput>
                </cfif>
            
            <!-- integrate --->

                <cfswitch expression="#URL.ccampaignID#">

                    <cfcase value="tara-coreg-pt-0001">

                        <cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
                            
                            <cfset int_try_flag=1>

                            <cfif URL.show eq 'yes'>
                            <cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
                            </cfif>

                            <cfhttp url="http://www.tara-vidente.com/remote-registration.php" method="post" result="cresult" charset="utf-8"> 
                                <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
                                <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#"> 
                                <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#"> 
                                <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#"> 
                                <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
                                <cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> 
                                <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#"> 
                                <cfhttpparam type="url" name="campaignarea" value="PT"> 
                                <cfhttpparam type="url" name="media" value="coreg">
                                <cfhttpparam type="url" name="partner" value="Yours">  
                                <cfhttpparam type="url" name="campaign" value="coreg2016">  
                            </cfhttp>

                        <cfelse>

                            <cfset int_try_flag=0>

                        </cfif>
                            
                    </cfcase>


                    <cfcase value="maria-coreg-pt-0003">

                        <cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>
                            
                            <cfset int_try_flag=1>

                            <cfif URL.show eq 'yes'>
                            <cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
                            </cfif>

                            <cfhttp url="http://www.vidente-maria.com/remote-registration.php" method="get" result="cresult" charset="utf-8"> 
                                <cfhttpparam type="url" name="email" value="#rsLead.lead_email#"> 
                                <cfhttpparam type="url" name="civilstatus" value="#ftranslate['lead_gender'][rsLead.lead_gender]#"> 
                                <cfhttpparam type="url" name="fname" value="#rsLead.lead_name#"> 
                                <cfhttpparam type="url" name="lname" value="#rsLead.lead_surname#"> 
                                <cfhttpparam type="url" name="country" value="#rsLead.lead_country#">
                                <!---<cfhttpparam type="url" name="wish" value="#ftranslate['lead_interest'][rsLead.lead_interest]#"> --->
                                <cfhttpparam type="url" name="dob" value="#rsLead.lead_birthdate#">
                                <cfhttpparam type="url" name="optin" value="#rsLead.lead_optin#">
                                <cfhttpparam type="url" name="campaignarea" value="PT"> 
                                <cfhttpparam type="url" name="media" value="coreg">
                                <cfhttpparam type="url" name="partner" value="Yours">  
                                <cfhttpparam type="url" name="campaign" value="coreg2016">  
                            </cfhttp>


                        <cfelse>

                                <cfset int_try_flag=0>

                        </cfif>
                            
                    </cfcase>


                    <cfcase value="webpilots-coreg-pt-0006">

                        <cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 18>
                            
                            <cfif len(rsLead.lead_phone) gte 9 and len(rsLead.lead_zip) gte 4>
                                
                                    <cfif len(rsLead.lead_zip) eq 7>
                                        <cfset lz=left(rsLead.lead_zip,4) & "-" & mid(rsLead.lead_zip,5,3)>
                                    <cfelse>
                                        <cfset lz=rsLead.lead_zip + "-000">
                                    </cfif>

                                    <cfset int_try_flag=1>
                                    <cfif URL.show eq 'yes'>
                                    <cfoutput>SATISFIES CAMPAIGN AGE, PHONE AND ZIP<br></cfoutput>
                                    </cfif>
                                    <cfoutput>
                                    <!--- http://coreg.webpilots.com/wscampaign.php?realtime=1&country=pt&companyid=9&publisherid=28&campaignid=103&type=S&subid=YOURSPORTO&birthday=[BIRTHDAY]&gender=[GENDER]&firstname=[FIRSTNAME]&lastname=[LASTNAME]&mobile=[MOBILE]&email=[EMAIL]&zip=[ZIP]&country_residence=[COUNTRY_RESIDENCE]&nationality=[NATIONALITY]&data1=[GAME_DESCRIPTION]&ipenduser=[xxx.xxx.xxx.xxx] --->
                                    <!--- http://coreg.webpilots.com/wscampaign.php?realtime=1&country=pt&companyid=9&publisherid=28&campaignid=103&type=S&subid=YOURSPORTO&birthday=#DateFormat(rsLead.lead_birthdate,"YYYY-MM-DD")#&gender=#rsLead.lead_gender#&firstname=#rsLead.lead_name#&lastname=#rsLead.lead_surname#&mobile=#rsLead.lead_phone#&email=#rsLead.lead_email#&zip=#rsLead.lead_zip#&country_residence=#rsLead.lead_country#&nationality=#rsLead.lead_country#&data1=&ipenduser=#rsLead.lead_ip#--->    
                                    <br>
                                    </cfoutput>
                                    
                                    <cfhttp url="http://coreg.webpilots.com/wscampaign.php" method="get" result="cresult" charset="utf-8"> 
                                        <cfhttpparam type="url" name="realtime" value="1"> 
                                        <cfhttpparam type="url" name="country" value="pt"> 
                                        <cfhttpparam type="url" name="companyid" value="9"> 
                                        <cfhttpparam type="url" name="publisherid" value="28"> 
                                        <cfhttpparam type="url" name="campaignid" value="103">
                                        <cfhttpparam type="url" name="type" value="S">
                                        <cfhttpparam type="url" name="subid" value="YOURSPORTO">
                                        <cfhttpparam type="url" name="birthday" value="#DateFormat(rsLead.lead_birthdate,"YYYY-MM-DD")#">
                                        <cfhttpparam type="url" name="gender" value="#rsLead.lead_gender#">
                                        <cfhttpparam type="url" name="firstname" value="#rsLead.lead_name#">
                                        <cfhttpparam type="url" name="lastname" value="#rsLead.lead_surname#">
                                        <cfhttpparam type="url" name="mobile" value="#rsLead.lead_phone#">
                                        <cfhttpparam type="url" name="email" value="#rsLead.lead_email#">
                                        <cfhttpparam type="url" name="zip" value="#lz#">
                                        <cfhttpparam type="url" name="country_residence" value="#rsLead.lead_country#">
                                        <cfhttpparam type="url" name="nationality" value="#rsLead.lead_country#">
                                        <cfhttpparam type="url" name="data1" value="#rsLead.platformID#">   
                                        <cfhttpparam type="url" name="ipenduser" value="#rsLead.lead_ip#">
                                    </cfhttp>

                            <cfelse>
                                <cfset int_try_flag=0>
                            </cfif>

                        <cfelse>

                                <cfset int_try_flag=0>

                        </cfif>

                    </cfcase>

                    <cfcase value="bonotour-coreg-pt-0007">

                        <cfif rsBonoTour.lead_optin eq 1>
                        
                            <cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 25>
                                
                                <cfif len(rsLead.lead_zip) gte 4>
                                    
                                    <cfset int_try_flag=1>
                                    <cfif URL.show eq 'yes'>
                                    <cfoutput>SATISFIES CAMPAIGN AGE AND ZIP<br></cfoutput>
                                    </cfif>
                                    <cfoutput>
                                    <!--- http://new.hotelbono.com/process/receptor/receptor.php?pk_camp=MTM2MDk%3Dk9x&fecha_alta=[fecha_alta]&url_refered=[url_refered]&ip=[ip]&email=[email]&nombre=[nombre]&apellidos=[apellidos]&cp=[cp]&sexo=[sexo]&fecha_nac=[fecha_nac]&fk_pais=[fk_pais]&supermercado=[supermercado] --->
                                    <!---http://new.hotelbono.com/process/receptor/receptor.php?pk_camp=MTM2MDk%3Dk9x&fecha_alta=#DateFormat(rsLead.lead_tstamp,"YYYY-MM-DD")# #TimeFormat(rsLead.lead_tstamp,"HH:MM:SS")#&url_refered=http://www.videntecibele.com&ip=#rsLead.lead_ip#&email=#rsLead.lead_email#&nombre=#rsLead.lead_name#&
                                    apellidos=#rsLead.lead_surname#&cp=#rsLead.lead_zip#&sexo=#ftranslate['lead_gender'][rsLead.lead_gender]#&fecha_nac=#DateFormat(rsLead.lead_birthdate,"YYYY-MM-DD")#&fk_pais=72&supermercado=#rsLead.platformID#--->
                                    <br>
                                    </cfoutput>
                                    
                                    <cfhttp url="http://new.hotelbono.com/process/receptor/receptor.php" method="get" result="cresult" charset="utf-8"> 
                                        <cfhttpparam type="url" name="pk_camp" value="MTM2MDk%3Dk9x"> 
                                        <cfhttpparam type="url" name="fecha_alta" value="#DateFormat(rsLead.lead_tstamp,"YYYY-MM-DD")# #TimeFormat(rsLead.lead_tstamp,"HH:MM:SS")#"> 
                                        <cfhttpparam type="url" name="url_refered" value="http://www.videntecibele.com/pt-pt/"> 
                                        <cfhttpparam type="url" name="ip" value="#rsLead.lead_ip#"> 
                                        <cfhttpparam type="url" name="email" value="#rsLead.lead_email#">
                                        <cfhttpparam type="url" name="nombre" value="#rsLead.lead_name#">
                                        <cfhttpparam type="url" name="apellidos" value="#rsLead.lead_surname#">
                                        <cfhttpparam type="url" name="cp" value="#rsLead.lead_zip#">
                                        <cfhttpparam type="url" name="sexo" value="#ftranslate['lead_gender'][rsLead.lead_gender]#">
                                        <cfhttpparam type="url" name="fecha_nac" value="#DateFormat(rsLead.lead_birthdate,"YYYY-MM-DD")#">
                                        <cfhttpparam type="url" name="fk_pais" value="72">
                                        <cfhttpparam type="url" name="supermercado" value="#rsLead.platformID#">
                                    </cfhttp>

                                <cfelse>
                                    <cfset int_try_flag=0>
                                </cfif>

                            <cfelse>

                                    <cfset int_try_flag=0>

                            </cfif>

                        </cfif>

                    </cfcase>

                    <cfcase value="padre-coreg-pt-0008">
                        
                        <cfif DATEDIFF('yyyy',rsLead.lead_birthdate, now()) gte 35>

                            <cfset int_try_flag=1>
                            <cfif URL.show eq 'yes'>
                            <cfoutput>SATISFIES CAMPAIGN AGE<br></cfoutput>
                            </cfif>
                            <cfoutput>
                            
                            <br>
                            </cfoutput>

                            <cfif URL.token eq ''>
                            
                                <!--- GET TOKEN FOR PADRE --->
                                <cfset loginFields = {'email': 'andre.amorim@yoursporto.com','password': '**Yours2016'} >   

                                <cfhttp url="https://api.mensagens-dos-anjos.com/api/Partners/login" method="post" result="cresult" timeout="60">
                                    <cfhttpparam type="header" name="Content-Type" value="application/json" />
                                    <cfhttpparam type="body" value="#serializeJSON(loginFields)#">
                                </cfhttp>

                                <cfset tempvar=deserializeJSON(cresult.filecontent)>
                                <cfset login_token=tempvar.id>

                                <cfset URL.token=login_token>

                            </cfif>

                            <cfset intFields = {
                            "email": "#rsLead.lead_email#",
                            "birthdate": "#DateFormat(rsLead.lead_birthdate,'YYYY-MM-DD')#",
                            "language" : "pt-pt",
                            "timezone" : 0,
                            "gender": "#ftranslate['lead_gender'][rsLead.lead_gender]#",
                            "firstName": "#rsLead.lead_name#",
                            "lastName": "#rsLead.lead_surname#",
                            "originDetail": {
                            "partner" : "YOURS",
                            "referer": "http://www.videntecibele.com/pt-pt",
                            "subid": "#rsLead.platformID#",
                            "media": "COREG",
                            "theme": "angel",
                            "campaign": "campaign_2016",
                            "campaignarea": "PT"
                            },
                            "location":{
                            "registrationIpV4": "#rsLead.lead_ip#",
                            "registrationIpV6": "",
                            "number": "",
                            "street": "",
                            "street2": "",
                            "street3": "",
                            "zip": "",
                            "city": "",
                            "state": "",
                            "country": "PRT"
                            }
                            }>
                            
                            <cfhttp url="https://api.mensagens-dos-anjos.com/api/Customers" method="post" result="cresult" timeout="60">
                                <cfhttpparam type="url" name="access_token" value="#URL.token#">
                                <cfhttpparam type="header" name="Content-Type" value="application/json" />
                                <cfhttpparam type="body" value="#serializeJSON(intFields)#">
                            </cfhttp>

                        <cfelse>

                            <cfset int_try_flag=0>

                        </cfif>
                        
                    </cfcase>


                </cfswitch>

        </cfif>

        <cfreturn cresult>

    </cffunction>
</cfcomponent>