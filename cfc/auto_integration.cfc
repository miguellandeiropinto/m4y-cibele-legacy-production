<cfcomponent>
	<cfsetting requesttimeout="9000">
<cfprocessingdirective pageencoding="utf-8">

	<!---
	http://www.videntecibele.com/cfc/auto_integration.cfc?method=prepareleadset&campaign=egoi&order=desc&type=NOTINT&nof=1
	--->

	<cffunction name="prepareleadset" access="remote">

		<cfparam name="URL.nof" default="1">
		<cfparam name="URL.type" default="NOTINT">
		<cfparam name="URL.order" default="DESC">
		<cfparam name="URL.campaign" default="">
		<cfparam name="URL.ccampaignID" default="">
		

		<cfif URL.campaign eq 'egoi_prepare'>

			<!--- 
			EGOI_PREPARE
			SCHEDULED TASK
			Execute once a day, before EGOI_DAILY AUTO
			http://www.videntecibele.com/cfc/auto_integration.cfc?method=prepareleadset&campaign=egoi_prepare&order=&type=egoy_daily_auto_prepare&nof=
			--->

			<cfif URL.type eq 'egoy_daily_auto_prepare'>

				<cfquery name="delete_egoi_integration">
					delete from lead_integration
					where 
					campaignID='egoi-0000' or campaignID='egoi-0001'
					and Convert(date, lead_tstamp)=Dateadd(d,-1,Convert(date, getdate())) 
				</cfquery>

			</cfif>

		</cfif>


		<cfif URL.campaign eq 'egoi'>
			
			<!--- 
			EGOI_DAILY_AUTO
			SCHEDULED TASK 
			EVERY DAY, FROM 3 TO 10, INTEGRATE 100 LEADS HOURLY FROM THE PREVIOUS DAY, THAT WERE ALREADY INTEGRATED
			http://www.videntecibele.com/cfc/auto_integration.cfc?method=prepareleadset&campaign=egoi&order=ASC&type=egoi_daily_auto&nof=100
			--->

			<cfif URL.type eq 'egoi_daily_auto'>
	
				<cfquery name="rsLeadSet">
				
					select top #URL.nof# * 
					from lead_feed 
					where 
					leadID not in (select leadID from lead_integration where campaignID='egoi-0000')
					Convert(date, lead_tstamp)=Dateadd(d,-1,Convert(date, getdate()))
					order by lead_tstamp #URL.order#
				
				</cfquery>				

			</cfif>


			<!--- 
			TEMP
			http://www.videntecibele.com/cfc/auto_integration.cfc?method=prepareleadset&campaign=egoi&order=ASC&type=egoi_integrate_all_new&nof=100
			--->

			<cfif URL.type eq 'egoi_integrate_all_new'>
	
				<cfquery name="rsLeadSet">
				
					select top #URL.nof# * 
					from lead_feed 
					where 
					leadID not in (select leadID from lead_integration where campaignID='egoi-0000')
					order by lead_tstamp #URL.order#
				
				</cfquery>				

			</cfif>


			<!--- 
			TEMP
			http://www.videntecibele.com/cfc/auto_integration.cfc?method=prepareleadset&campaign=egoi&order=ASC&type=egoi_just_the_old&nof=25
			--->

			<cfif URL.type eq 'egoi_just_the_old'>
	
				<cfquery name="rsLeadSet">
				
					select top #URL.nof# * 
					from lead_feed 
					where --leadID in (select leadID from lead_integration where campaignID='egoi-0000')
					leadID in (select leadID from lead_integration where campaignID<>'egoi-0000')
					and lead_tstamp>'2016-08-23' and lead_tstamp<'2016-08-28'

				</cfquery>				

			</cfif>

			<!--- 
			TEMP
			http://www.videntecibele.com/cfc/auto_integration.cfc?method=prepareleadset&campaign=egoi&order=ASC&type=egoi_reint&nof=25
			--->

			<cfif URL.type eq 'egoi_reint'>
	
				<cfquery name="rsLeadSet">
				
					select top #URL.nof# * 
					from lead_feed 
					where leadID not in (select leadID from lead_integration where campaignID='egoi-0000')
					order by lead_tstamp asc

				</cfquery>				

			</cfif>

			<cfloop query="rsLeadSet">
				<cfinvoke component="integration_actions" method="doIntegrationEgoi" leadID="#rsLeadSet.leadID#">
			</cfloop>	

		</cfif>




		<cfif URL.campaign eq 'clients'>

			<!--- 
			CLIENTS_DAILY_AUTO
			SCHEDULED TASK 
			EVERY 15 MIN, INTEGRATE 50 LEADS THAT AREN'T INTEGRATED WITH ANY CLIENT BUT ARE WITH EGOI
			http://www.videntecibele.com/cfc/auto_integration.cfc?method=prepareleadset&campaign=clients&order=ASC&type=clients_daily_auto&nof=50
			--->			

			<cfif URL.type eq 'clients_daily_auto'>
	
				<cfquery name="rsLeadSet">
				
					select top #URL.nof# * 
					from lead_feed 
					where 
					leadID in (select leadID from lead_integration where campaignID='egoi-0000')
					and leadID not in (select leadID from lead_integration where campaignID<>'egoi-0000')
					and rtrim(ltrim(lead_name))<>'' and rtrim(ltrim(lead_surname))<>'' and rtrim(ltrim(lead_gender))<>''
					-- and rtrim(ltrim(lead_interest))<>''
					order by lead_tstamp #URL.order#
				
				</cfquery>				

			</cfif>

			<!--- 
			PADRE RE INT WIP
			
			http://www.videntecibele.com/cfc/auto_integration.cfc?method=prepareleadset&campaign=clients&ccampaignID=padre-coreg-pt-0008&order=ASC&type=clients_padre_reint_pt&nof=50&show=yes
			--->	


			<cfif URL.type eq 'clients_padre_reint_pt'>
	
				<cfquery name="rsLeadSet">
								
					select top 2 * 
					from lead_feed
					where 
					leadID in (select leadID from lead_integration where campaignID='egoi-0000')
					and leadID not in (select leadID from lead_integration where campaignID='padre-coreg-pt-0008')
					order by lead_tstamp asc

					/*
					select top #URL.nof# * 
					from lead_feed 
					where 
					leadID in (select leadID from lead_integration where campaignID='egoi-0000')
					and leadID not in (select leadID from lead_integration where campaignID<>'egoi-0000')
					and rtrim(ltrim(lead_name))<>'' and  rtrim(ltrim(lead_surname))<>'' and rtrim(ltrim(lead_gender))<>''
					and rtrim(ltrim(lead_interest))<>''
					order by lead_tstamp #URL.order#
					*/

				</cfquery>				

			</cfif>


		<!--- 
		ALL REINT
		
		http://www.videntecibele.com/cfc/auto_integration.cfc?method=prepareleadset&campaign=clients&order=ASC&type=clients_auto_reint&nof=10&show=yes
		--->	


			<cfif URL.type eq 'clients_auto_reint'>
	
				<cfquery name="rsLeadSet">
								
					select top #URL.nof# *
					from lead_feed
					where statusID=1
					order by lead_tstamp #URL.order#

				</cfquery>				

			</cfif>


			<!--- GET TOKEN FOR PADRE --->
			<cfset loginFields = {'email': 'andre.amorim@yoursporto.com','password': '**Yours2016'} >   

			<cfhttp url="https://api.mensagens-dos-anjos.com/api/Partners/login" method="post" result="cresult" timeout="60">
			    <cfhttpparam type="header" name="Content-Type" value="application/json" />
			    <cfhttpparam type="body" value="#serializeJSON(loginFields)#">
			</cfhttp>

			<cfset tempvar=deserializeJSON(cresult.filecontent)>
			<cfset login_token=tempvar.id>

			<cfloop query="rsLeadSet">
				<cfinvoke component="integration_actions" method="doIntegrationClients" leadID="#rsLeadSet.leadID#" ccampaignID="#URL.ccampaignID#" token="#login_token#">
			</cfloop>	

		</cfif>


	</cffunction>



</cfcomponent>