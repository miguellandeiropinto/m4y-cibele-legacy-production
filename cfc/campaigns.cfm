<cfprocessingdirective pageencoding="utf-8">
<cfset requestMethod = #cgi.request_method#/>
<!DOCTYPE html>
<html>
<head>
    <title>Prepare For Campaign</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.7.1/clipboard.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>

        body {
            font-family: 'Arial';
            width: 100%;
            background: #ededed;
        }

        form::after {
            display: block;
            content: '';
            clear: both;
        }

        form p span {
            color: #4d4d4d;
            display: block;
            margin: 3px 0px;
            font-size: 9pt;
        }
        span.info {
            color: #333;
            font-size: 10pt;
            font-weight: bold;
            float: right;
            margin-top: 5px;
        }

        form p span::before {
            content: "Ex: ";
        }

        form p input {
            width: 400px;
            padding: 5px 10px;
            width: 100%;
        }

        form p label {
            font-weight: bold;
            display: block;
            margin: 3px 0px;
        }

        form a {
            display: inline-block;
            max-width: 100%;
        }

        form input[type="submit"] {
            width: auto;
        }

        h2 {
            text-align: center;
            margin-bottom: 30px;
            font-weight: bold;
            color: #337ab7;
        }

        label {
            display: block;
            font-weight: bold;
            margin-bottom: 3px;
        }


        .result a {
            display: block;
            max-width: 100%;
            width: 100%;
            margin: 0px;
            padding: 0px;
        }

        .result p {
            word-wrap: break-word;
        }
        .table {
            display: table;
            height: 100VH;
        }
        .table-cell {
            display: table-cell;
            width: 100%;
            vertical-align: middle;
        }
        span.info::before {
            content: '* ';
            color: darkred;
            font-weight: bold;
            font-size: 13pt;
        }
        table {
            width: 100%;
        }
        table th {
            background-color: #337ab7;
            color: #fff;
        }
        table th, table td{
            padding: 5px 20px;
            border-bottom: #ccc 1px solid;
            vertical-align: middle;
        }

        button.remove {
            background-color: darkred;
            color: #fff;
            font-weight: bold;
            border: none;
        }
        input[type="number"] {
            width: 80px;
            display: inline-block;
            margin: 0px 10px;

        }
        span.per_page {
            font-weight: bold;
            display: inline-block;

        }
        h1 small {
            font-size: 9pt;
            float: right;
        }
        h1 small a {
            text-decoration: none;
            display: inline-block;
            padding: 8px;
            border-radius: 5px;
            border: #fff 2px solid;
            color: #fff;
            font-weight: bold;
            background: #337ab7;
        }

        .table-container {
            margin: 30px 0px;
        }

        #rsTable td {
            background: #fff;
        }

        span.found {float: right; font-size: 9pt;}
        h1 {
            font-weight: bold;
        }
    </style>
</head>
<body>
<div class="container">
<div class="row">
<cfset whereClause= "">
<cfif cgi.request_method eq 'POST' and structKeyExists(FORM, 'table_field[]')>
    <cfset arrFields=ListToArray(#FORM["table_field[]"]#)>
    <cfset arrOperators=ListToArray(#FORM["operator[]"]#)>
    <cfset arrValues=ListToArray(#FORM["field_value[]"]#)>
    <cfset whereClause= " WHERE ">
    <cfset counter=0>
    <cfloop index="i" from="1" to="#arrayLen(arrFields)#">
        <cfoutput>
            #i# - #arrFields[i]#
        </cfoutput>
        <cfset whereClause=whereClause&" "&#arrFields[i]#&" "&#arrOperators[i]#&" ">
        <cfif IsNumeric(#arrValues[i]#)>
            <cfset whereClause=whereClause&#arrValues[i]#&" ">
        <cfelse>
            <cfset whereClause=whereClause&"'"&#arrValues[i]#&"' ">
        </cfif>
        <cfset whereClause=whereClause&" and ">
        <cfset counter=counter+1>
    </cfloop>
    <cfset whereClause=Left(whereClause, len(whereClause)-5)>
</cfif>

<cfset startAt="1">
<cfset perPage="10">
<cfset page=0>

<cfif structKeyExists(FORM, "per_page")>
    <cfset perPage=#FORM.per_page#>
</cfif>

<cfif !structKeyExists(URL, "page")>
    <cfset startAt=0 * perPage>
<cfelseif structKeyExists(URL, "page") and IsNumeric(#URL.page#) and #URL.page# gt 0>
    <cfset page=URL.page -1>
    <cfset startAt=(URL.page-1) * perPage>
</cfif>

    <h1>Campaigns History <small><a href="create-campaign.cfm">Create Campaign <span class="glyphicon glyphicon-plus"></span></a></small></h1>
    <form method="post">
    <label>Filtros:</label>
    <button type="button" id="addFilterBtn" class="btn btn-info">Adicionar Filtro <span class="glyphicon glyphicon-plus"></span></button>
    <button type="submit" class="btn btn-primary">Efetuar pesquisa <span class="glyphicon glyphicon-search"></span></button>
    <span class="per_page">Per page</span>
        <cfoutput>
    <input class="form-control" name="per_page" type="number" max="100" min="1" value="#perPage#" step="1">
        </cfoutput>
     <!-- THIS IS TEMPLATE FRO FILTER ROW -->
    <code id="filter_row"><!--
            <tr>
            <td>
                <p>Campo:
                <select class="form-control" name="table_field[]">
                    <option value="vertical">Vertical</option>
                    <option value="opens">Aberturas</option>
                    <option value="clicks">Cliques</option>
                    <option value="campaignID">Id da Campanha</option>
                    <option value="sourceID">Source</option>
                    <option value="mediumID">Medium</option>
                    <option value="externalID">ID Externo</option>
                </select>
                </p>
            </td>
            <td>
                <p>Operador:
                <select class="form-control" name="operator[]">
                    <option value="=">Igual a</option>
                    <option value=">=">Maior ou igual a</option>
                    <option value="<=">Menor ou igual a</option>
                </select>
                </p>
            </td>
            <td>
                <p>Valor:
                <input class="form-control" type="text" value="" name="field_value[]">
                </p>
            </td>
            <td>
                <p><button class="remove btn btn-warning">&times</button></p>
            </td>
        </tr>
        --></code>
    <table id="filter_table">
        <!-- HERE GOES FILTERS ROWS -->
    </table>
    </form>

    <cfquery name="rsCampaigns">
        SELECT *
        FROM campaigns_history #PreserveSingleQuotes(whereClause)#
        ORDER BY tstamp
        OFFSET #startAt# ROWS
        FETCH NEXT #perPage# ROWS ONLY;
    </cfquery>
    <cfquery name="rsTotal">
        SELECT count(*) as total FROM campaigns_history
    </cfquery>
    <div class="table-container">
    <cfoutput>
        <span class="found">Displaying #rsCampaigns.recordcount# of #rsTotal.total# records.</span>
    </cfoutput>

    <table id="rsTable">
        <tr>
            <th>Id</th>
            <th>Id Campanha</th>
            <th>Source</th>
            <th>Medium</th>
            <th>Vertical</th>
            <th>opens</th>
            <th>clicks</th>
            <th></th>
        </tr>
        <cfloop query="rsCampaigns">
            <cfoutput>
                <tr>
                    <td>#rsCampaigns.id#</td>
                    <td>#rsCampaigns.campaignID#</td>
                    <td>#rsCampaigns.sourceID#</td>
                    <td>#rsCampaigns.mediumID#</td>
                    <td>#rsCampaigns.vertical#</td>
                    <td>#rsCampaigns.opens#</td>
                    <td>#rsCampaigns.clicks#</td>
                    <td><a href="campaign-report.cfm?cid=#rsCampaigns.id#" title="Mais informações">Ver mais <span class="glyphicon glyphicon-eye-open"</a></td>
                </tr>
            </cfoutput>
        </cfloop>
    </table>
    </div>
    <ul class="pagination">
        <cfoutput>
            <li><a href="campaigns.cfm?page=1&per_page=#perPage#">First</a></li>
            <li><a href="campaigns.cfm?page=#page-1#&per_page=#perPage#">Previous</a></li>

            <cfset pagesTotal = #Ceiling(rsTotal.total/perPage)#>
            <cfloop index="i" from="1" to="#pagesTotal#">
                <li><a href="campaigns.cfm?page=#i#&per_page=#perPage#">#i#</a></li>
            </cfloop>
            <li><a href="campaigns.cfm?page=#page+1#&per_page=#perPage#">Next</a></li>
            <li><a href="campaigns.cfm?page=#Ceiling(rsTotal.total/perPage)#&per_page=#perPage#">Last</a></li>
        </cfoutput>
    </ul>
    </div></div>
    <script>
        $(function () {
           $(document).on("click", "#addFilterBtn", function (e) {
              var txt = $("#filter_row").html();
              var html = txt.slice(4, txt.length - 3);
              $("#filter_table").append($(html));
           });

            $(document).on("click", "button.remove", function (e) {
                $(this).closest("tr").remove();
            });
        });
    </script>
    </form>
</body>
</html>
