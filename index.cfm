<cfprocessingdirective pageencoding="utf-8">
<cfcontent type="text/html; charset=utf-8">
<cfset setEncoding("URL", "utf-8")>
<cfset setEncoding("Form", "utf-8")>

<cfinclude template="cfc/tracking.cfc">



<CFIF #CGI.HTTP_X_Forwarded_For# EQ "">
	<CFSET theip="#CGI.Remote_Addr#">
<CFELSE>
	<CFSET theip="#CGI.HTTP_X_Forwarded_For#">
</CFIF>

<cftry>

	<CFSTOREDPROC procedure="dbo.getCountry">
	  <CFPROCPARAM type="IN" value="#theip#" cfsqltype="CF_SQL_VARCHAR">
	  <CFPROCPARAM type="OUT" variable="country" cfsqltype="CF_SQL_VARCHAR">
	</CFSTOREDPROC>

	<cfswitch expression="#country#">
		<cfdefaultcase>
			<cflocation url="pt-pt/?#cgi.QUERY_STRING#">
		</cfdefaultcase>
		<cfcase value="BR">
			<cflocation url="pt-br/?#cgi.QUERY_STRING#">
		</cfcase>
		<cfcase value="AR">
			<cflocation url="es-ar/?#cgi.QUERY_STRING#">
		</cfcase>
		<cfcase value="ES">
			<cflocation url="es-es/?#cgi.QUERY_STRING#">
		</cfcase>
	</cfswitch>

<cfcatch>
 	<cflocation url="pt-pt/?#cgi.QUERY_STRING#">
</cfcatch>
</cftry>



<!DOCTYPE HTML>
<html>
<head>
<!-- META -->
<cfinclude template="i_meta.cfm">
<cfinclude template="i_analytics.cfm">
</head>

<body>
</body>
</html>
